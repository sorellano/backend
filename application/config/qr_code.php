<?php

$config['cacheable'] = TRUE;
$config['cachedir'] = 'tmp/cache';
$config['imagedir'] = 'tmp/qr_codes';
$config['errorlog'] = 'tmp/logs';
$config['ciqrcodelib'] = 'application/third_party/phpqrcode/';
$config['quality'] = TRUE;
$config['size'] = 1024;
$config['black'] = array(255,255,255);
$config['white'] = array(70,130,180);
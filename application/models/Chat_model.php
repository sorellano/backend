<?php
class Chat_model extends CI_Model {

    public function SalasDisponibles($inicio = FALSE, $cantidadregistros = FALSE){
    	$ahora = date('Y-m-d H:i:s');
    	$this->db->select('*');
    	$this->db->from('SALA');
    	$this->db->where('cerrada', 0);
    	$this->db->where('idempleado', NULL);
    	$this->db->where('expira >', $ahora);
        if ($inicio !== FALSE && $cantidadregistros !== FALSE) {
            $this->db->limit($cantidadregistros,$inicio);
        }

    	$query = $this->db->get();
    	return $query->result_array();
    }

    public function estaDisponible($idsala){
    	$ahora = date('Y-m-d H:i:s');
    	$this->db->select('*');
    	$this->db->from('SALA');
    	$this->db->where('cerrada', 0);
    	$this->db->where('idsala', $idsala);
    	$this->db->where('idempleado', NULL);
    	$this->db->where('expira >', $ahora);
    	$query = $this->db->get();
    	if ($query->num_rows() == 0)
    		return false;            
        else
        	return true;
    }

    public function estadodelaSala($idsala){
        $this->db->select('*');
        $this->db->from('SALA');
        $this->db->where('idsala', $idsala);
        $query = $this->db->get();

        return $query->result_array();
    }

    public function actualizarExpira($idsala){
        $expira = new DateTime();
        $expira->modify("+30 minutes");
        $valor = date_format($expira, 'Y-m-d H:i:s');

        $this->db->set   ('expira', $valor);
        $this->db->where ('idsala', $idsala);

        if ( !$this->db->update ('SALA') )
            return false;

        return true;
    }

    public function SalasActivas($inicio = FALSE, $cantidadregistros = FALSE){
    	$idempleado = $this->session->userdata('idusuario');
    	$ahora = date('Y-m-d H:i:s');
    	$this->db->select('idsala');
    	$this->db->from('SALA');
    	$this->db->where('cerrada', 0);
    	$this->db->where('idempleado', $idempleado);
    	$this->db->where('expira >', $ahora);
        if ($inicio !== FALSE && $cantidadregistros !== FALSE) {
            $this->db->limit($cantidadregistros,$inicio);
        }

    	$query = $this->db->get();
    	return $query->result_array();
    }

    public function ElegirSala($idsala){
    	$idempleado = $this->session->userdata('idusuario');
    	
    	$this->db->set   ('idempleado', $idempleado);
        $this->db->where ('idsala', $idsala);

        if ( !$this->db->update ('SALA') )
            return false;

        $mensaje = array(
                "idsala" => $idsala,
                "idautor" => 0,
                "tipoautor" => "INFO",
                "mensaje" => "Nuestro empleado N°".$idempleado." esta aqui para ayudarte, contanos tu inconveniente."
        );
        $this->enviarMensaje($mensaje);
        return true;
    }

    public function CerrarSala($idsala){
    	$this->db->set   ('cerrada', 1);
        $this->db->where ('idsala', $idsala);

        if ( !$this->db->update ('SALA') )
            return false;

        return true;
    }

    public function ObtenerMensajes($idsala){
    	$ahora = date('Y-m-d H:i:s');
    	$this->db->select('idautor,tipoautor,mensaje');
    	$this->db->from('MENSAJE');
    	$this->db->join('SALA', 'MENSAJE.idsala = SALA.idsala');
    	$this->db->where('cerrada', 0);
    	$this->db->where('expira >', $ahora);
    	$this->db->where('MENSAJE.idsala', $idsala);
        $this->db->order_by('idmensaje', 'desc');
    	
    	$query = $this->db->get();
    	return $query->result_array();
    }

    public function ultimoMensaje($idsala){
        $this->db->select('mensaje,timestamp,tipoautor');
        $this->db->from('MENSAJE');
        $this->db->join('SALA', 'MENSAJE.idsala = SALA.idsala');
        $this->db->where('MENSAJE.idsala', $idsala);
        $this->db->order_by('idmensaje', 'desc');
        $query = $this->db->get();
        $row = $query->row();
            if (isset($row)){
                return "'".$row->mensaje."' a las ".$row->timestamp;
            }else{
                return false;
            }
    }

    public function nombreusuario($idsala){
        $this->db->select('CLIENTE.nombre');
        $this->db->from('CLIENTE');
        $this->db->join('SALA', 'SALA.idcliente = CLIENTE.idusuario');
        $this->db->where('SALA.idsala', $idsala);
        
        $query = $this->db->get();
        $row = $query->row();
            if (isset($row)){
                return $row->nombre;
            }else{
                return false;
            }
    }

    public function EnviarMensaje($mensaje){
    	if ( !$this->db->insert("MENSAJE", $mensaje) )
            return false;

        $idsala = $mensaje['idsala'];
        $this->actualizarExpira($idsala);
        return true;
    }

    public function SalasCerradas($inicio = FALSE, $cantidadregistros = FALSE){
    	$idempleado = $this->session->userdata('idusuario');
    	$ahora = date('Y-m-d H:i:s');
    	$this->db->select('*');
    	$this->db->from('SALA');
    	$this->db->where('idempleado', $idempleado);
    	$this->db->where('cerrada', 1);
    	$this->db->or_where('expira <', $ahora);
    	$this->db->where('idempleado', $idempleado);
        $this->db->order_by('idsala', 'desc');
    	if ($inicio !== FALSE && $cantidadregistros !== FALSE) {
            $this->db->limit($cantidadregistros,$inicio);
        }
    	$query = $this->db->get();
    	return $query->result_array();
    }

    public function ObtenerMensajesSalaCerrada($idsala){
    	$idempleado = $this->session->userdata('idusuario');
    	$ahora = date('Y-m-d H:i:s');

    	$this->db->select('*');
    	$this->db->from('MENSAJE');
    	$this->db->join('SALA', 'MENSAJE.idsala = SALA.idsala');
    	$this->db->where('cerrada', 1);
    	$this->db->where ('MENSAJE.idsala', $idsala);
    	$this->db->where('idempleado', $idempleado);
    	$this->db->or_where('expira <', $ahora);
        $this->db->where ('MENSAJE.idsala', $idsala);
        $this->db->where('idempleado', $idempleado);
    	
    	$query = $this->db->get();
    	return $query->result_array();
    }

    public function pertenezco($idsala){
    	$idempleado = $this->session->userdata('idusuario');

    	$this->db->select('idsala');
    	$this->db->from('SALA');
    	$this->db->where ('idsala', $idsala);
    	$this->db->where('idempleado', $idempleado);
    	$query = $this->db->get();

    	if ($query->num_rows() == 0)
    		return false;            
        else
        	return true;
    }

    public function estaCerrada($idsala){
    	$this->db->select('idsala');
    	$this->db->from('SALA');
    	$this->db->where ('idsala', $idsala);
    	$this->db->where('cerrada', 1);
    	$query = $this->db->get();

    	if ($query->num_rows() == 0)
    		return false;            
        else
        	return true;
    }

    public function expiro($idsala){
    	$ahora = date('Y-m-d H:i:s');
    	$this->db->select('idsala');
    	$this->db->from('SALA');
    	$this->db->where ('idsala', $idsala);
    	$this->db->where('expira <', $ahora);
    	$query = $this->db->get();

    	if ($query->num_rows() == 0)
    		return false;            
        else
        	return true;
    }
}
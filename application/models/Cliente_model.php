<?php
class Cliente_model extends CI_Model {

    public function buscar($buscar, $baja = 0, $inicio = FALSE, $cantidadregistros = FALSE)
    {
        $this->db->like("mail",$buscar);

        if($baja==0){
            $this->db->where ('baja', 0);
        }
        if ($inicio !== FALSE && $cantidadregistros !== FALSE) {
            $this->db->limit($cantidadregistros,$inicio);
        }
        $this->db->order_by('idusuario', 'desc');
        $consulta = $this->db->get("CLIENTE");
        return $consulta->result();
    }


    public function buscarPorId($id) {
        $resultado = $this->db->query('SELECT * FROM CLIENTE WHERE idusuario=?', $id);

        if ($resultado->num_rows() == 0)
            return false;

        $cliente = $resultado->row_array();

        return $cliente;
    }

    public function existeIdCliente($idusuario) {
        $resultado = $this->db->query('SELECT * FROM CLIENTE WHERE idusuario=?', $idusuario);

        if ($resultado->num_rows() == 0)
            return false;
        else
        return true;
    }

    public function dadoDeBaja($idusuario) {
        $resultado = $this->db->query('SELECT * FROM CLIENTE WHERE idusuario=? AND BAJA=1', $idusuario);

        if ($resultado->num_rows() == 0)
            return false;
        else
        return true;
    }

    public function altalogica($idusuario) {
        $this->db->set   ('baja', false);
        $this->db->where ('idusuario', $idusuario);

        if ( !$this->db->update ('CLIENTE') )
            return false;

        return true;
    }

    public function darDeBaja($idusuario) {
        $this->db->set   ('baja', true);
        $this->db->where ('idusuario', $idusuario);

        if ( !$this->db->update ('CLIENTE') )
            return false;

        return true;
    }

    public function modificarCliente($idusuario, $clave, $valor) {
        $this->db->set   ($clave, $valor);
        $this->db->where ('idusuario', $idusuario);

        if ( !$this->db->update ('CLIENTE') )
            return false;

        return true;
    }

    public function validarNombre($nombre){
        $errores = array();
        /*--------------- Checks de nombre ----------------*/
        if ( !is_string($nombre) ) {
            $errores['NOMBRE_INVALIDO'] = "El nombre debe ser texto";
        } else {
            $largoNombre = strlen($nombre);
            if ($largoNombre > 20) {
                $errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 20 caracteres";
            }
        }
        return $errores;
    }

    public function validarApellido($apellido){
        $errores = array();
        /*--------------- Checks de apellido ----------------*/
        if ( !is_string($apellido) ) {
            $errores['APELLIDO_INVALIDO'] = "El apellido debe ser texto";
        } else {
            $largoNombre = strlen($apellido);
            if ($largoNombre > 20) {
                $errores['APELLIDO_INVALIDO'] = "El apellido debe ser menor o igual a 20 caracteres";
            }
        }
        return $errores;
    }

    public function validarTelefono($telefono){
        $errores = array();
        /*--------------- Checks de telefono ----------------*/
        if ( !ctype_digit ( $telefono )) {
            $errores['TELEFONO_INVALIDO'] = "El telefono ".$telefono." contiene caracteres no numericos";
        }
        else{
            if ( !is_string($telefono) ) {
                $errores['TELEFONO_INVALIDO'] = "El telefono tiene que ser un texto";
            } else {
                $largoTelefono = strlen($telefono);
                if ( $largoTelefono < 8 || $largoTelefono > 9 ) {
                    $errores['TELEFONO_INVALIDO'] = "Largo del telefono incorrecto, el largo debe ser entre 8 y 9 caracteres y es de ".$largoTelefono." caracteres";
                } else {
                    //Si el largo es 8 es un telefono de linea
                    if ($largoTelefono == 8) {
                        $prefijo = substr($telefono, 0, 1);
                        if ($prefijo !== '2' || $prefijo !== '4') {
                            $errores['TELEFONO_INVALIDO'] = "Los telefonos de linea tienen que empezar con 2 o 4";
                        }
                    }
                    //Si el largo es 9 es un celular
                    if ($largoTelefono == 9) {
                        $prefijo = substr($telefono, 0, 2);
                        if ($prefijo !== '09') {
                            $errores['TELEFONO_INVALIDO'] = "Los celulares tienen que empezar con 09";
                        }
                    }
                }
            }
        }
        return $errores;
    }

    public function validarCiudad($ciudad){
        $errores = array();
        /*---------------- Checks de ciudad ----------------------*/
        if ( !is_string($ciudad) ) {
            $errores['CIUDAD_INVALIDA'] = "El ciudad debe ser texto";
        } else {
            $largoNombre = strlen($ciudad);
            if ($largoNombre > 15) {
                $errores['CIUDAD_INVALIDA'] = "El ciudad debe ser menor o igual a 20 caracteres";
            }
        }
        return $errores;
    }

    public function validarCalle($calle){
        $errores = array();
        /*---------------- Checks de calle ----------------------*/
        if ( !is_string($calle) ) {
            $errores['CALLE_INVALIDO'] = "El calle debe ser texto";
        } else {
            $largoNombre = strlen($calle);
            if ($largoNombre > 20) {
                $errores['CALLE_INVALIDO'] = "El calle debe ser menor o igual a 20 caracteres";
            }
        }
        return $errores;
    }

    public function validarBarrio($barrio){
        $errores = array();
        /*---------------- Checks de barrio ----------------------*/
        if ( !is_string($barrio) ) {
            $errores['BARRIO_INVALIDO'] = "El barrio debe ser texto";
        } else {
            $largoNombre = strlen($barrio);
            if ($largoNombre > 15) {
                $errores['BARRIO_INVALIDO'] = "El barrio debe ser menor o igual a 20 caracteres";
            }
        }
        return $errores;
    }

}

<?php
class Fechas_model extends CI_Model {

	public function existe($idevento,$fecha) {
		$this->db->where ('idevento', $idevento);
		$this->db->where ('fecha', $fecha);
		$resultado = $this->db->get('EN');

		if ($resultado->num_rows() == 0)
            return false;

        return true;
	}

	public function validarFecha($fecha) {
		$errores = array();

		$hoy = date("Y-m-d H:i:s");

		/*--------------------- Checks de fecha ------------------*/
		if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$fecha)) {
		    $errores['FECHA_INVALIDA'] = "La formato invalido de la fecha";
		} else {
		    if ($fecha < $hoy) {
		    	$errores['FECHA_INVALIDA'] = "La fecha no puede ser menor a la actual";
		    }
		}

		return $errores; 
	}

	public function agregar($funcion) {
		if ( !$this->db->insert("EN", $funcion) )
            return false;

        return true;
	}

	public function borrar($idevento,$funcion) {
		$this->db->where ('idevento', $idevento);
		$this->db->where ('fecha', $funcion);

		if (!$this->db->delete('EN'))
			return FALSE;

		return TRUE;
		
	}

	public function listar($idevento,$inicio = FALSE, $cantidadregistros = FALSE) {
		$this->db->select('*');
        $this->db->from('EN');
        $this->db->where('idevento', $idevento);
        if ($inicio !== FALSE && $cantidadregistros !== FALSE) {
            $this->db->limit($cantidadregistros,$inicio);
        }
        $query = $this->db->get();
        return $query->result();
	}

	public function buscarPorIdevento($idevento) {
		$fechas = array();

        $resultado = $this->db->query('SELECT * from EN WHERE idevento=?',$idevento)->result_array();
        foreach($resultado as $fecha)
            array_push($fechas, $fecha);

        return $fechas;
	}

}
<?php
class Paquete_model extends CI_Model {

	public function agregar($paquete) {
			if ( !$this->db->insert("PAQUETE", $paquete) )
            return false;

        return true;
	}

    public function validarNombre($nombre) {
    	$errores = array();
        if ( !is_string($nombre) ) {
            $errores['NOMBRE_INVALIDO'] =  "El nombre debe ser texto";
        } else {
            $largoNombre = strlen($nombre);
            if ($largoNombre > 50) {
                $errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 50 caracateres";
            }
        }
        return $errores;
    }

    public function listar($idevento,$buscar,$inicio = FALSE, $cantidadregistros = FALSE) {
        $this->db->select('*');
        $this->db->from('PAQUETE');
        $this->db->like('nombre', $buscar);
        $this->db->where('idevento', $idevento);
        $this->db->where('baja', 0);
        if ($inicio !== FALSE && $cantidadregistros !== FALSE) {
            $this->db->limit($cantidadregistros,$inicio);
        }
        $this->db->order_by("fecha asc");
        $query = $this->db->get();
        return $query->result();
    }

    public function dadoDeBaja($idpaquete) {
        $resultado = $this->db->query('SELECT * FROM PAQUETE WHERE idpaquete=? AND BAJA=1', $idpaquete);

        if ($resultado->num_rows() == 0) 
            return false;
        else
        return true;
    }

    public function existe($idpaquete) {
        $resultado = $this->db->query('SELECT * FROM PAQUETE WHERE idpaquete=?', $idpaquete);

        if ($resultado->num_rows() == 0) 
            return false;
        else
        return true;
    }

    public function darDeBaja($idpaquete) {
        $this->db->set   ('baja', true);
        $this->db->where ('idpaquete', $idpaquete);
        if ( !$this->db->update('PAQUETE') )
            return false;

        return true;
    }

    public function buscarPorId($idpaquete) {
        $resultado = $this->db->query('SELECT * FROM PAQUETE WHERE idpaquete=?', $idpaquete);

        if ($resultado->num_rows() == 0)
            return false;

        $paquete = $resultado->row_array();

        return $paquete;

    }

    public function comidasAsignadas($idpaquete,$buscar = FALSE,$inicio = FALSE, $cantidadregistros = FALSE) {
        $this->db->select('*');

        $this->db->from('PAQUETECOMIDA');

        $this->db->join('PAQUETE', 'PAQUETE.idpaquete = PAQUETECOMIDA.idpaquete');
        $this->db->join('MENU', 'MENU.idcomida = PAQUETECOMIDA.idcomida');
        $this->db->join('COMIDA', 'MENU.idcomida = COMIDA.idcomida');

        $this->db->where('PAQUETE.idevento = MENU.idevento');
        $this->db->where('PAQUETE.baja', 0);
        $this->db->where('MENU.baja', 0);
        $this->db->where('COMIDA.baja', 0);

        if ($buscar !== FALSE ){
            $this->db->like('COMIDA.nombre', $buscar);
        }
        if ($inicio !== FALSE && $cantidadregistros !== FALSE) {
            $this->db->limit($cantidadregistros,$inicio);
        }

        $this->db->where('PAQUETE.idpaquete', $idpaquete);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function comidasNoAsignadas($idpaquete) {
        $this->load->model('Menu_model');

        $query = $this->db->query('SELECT * FROM PAQUETE WHERE idpaquete=?', $idpaquete);
        $paquete = $query->row_array();
        $idevento = $paquete['idevento'];

        $menu = $this->Menu_model->Asignadas($idevento);
        $asignadas = $this->comidasAsignadas($idpaquete);
        $noasignados = array();

        foreach($menu as $comida){ 
            $coincide = FALSE;
            foreach ($asignadas as $asignada) { 

                if ($comida['idcomida']==$asignada['idcomida']){ 
                    $coincide = TRUE;} 

            }
            if(!$coincide){ 
            array_push($noasignados, $comida);}
        }
        return $noasignados;
    }

    public function tipoticketsAsignados($idpaquete,$buscar = FALSE,$inicio = FALSE, $cantidadregistros = FALSE) {
        $this->db->select('
            PAQUETETIPOTICKET.cantidad,
            PAQUETETIPOTICKET.descuento,
            TIPOTICKET.fecha,
            PAQUETE.idevento,
            PAQUETE.idpaquete,
            TIPOTICKET.idtipoticket,
            TIPOTICKET.nombre,
            TIPOTICKET.numerado,
            TIPOTICKET.precio,
            TIPOTICKET.referencia');
        $this->db->from('PAQUETETIPOTICKET');
        $this->db->join('PAQUETE', 'PAQUETE.idpaquete = PAQUETETIPOTICKET.idpaquete');
        $this->db->join('TIPOTICKET', 'TIPOTICKET.idtipoticket = PAQUETETIPOTICKET.idtipoticket');
        $this->db->where('PAQUETE.baja', 0);
        $this->db->where('TIPOTICKET.idevento = PAQUETE.idevento');
        $this->db->where('TIPOTICKET.baja', 0);

        if ($buscar !== FALSE ){
            $this->db->like('TIPOTICKET.nombre', $buscar);
        }
        if ($inicio !== FALSE && $cantidadregistros !== FALSE) {
            $this->db->limit($cantidadregistros,$inicio);
        }

        $this->db->where('PAQUETE.idpaquete', $idpaquete);
        $query = $this->db->get();
        return $query->result_array();

    }

    public function tipoticketsnoAsignados($idpaquete) {
        $this->load->model('Tipoticket_model');

        $query = $this->db->query('SELECT * FROM PAQUETE WHERE idpaquete=?', $idpaquete);
        $paquete = $query->row_array();
        $idevento = $paquete['idevento'];
        $fecha = $paquete['fecha'];
        $todos = $this->tipoticketsxideventoxfecha($idevento,$fecha);
        $asignados = $this->tipoticketsAsignados($idpaquete);
        $noasignados = array();

        foreach($todos as $tipoticket){ 
            $coincide = FALSE;
            foreach ($asignados as $asignado) { 

                if ($tipoticket['idtipoticket']==$asignado['idtipoticket']){ 
                    $coincide = TRUE;} 

            }
            if(!$coincide){ 
            array_push($noasignados, $tipoticket);}
        }
        return $noasignados;
    }

    public function metodospagoAsignados($idpaquete) {
        $this->db->select('*');
        $this->db->from('PAQUETEMETODOPAGO');
        $this->db->join('METODOPAGO', 'METODOPAGO.idmetodopago = PAQUETEMETODOPAGO.idmetodopago');
        $this->db->where('METODOPAGO.baja', 0);
        $this->db->where('idpaquete', $idpaquete);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function metodospagoNoAsignados($idpaquete) {
        $this->load->model('Metodopago_model');
        $todos = $this->Metodopago_model->listar();

        $asignados = $this->metodospagoAsignados($idpaquete);
        $noasignados = array();

        foreach($todos as $metodopago){ 
            $coincide = FALSE;
            foreach ($asignados as $asignado) { 

                if ($metodopago['idmetodopago']==$asignado['idmetodopago']){ 
                    $coincide = TRUE;} 

            }
            if(!$coincide){ 
            array_push($noasignados, $metodopago);}
        }
        return $noasignados;
    }

    public function agregarMetodopago($idpaquete,$metodopago) {

        if ( !$this->db->insert("PAQUETEMETODOPAGO", ['idpaquete' => $idpaquete, 'idmetodopago' => $metodopago]) )
            return false;

        return true;
    }

    public function buscarMetodopago($idpaquete,$buscar,$inicio = FALSE, $cantidadregistros = FALSE)
    {
        $this->db->select('*');
        $this->db->from('PAQUETEMETODOPAGO');
        $this->db->join('METODOPAGO', 'METODOPAGO.idmetodopago = PAQUETEMETODOPAGO.idmetodopago');
        $this->db->where('METODOPAGO.baja', 0);
        $this->db->where('PAQUETEMETODOPAGO.idpaquete', $idpaquete);
        $this->db->like("METODOPAGO.nombre",$buscar);
        if ($inicio !== FALSE && $cantidadregistros !== FALSE) {
            $this->db->limit($cantidadregistros,$inicio);
        }
        
        $query = $this->db->get();
        return $query->result();
    }

    public function borrarMetodopago($idpaquete,$idmetodopago) {
        $this->db->where ('idpaquete', $idpaquete);
        $this->db->where ('idmetodopago', $idmetodopago);

        if (!$this->db->delete('PAQUETEMETODOPAGO'))
            return FALSE;

        return TRUE;
        
    }

    public function agregarTipoTicket($idpaquete,$tipoticket,$cantidad,$descuento) {

        if ( !$this->db->insert("PAQUETETIPOTICKET", ['idpaquete' => $idpaquete, 'idtipoticket' => $tipoticket, 'cantidad' => $cantidad, 'descuento' => $descuento]) )
            return false;

        return true;
    }

    public function buscarTipotickets($idpaquete,$buscar,$inicio = FALSE, $cantidadregistros = FALSE)
    {
        $this->db->select('*');
        $this->db->from('PAQUETETIPOTICKET');
        $this->db->join('TIPOTICKET', 'TIPOTICKET.idtipoticket = TIPOTICKET.idtipoticket');
        $this->db->where('TIPOTICKET.baja', 0);
        $this->db->where('PAQUETETIPOTICKET.idpaquete', $idpaquete);
        $this->db->like("TIPOTICKET.nombre",$buscar);
        if ($inicio !== FALSE && $cantidadregistros !== FALSE) {
            $this->db->limit($cantidadregistros,$inicio);
        }
        $query = $this->db->get();
        return $query->result();
    }

    public function borrarTipoTicket($idpaquete,$idtipoticket) {
        $this->db->where ('idpaquete', $idpaquete);
        $this->db->where ('idtipoticket', $idtipoticket);

        if (!$this->db->delete('PAQUETETIPOTICKET'))
            return FALSE;

        return TRUE;
        
    }

    public function borrarComida($idpaquete,$idcomida) {
        $this->db->where ('idpaquete', $idpaquete);
        $this->db->where ('idcomida', $idcomida);

        if (!$this->db->delete('PAQUETECOMIDA'))
            return FALSE;

        return TRUE;
        
    }

    public function modificartipoticket($idpaquete, $idtipoticket,$clave , $valor) {
        $this->db->set   ($clave, $valor);
        $this->db->where ('idtipoticket', $idtipoticket);
        $this->db->where ('idpaquete', $idpaquete);

        if ( !$this->db->update ('PAQUETETIPOTICKET') )
            return false;

        return true;
    }

    public function modificarcomida($idpaquete, $idcomida,$clave , $valor) {
        $this->db->set   ($clave, $valor);
        $this->db->where ('idcomida', $idcomida);
        $this->db->where ('idpaquete', $idpaquete);

        if ( !$this->db->update ('PAQUETECOMIDA') )
            return false;

        return true;
    }

    public function modificarimagen($idpaquete,$clave,$valor) {
        $this->db->set   ($clave, $valor);
        $this->db->where ('idpaquete', $idpaquete);

        if ( !$this->db->update ('PAQUETE') )
            return false;

        return true;
    }

    public function agregarComida($idpaquete,$idcomida,$cantidad,$descuento) {

        if ( !$this->db->insert("PAQUETECOMIDA", ['idpaquete' => $idpaquete, 'idcomida' => $idcomida, 'cantidad' => $cantidad, 'descuento' => $descuento]) )
            return false;

        return true;
    }


    //TO DO: APLICAR VALIDACIONES A LAS MODIFICACIONES Y LOS REGISTROS
    public function validarCantidad($cantidad) {
        $errores = array();
        if ( !is_integer($cantidad) && !ctype_digit($cantidad) ) {
            $errores['CANTIDAD_INVALIDO'] = "La cantidad debe ser un entero";
        } else {
            $cantidad = (int) $cantidad; //Casteo por si las dudas que sea un string con numero
            if ($cantidad < 1 || $cantidad > 20) {
                $errores['CANTIDAD_INVALIDO'] = "La cantidad debe ser de 1 a 20";
            }
        }
        return $errores; 
    }

    public function validarDescuento($descuento) {
        $errores = array();
        if ( !is_integer($descuento) && !ctype_digit($descuento) ) {
            $errores['DESCUENTO_INVALIDO'] = "El descuento debe ser un entero";
        } else {
            $descuento = (int) $descuento; //Casteo por si las dudas que sea un string con numero
            if ($descuento < 1 || $descuento > 100) {
                $errores['DESCUENTO_INVALIDO'] = "El descuento debe ser de 1 a 99";
            }
        }
        return $errores; 
    }

    public function fecha($idpaquete) {
        $this->db->select('fecha');
        $this->db->from('PAQUETE');
        $this->db->where('idpaquete', $idpaquete);
        $this->db->where('baja', 0);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function tipoticketsxideventoxfecha($idevento,$fecha) {
        $this->db->select('*');
        $this->db->from('TIPOTICKET');
        $this->db->where('idevento', $idevento);
        $this->db->where('fecha', $fecha);
        $this->db->where('baja', 0);
        $query = $this->db->get();
        return $query->result_array();
    }

}
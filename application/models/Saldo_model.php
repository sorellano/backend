<?php
class Saldo_model extends CI_Model {

    public function buscar($buscar, $fecha,$procesados = 0, $inicio = FALSE, $cantidadregistros = FALSE)
    {
        
        $this->db->like("idusuario",$buscar);
        $this->db->like("fecha",$fecha);
        $this->db->join('METODOPAGO', 'METODOPAGO.idmetodopago = SALDO.idmetodopago');
        $this->db->order_by('fecha', 'desc');
        if($procesados==0){
            $this->db->where ('aprobado', 1);
        }
        if ($inicio !== FALSE && $cantidadregistros !== FALSE) {
            $this->db->limit($cantidadregistros,$inicio);
        }

        $consulta = $this->db->get("SALDO");
        return $consulta->result();
        
    }

    public function aprobado($idrecarga)
    {
        $query = $this->db->query('SELECT * FROM SALDO WHERE idrecarga=?', $idrecarga);
        $recarga = $query->row_array();

        if ($query->num_rows() == 0){
            return false;
        }else{
            $aprobado = $recarga['aprobado'];
            return $aprobado;
        }
    }

    public function aprobar($idrecarga){
        $this->db->set   ('aprobado', 2);
        $this->db->where ('idrecarga', $idrecarga);
        if ( !$this->db->update('SALDO') )
            return false;


        $query   = $this->db->query('SELECT * FROM SALDO WHERE idrecarga=?', $idrecarga);
        $recarga = $query->row_array();

        if ($query->num_rows() == 0){
            return false;
        }else{
            $idusuario   = $recarga['idusuario'];
            $saldocargar = $recarga['saldo'];
            
            $query       = $this->db->query('SELECT * FROM CLIENTE WHERE idusuario=?', $idusuario);
            $usuario     = $query->row_array();
            $saldoactual = $usuario['saldo'];
            $nuevosaldo = $saldoactual+$saldocargar;

            $this->db->set   ('saldo', $nuevosaldo);
            $this->db->where ('idusuario', $idusuario);
            if ( !$this->db->update('CLIENTE') )
                return false;

            return true;
        }
    }

    public function rechazar($idrecarga){
        $this->db->set   ('aprobado', 3);
        $this->db->where ('idrecarga', $idrecarga);
        if ( !$this->db->update('SALDO') )
            return false;

        return true;
    }
}

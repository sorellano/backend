<?php
class Informes_model extends CI_Model {

	public function eventosmasvendidos($fechainicio,$fechafin,$cantidad) {
		$this->db->select('COMPRA.idevento');
    	$this->db->select('COUNT(COMPRA.idevento) as ventas');
    	$this->db->order_by('ventas', 'desc');
    	$this->db->group_by('COMPRA.idevento');
    	$this->db->from('COMPRA');
		$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');

    	$this->db->join('EVENTO', 'COMPRA.idevento = EVENTO.idevento');
    	$this->db->select('EVENTO.nombre');
    	
    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	$this->db->limit($cantidad);
    	$query = $this->db->get();
    	return $query->result_array();
	}

	public function eventosmenosvendidos($fechainicio,$fechafin,$cantidad) {
		$this->db->select('COMPRA.idevento');
    	$this->db->select('COUNT(COMPRA.idevento) as ventas');
    	$this->db->order_by('ventas', 'asc');
    	$this->db->group_by('COMPRA.idevento');
    	$this->db->from('COMPRA');
		$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	
    	$this->db->join('EVENTO', 'COMPRA.idevento = EVENTO.idevento');
    	$this->db->select('EVENTO.nombre');

    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	$this->db->limit($cantidad);
    	$query = $this->db->get();
    	return $query->result_array();
	}

	public function promedioventasevento($fechainicio,$fechafin) {
		$this->db->select('COMPRA.idevento');
    	$this->db->select('COUNT(COMPRA.idevento) as ventas' );
    	$this->db->group_by('COMPRA.idevento');
    	$this->db->from('COMPRA');
    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	$query = $this->db->get();
    	$promedio = array();
    	foreach ($query->result_array() as $index => $value) {
    		array_push($promedio,$value['ventas']);
    	}
    	$promedio = array_filter($promedio);
    	$resultado = array_sum($promedio)/count($promedio);
    	return $resultado;
	}

	public function eventosconmasganancia($fechainicio,$fechafin,$cantidad) {
		$this->db->select('COMPRA.idevento');
    	$this->db->select('SUM(COMPRA.importe) as ganancia' );
    	$this->db->order_by('ganancia', 'desc');
    	$this->db->group_by('COMPRA.idevento');
    	$this->db->from('COMPRA');
    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');

    	$this->db->join('EVENTO', 'COMPRA.idevento = EVENTO.idevento');
    	$this->db->select('EVENTO.nombre');
    	
    	$this->db->limit($cantidad);
    	$query = $this->db->get();
    	return $query->result_array();
	}

	public function eventosconmenosganancia($fechainicio,$fechafin,$cantidad) {
		$this->db->select('COMPRA.idevento');
    	$this->db->select('SUM(COMPRA.importe) as ganancia' );
    	$this->db->order_by('ganancia', 'asc');
    	$this->db->group_by('COMPRA.idevento');
    	$this->db->from('COMPRA');
    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');

    	$this->db->join('EVENTO', 'COMPRA.idevento = EVENTO.idevento');
    	$this->db->select('EVENTO.nombre');
    	
    	$this->db->limit($cantidad);
    	$query = $this->db->get();
    	return $query->result_array();
	}

	public function promediogananciasevento($fechainicio,$fechafin) {
		$this->db->select('COMPRA.idevento');
    	$this->db->select('SUM(COMPRA.importe) as ganancias' );
    	$this->db->group_by('COMPRA.idevento');
    	$this->db->from('COMPRA');
    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	//$this->db->limit($cantidad);
    	$query = $this->db->get();
    	$promedio = array();
    	foreach ($query->result_array() as $index => $value) {
    		array_push($promedio,$value['ganancias']);
    	}
    	$promedio = array_filter($promedio);
    	$resultado = array_sum($promedio)/count($promedio);
		return $resultado;
	}

	public function tipoticketmasvendido($fechainicio,$fechafin,$cantidad) {
		$this->db->select('TICKET.idtipoticket');
    	$this->db->select('COUNT(TICKET.idtipoticket) as vendidas' );
    	$this->db->order_by('vendidas', 'desc');
    	$this->db->group_by('TICKET.idtipoticket');
    	$this->db->from('TICKET');
    	$this->db->join('COMPRA', 'COMPRA.idcompra = TICKET.idcompra');
    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	$this->db->join('TIPOTICKET', 'TIPOTICKET.idtipoticket = TICKET.idtipoticket');
    	$this->db->select('TIPOTICKET.nombre');
    	$this->db->limit($cantidad);
    	$query = $this->db->get();
    	return $query->result_array();
	}

	public function tipoticketmenosvendido($fechainicio,$fechafin,$cantidad) {
		$this->db->select('TICKET.idtipoticket');
    	$this->db->select('COUNT(TICKET.idtipoticket) as vendidas' );
    	$this->db->order_by('vendidas', 'asc');
    	$this->db->group_by('TICKET.idtipoticket');
    	$this->db->from('TICKET');
    	$this->db->join('COMPRA', 'COMPRA.idcompra = TICKET.idcompra');
    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	$this->db->join('TIPOTICKET', 'TIPOTICKET.idtipoticket = TICKET.idtipoticket');
    	$this->db->select('TIPOTICKET.nombre');
    	$this->db->limit($cantidad);
    	$query = $this->db->get();
    	return $query->result_array();
	}

	public function ticketsconmayorganancia($fechainicio,$fechafin,$cantidad) {
		$this->db->select('TIPOTICKET.nombre');
    	$this->db->select('SUM(TIPOTICKET.idtipoticket) as cantidadvendidas' );
    	$this->db->select('TIPOTICKET.precio as precioindividual');
    	$this->db->select('SUM(TIPOTICKET.idtipoticket) * TIPOTICKET.precio as ganancia');
    	$this->db->order_by('ganancia', 'desc');

    	$this->db->group_by('TIPOTICKET.idtipoticket');
    	$this->db->from('TIPOTICKET');

    	$this->db->join('TICKET', 'TICKET.idtipoticket = TIPOTICKET.idtipoticket');
    	
    	$this->db->join('COMPRA', 'COMPRA.idcompra = TICKET.idcompra');
    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	$this->db->limit($cantidad);
    	$query = $this->db->get();
    	return $query->result_array();
	}

	public function ticketsconmenorganancia($fechainicio,$fechafin,$cantidad) {
		$this->db->select('TIPOTICKET.nombre');
    	$this->db->select('SUM(TIPOTICKET.idtipoticket) as cantidadvendidas' );
    	$this->db->select('TIPOTICKET.precio as precioindividual');
    	$this->db->select('SUM(TIPOTICKET.idtipoticket) * TIPOTICKET.precio as ganancia');
    	$this->db->order_by('ganancia', 'asc');

    	$this->db->group_by('TIPOTICKET.idtipoticket');
    	$this->db->from('TIPOTICKET');

    	$this->db->join('TICKET', 'TICKET.idtipoticket = TIPOTICKET.idtipoticket');
    	
    	$this->db->join('COMPRA', 'COMPRA.idcompra = TICKET.idcompra');
    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	$this->db->limit($cantidad);
    	$query = $this->db->get();
    	return $query->result_array();
	}

	public function promediogananciatipoticket($fechainicio,$fechafin) {
		$this->db->select('TIPOTICKET.nombre');
    	$this->db->select('SUM(TIPOTICKET.idtipoticket) as cantidadvendidas' );
    	$this->db->select('TIPOTICKET.precio as precioindividual');
    	$this->db->select('SUM(TIPOTICKET.idtipoticket) * TIPOTICKET.precio as ganancia');
    	$this->db->order_by('ganancia', 'asc');

    	$this->db->group_by('TIPOTICKET.idtipoticket');
    	$this->db->from('TIPOTICKET');

    	$this->db->join('TICKET', 'TICKET.idtipoticket = TIPOTICKET.idtipoticket');
    	
    	$this->db->join('COMPRA', 'COMPRA.idcompra = TICKET.idcompra');
    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	$query = $this->db->get();
    	$promedio = array();
    	foreach ($query->result_array() as $index => $value) {
    		array_push($promedio,$value['ganancia']);
    	}
    	$promedio = array_filter($promedio);
    	$resultado = array_sum($promedio)/count($promedio);
		return $resultado;
	}

	public function lugaresmasconcurridos($fechainicio,$fechafin,$cantidad) {
		$this->db->select('LUGAR.nombre as nombrelugar');
    	$this->db->select('EVENTO.nombre as nombrevento');
    	$this->db->select('COUNT(LUGAR.idlugar) as asistencias' );
    	$this->db->order_by('asistencias', 'desc');
    	$this->db->group_by('LUGAR.idlugar');
    	$this->db->from('TICKET');
    	$this->db->join('COMPRA', 'TICKET.idcompra = COMPRA.idcompra');
    	$this->db->join('EVENTO', 'COMPRA.idevento = EVENTO.idevento');
    	$this->db->join('LUGAR', 'EVENTO.idlugar = LUGAR.idlugar');
    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	$this->db->where('verificado', 1);
    	$this->db->limit($cantidad);
    	$query = $this->db->get();
    	return $query->result_array();
	}

	public function lugaresmenosconcurridos($fechainicio,$fechafin,$cantidad) {
		$this->db->select('LUGAR.nombre as nombrelugar');
    	$this->db->select('EVENTO.nombre as nombrevento');
    	$this->db->select('COUNT(LUGAR.idlugar) as asistencias' );
    	$this->db->order_by('asistencias', 'asc');
    	$this->db->group_by('LUGAR.idlugar');
    	$this->db->from('TICKET');
    	$this->db->join('COMPRA', 'TICKET.idcompra = COMPRA.idcompra');
    	$this->db->join('EVENTO', 'COMPRA.idevento = EVENTO.idevento');
    	$this->db->join('LUGAR', 'EVENTO.idlugar = LUGAR.idlugar');
    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	$this->db->where('verificado', 1);
    	$this->db->limit($cantidad);
    	$query = $this->db->get();
    	return $query->result_array();
	}

	public function comidasmasvendidas($fechainicio,$fechafin,$cantidad) {
		$this->db->select('COMIDA.nombre as nombrecomida');
    	$this->db->select('COUNT(COMPRACOMIDA.idcomida) as compras' );
    	$this->db->order_by('compras', 'desc');
    	$this->db->group_by('COMPRACOMIDA.idcomida');
    	$this->db->from('COMPRACOMIDA');
    	$this->db->join('COMIDA', 'COMIDA.idcomida = COMPRACOMIDA.idcomida');
    	$this->db->join('COMPRA', 'COMPRACOMIDA.idcompra = COMPRA.idcompra');
    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	$this->db->limit($cantidad);
    	$query = $this->db->get();
    	return $query->result_array();
	}

	public function comidasmenosvendidas($fechainicio,$fechafin,$cantidad) {
		$this->db->select('COMIDA.nombre as nombrecomida');
    	$this->db->select('COUNT(COMPRACOMIDA.idcomida) as compras' );
    	$this->db->order_by('compras', 'asc');
    	$this->db->group_by('COMPRACOMIDA.idcomida');
    	$this->db->from('COMPRACOMIDA');
    	$this->db->join('COMIDA', 'COMIDA.idcomida = COMPRACOMIDA.idcomida');
    	$this->db->join('COMPRA', 'COMPRACOMIDA.idcompra = COMPRA.idcompra');
    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	$this->db->limit($cantidad);
    	$query = $this->db->get();
    	return $query->result_array();
	}

	public function promedioventasdecomidas($fechainicio,$fechafin) {
		$this->db->select('COMIDA.nombre as nombrecomida');
    	$this->db->select('COUNT(COMPRACOMIDA.idcomida) as compras' );
    	$this->db->order_by('compras', 'asc');
    	$this->db->group_by('COMPRACOMIDA.idcomida');
    	$this->db->from('COMPRACOMIDA');
    	$this->db->join('COMIDA', 'COMIDA.idcomida = COMPRACOMIDA.idcomida');
    	$this->db->join('COMPRA', 'COMPRACOMIDA.idcompra = COMPRA.idcompra');
    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	$query = $this->db->get();
    	$promedio = array();
    	foreach ($query->result_array() as $index => $value) {
    		array_push($promedio,$value['compras']);
    	}
    	$promedio = array_filter($promedio);
    	$resultado = array_sum($promedio)/count($promedio);
		return $resultado;
	}

	public function comidaconmasganancia($fechainicio,$fechafin,$cantidad) {
		$this->db->select('COMIDA.nombre as nombrecomida');
    	$this->db->select('SUM(MENU.precio) as ganancia' );
    	$this->db->order_by('ganancia', 'desc');
    	$this->db->group_by('COMPRACOMIDA.idcomida');

    	$this->db->from('COMPRACOMIDA');
    	$this->db->join('COMIDA', 'COMIDA.idcomida = COMPRACOMIDA.idcomida');
    	$this->db->join('MENU', 'COMIDA.idcomida = MENU.idcomida');
    	$this->db->join('COMPRA', 'COMPRACOMIDA.idcompra = COMPRA.idcompra');
    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	$this->db->limit($cantidad);
    	$query = $this->db->get();
    	//print("<pre>".print_r($query->result_array(),true)."</pre>");
    	return $query->result_array();
	}

	public function comidaconmenosganancia($fechainicio,$fechafin,$cantidad) {
		$this->db->select('COMIDA.nombre as nombrecomida');
    	$this->db->select('SUM(MENU.precio) as ganancia' );
    	$this->db->order_by('ganancia', 'asc');
    	$this->db->group_by('COMPRACOMIDA.idcomida');

    	$this->db->from('COMPRACOMIDA');
    	$this->db->join('COMIDA', 'COMIDA.idcomida = COMPRACOMIDA.idcomida');
    	$this->db->join('MENU', 'COMIDA.idcomida = MENU.idcomida');
    	$this->db->join('COMPRA', 'COMPRACOMIDA.idcompra = COMPRA.idcompra');
    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	$this->db->limit($cantidad);
    	$query = $this->db->get();
    	return $query->result_array();
	}

	public function promediodegananciaporcomida($fechainicio,$fechafin) {
		$this->db->select('COMIDA.nombre as nombrecomida');

    	$this->db->select('SUM(MENU.precio) as ganancia' );
    	$this->db->order_by('ganancia', 'asc');
    	$this->db->group_by('COMPRACOMIDA.idcomida');

    	$this->db->from('COMPRACOMIDA');
    	$this->db->join('COMIDA', 'COMIDA.idcomida = COMPRACOMIDA.idcomida');
    	$this->db->join('MENU', 'COMIDA.idcomida = MENU.idcomida');
    	$this->db->join('COMPRA', 'COMPRACOMIDA.idcompra = COMPRA.idcompra');
    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	//$this->db->limit($cantidad);
    	$query = $this->db->get();
    	$promedio = array();
    	foreach ($query->result_array() as $index => $value) {
    		array_push($promedio,$value['ganancia']);
    	}
    	$promedio = array_filter($promedio);
    	$resultado = array_sum($promedio)/count($promedio);
		return $resultado;
	}

	public function paquetemasvendido($fechainicio,$fechafin,$cantidad) {
		$this->db->select('PAQUETE.nombre as nombrepaquete');
    	$this->db->select('COUNT(COMPRAPAQUETE.idpaquete) as compras' );
    	$this->db->order_by('compras', 'desc');
    	$this->db->group_by('COMPRAPAQUETE.idpaquete');
    	$this->db->from('COMPRAPAQUETE');

    	$this->db->join('PAQUETE', 'PAQUETE.idpaquete = COMPRAPAQUETE.idpaquete');
    	$this->db->join('COMPRA', 'COMPRAPAQUETE.idcompra = COMPRA.idcompra');

    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	$this->db->limit($cantidad);
    	$query = $this->db->get();
    	return $query->result_array();
	}

	public function paquetemenosvendido($fechainicio,$fechafin,$cantidad) {
		$this->db->select('PAQUETE.nombre as nombrepaquete');
    	$this->db->select('COUNT(COMPRAPAQUETE.idpaquete) as compras' );

    	$this->db->order_by('compras', 'desc');
    	$this->db->group_by('COMPRAPAQUETE.idpaquete');
    	$this->db->from('COMPRAPAQUETE');

    	$this->db->join('PAQUETE', 'PAQUETE.idpaquete = COMPRAPAQUETE.idpaquete');
    	$this->db->join('COMPRA', 'COMPRAPAQUETE.idcompra = COMPRA.idcompra');

    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	$this->db->limit($cantidad);
    	$query = $this->db->get();
    	
    	return $query->result_array();
	}

	public function promedioventasdepaquetes($fechainicio,$fechafin) {
		$this->db->select('PAQUETE.nombre as nombrepaquete');
    	$this->db->select('COUNT(COMPRAPAQUETE.idpaquete) as compras' );
    	$this->db->order_by('compras', 'desc');
    	$this->db->group_by('COMPRAPAQUETE.idpaquete');
    	$this->db->from('COMPRAPAQUETE');
    	$this->db->join('PAQUETE', 'PAQUETE.idpaquete = COMPRAPAQUETE.idpaquete');
    	$this->db->join('COMPRA', 'COMPRAPAQUETE.idcompra = COMPRA.idcompra');
    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	$query = $this->db->get();
    	$promedio = array();
    	foreach ($query->result_array() as $index => $value) {
    		array_push($promedio,$value['compras']);
    	}
    	$promedio = array_filter($promedio);
    	$resultado = array_sum($promedio)/count($promedio);
		return $resultado;
	}

	public function metodosdepagomasutilizados($fechainicio,$fechafin,$cantidad) {
		$this->db->select('METODOPAGO.nombre as nombremetodopago');
    	$this->db->select('COUNT(METODOPAGO.idmetodopago) as transacciones' );

    	$this->db->order_by('transacciones', 'desc');
    	$this->db->group_by('METODOPAGO.idmetodopago');
    	$this->db->from('COMPRA');

    	$this->db->join('METODOPAGO', 'COMPRA.idmetodopago = METODOPAGO.idmetodopago');
    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	$this->db->limit($cantidad);
    	$query = $this->db->get();
    	return $query->result_array();
	}

	public function metodosdepagomenosutilizados($fechainicio,$fechafin,$cantidad) {
		$this->db->select('METODOPAGO.nombre as nombremetodopago');
    	$this->db->select('COUNT(METODOPAGO.idmetodopago) as transacciones' );

    	$this->db->order_by('transacciones', 'asc');
    	$this->db->group_by('METODOPAGO.idmetodopago');
    	$this->db->from('COMPRA');

    	$this->db->join('METODOPAGO', 'COMPRA.idmetodopago = METODOPAGO.idmetodopago');
    	$this->db->where('COMPRA.fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	$this->db->limit($cantidad);
    	$query = $this->db->get();
    	return $query->result_array();
	}

	public function promediocargasaldo($fechainicio,$fechafin) {
		$this->db->select('SUM(saldo) as totalcargado' );

    	$this->db->order_by('totalcargado', 'desc');
    	$this->db->group_by('idusuario');

    	$this->db->from('SALDO');
    	$this->db->where('fecha BETWEEN "'.$fechainicio.'" and "'.$fechafin.'"');
    	$query = $this->db->get();
    	$promedio = array();
    	foreach ($query->result_array() as $index => $value) {
    		array_push($promedio,$value['totalcargado']);
    	}
    	$promedio = array_filter($promedio);
        if(count($promedio) > 0){
            $resultado = array_sum($promedio)/count($promedio);
        }else{
            $resultado = 0;
        }
    	
		return $resultado;
	}
}
<?php
class Compracomida_model extends CI_Model {

	public function validar($identificador) {
		$this->db->set   ('verificado', true);
        $this->db->where ('identificador', $identificador);

        if ( !$this->db->update ('COMPRACOMIDA') )
            return false;

        return true;
    }

    public function buscarPorId($identificador) {
    	$resultado = $this->db->query('SELECT * FROM COMPRACOMIDA WHERE identificador=?', $identificador);

        if ($resultado->num_rows() == 0)
            return false;

        $comida = $resultado->row_array();

        return $comida;
    }

}
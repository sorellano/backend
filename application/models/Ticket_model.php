<?php
class Ticket_model extends CI_Model {

	public function validar($idticket) {
		$this->db->set   ('verificado', true);
        $this->db->where ('idticket', $idticket);

        if ( !$this->db->update ('TICKET') )
            return false;

        return true;

    }

    public function buscarPorId($idticket) {
    	$resultado = $this->db->query('SELECT * FROM TICKET WHERE idticket=?', $idticket);

        if ($resultado->num_rows() == 0)
            return false;

        $ticket = $resultado->row_array();

        return $ticket;
    }

}
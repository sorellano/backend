<?php
class Metodopago_model extends CI_Model {

	public function validarRegistro($metodopago) {
        $errores = array();

        /*--------------------- Checks de nombre ------------------*/
        if ( !is_string($metodopago['nombre']) ) {
            $errores['NOMBRE_INVALIDO'] =  "El nombre debe ser texto";
        } else {
            $largoNombre = strlen($metodopago['nombre']);
            if ($largoNombre > 30) {
                $errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 30 caracateres";
            }
        }

        /*------------------- Checks de descripcion --------------*/
        if ( !is_string($metodopago['descripcion']) ) {
            $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser texto";
        } else {
            $largoDescripcion = strlen($metodopago['descripcion']);
            if ($largoDescripcion > 200) {
                $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser menor o igual a 200 caracteres";
            }
        }

        return $errores;
    }

    public function validarNombre($nombre) {
       $errores = array();
       if ( !is_string($nombre) ) {
            $errores['NOMBRE_INVALIDO'] =  "El nombre debe ser texto";
        } else {
            $largoNombre = strlen($nombre);
            if ($largoNombre > 30) {
                $errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 30 caracateres";
            }
        }
       return $errores;
    }

    public function validarDescripcion($descripcion) {
       $errores = array();
       if ( !is_string($descripcion) ) {
            $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser texto";
        } else {
            $largoDescripcion = strlen($descripcion);
            if ($largoDescripcion > 200) {
                $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser menor o igual a 200 caracteres";
            }
        }
       return $errores;
    }

    public function listar() {
       $metodospago = array();

        $resultado = $this->db->query('SELECT * from METODOPAGO WHERE baja=0')->result_array();
        foreach($resultado as $metodopago)
            array_push($metodospago, $metodopago);

        return $metodospago;
    }

    public function alta($metodopago) {
       if ( !$this->db->insert("METODOPAGO", $metodopago) )
            return false;

        return true;
    }

    public function darDeBaja($nombre) {
        $this->db->set   ('baja', true);
		$this->db->where ('nombre', $nombre);
		if ( !$this->db->update('METODOPAGO') )
			return false;

		return true;
    }

    public function actualizar($nombre, $clave, $valor) {
        $this->db->set   ($clave, $valor);
        $this->db->where ('nombre', $nombre);

        if ( !$this->db->update ('METODOPAGO') )
            return false;

        return true;
    }

    public function altalogica($nombre) {
        $this->db->set   ('baja', false);
        $this->db->where ('nombre', $nombre);

        if ( !$this->db->update ('METODOPAGO') )
            return false;

        return true;
    }

    public function existeNombre($nombre) {
        $resultado = $this->db->query('SELECT * FROM METODOPAGO WHERE nombre=?', $nombre);

        if ($resultado->num_rows() == 0)
            return false;

        return $resultado->row_array();
    }

    public function buscarPorNombre($nombre) {
        $resultado = $this->db->query('SELECT * FROM METODOPAGO WHERE nombre=? AND baja=0', $nombre);

        if ($resultado->num_rows() == 0)
            return false;

        return $resultado->row_array();
    }

    public function buscar($buscar, $baja = 0, $inicio = FALSE, $cantidadregistros = FALSE)
    {
        $this->db->like("nombre",$buscar);

        if($baja==0){
            $this->db->where ('baja', 0);
        }
        if ($inicio !== FALSE && $cantidadregistros !== FALSE) {
            $this->db->limit($cantidadregistros,$inicio);
        }
        $this->db->order_by('idmetodopago', 'desc');
        $consulta = $this->db->get("METODOPAGO");
        return $consulta->result();
    }

    public function dadoDeBaja($nombre) {
        $resultado = $this->db->query('SELECT * FROM METODOPAGO WHERE nombre=? AND BAJA=1', $nombre);

        if ($resultado->num_rows() == 0)
            return false;
        else
        return true;
    }

    public function existe($idmetodopago) {
        $resultado = $this->db->query('SELECT * FROM METODOPAGO WHERE idmetodopago=?', $idmetodopago);

        if ($resultado->num_rows() == 0)
            return false;

        return $resultado->row_array();
    }

    public function dadoDeBajaId($idmetodopago) {
        $resultado = $this->db->query('SELECT * FROM METODOPAGO WHERE idmetodopago=? AND BAJA=1', $idmetodopago);

        if ($resultado->num_rows() == 0)
            return false;
        else
        return true;
    }

    
}

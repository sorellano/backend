<?php
class Empleado_model extends CI_Model {

    public function registrar($empleado) {
        if ( !$this->db->insert("EMPLEADO", $empleado) )
            return false;

        return true;
    }

    public function buscarPorMail($mail) {
        $resultado = $this->db->query('SELECT * FROM EMPLEADO WHERE mail=?', $mail);

        if ($resultado->num_rows() == 0)
			return false;

        $empleado = $resultado->row_array();

        return $empleado;
    }

    public function buscar($buscar, $baja = 0, $inicio = FALSE, $cantidadregistros = FALSE)
    {
        $this->db->like("mail",$buscar);

        if($baja==0){
            $this->db->where ('baja', 0);
        }
        if ($inicio !== FALSE && $cantidadregistros !== FALSE) {
            $this->db->limit($cantidadregistros,$inicio);
        }
        $this->db->order_by('idusuario', 'desc');
        $consulta = $this->db->get("EMPLEADO");
        return $consulta->result();
    }

    public function validarRegistro($empleado) {
        $errores = array();

        /*---------------- Checks de tipo ----------------*/
        //Hay que contemplar el caso que el tipo se mande como un string con un numero, en vez de un numero
        if ( !is_integer($empleado['tipo']) && !ctype_digit($empleado['tipo']) ) {
            $errores['TIPO_INVALIDO'] = "El tipo debe ser un entero";
        } else {
            $empleado['tipo'] = (int) $empleado['tipo']; //Casteo por si las dudas que sea un string con numero
            if ($empleado['tipo'] < 1 || $empleado['tipo'] > 4) {
                $errores['TIPO_INVALIDO'] = "El tipo debe ser de 1 a 4";
            }
        }

        /*---------------- Checks de email ----------------*/
        if ( !is_string($empleado['mail']) ) {
            $errores['MAIL_INVALIDO'] = "El email debe ser texto";
        } else {
            $largoMail = strlen($empleado['mail']);
            if ( $largoMail > 64 ) {
                $errores['MAIL_INVALIDO'] = "El largo del email debe ser menor o igual a 64 caracarteres";
            } else {
                if ( !preg_match('/@/', $empleado['mail']) ) {
                    $errores['MAIL_INVALIDO'] = "El email debe contener un arroba";
                } else {
                    $primer_caracter = substr($empleado['mail'], 0, 1);
                    $ultimo_caracter = substr($empleado['mail'], -1);
                    
                    $explode = explode( "." , $empleado['mail']);
                    if(!isset($explode[1])){
                        $errores['MAIL_INVALIDO'] = "Falta el sub-dominio";
                    }

                    if ($primer_caracter === '@' || $ultimo_caracter === '@') {
                        $errores['MAIL_INVALIDO'] = "El arroba debe estar en el medio";
                    }

                    //check de sub dominio
                }
            }
        }

        /*-------------- Checks de password ------------*/
        if ( !is_string($empleado['password']) ) {
            $errores['PASSWORD_INVALIDO'] = "La password debe ser texto";
        }

        /*---------------- Checks de username --------------*/
        if ( !is_string($empleado['username']) ) {
            $errores['USERNAME_INVALIDO'] = "El nombre de usuario debe ser texto";
        } else {
            $largoUsername = strlen($empleado['username']);
            if ($largoUsername > 24) {
                $errores['USERNAME_INVALIDO'] = "El nombre de usuario debe ser menos o igual a 24 caracaters";
            }
        }

        return $errores;
    }

    public function validarpassword($password) {
        $errores = array();
        if ( !is_string($password) ) {
            $errores['PASSWORD_INVALIDO'] = "La password debe ser texto";
        }
        return $errores; 
    }

    public function validartipo($tipo) {
        $errores = array();
        if ( !is_integer($tipo) && !ctype_digit($tipo) ) {
            $errores['TIPO_INVALIDO'] = "El tipo debe ser un entero";
        } else {
            $tipo = (int) $tipo; //Casteo por si las dudas que sea un string con numero
            if ($tipo < 1 || $tipo > 4) {
                $errores['TIPO_INVALIDO'] = "El tipo debe ser de 1 a 4";
            }
        }
        return $errores; 
    }

    public function validarusername($username) {
        $errores = array();
        if ( !is_string($username) ) {
            $errores['USERNAME_INVALIDO'] = "El nombre de usuario debe ser texto";
        } else {
            $largoUsername = strlen($username);
            if ($largoUsername > 24) {
                $errores['USERNAME_INVALIDO'] = "El nombre de usuario debe ser menos o igual a 24 caracaters";
            }
        }
        return $errores;        
    }

    public function modificarEmpleado($idusuario, $clave, $valor) {
        $this->db->set   ($clave, $valor);
        $this->db->where ('idusuario', $idusuario);

        if ( !$this->db->update ('EMPLEADO') )
            return false;

        return true;
    }


    public function darDeBaja($idusuario) {
        $this->db->set   ('baja', true);
        $this->db->where ('idusuario', $idusuario);

        if ( !$this->db->update ('EMPLEADO') )
            return false;

        return true;
    }

    public function existeIdUsuario($idusuario) {
        $resultado = $this->db->query('SELECT * FROM EMPLEADO WHERE idusuario=?', $idusuario);

        if ($resultado->num_rows() == 0)
            return false;
        else
        return true;
    }

    public function dadoDeBaja($idusuario) {
        $resultado = $this->db->query('SELECT * FROM EMPLEADO WHERE idusuario=? AND BAJA=1', $idusuario);

        if ($resultado->num_rows() == 0)
            return false;
        else
        return true;
    }

    public function altalogica($mailempleado) {
        $this->db->set   ('baja', false);
        $this->db->where ('mail', $mailempleado);

        if ( !$this->db->update ('EMPLEADO') )
            return false;

        return true;
    }
}

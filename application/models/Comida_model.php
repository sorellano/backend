<?php
class Comida_model extends CI_Model {

	public function validarRegistro($comida) {
        $errores = array();

        /*--------------------- Checks de nombre ------------------*/
        if ( !is_string($comida['nombre']) ) {
            $errores['NOMBRE_INVALIDO'] =  "El nombre debe ser texto";
        } else {
            $largoNombre = strlen($comida['nombre']);
            if ($largoNombre > 30) {
                $errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 30 caracateres";
            }
        }

        /*------------------- Checks de descripcion --------------*/
        if ( !is_string($comida['descripcion']) ) {
            $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser texto";
        } else {
            $largoDescripcion = strlen($comida['descripcion']);
            if ($largoDescripcion > 60) {
                $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser menor o igual a 60 caracteres";
            }
        }

        return $errores;
    }

    public function validarNombre($nombre) {
       $errores = array();
       if ( !is_string($nombre) ) {
            $errores['NOMBRE_INVALIDO'] =  "El nombre debe ser texto";
        } else {
            $largoNombre = strlen($nombre);
            if ($largoNombre > 30) {
                $errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 30 caracateres";
            }
        }
       return $errores;
    }

    public function validarDescripcion($descripcion) {
       $errores = array();
       if ( !is_string($descripcion) ) {
            $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser texto";
        } else {
            $largoDescripcion = strlen($descripcion);
            if ($largoDescripcion > 60) {
                $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser menor o igual a 60 caracteres";
            }
        }
       return $errores;
    }

    public function listar() {
       $comidas = array();

        $resultado = $this->db->query('SELECT * from COMIDA WHERE baja=0')->result_array();
        foreach($resultado as $comida)
            array_push($comidas, $comida);

        return $comidas;
    }

    public function contar() {
        return $this->db->query('SELECT * from COMIDA WHERE baja=0')->num_rows();
    }

    public function paginar($cant) {
        return $this->db->get('COMIDA',$cant,$this->uri->segment(3))->result_array(); //Uri segment 3 se refiere al 3er valor en la URL a partir de la base , en este caso /controller/funcion/numero
    }

    public function alta($comida) {
       if ( !$this->db->insert("COMIDA", $comida) )
            return false;

        return true;
    }

    public function darDeBaja($nombre) {
        $this->db->set   ('baja', true);
		$this->db->where ('nombre', $nombre);
		if ( !$this->db->update('COMIDA') )
			return false;

		return true;
    }

    public function actualizar($nombre, $clave, $valor) {
        $this->db->set   ($clave, $valor);
        $this->db->where ('nombre', $nombre);

        if ( !$this->db->update ('COMIDA') )
            return false;

        return true;
    }

    public function altalogica($nombre) {
        $this->db->set   ('baja', false);
        $this->db->where ('nombre', $nombre);

        if ( !$this->db->update ('COMIDA') )
            return false;

        return true;
    }

    public function existeNombre($nombre) {
        $resultado = $this->db->query('SELECT * FROM COMIDA WHERE nombre=?', $nombre);

        if ($resultado->num_rows() == 0)
            return false;

        return $resultado->row_array();
    }

    public function existe($id) {
        $resultado = $this->db->query('SELECT * FROM COMIDA WHERE idcomida=?', $id);

        if ($resultado->num_rows() == 0)
            return false;

        return true;
    }

    public function buscarPorNombre($nombre) {
        $resultado = $this->db->query('SELECT * FROM COMIDA WHERE nombre=? AND baja=0', $nombre);

        if ($resultado->num_rows() == 0)
            return false;

        return $resultado->row_array();
    }

    public function buscar($buscar, $baja = 0, $inicio = FALSE, $cantidadregistros = FALSE)
    {
        $this->db->like("nombre",$buscar);

        if($baja==0){
            $this->db->where ('baja', 0);
        }
        if ($inicio !== FALSE && $cantidadregistros !== FALSE) {
            $this->db->limit($cantidadregistros,$inicio);
        }
        $this->db->order_by('idcomida', 'desc');
        $consulta = $this->db->get("COMIDA");
        return $consulta->result();
    }

    public function dadaDeBaja($nombre) {
        $resultado = $this->db->query('SELECT * FROM COMIDA WHERE nombre=? AND BAJA=1', $nombre);

        if ($resultado->num_rows() == 0)
            return false;
        else
        return true;
    }

    public function dadaDeBajaId($id) {
        $resultado = $this->db->query('SELECT * FROM COMIDA WHERE idcomida=? AND BAJA=1', $id);

        if ($resultado->num_rows() == 0)
            return false;
        else
        return true;
    }
    
}

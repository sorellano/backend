<?php
class Menu_model extends CI_Model {

	//Lista todas las comidas que no estan asignadas a un determinado evento
    public function noAsignadas($idevento) {

    	$this->load->model('Comida_model');
    	$todas = $this->Comida_model->listar();

    	$asignadas = $this->Asignadas($idevento);
    	$noasignadas = array();

    	foreach($todas as $comida){ //Para todas las comidas que existen
            $coincide = FALSE;
    		foreach ($asignadas as $asignada) { //En relacion con las comidas que ya estan asignadas

    			if ($comida['idcomida']==$asignada['idcomida']){ //Comparo si es igual
    				$coincide = TRUE;} //Si es igual lo marco como igual

    		}
            if(!$coincide){ //Si dio que no era igual entonces la asigno al array de las no asignadas
            array_push($noasignadas, $comida);} //Si no quedo marcada no asigna nada y continua a la siguiente iteracion (Osea la siguiente de todas las comidas)  
        }
        return $noasignadas; 	
    }

    //Lista todas las comidas que si estan asignadas a un determinado evento
    public function Asignadas($idevento) {

    	$asignadas = array();

        $this->db->select('*');
        $this->db->from('MENU');
        $this->db->join('COMIDA', 'MENU.idcomida = COMIDA.idcomida');
        $this->db->where('MENU.baja', 0);
        $this->db->where('MENU.idevento', $idevento);
        $resultado = $this->db->get();
        return $resultado->result_array();

        //$resultado = $this->db->query('SELECT * from MENU WHERE baja=0 and idevento=?', $idevento)->result_array();


        foreach($resultado as $comida)
            array_push($asignadas, $comida);

        return $asignadas;

    }

    public function validarprecio($precio) {
        $errores = array();
        if ( !is_integer($precio) && !ctype_digit($precio) ) {
            $errores['PRECIO_INVALIDO'] = "El precio debe ser un entero";
        } else {
            $precio = (int) $precio; //Casteo por si las dudas que sea un string con numero
            if ($precio < 1 || $precio > 50000) {
                $errores['PRECIO_INVALIDO'] = "El precio debe ser de 1 a 50000";
            }
        }
        return $errores; 
    }

    public function agregarComida($comida) {
        if ( !$this->db->insert("MENU", $comida) )
            return false;

        return true;
    }

    public function dadoDeBaja($idcomida,$idevento) {

        $this->db->from('MENU');
        $this->db->where('idcomida', $idcomida);
        $this->db->where('idevento', $idevento);
        $this->db->where('baja', 1);
        $resultado = $this->db->get();

        if ($resultado->num_rows() == 0)
            return false;

        return true;
    }

    public function darDeBaja($idevento,$idcomida) {
        $this->db->set   ('baja', true);
        $this->db->where ('idevento', $idevento);
        $this->db->where ('idcomida', $idcomida);
        if ( !$this->db->update('MENU') )
            return false;

        return true;
    }

    public function cambiarPrecio($idevento,$idcomida,$precio) {
        $this->db->set   ('precio', $precio);
        $this->db->where ('idevento', $idevento);
        $this->db->where ('idcomida', $idcomida);
        $this->db->where ('baja', 0);
        if ( !$this->db->update('MENU') )
            return false;

        return true;
    }

    public function altaLogica($idevento,$idcomida) {
        $this->db->set   ('baja', false);
        $this->db->where ('idevento', $idevento);
        $this->db->where ('idcomida', $idcomida);
        if ( !$this->db->update('MENU') )
            return false;

        return true;
    }

    public function buscar($idevento,$buscar,$inicio = FALSE, $cantidadregistros = FALSE)
    {
        $this->db->select('*');
        $this->db->from('MENU');
        $this->db->join('COMIDA', 'MENU.idcomida = COMIDA.idcomida');
        $this->db->where('MENU.baja', 0);
        $this->db->where('MENU.idevento', $idevento);
        $this->db->like("COMIDA.nombre",$buscar);
        if ($inicio !== FALSE && $cantidadregistros !== FALSE) {
            $this->db->limit($cantidadregistros,$inicio);
        }
        $this->db->order_by('MENU.idcomida', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function comidaxidevento($idevento,$idcomida) //funcion para ver si el idcomida pertenece al idevento
    {
        $this->db->select('*');
        $this->db->from('MENU');
        $this->db->where('baja', 0);
        $this->db->where('idevento', $idevento);
        $this->db->where('idcomida', $idcomida);
        $query = $this->db->get();
        if ($query->num_rows() == 0)
            return false;

        return true;
    }

        

}
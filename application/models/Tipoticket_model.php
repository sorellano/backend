<?php
class Tipoticket_model extends CI_Model {

	public function agregar($tipoticket) {
			if ( !$this->db->insert("TIPOTICKET", $tipoticket) )
            return false;

        return true;
	}

    public function listar($idevento,$buscar,$inicio = FALSE, $cantidadregistros = FALSE) {
        $this->db->select('*');
        $this->db->from('TIPOTICKET');
        $this->db->like('nombre', $buscar);
        $this->db->where('idevento', $idevento);
        $this->db->where('baja', 0);
        if ($inicio !== FALSE && $cantidadregistros !== FALSE) {
            $this->db->limit($cantidadregistros,$inicio);
        }
        $this->db->order_by("fecha asc");
        $query = $this->db->get();
        return $query->result();
    }

    public function tipoticketsxidevento($idevento) {
        $this->db->select('*');
        $this->db->from('TIPOTICKET');
        $this->db->where('idevento', $idevento);
        $this->db->where('baja', 0);
        $query = $this->db->get();
        return $query->result_array();
    }

	public function validarNombre($nombre) {
    	$errores = array();
        if ( !is_string($nombre) ) {
            $errores['NOMBRE_INVALIDO'] =  "El nombre debe ser texto";
        } else {
            $largoNombre = strlen($nombre);
            if ($largoNombre > 30) {
                $errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 30 caracateres";
            }
        }
        return $errores;
    }

    public function buscarPorId($idtipoticket) {
        $resultado = $this->db->query('SELECT * FROM TIPOTICKET WHERE idtipoticket=?', $idtipoticket);

        if ($resultado->num_rows() == 0)
            return false;

        $tipoticket = $resultado->row_array();

        return $tipoticket;

    }

    public function dadoDeBaja($idtipoticket) {
        $resultado = $this->db->query('SELECT * FROM TIPOTICKET WHERE idtipoticket=? AND BAJA=1', $idtipoticket);

        if ($resultado->num_rows() == 0)
            return false;
        else
        return true;
    }

    public function modificarTipoTicket($idtipoticket, $clave, $valor) {
        $this->db->set   ($clave, $valor);
        $this->db->where ('idtipoticket', $idtipoticket);

        if ( !$this->db->update ('TIPOTICKET') )
            return false;

        return true;
    }

    public function darDeBaja($idtipoticket) {
        $this->db->set   ('baja', true);
        $this->db->where ('idtipoticket', $idtipoticket);
        if ( !$this->db->update('TIPOTICKET') )
            return false;

        return true;
    }

    public function altaLogica($idtipoticket,$idevento) {
        $this->db->set   ('baja', false);
        $this->db->where ('idtipoticket', $idtipoticket);
        $this->db->where ('idevento', $idevento);
        if ( !$this->db->update('TIPOTICKET') )
            return false;

        return true;
    }

    public function validarReferencia($referencia) {
        $errores = array();
        if ( !is_string($referencia) ) {
            $errores['REFERENCIA_INVALIDA'] = "La referencia debe ser texto";
        } else {
            $largoImagen = strlen($referencia);
            if ($largoImagen> 50) {
                $errores['IMAGEN_INVALIDA'] = "La referencia debe ser menor o igual a 50 caracteres";
            }
        }
        return $errores;
    }

    public function existe($idtipoticket) {
        $resultado = $this->db->query('SELECT * FROM TIPOTICKET WHERE idtipoticket=?', $idtipoticket);

        if ($resultado->num_rows() == 0) 
            return false;
        else
        return true;
    }

    public function validarPrecio($precio) {
        $errores = array();
        if ( !is_integer($precio) && !ctype_digit($precio) ) {
            $errores['PRECIO_INVALIDO'] = "El precio debe ser un entero";
        } else {
            $precio = (int) $precio; //Casteo por si las dudas que sea un string con numero
            if ($precio < 1 || $precio > 99999) {
                $errores['PRECIO_INVALIDO'] = "El precio debe ser de 1 a 99999";
            }
        }
        return $errores; 
    }

    public function validarCantidad($cantidad,$idevento,$fecha) {
        $errores = array();
        if ( !is_integer($cantidad) && !ctype_digit($cantidad) ) {
            $errores['CANTIDAD_INVALIDO'] = "La cantidad debe ser un numero";
        } else {
            $cantidad = (int) $cantidad;
            if ($cantidad < 1) { //Establesco la cota minima del precio
                $errores['CANTIDAD_INVALIDO'] = "La cantidad debe ser mayor a 1";
            }
            else{
            	$this->load->model('Evento_model'); 
            	$plazas = $this->Evento_model->cantidadDePlazas($idevento); //obtengo la cantidad de plazas que soporta el evento por su id

            		//Con esto obtengo la suma de la cantidad de tickets que estan disponibles para la venta para un evento a una hora dada.
            	$insertadas = 0;
            	$this->db->select('*');
            	$this->db->from('TIPOTICKET');
            	$this->db->where('idevento', $idevento);
            	$this->db->where('fecha', $fecha);
                $this->db->where('baja', 0);
            	$get = $this->db->get();
            	$resultado = $get->result();
            	foreach ($resultado as $key) {
            		$insertadas = $key->cantidad+$insertadas;
            	}
				if (($insertadas + $cantidad) > $plazas) {
				    $errores['CANTIDAD_INVALIDO'] = 'La cantidad de tickets que desea insertar hace que se exceda el limite de plazas para el evento por '.($insertadas+$cantidad-$plazas);
				    $errores['Cantidad de tickets a la venta para el evento :'.$insertadas] = '';
				    $errores['Cantidad de tickets a insertar :'.$cantidad] = '';
				    $errores['Sumatoria :'.($insertadas+$cantidad)] = '';
            		$errores['Plazas disponibles en el lugar del evento :'.$plazas] = '';
				}           	
            }
        }
        return $errores;
    }

    public function validarRegistro($tipoticket) {
        $errores = array();

		$cantidad = $tipoticket['cantidad'];
		$fecha = $tipoticket['fecha'];
		$idevento = $tipoticket['idevento'];
		/*--------------------- Checks de Cantidad ------------------*/
		if ( !is_integer($cantidad) && !ctype_digit($cantidad) ) {
            $errores['CANTIDAD_INVALIDO'] = "La cantidad debe ser un numero";
        } else {
            $cantidad = (int) $cantidad;
            if ($cantidad < 1) { //Establesco la cota minima del precio
                $errores['CANTIDAD_INVALIDO'] = "La cantidad debe ser mayor a 1";
            }
            else{
            	$this->load->model('Evento_model'); 
            	$plazas = $this->Evento_model->cantidadDePlazas($idevento); //obtengo la cantidad de plazas que soporta el evento por su id

            		//Con esto obtengo la suma de la cantidad de tickets que estan disponibles para la venta para un evento a una hora dada.
            	$insertadas = 0;
            	$this->db->select('*');
            	$this->db->from('TIPOTICKET');
            	$this->db->where('idevento', $idevento);
            	$this->db->where('fecha', $fecha);
                $this->db->where('baja', 0);
            	$get = $this->db->get();
            	$resultado = $get->result();
            	foreach ($resultado as $key) {
            		$insertadas = $key->cantidad+$insertadas;
            	}
				if (($insertadas + $cantidad) > $plazas) {
				    $errores['CANTIDAD_INVALIDO'] = 'La cantidad de tickets que desea insertar hace que se exceda el limite de plazas para el evento por '.($insertadas+$cantidad-$plazas);
				    $errores['Cantidad de tickets a la venta para el evento :'.$insertadas] = '';
				    $errores['Cantidad de tickets a insertar :'.$cantidad] = '';
				    $errores['Sumatoria :'.($insertadas+$cantidad)] = '';
            		$errores['Plazas disponibles en el lugar del evento :'.$plazas] = '';
				}           	
            }
        }
        $precio = $tipoticket['precio'];
        /*--------------------- Checks de Precio ------------------*/
        if ( !is_integer($precio) && !ctype_digit($precio) ) {
            $errores['PRECIO_INVALIDO'] = "El precio debe ser un entero";
        } else {
            $precio = (int) $precio; //Casteo por si las dudas que sea un string con numero
            if ($precio < 1 || $precio > 99999) {
                $errores['PRECIO_INVALIDO'] = "El precio debe ser de 1 a 99999";
            }
        }
        $nombre = $tipoticket['nombre'];
        /*--------------------- Checks de Nombre ------------------*/
        if ( !is_string($nombre) ) {
            $errores['NOMBRE_INVALIDO'] =  "El nombre debe ser texto";
        } else {
            $largoNombre = strlen($nombre);
            if ($largoNombre > 30) {
                $errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 30 caracateres";
            }
        }
        return $errores;
    }

    public function fecha($idtipoticket) {
    	$this->db->select('fecha');
        $this->db->from('TIPOTICKET');
        $this->db->where('idtipoticket', $idtipoticket);
        $this->db->where('baja', 0);
        $query = $this->db->get();
        return $query->result_array();
    }

}
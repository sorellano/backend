<?php
class Lugar_model extends CI_Model {

    public function validarInformacion($lugar) {
        $errores = array();


        /*---------------- Checks de nombre -------------------*/
        if ( !is_string($lugar['nombre']) ) {
            $errores['NOMBRE_INVALIDO'] = "El nombre debe ser texto";
        } else {
            $largoNombre = strlen($lugar['nombre']);
            if ($largoNombre > 30) {
                $errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 30 caracteres";
            }
        }

        /*------------------ Checks de plazas -----------------*/
        if ( !is_integer($lugar['plazas']) && !ctype_digit($lugar['plazas']) ) {
            $errores['PLAZAS_INVALIDO'] = "Plazas debe ser un numero";
        } else {
            $plazas = (int) $lugar['plazas'];
            if ($plazas < 0 || $plazas > 99999) {
                $errores['PLAZAS_INVALIDO'] = "Plazas debe estar entre 1 y 99999";
            }
        }

        /*------------------ Checks de direccion ------------*/
        if ( !is_string($lugar['direccion']) ) {
            $errores['DIRECCION_INVALIDA'] = "La direccion debe ser texto";
        } else {
            $largoDireccion = strlen($lugar['direccion']);
            if ($largoDireccion > 50) {
                $errores['DIRECCION_INVALIDA'] = "La direccion debe ser menor o igual a 50 caracteres";
            }
        }

        /*------------------- Checks de plano ---------------*/
        if ( !is_string($lugar['plano']) ) {
            $errores['PLANO_INVALIDO'] = "El plano debe ser texto";
        } else {
            $largoPlano = strlen($lugar['plano']);
            if ($largoPlano > 100) {
                $errores['PLANO_INVALIDO'] = "El plano debe ser menor o igual a 100 caracteres";
            }
        }

        /*------------------- Checks de web ------------------*/
        if ( !is_string($lugar['web']) ) {
            $errores['WEB_INVALIDA'] = "La web debe ser texto";
        } else {
            $largoWeb = strlen($lugar['web']);
            if ($largoWeb > 30) {
                $errores['WEB_INVALIDA'] = "La web debe ser menor o igual a 30 caracteres";
            }
        }

        /*------------------ Checks de descripcion -------------*/
        if ( !is_string($lugar['descripcion']) ) {
            $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser texto";
        } else {
            $largoDescripcion = strlen($lugar['descripcion']);
            if ($largoDescripcion > 100) {
                $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser menor o igual a 100 caracteres";
            }
        }


        return $errores;
    }

    public function validarnombre($nombre) {
        $errores = array();
        if ( !is_string($nombre) ) {
            $errores['NOMBRE_INVALIDO'] = "El nombre debe ser texto";
        } else {
            $largoNombre = strlen($nombre);
            if ($largoNombre > 30) {
                $errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 30 caracteres";
            }
        }
        return $errores;
    }

    public function validarplazas($plazas) {
        $errores = array();
        if ( !is_integer($plazas) && !ctype_digit($plazas) ) {
            $errores['PLAZAS_INVALIDO'] = "Plazas debe ser un numero";
        } else {
            $plazas = (int) $plazas;
            if ($plazas < 1 || $plazas > 99999) {
                $errores['PLAZAS_INVALIDO'] = "Plazas debe estar entre 1 y 99999";
            }
        }
        return $errores;
    }

    public function validardireccion($direccion) {
        $errores = array();
        if ( !is_string($direccion) ) {
            $errores['DIRECCION_INVALIDA'] = "La direccion debe ser texto";
        } else {
            $largoDireccion = strlen($direccion);
            if ($largoDireccion > 50) {
                $errores['DIRECCION_INVALIDA'] = "La direccion debe ser menor o igual a 50 caracteres";
            }
        }
        return $errores;
    }

    public function validarplano($plano) {
        $errores = array();
        if ( !is_string($plano) ) {
            $errores['PLANO_INVALIDO'] = "El plano debe ser texto";
        } else {
            $largoPlano = strlen($plano);
            if ($largoPlano > 100) {
                $errores['PLANO_INVALIDO'] = "El plano debe ser menor o igual a 100 caracteres";
            }
        }
        return $errores;
    }

    public function validarweb($web) {
        $errores = array();
        if ( !is_string($web) ) {
            $errores['WEB_INVALIDA'] = "La web debe ser texto";
        } else {
            $largoWeb = strlen($web);
            if ($largoWeb > 30) {
                $errores['WEB_INVALIDA'] = "La web debe ser menor o igual a 30 caracteres";
            }
        }
        return $errores;
    }

    public function validardescripcion($descripcion) {
        $errores = array();
        if ( !is_string($descripcion) ) {
            $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser texto";
        } else {
            $largoDescripcion = strlen($descripcion);
            if ($largoDescripcion > 100) {
                $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser menor o igual a 100 caracteres";
            }
        }
        return $errores;
    }    

    public function alta($lugar) {
        if ( !$this->db->insert("LUGAR", $lugar) )
            return false;

        return true;
    }

    public function modificar($nombre, $clave, $valor) {
        $this->db->set   ($clave, $valor);
        $this->db->where ('nombre', $nombre);

        if ( !$this->db->update ('LUGAR') )
            return false;

        return true;
    }

    public function darDeBaja($nombre) {
        $this->db->set   ('baja', true);
        $this->db->where ('nombre', $nombre);

        if ( !$this->db->update ('LUGAR') )
            return false;

        return true;
    }

    public function existeNombre($nombre) {
        $resultado = $this->db->query('SELECT * FROM LUGAR WHERE nombre=?', $nombre);

        if ($resultado->num_rows() == 0)
            return false;
        else
        return true;
    }

    public function buscarPorNombre($nombre) {
        $resultado = $this->db->query('SELECT * FROM LUGAR WHERE nombre=? AND baja=0', $nombre);

        if ($resultado->num_rows() == 0)
            return false;

        return $resultado->row_array();
    }

    public function buscar($buscar, $baja = 0, $inicio = FALSE, $cantidadregistros = FALSE)
    {
        $this->db->like("nombre",$buscar);

        if($baja==0){
            $this->db->where ('baja', 0);
        }
        if ($inicio !== FALSE && $cantidadregistros !== FALSE) {
            $this->db->limit($cantidadregistros,$inicio);
        }
        $this->db->order_by('idlugar', 'desc');
        $consulta = $this->db->get("LUGAR");
        return $consulta->result();
    }

    public function dadoDeBaja($nombre) {
        $resultado = $this->db->query('SELECT * FROM LUGAR WHERE nombre=? AND BAJA=1', $nombre);

        if ($resultado->num_rows() == 0)
            return false;
        else
        return true;
    }

    public function altalogica($nombre) {
        $this->db->set   ('baja', false);
        $this->db->where ('nombre', $nombre);

        if ( !$this->db->update ('LUGAR') )
            return false;

        return true;
    }

    public function listar() {
       $lugares = array();

        $resultado = $this->db->query('SELECT * from LUGAR WHERE baja=0')->result_array();
        foreach($resultado as $lugar)
            array_push($lugares, $lugar);

        return $lugares;
    }

    public function buscarPorId($id) {
        $resultado = $this->db->query('SELECT * FROM LUGAR WHERE idlugar=? AND baja=0', $id);

        if ($resultado->num_rows() == 0)
            return false;

        return $resultado->row_array();
    }
}

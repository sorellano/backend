<?php
class Categoria_model extends CI_Model {

    public function validarRegistro($categoria) {
        $errores = array();

        /*--------------------- Checks de nombre ------------------*/
        if ( !is_string($categoria['nombre']) ) {
            $errores['NOMBRE_INVALIDO'] =  "El nombre debe ser texto";
        } else {
            $largoNombre = strlen($categoria['nombre']);
            if ($largoNombre > 25) {
                $errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 25 caracateres";
            }
        }

        /*------------------- Checks de descripcion --------------*/
        if ( !is_string($categoria['descripcion']) ) {
            $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser texto";
        } else {
            $largoDescripcion = strlen($categoria['descripcion']);
            if ($largoDescripcion > 100) {
                $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser menor o igual a 100 caracteres";
            }
        }

        return $errores;
    }

    public function validarNombre($nombre) {
        $errores = array();

        /*--------------------- Checks de nombre ------------------*/
        if ( !is_string($nombre) ) {
            $errores['NOMBRE_INVALIDO'] =  "El nombre debe ser texto";
        } else {
            $largoNombre = strlen($nombre);
            if ($largoNombre > 25) {
                $errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 25 caracateres";
            }
        }

        return $errores;
    }

     public function validarDescripcion($descripcion) {
        $errores = array();

        /*------------------- Checks de descripcion --------------*/
        if ( !is_string($descripcion) ) {
            $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser texto";
        } else {
            $largoDescripcion = strlen($descripcion);
            if ($largoDescripcion > 100) {
                $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser menor o igual a 100 caracteres";
            }
        }

        return $errores;
    }

    public function listar() {
        $categorias = array();

        $resultado = $this->db->query('SELECT * from CATEGORIA WHERE baja=0')->result_array();
        foreach($resultado as $categoria)
            array_push($categorias, $categoria);

        return $categorias;
    }

    public function buscarPorNombre($nombre) {
        $resultado = $this->db->query('SELECT * FROM CATEGORIA WHERE nombre=? AND baja=0', $nombre);

        if ($resultado->num_rows() == 0)
            return false;

        return $resultado->row_array();
    }

    public function buscar($buscar, $baja = 0, $inicio = FALSE, $cantidadregistros = FALSE)
    {
        $this->db->like("nombre",$buscar);

        if($baja==0){
            $this->db->where ('baja', 0);
        }
        if ($inicio !== FALSE && $cantidadregistros !== FALSE) {
            $this->db->limit($cantidadregistros,$inicio);
        }
        $this->db->order_by('idcategoria', 'desc');
        $consulta = $this->db->get("CATEGORIA");
        return $consulta->result();
    }

    public function buscarPorId($id) {
        $resultado = $this->db->query('SELECT * FROM CATEGORIA WHERE idcategoria=? AND baja=0', $id);

        if ($resultado->num_rows() == 0)
            return false;

        return $resultado->row_array();
    }

    public function insertar($categoria) {
        if ( !$this->db->insert("CATEGORIA", $categoria) )
            return false;

        return true;
    }

    public function existeNombre($nombre) {
        $resultado = $this->db->query('SELECT * FROM CATEGORIA WHERE nombre=?', $nombre);

        if ($resultado->num_rows() == 0)
            return false;

        return $resultado->row_array();
    }

    public function darDeBaja($nombre) {
        $this->db->set   ('baja', true);
		$this->db->where ('nombre', $nombre);
		if ( !$this->db->update('CATEGORIA') )
			return false;

		return true;
    }

    public function actualizar($nombre, $clave, $valor) {
        $this->db->set   ($clave, $valor);
        $this->db->where ('nombre', $nombre);

        if ( !$this->db->update ('CATEGORIA') )
            return false;

        return true;
    }

    public function dadaDeBaja($nombre) {
        $resultado = $this->db->query('SELECT * FROM CATEGORIA WHERE nombre=? AND BAJA=1', $nombre);

        if ($resultado->num_rows() == 0)
            return false;
        else
        return true;
    }

    public function predeterminada($nombre) {
        $resultado = $this->db->query('SELECT * FROM CATEGORIA WHERE nombre=? AND predeterminado=1', $nombre);

        if ($resultado->num_rows() == 0)
            return false;
        else
        return true;
    }

    public function altalogica($nombre) {
        $this->db->set   ('baja', false);
        $this->db->where ('nombre', $nombre);

        if ( !$this->db->update ('CATEGORIA') )
            return false;

        return true;
    }
}

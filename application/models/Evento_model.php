<?php
class Evento_model extends CI_Model {

    public function validarRegistro($evento) {

    	$errores = array();
    	/*--------------------- Checks de nombre ------------------*/
    	if ( !is_string($evento['nombre']) ) {
            $errores['NOMBRE_INVALIDO'] =  "El nombre debe ser texto";
        } else {
            $largoNombre = strlen($evento['nombre']);
            if ($largoNombre > 50) {
                $errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 50 caracateres";
            }
        }

        /*------------------- Checks de descripcion --------------*/
        if ( !is_string($evento['descripcion']) ) {
            $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser texto";
        } else {
            $largoDescripcion = strlen($evento['descripcion']);
            if ($largoDescripcion > 1600) {
                $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser menor o igual a 1600 caracteres";
            }
        }

        /*---------------- Checks de categoria ----------------*/
        if ( !is_integer($evento['idcategoria']) && !ctype_digit($evento['idcategoria']) ) {
            $errores['CATEGORIA_INVALIDA'] = "La categoria debe ser un entero";
        } else {
            $evento['idcategoria'] = (int) $evento['idcategoria']; //Casteo por si las dudas que sea un string con numero
            $this->load->model('Categoria_model');
            if (!$this->Categoria_model->buscarPorId($evento['idcategoria'])) {
                $errores['CATEGORIA_INVALIDA'] = "La categoria no existe";
            }
        }

        /*---------------- Checks de prioridad ----------------*/
        if ( !is_integer($evento['prioridad']) && !ctype_digit($evento['prioridad']) ) {
            $errores['PRIORIDAD_INVALIDA'] = "La prioridad debe ser un entero";
        } else {
           $evento['prioridad'] = (int) $evento['prioridad']; //Casteo por si las dudas que sea un string con numero
            if ($evento['prioridad'] < 1 || $evento['prioridad'] > 99) {
                $errores['PRIORIDAD_INVALIDA'] = "La prioridad debe ser de 1 a 99";
            }
        }

        /*---------------- Checks de lugar ----------------*/
        if ( !is_integer($evento['idlugar']) && !ctype_digit($evento['idlugar']) ) {
            $errores['LUGAR_INVALIDO'] = "El lugar debe ser un entero";
        } else {
            $evento['idlugar'] = (int) $evento['idlugar']; //Casteo por si las dudas que sea un string con numero
            $this->load->model('Lugar_model');
            if (!$this->Lugar_model->buscarPorId($evento['idlugar'])) {
                $errores['LUGAR_INVALIDO'] = "El lugar no existe";
            }
        }
        return $errores;

    }

    public function validarNombre($nombre) {
    	$errores = array();
        if ( !is_string($nombre) ) {
            $errores['NOMBRE_INVALIDO'] =  "El nombre debe ser texto";
        } else {
            $largoNombre = strlen($nombre);
            if ($largoNombre > 50) {
                $errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 50 caracateres";
            }
        }
        return $errores;
    }

    public function validarDescripcion($descripcion) {
        $errores = array();
        if ( !is_string($descripcion) ) {
            $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser texto";
        } else {
            $largoDescripcion = strlen($descripcion);
            if ($largoDescripcion > 1600) {
                $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser menor o igual a 1600 caracteres";
            }
        }
        return $errores;
    }

    public function validarImagen($imagen) {
        $errores = array();
        if ( !is_string($imagen) ) {
            $errores['IMAGEN_INVALIDA'] = "La imagen debe ser texto";
        } else {
            $largoImagen = strlen($imagen);
            if ($largoImagen> 100) {
                $errores['IMAGEN_INVALIDA'] = "La imagen debe ser menor o igual a 100 caracteres";
            }
        }
        return $errores;
    }

    public function validarLugar($lugar) {
        $errores = array();
        if ( !is_integer($lugar) && !ctype_digit($lugar) ) {
            $errores['LUGAR_INVALIDO'] = "El lugar debe ser un entero";
        } else {
            $lugar = (int) $lugar; //Casteo por si las dudas que sea un string con numero
            $this->load->model('Lugar_model');
            if (!$this->Lugar_model->buscarPorId($lugar)) {
                $errores['LUGAR_INVALIDO'] = "El lugar no existe";
            }
        }
        return $errores;
    }

    public function validarCategoria($categoria) {
        $errores = array();
        if ( !is_integer($categoria) && !ctype_digit($categoria) ) {
            $errores['CATEGORIA_INVALIDA'] = "La categoria debe ser un entero";
        } else {
            $categoria = (int) $categoria; //Casteo por si las dudas que sea un string con numero
            $this->load->model('Categoria_model');
            if (!$this->Categoria_model->buscarPorId($categoria)) {
                $errores['CATEGORIA_INVALIDA'] = "La categoria no existe";
            }
        }
        return $errores;
    }

    public function validarPrioridad($prioridad) {
        $errores = array();
        if ( !is_integer($prioridad) && !ctype_digit($prioridad) ) {
            $errores['PRIORIDAD_INVALIDA'] = "La prioridad debe ser un entero";
        } else {
           $prioridad = (int) $prioridad; //Casteo por si las dudas que sea un string con numero
            if ($prioridad < 1 || $prioridad > 99) {
                $errores['PRIORIDAD_INVALIDA'] = "La prioridad debe ser de 1 a 99";
            }
        }
        return $errores;
    }

    public function alta($evento) {
        if ( !$this->db->insert("EVENTO", $evento) )
            return false;

        return true;
    }

    public function buscarPorId($id) {
        $resultado = $this->db->query('SELECT * FROM EVENTO WHERE idevento=?', $id);

        if ($resultado->num_rows() == 0)
            return false;

        $evento = $resultado->row_array();

        return $evento;
    }

    public function darDeBaja($idevento) {
        $this->db->set   ('baja', true);
        $this->db->where ('idevento', $idevento);

        if ( !$this->db->update ('EVENTO') )
            return false;

        return true;
    }

    public function existeIdEvento($idevento) {
        $resultado = $this->db->query('SELECT * FROM EVENTO WHERE idevento=?', $idevento);

        if ($resultado->num_rows() == 0)
            return false;
        else
        return true;
    }

    public function dadoDeBaja($idevento) {
        $resultado = $this->db->query('SELECT * FROM EVENTO WHERE idevento=? AND BAJA=1', $idevento);

        if ($resultado->num_rows() == 0)
            return false;
        else
        return true;
    }

    public function altalogica($idevento) {
        $this->db->set   ('baja', false);
        $this->db->where ('idevento', $idevento);

        if ( !$this->db->update ('EVENTO') )
            return false;

        return true;
    }

    public function modificarEvento($idevento, $clave, $valor) {
        $this->db->set   ($clave, $valor);
        $this->db->where ('idevento', $idevento);

        if ( !$this->db->update ('EVENTO') )
            return false;

        return true;
    }

    public function cantidadDePlazas($idevento) {
        $this->db->select('*');
        $this->db->from('EVENTO');
        $this->db->join('LUGAR', 'EVENTO.idlugar = LUGAR.idlugar');
        $this->db->where('EVENTO.idevento', $idevento);
        $query = $this->db->get();
        $data = $query->row_array();
        if (isset($data))
            {
        return $data['plazas'];
        }
        else{
            return false;
        }
    }

    public function buscar($buscar, $baja = 0,$vigentes = 0, $inicio = FALSE, $cantidadregistros = FALSE)
    {
        
        $this->db->like("nombre",$buscar);
        if($baja==0){
            $this->db->where ('baja', 0);
        }
        if ($inicio !== FALSE && $cantidadregistros !== FALSE) {
            $this->db->limit($cantidadregistros,$inicio);
        }
        $this->db->order_by('idevento', 'desc');
        $consulta = $this->db->get("EVENTO");

        if($vigentes==0){
            return $consulta->result();
        }
        else{

            $resultado = $consulta->result_array();
        
            $return = array();
            foreach ($resultado as $fila)
                {
                    $fechas = [ 'fechas' => $this->fechasPorId($fila['idevento']) ];

                    if($fechas['fechas']){
                        $fila = array_merge($fila, $fechas);
                        array_push($return, $fila);
                    }
                    
                } 
            return $return;

        }
        
    }

    //fechasPorId solo devuelve las fechas vigentes para dicho evento.
    public function fechasPorId($idevento) {
        $fechas = array();

        $query = $this->db->query('SELECT * from EN WHERE idevento=? AND fecha >=?',array($idevento, date("Y-m-d h:i:sa")) );
        $resultado = $query->result_array();
        foreach($resultado as $fecha){
            array_push($fechas, $fecha);
        }

        if ($query->num_rows() == 0)
            return false;
        else
        return $fechas;
    }


}
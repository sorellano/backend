<?php
class Validacion extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Evento_model');
        $this->load->model('Tipoticket_model');
        $this->load->model('Ticket_model');
        $this->load->model('Compracomida_model');
        $this->load->model('Menu_model');
        $this->load->model('Comida_model');
    }

        public function index() {
        $permitidos = array(5,3); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $this->load->view('validacion');
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }

    }

    public function buscar() {
    	$permitidos = array(5,3); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
		        $buscar = $this->input->post("buscar");
		        $numeropagina = $this->input->post("nropagina");
		        $cantidad = $this->input->post("cantidad");
		        
		        $inicio = ($numeropagina -1)*$cantidad;
		        $data = array(
		            "eventos" => $this->Evento_model->buscar($buscar,0,1,$inicio,$cantidad),
		            "totalregistros" => count($this->Evento_model->buscar($buscar,0,1)),
		            "cantidad" => $cantidad
		            
		        );
		        echo json_encode($data);
		    }else{
		        $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
		            //Cargo el array de datos a la vista de errores
		        $data = array(
		            //Esto me sirve para saber a donde tiene que volver la plantilla de error
		            'redirect' => '/',
		            'errores' => $errores
		        );
		        //Envio los datos a la vista de errores
		        $this->load->view('Error', $data );
		    }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function validar() {
    	$permitidos = array(5,3); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){

        $idevento = $this->input->get("idevento");
        if (!$this->Evento_model->existeIdEvento($idevento) || $this->Evento_model->dadoDeBaja($idevento)) {
                //Cargo un array con los errores
            $errores = ['Error' => 'El evento no existe o esta dado de baja '.$idevento.'.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/validacion',
                'errores'  => $errores
            );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        else{$data = array(
            "idevento" => $idevento
        );
        $this->load->view('validar', $data);
    	}
        

        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function verificar() {
    	$permitidos = array(5,3); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){

        $idevento = $this->input->get("idevento");
        $clave = $this->input->get("clave");
        $valor = $this->input->get("valor");


        // validar que el evento este dado de alta y sea vigente

        if (!$this->Evento_model->existeIdEvento($idevento) || $this->Evento_model->dadoDeBaja($idevento)) {
				//Cargo un array con los errores
        	$errores = ['Error' => 'El evento no existe o esta dado de baja'];
				//Cargo el array de datos a la vista de errores
        	$data = array(
				//Esto me sirve para saber a donde tiene que volver la plantilla de error
        		'redirect' => '/evento',
        		'errores'  => $errores
        	);
				//Envio los datos a la vista de errores
        	$this->load->view('Error', $data );
        }
        else{
        	$fechas = $this->Evento_model->fechasPorId($idevento);
        	if( count($fechas) > 0) {
        		$valor = explode( ":" , $valor );
	            if($clave == "ticket"){
	            	$idticket = $valor[0];
	            	if(isset($valor[1])){$codigoseguridad = $valor[1];}else{$codigoseguridad = "";}//el codigo de seguridad que se utiliza para verificar contra la bd
	            	$ticket = $this->Ticket_model->buscarPorId($idticket);
	            	if($ticket){ //verifico que exista algun ticket con el idticket que me pasan
	            		$tipoticket = $this->Tipoticket_model->buscarPorId($ticket['idtipoticket']);
	            		if ($tipoticket) { //verifico que exista el tipo ticket que le corresponde al ticket
	            			$evento = $this->Evento_model->buscarPorId($tipoticket['idevento']);
	            			if ($evento) { //verifico que exista el evento asociado al id del tipo de ticket
	            				if($evento['idevento'] == $idevento){ //compruebo que el id del evento correspondiente con el ticket enviado sea el mismo que me esta enviando el cliente (que selecciono el usuario para realizar la validacion)
	            					if ($evento['baja'] == 0) {//compruebo que el evento no este dado de baja
	            						if ($tipoticket['baja'] == 0) { //verifico que el tipo de ticket no este dado de baja
	            							$hoy = date("Y-m-d");
	            							$fechaticket = explode( " " , $tipoticket['fecha'] );
	            							$fechaticket = $fechaticket[0];
	            							if ($hoy == $fechaticket) { //verifico que la fecha del ticket que se desea validar corresponda con la fecha actual
	            								if ($codigoseguridad == $ticket['autogenerado']) {
	            									if($ticket['verificado'] == false){
		            									if ($this->Ticket_model->validar($idticket)) {
		            										$data = [
										                        'info'     => 'Se verificó el ticket correctamente: '.$idticket.'.',
										                        'redirect' => '/validacion/validar/?idevento='.$idevento
										                    ];
										                    $this->load->view('success', $data );
		            									}else{
		            										//Cargo un array con los errores
												        	$errores = ['Error' => 'Error al intentar validar el ticket.'];
																//Cargo el array de datos a la vista de errores
												        	$data = array(
																//Esto me sirve para saber a donde tiene que volver la plantilla de error
												        		'redirect' =>'/validacion/validar/?idevento='.$idevento,
												        		'errores'  => $errores
												        	);
																//Envio los datos a la vista de errores
												        	$this->load->view('Error', $data );
		            									}
	            									}else{
	            										//Cargo un array con los errores
												        	$errores = ['Error' => 'El ticket que intenta verificar ya fue verificado.'];
																//Cargo el array de datos a la vista de errores
												        	$data = array(
																//Esto me sirve para saber a donde tiene que volver la plantilla de error
												        		'redirect' =>'/validacion/validar/?idevento='.$idevento,
												        		'errores'  => $errores
												        	);
																//Envio los datos a la vista de errores
												        	$this->load->view('Error', $data );
	            									}
	            								}else{
		            								//Cargo un array con los errores
										        	$errores = ['Error' => 'El codigo de seguridad provisto no es correcto.'];
														//Cargo el array de datos a la vista de errores
										        	$data = array(
														//Esto me sirve para saber a donde tiene que volver la plantilla de error
										        		'redirect' => '/validacion/validar/?idevento='.$idevento,
										        		'errores'  => $errores
										        	);
														//Envio los datos a la vista de errores
										        	$this->load->view('Error', $data );	
	            								}
	            							}else{
	            								//Cargo un array con los errores
									        	$errores = ['Error' => 'La fecha del ticket: '.$fechaticket.' no corresponde con la fecha actual: '.$hoy.'.'];
													//Cargo el array de datos a la vista de errores
									        	$data = array(
													//Esto me sirve para saber a donde tiene que volver la plantilla de error
									        		'redirect' => '/validacion/validar/?idevento='.$idevento,
									        		'errores'  => $errores
									        	);
													//Envio los datos a la vista de errores
									        	$this->load->view('Error', $data );
	            							}
	            						}else{
	            							//Cargo un array con los errores
								        	$errores = ['Error' => 'El tipo de ticket id '.$ticket['idtipoticket'].' parece estar dado de baja.'];
												//Cargo el array de datos a la vista de errores
								        	$data = array(
												//Esto me sirve para saber a donde tiene que volver la plantilla de error
								        		'redirect' => '/validacion/validar/?idevento='.$idevento,
								        		'errores'  => $errores
								        	);
												//Envio los datos a la vista de errores
								        	$this->load->view('Error', $data );
	            						}
	            					}else{
	            						//Cargo un array con los errores
							        	$errores = ['Error' => 'El evento parece estar dado de baja '.$idevento.''];
											//Cargo el array de datos a la vista de errores
							        	$data = array(
											//Esto me sirve para saber a donde tiene que volver la plantilla de error
							        		'redirect' => '/validacion',
							        		'errores'  => $errores
							        	);
											//Envio los datos a la vista de errores
							        	$this->load->view('Error', $data );	
	            					}
	            				}else{
	            					//Cargo un array con los errores
						        	$errores = ['Error' => 'El ticket que esta intentando validar no corresponde con el evento seleccionado.'];
										//Cargo el array de datos a la vista de errores
						        	$data = array(
										//Esto me sirve para saber a donde tiene que volver la plantilla de error
						        		'redirect' => '/validacion',
						        		'errores'  => $errores
						        	);
										//Envio los datos a la vista de errores
						        	$this->load->view('Error', $data );
	            				}
	            			}else{
	            				//Cargo un array con los errores
					        	$errores = ['Error' => 'No se reconoce ningun evento con el id: '.$tipoticket['idevento'].'.'];
									//Cargo el array de datos a la vista de errores
					        	$data = array(
									//Esto me sirve para saber a donde tiene que volver la plantilla de error
					        		'redirect' => '/validacion',
					        		'errores'  => $errores
					        	);
									//Envio los datos a la vista de errores
					        	$this->load->view('Error', $data );	
	            			}	
	            		}else{
	            			//Cargo un array con los errores
				        	$errores = ['Error' => 'No se reconoce ningun tipo de ticket con el id: '.$ticket['idtipoticket'].'.'];
								//Cargo el array de datos a la vista de errores
				        	$data = array(
								//Esto me sirve para saber a donde tiene que volver la plantilla de error
				        		'redirect' => '/validacion',
				        		'errores'  => $errores
				        	);
								//Envio los datos a la vista de errores
				        	$this->load->view('Error', $data );		
	            		}
	            	}else{
	            		//Cargo un array con los errores
			        	$errores = ['Error' => 'No se reconoce ningun ticket con el id: '.$idticket.'.'];
							//Cargo el array de datos a la vista de errores
			        	$data = array(
							//Esto me sirve para saber a donde tiene que volver la plantilla de error
			        		'redirect' => '/validacion',
			        		'errores'  => $errores
			        	);
							//Envio los datos a la vista de errores
			        	$this->load->view('Error', $data );	
	            	}
	            }elseif ($clave == "comida") {
	            	$identificador = $valor[0];
	            	if(isset($valor[1])){$codigoseguridad = $valor[1];}else{$codigoseguridad = "";} //el codigo de seguridad que se utiliza para verificar contra la bd
	            	$comidacomprada = $this->Compracomida_model->buscarPorId($identificador);
	            	if ($comidacomprada) {//verifico que exista una comida comprada con el id que me estan pasando
	            		$evento = $this->Evento_model->buscarPorId($comidacomprada['idevento']);
	            		if ($evento) { //verifico que exista el evento asociado al id del tipo de ticket
	            			if($evento['idevento'] == $idevento){ //compruebo que el id del evento correspondiente con el ticket
	            				if ($evento['baja'] == 0) {//verifico que el evento este dado de alta
	            					if ($this->Menu_model->comidaxidevento($idevento,$comidacomprada['idcomida'])){//verifico que el valor del IDcomida que esta dentro de la compra se encuentre en el menu del evento seleccionado por el usuario
	            						if(!$this->Comida_model->dadaDeBajaId($identificador)){ //Verifico que la comida no este dada de baja del sistema
	            							if ($codigoseguridad == $comidacomprada['autogenerado']) {//verifico que el autogenerado corresponda
	            									if($comidacomprada['verificado'] == false){ //verifico que la comida no haya sido verificada anteriormente
		            									if ($this->Compracomida_model->validar($identificador)) {
		            										$data = [
										                        'info'     => 'Se verificó la comida: '.$identificador.'.',
										                        'redirect' => '/validacion/validar/?idevento='.$idevento
										                    ];
										                    $this->load->view('success', $data );
		            									}else{
		            										//Cargo un array con los errores
												        	$errores = ['Error' => 'Error al intentar verificar la comida.'];
																//Cargo el array de datos a la vista de errores
												        	$data = array(
																//Esto me sirve para saber a donde tiene que volver la plantilla de error
												        		'redirect' =>'/validacion/validar/?idevento='.$idevento,
												        		'errores'  => $errores
												        	);
																//Envio los datos a la vista de errores
												        	$this->load->view('Error', $data );
		            									}
	            									}else{
	            										//Cargo un array con los errores
												        	$errores = ['Error' => 'La comida que intenta verificar ya fue varificada.'];
																//Cargo el array de datos a la vista de errores
												        	$data = array(
																//Esto me sirve para saber a donde tiene que volver la plantilla de error
												        		'redirect' =>'/validacion/validar/?idevento='.$idevento,
												        		'errores'  => $errores
												        	);
																//Envio los datos a la vista de errores
												        	$this->load->view('Error', $data );
	            									}
	            								}else{
		            								//Cargo un array con los errores
										        	$errores = ['Error' => 'El codigo de seguridad provisto no es correcto.'];
														//Cargo el array de datos a la vista de errores
										        	$data = array(
														//Esto me sirve para saber a donde tiene que volver la plantilla de error
										        		'redirect' => '/validacion/validar/?idevento='.$idevento,
										        		'errores'  => $errores
										        	);
														//Envio los datos a la vista de errores
										        	$this->load->view('Error', $data );	
	            								}	
	            						}else{
	            							//Cargo un array con los errores
									        $errores = ['Error' => 'La comida que se intenta validar se encuentra dada de baja '.$comidacomprada['idcomida'].'.'];
												//Cargo el array de datos a la vista de errores
									        $data = array(
												//Esto me sirve para saber a donde tiene que volver la plantilla de error
									        	'redirect' => '/validacion',
									        	'errores'  => $errores
									        );
												//Envio los datos a la vista de errores
									        $this->load->view('Error', $data );		
	            						}
	            					}else{
	            						//Cargo un array con los errores
								        $errores = ['Error' => 'La comida que se intenta validar no se encuentra en el menu del evento id '.$idevento.''];
											//Cargo el array de datos a la vista de errores
								        $data = array(
											//Esto me sirve para saber a donde tiene que volver la plantilla de error
								        	'redirect' => '/validacion',
								        	'errores'  => $errores
								        );
											//Envio los datos a la vista de errores
								        $this->load->view('Error', $data );	
	            					}
	            				}else{
	            					//Cargo un array con los errores
							        $errores = ['Error' => 'El evento parece estar dado de baja '.$idevento.''];
										//Cargo el array de datos a la vista de errores
							        $data = array(
										//Esto me sirve para saber a donde tiene que volver la plantilla de error
							        	'redirect' => '/validacion',
							        	'errores'  => $errores
							        );
										//Envio los datos a la vista de errores
							        $this->load->view('Error', $data );	
	            				}
	            			}else{
	            				//Cargo un array con los errores
						        $errores = ['Error' => 'La compra de comida que esta intentando validar no corresponde con el evento seleccionado.'];
								//Cargo el array de datos a la vista de errores
						        $data = array(
									//Esto me sirve para saber a donde tiene que volver la plantilla de error
						        	'redirect' => '/validacion',
						        	'errores'  => $errores
						        );
									//Envio los datos a la vista de errores
						        $this->load->view('Error', $data );
	            			}
	            		}else{
	            				//Cargo un array con los errores
					        	$errores = ['Error' => 'No se reconoce ningun evento con el id: '.$comidacomprada['idevento'].'.'];
									//Cargo el array de datos a la vista de errores
					        	$data = array(
									//Esto me sirve para saber a donde tiene que volver la plantilla de error
					        		'redirect' => '/validacion',
					        		'errores'  => $errores
					        	);
									//Envio los datos a la vista de errores
					        	$this->load->view('Error', $data );	
	            			}
	            	}else{
	            		//Cargo un array con los errores
			        	$errores = ['Error' => 'No se reconoce ninguna compra de comida con el id: '.$identificador.'.'];
							//Cargo el array de datos a la vista de errores
			        	$data = array(
							//Esto me sirve para saber a donde tiene que volver la plantilla de error
			        		'redirect' => '/validacion',
			        		'errores'  => $errores
			        	);
							//Envio los datos a la vista de errores
			        	$this->load->view('Error', $data );	
	            	}
	            }else{
	            	//Cargo un array con los errores
		        	$errores = ['Error' => 'No se reconoce el item que se desea validar: '.$clave.'.'];
						//Cargo el array de datos a la vista de errores
		        	$data = array(
						//Esto me sirve para saber a donde tiene que volver la plantilla de error
		        		'redirect' => '/validacion',
		        		'errores'  => $errores
		        	);
						//Envio los datos a la vista de errores
		        	$this->load->view('Error', $data );
	            }
        	}else{
        			//Cargo un array con los errores
        	$errores = ['Error' => 'El evento no tiene fechas vigentes'];
				//Cargo el array de datos a la vista de errores
        	$data = array(
				//Esto me sirve para saber a donde tiene que volver la plantilla de error
        		'redirect' => '/validacion',
        		'errores'  => $errores
        	);
				//Envio los datos a la vista de errores
        	$this->load->view('Error', $data );
        	}
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }
}
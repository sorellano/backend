<?php
class Test extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('Categoria_model');
        $this->load->model('Comida_model');
        $this->load->model('Cliente_model');
        $this->load->model('Empleado_model');
        $this->load->model('Evento_model');
        $this->load->model('Fechas_model');
        $this->load->model('Lugar_model');
        $this->load->model('Metodopago_model');
        $this->load->model('Paquete_model');
        $this->load->model('Tipoticket_model');
        $this->load->library('unit_test');
    }

    public function index() {

        $this->unit->set_test_items(array('test_name', 'test_datatype','result', 'line'));

        echo "<head><title>Unit test Backend</title><head>";
        // ---------------------------------- Categoria model
        echo "<hr>Tests de Categoria:";

        $test_name = 'Validar registro categoria: Descripcion demaciado larga';
        $categoria = [
            'nombre' => "NOMBRECATEGORIA",
            'descripcion'     => "DESCRIPCIONCATEGsdkljfklsdjfsñladfkjsdañlfkjsafñlksajdflñksajflñksadjflkñsajdflñksajfñlksdjflñsakjfdlñksajflñksadjflksajdflñksjaflñsadkjflñskdjfaslñkdfjsalkñdfjaslñkfjsdlñkfjsdlñfkajsdfñlkjsadfñlksajdfñlaskdjfORIA"
        ];
        $test = $this->Categoria_model->validarRegistro($categoria);
        $errores = array();
        $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser menor o igual a 100 caracteres";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);

        //----------------------------------------------------------
        $test_name = 'Validar registro categoria: Nombre null';
        $categoria = [
            'nombre' => null,
            'descripcion' => "asfsadsf"
        ];
        $test = $this->Categoria_model->validarRegistro($categoria);
        $errores = array();
        $errores['NOMBRE_INVALIDO'] = "El nombre debe ser texto";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar registro categoria: OK';
        $categoria = [
            'nombre' => "asssda",
            'descripcion' => "asfsadsf"
        ];
        $test = $this->Categoria_model->validarRegistro($categoria);
        $errores = array();
        //$errores['NOMBRE_INVALIDO'] = "El nombre debe ser texto";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);

        //----------------------------------------------------------
        $test_name = 'Validar nombre categoria: OK';
        $nombre = "CATEGORIA";
        $test = $this->Categoria_model->validarNombre($nombre);
        $errores = array();
        //$errores['NOMBRE_INVALIDO'] = "El nombre debe ser texto";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);

        //----------------------------------------------------------
        $test_name = 'Validar nombre categoria: Null';
        $nombre = null;
        $test = $this->Categoria_model->validarNombre($nombre);
        $errores = array();
        $errores['NOMBRE_INVALIDO'] = "El nombre debe ser texto";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);

        // ---------------------------------- Cliente model
        echo "<hr>Tests de Cliente :";
        $test_name = 'Validar nombre Cliente: OK';
        $nombre = "cliente";
        $test = $this->Cliente_model->validarNombre($nombre);
        $errores = array();
        //$errores['NOMBRE_INVALIDO'] = "El nombre debe ser texto";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar nombre Cliente: Null';
        $nombre = null;
        $test = $this->Cliente_model->validarNombre($nombre);
        $errores = array();
        $errores['NOMBRE_INVALIDO'] = "El nombre debe ser texto";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        
        //----------------------------------------------------------
        $test_name = 'Validar telefono Cliente: Null';
        $telefono = null;
        $test = $this->Cliente_model->validarTelefono($telefono);
        $errores = array();
        $errores['TELEFONO_INVALIDO'] = "El telefono ".$telefono." contiene caracteres no numericos";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar telefono Cliente: OK';
        $telefono = "098560874";
        $test = $this->Cliente_model->validarTelefono($telefono);
        $errores = array();
        //$errores['NOMBRE_INVALIDO'] = "El nombre debe ser texto";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        
        // ---------------------------------- Empleado model
        echo "<hr>Tests de Empleado:";
        $test_name = 'Validar registro Empleado: OK';
        $empleado = [
            'username' => "empleado",
            'mail'     => "mail@mail.com",
            'tipo'     => "1",
            'password' => password_hash("password", PASSWORD_BCRYPT)
        ];
        $test = $this->Empleado_model->validarRegistro($empleado);
        $errores = array();
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar registro Empleado: Tipo de empleado invalido';
        $empleado = [
            'username' => "empleado",
            'mail'     => "mail@mail.com",
            'tipo'     => "a",
            'password' => password_hash("password", PASSWORD_BCRYPT)
        ];
        $test = $this->Empleado_model->validarRegistro($empleado);
        $errores = array();
        $errores['TIPO_INVALIDO'] = "El tipo debe ser un entero";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar registro Empleado: Mail sin dominio';
        $empleado = [
            'username' => "empleado",
            'mail'     => "mailmail.com",
            'tipo'     => "2",
            'password' => password_hash("password", PASSWORD_BCRYPT)
        ];
        $test = $this->Empleado_model->validarRegistro($empleado);
        $errores = array();
        $errores['MAIL_INVALIDO'] = "El email debe contener un arroba";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar registro Empleado: Mail sin sub-dominio';
        $empleado = [
            'username' => "empleado",
            'mail'     => "mail@mail",
            'tipo'     => "2",
            'password' => password_hash("password", PASSWORD_BCRYPT)
        ];
        $test = $this->Empleado_model->validarRegistro($empleado);
        $errores = array();
        $errores['MAIL_INVALIDO'] = "Falta el sub-dominio";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar password Empleado: Ok';
        $password = password_hash("password", PASSWORD_BCRYPT);
        $test = $this->Empleado_model->validarpassword($password);
        $errores = array();
        //$errores['MAIL_INVALIDO'] = "Falta el sub-dominio";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar password Empleado: Null';
        $password = null;
        $test = $this->Empleado_model->validarpassword($password);
        $errores = array();
        $errores['PASSWORD_INVALIDO'] = "La password debe ser texto";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar tipo Empleado: Null';
        $tipo = null;
        $test = $this->Empleado_model->validartipo($tipo);
        $errores = array();
        $errores['TIPO_INVALIDO'] = "El tipo debe ser un entero";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar tipo Empleado: Numero invalido';
        $tipo = 20;
        $test = $this->Empleado_model->validartipo($tipo);
        $errores = array();
        $errores['TIPO_INVALIDO'] = "El tipo debe ser de 1 a 4";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar tipo Empleado: Tipo con letras';
        $tipo = "a";
        $test = $this->Empleado_model->validartipo($tipo);
        $errores = array();
        $errores['TIPO_INVALIDO'] = "El tipo debe ser un entero";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar tipo Empleado: OK';
        $tipo = 1;
        $test = $this->Empleado_model->validartipo($tipo);
        $errores = array();
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);


        // ---------------------------------- Evento model
        echo "<hr>Tests de Evento:";

        $test_name = 'Validar nombre Evento: Ok';
        $nombre = "eventito";
        $test = $this->Evento_model->validarNombre($nombre);
        $errores = array();
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        
        //----------------------------------------------------------
        $test_name = 'Validar nombre Evento: null';
        $nombre = null;
        $test = $this->Evento_model->validarNombre($nombre);
        $errores = array();
        $errores['NOMBRE_INVALIDO'] =  "El nombre debe ser texto";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar nombre Evento: array';
        $nombre = array();
        $test = $this->Evento_model->validarNombre($nombre);
        $errores = array();
        $errores['NOMBRE_INVALIDO'] =  "El nombre debe ser texto";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar prioridad Evento: Ok';
        $prioridad = 20;
        $test = $this->Evento_model->validarPrioridad($prioridad);
        $errores = array();
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar prioridad Evento: null';
        $prioridad = null;
        $test = $this->Evento_model->validarPrioridad($prioridad);
        $errores = array();
        $errores['PRIORIDAD_INVALIDA'] = "La prioridad debe ser un entero";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar prioridad Evento: Fuera de rango';
        $prioridad = 149;
        $test = $this->Evento_model->validarPrioridad($prioridad);
        $errores = array();
        $errores['PRIORIDAD_INVALIDA'] = "La prioridad debe ser de 1 a 99";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);

        // ---------------------------------- Fechas model
        echo "<hr>Tests de Fechas:";

        $test_name = 'Validar Fecha: Ok';
        $fecha = "2022-02-10";
        $test = $this->Fechas_model->validarFecha($fecha);
        $errores = array();
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar Fecha: Formato al reves';
        $fecha = "10-02-2022";
        $test = $this->Fechas_model->validarFecha($fecha);
        $errores = array();
        $errores['FECHA_INVALIDA'] = "La formato invalido de la fecha";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar Fecha: Fuera de rango';
        $fecha = "2009-02-10";
        $test = $this->Fechas_model->validarFecha($fecha);
        $errores = array();
        $errores['FECHA_INVALIDA'] = "La fecha no puede ser menor a la actual";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);

        // ---------------------------------- Lugar model
        echo "<hr>Tests de Lugar:";
        $test_name = 'Validar informacion Lugar: Ok';
        $lugar = [
            'nombre'          => "Lugar",
            'plazas'          => 40,
            'plano'           => "plano",
            'direccion'       => "Direccion",
            'web'             => "Sitio web",
            'descripcion'     => "Descripcion"
        ];
        $test = $this->Lugar_model->validarInformacion($lugar);
        $errores = array();
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar informacion Lugar: Mal nombre';
        $lugar = [
            'nombre'          => null,
            'plazas'          => 40,
            'plano'           => "plano",
            'direccion'       => "Direccion",
            'web'             => "Sitio web",
            'descripcion'     => "Descripcion"
        ];
        $test = $this->Lugar_model->validarInformacion($lugar);
        $errores = array();
        $errores['NOMBRE_INVALIDO'] = "El nombre debe ser texto";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar informacion Lugar: plazas cantidad negativa';
        $lugar = [
            'nombre'          => "Nombre",
            'plazas'          => -20,
            'plano'           => "plano",
            'direccion'       => "Direccion",
            'web'             => "Sitio web",
            'descripcion'     => "Descripcion"
        ];
        $test = $this->Lugar_model->validarInformacion($lugar);
        $errores = array();
        $errores['PLAZAS_INVALIDO'] = "Plazas debe estar entre 1 y 99999";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar informacion Lugar: Descripcion invalida';
        $lugar = [
            'nombre'          => "Nombre",
            'plazas'          => 20,
            'plano'           => "plano",
            'direccion'       => "Direccion",
            'web'             => "Sitio web",
            'descripcion'     => null
        ];
        $test = $this->Lugar_model->validarInformacion($lugar);
        $errores = array();
        $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser texto";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar nombre Lugar: Ok';
        $nombre = "Nombre";
        $test = $this->Lugar_model->validarnombre($nombre);
        $errores = array();
        //$errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser texto";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar nombre Lugar: null';
        $nombre = null;
        $test = $this->Lugar_model->validarnombre($nombre);
        $errores = array();
        $errores['NOMBRE_INVALIDO'] = "El nombre debe ser texto";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar nombre Lugar: Demaciado largo';
        $nombre = "jksadhfjksahdfjsahfajklsdhfkljashdflkjashdflkjashlkjsahdlkjahsljkhdsdfsaf";
        $test = $this->Lugar_model->validarnombre($nombre);
        $errores = array();
        $errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 30 caracteres";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar plazas Lugar: ok';
        $plazas = 40;
        $test = $this->Lugar_model->validarplazas($plazas);
        $errores = array();
        //$errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 30 caracteres";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar plazas Lugar: null';
        $plazas = null;
        $test = $this->Lugar_model->validarplazas($plazas);
        $errores = array();
        $errores['PLAZAS_INVALIDO'] = "Plazas debe ser un numero";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar plazas Lugar: negativo';
        $plazas = -20;
        $test = $this->Lugar_model->validarplazas($plazas);
        $errores = array();
        $errores['PLAZAS_INVALIDO'] = "Plazas debe estar entre 1 y 99999";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar plazas Lugar: fuera de rango';
        $plazas = 1000000;
        $test = $this->Lugar_model->validarplazas($plazas);
        $errores = array();
        $errores['PLAZAS_INVALIDO'] = "Plazas debe estar entre 1 y 99999";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);

        // ---------------------------------- Metodopago model
        echo "<hr>Tests de Metodopago:";
        $test_name = 'Validar Registro Metodopago: Ok';
        $metodopago = [
            'nombre' => "Metodo de pago",
            'descripcion' => "descripcion"
        ];
        $test = $this->Metodopago_model->validarRegistro($metodopago);
        $errores = array();
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar Registro Metodopago: Descripcion null';
        $metodopago = [
            'nombre' => "Metodo de pago",
            'descripcion' => null
        ];
        $test = $this->Metodopago_model->validarRegistro($metodopago);
        $errores = array();
        $errores['DESCRIPCION_INVALIDA'] = "La descripcion debe ser texto";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar Registro Metodopago: Nombre null';
        $metodopago = [
            'nombre' => null,
            'descripcion' => "descripcion"
        ];
        $test = $this->Metodopago_model->validarRegistro($metodopago);
        $errores = array();
        $errores['NOMBRE_INVALIDO'] =  "El nombre debe ser texto";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar Nombre Metodopago: ok';
        $nombre = "metodo de pago";
        $test = $this->Metodopago_model->validarNombre($nombre);
        $errores = array();
        //$errores['NOMBRE_INVALIDO'] =  "El nombre debe ser texto";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar Nombre Metodopago: Demaciado largo';
        $nombre = "sdkljfñlskdjfsñldkfjsñldfkjsñlkfjsañlfdkjsdñlfksjadñlfkasjñlfkdjsañlfkjsañldfkjsafsadf";
        $test = $this->Metodopago_model->validarNombre($nombre);
        $errores = array();
        $errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 30 caracateres";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar Nombre Metodopago: Null';
        $nombre = null;
        $test = $this->Metodopago_model->validarNombre($nombre);
        $errores = array();
        $errores['NOMBRE_INVALIDO'] =  "El nombre debe ser texto";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);

        // ---------------------------------- Paquete model
        echo "<hr>Tests de Paquete:";
        $test_name = 'Validar Nombre Paquete: Ok';
        $nombre = "Paquete";
        $test = $this->Paquete_model->validarNombre($nombre);
        $errores = array();
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar Nombre Paquete: Null';
        $nombre = null;
        $test = $this->Paquete_model->validarNombre($nombre);
        $errores = array();
        $errores['NOMBRE_INVALIDO'] =  "El nombre debe ser texto";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar Nombre Paquete: Demaciado largo';
        $nombre = "jskfladfklsjafklñjsafjsalñkfjaslkfjaslñkfjas lkñfjaslkñfjasfldkñajsdfñlkasjdflkasjdflkasjflñsakdfjlskñadfjaslñdfkjasdñlfksjafdñlkasjfñlksdajfñlkasdjf";
        $test = $this->Paquete_model->validarNombre($nombre);
        $errores = array();
        $errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 50 caracateres";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);

        // ---------------------------------- Tipoticket model
        echo "<hr>Tests de Tipo ticket:";
        $test_name = 'Validar Nombre Tipo ticket: Ok';
        $nombre = "Tipo ticket";
        $test = $this->Tipoticket_model->validarNombre($nombre);
        $errores = array();
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar Nombre Tipo ticket: Null';
        $nombre = null;
        $test = $this->Tipoticket_model->validarNombre($nombre);
        $errores = array();
        $errores['NOMBRE_INVALIDO'] =  "El nombre debe ser texto";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar Nombre Tipo ticket: Demaciado largo';
        $nombre = "sadkjfhsajdklfhsjakdhfjlkasdhfjklsahfjklsadhflkjsahflkjsahdfkjlsafhjklfhsajkldhsadkjlfhsajkldfsafasdf";
        $test = $this->Tipoticket_model->validarNombre($nombre);
        $errores = array();
        $errores['NOMBRE_INVALIDO'] = "El nombre debe ser menor o igual a 30 caracateres";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar Precio Tipo ticket: ok';
        $precio = 50;
        $test = $this->Tipoticket_model->validarPrecio($precio);
        $errores = array();
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar Precio Tipo ticket: Null';
        $precio = null;
        $test = $this->Tipoticket_model->validarPrecio($precio);
        $errores = array();
        $errores['PRECIO_INVALIDO'] = "El precio debe ser un entero";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar Precio Tipo ticket: Con letras';
        $precio = "asd";
        $test = $this->Tipoticket_model->validarPrecio($precio);
        $errores = array();
        $errores['PRECIO_INVALIDO'] = "El precio debe ser un entero";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar Precio Tipo ticket: Negativo';
        $precio = -50;
        $test = $this->Tipoticket_model->validarPrecio($precio);
        $errores = array();
        $errores['PRECIO_INVALIDO'] = "El precio debe ser de 1 a 99999";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Validar Precio Tipo ticket: Fuera de rango';
        $precio = 45465465456465;
        $test = $this->Tipoticket_model->validarPrecio($precio);
        $errores = array();
        $errores['PRECIO_INVALIDO'] = "El precio debe ser de 1 a 99999";
        $expected_result = $errores;
        echo $this->unit->run($test, $expected_result, $test_name);
        
    }

    public function integrationtest() {
        
        $this->unit->set_test_items(array('test_name', 'test_datatype','result', 'line'));

        echo "<head><title>Integration test Backend</title><head>";
        echo "<hr>Integration test Backend:<br><br>";
        
        //----------------------------------------------------------
     	
     	$this->db->query('DELETE FROM EMPLEADO WHERE mail = "correoprueba@correoprueba.com"');
     	$this->db->query('DELETE FROM CATEGORIA WHERE nombre = "categoriadetesting"');
     	$this->db->query('DELETE FROM COMIDA WHERE nombre = "comidadetesting"');
     	$this->db->query('DELETE FROM EVENTO WHERE nombre = "eventoparatesting"');
     	$this->db->query('DELETE FROM LUGAR WHERE nombre = "lugardetesting"');
     	$this->db->query('DELETE FROM METODOPAGO WHERE nombre = "metododepagotesting"');
     	$this->db->query('DELETE FROM CLIENTE WHERE mail ="correodetestins@correodetesting.com"');
     	
     	// Inicio de sesion
        $mail = 'mail@mail.com';
        $password = 'password';
        
        $data = array(
                'mail'     => $mail,
                'password' => $password
        );
        
        $curl = curl_init('http://localhost/login/loginpost');
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_COOKIEJAR, '/tmp/cookies.txt');
        curl_setopt($curl, CURLOPT_COOKIEFILE, '/tmp/cookies.txt');
        curl_setopt($curl, CURLOPT_COOKIE, 'cookiename=cookievalue');
        $response = curl_exec($curl);
        //echo $response;
        //----------------------------------------------------------
        
        $test_name = 'Alta categoria: OK';

        $nombre     = "categoriadetesting";
        $descripcion = "sadasdasda";
        $imagen = null;
		$data = Array(
					'nombre' => $nombre,
					'descripcion' => $descripcion,
                    'imagen'   => $imagen
					
				);
	    $url = 'http://localhost/Categoria/alta';

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $exito);
		$expected_result = 41;
		echo $this->unit->run($test, $expected_result, $test_name);

		//----------------------------------------------------------

        
        $test_name = 'Baja categoria: Ok';

        $nombre     = "categoriadetesting";
		$data = Array(
					'nombre' => $nombre
					
				);
	    $url = 'http://localhost/Categoria/baja?nombre='.$nombre;

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $exito); // String position
		$expected_result = 41;
		
		echo $this->unit->run($test, $expected_result, $test_name);
		//----------------------------------------------------------

        
        $test_name = 'Baja categoria: Dada de baja';

        $nombre     = "categoriadetesting";
		$data = Array(
					'nombre' => $nombre
					
				);
	    $url = 'http://localhost/Categoria/baja?nombre='.$nombre;

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $error); // String position
		$expected_result = 44;
		
		echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        
        $test_name = 'Alta Comida: OK';

        $nombre     = "comidadetesting";
        $descripcion = "sadasdasda";
        $imagen = null;
		$data = Array(
					'nombre' => $nombre,
					'descripcion' => $descripcion,
					'imagen'   => $imagen
					
				);
	    $url = 'http://localhost/Comida/alta';

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $exito);
		$expected_result = 41;
		echo $this->unit->run($test, $expected_result, $test_name);
		//----------------------------------------------------------
        $test_name = 'Baja comida: Ok';

        $nombre     = "comidadetesting";
		$data = Array(
					'nombre' => $nombre
					
				);
	    $url = 'http://localhost/Comida/baja?nombre='.$nombre;

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $exito); // String position
		$expected_result = 41;
		
		echo $this->unit->run($test, $expected_result, $test_name);
		//----------------------------------------------------------

        
        $test_name = 'Baja comida: Dada de baja';

        $nombre     = "comidadetesting";
		$data = Array(
					'nombre' => $nombre
					
				);
	    $url = 'http://localhost/Comida/baja?nombre='.$nombre;

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $error); // String position
		$expected_result = 44;
		
		echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------

       	$test_name = 'Alta empleado: Ok';

        $data = Array(
			'username' => "nuevoempleadode prueba",
            'mail'     => "correoprueba@correoprueba.com",
            'tipo'     => 1,
            'password' => "password"
		);

		$url = 'http://localhost/Empleado/registrar';

		curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $exito); // String position
		$expected_result = 41;
		
		echo $this->unit->run($test, $expected_result, $test_name);

		//----------------------------------------------------------

       	$test_name = 'Baja empleado: Ok';

       	$this->db->where ('mail', "correoprueba@correoprueba.com");
       	$resultado = $this->db->get("EMPLEADO");
       	$empleado = $resultado->row_array();

       	$idusuario = $empleado['idusuario'];
        $data = Array();

		$url = 'http://localhost/Empleado/baja?idusuario='.$idusuario;

		curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $exito); // String position
		$expected_result = 41;
		
		echo $this->unit->run($test, $expected_result, $test_name);
		//echo $response;
        //----------------------------------------------------------
        $test_name = 'Baja empleado: Dado de baja';

       	$this->db->where ('mail', "correoprueba@correoprueba.com");
       	$resultado = $this->db->get("EMPLEADO");
       	$empleado = $resultado->row_array();

       	$idusuario = $empleado['idusuario'];
        $data = Array();

		$url = 'http://localhost/Empleado/baja?idusuario='.$idusuario;

		curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $error); // String position
		$expected_result = 44;
		
		echo $this->unit->run($test, $expected_result, $test_name);
		//echo $response;
        //----------------------------------------------------------

        $this->db->where ('nombre', "teatro");
       	$resultado = $this->db->get("CATEGORIA");
       	$categoria = $resultado->row_array();

       	

        $nombre     = "eventoparatesting";
        $descripcion = "sadasdasda";
        $prioridad = 10;
        $idlugar = 1;
        $idcategoria = $categoria['idcategoria'];
        $imagen = null;

        $test_name = 'Alta evento: Ok';

        $data = Array(
			'nombre' => $nombre,
            'descripcion'     => $descripcion,
            'prioridad'     => $prioridad,
            'lugar' => $idlugar,
           	'categoria' => $idcategoria,
           	'imagen' => $imagen
		);

		$url = 'http://localhost/Evento/alta';

		curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $exito); // String position
		$expected_result = 41;
		
		echo $this->unit->run($test, $expected_result, $test_name);
		//echo $response;
        //----------------------------------------------------------

        $test_name = 'Baja Evento: Ok';

       	$this->db->where ('nombre', "eventoparatesting");
       	$resultado = $this->db->get("EVENTO");
       	$evento = $resultado->row_array();

       	$idevento = $evento['idevento'];
        $data = Array();

		$url = 'http://localhost/Evento/baja?idevento='.$idevento;

		curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $exito); // String position
		$expected_result = 41;
		
		echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
         $test_name = 'Baja Evento: Dado de baja';

       	$this->db->where ('nombre', "eventoparatesting");
       	$resultado = $this->db->get("EVENTO");
       	$evento = $resultado->row_array();

       	$idevento = $evento['idevento'];
        $data = Array();

		$url = 'http://localhost/Evento/baja?idevento='.$idevento;

		curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $error); // String position
		$expected_result = 44;
		
		echo $this->unit->run($test, $expected_result, $test_name);
        //----------------------------------------------------------
        $test_name = 'Alta Lugar: Ok';

        $data = Array(
        	'nombre'          => "lugardetesting",
        	'plazas'          => 1000,
        	'imagen'          => null,
        	'direccion'       => "nuevadireccion",
        	'web'             => "paginaweb",
        	'descripcion'     => "descripcion"
		);

		$url = 'http://localhost/Lugar/alta';

		curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $exito); // String position
		$expected_result = 41;
		
		echo $this->unit->run($test, $expected_result, $test_name);
		//echo $response;
        //----------------------------------------------------------
        
        $test_name = 'Baja Lugar: Ok';

       	$nombre = "lugardetesting";
        $data = Array();

		$url = 'http://localhost/Lugar/baja?nombre='.$nombre;

		curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $exito); // String position
		$expected_result = 41;
		
		echo $this->unit->run($test, $expected_result, $test_name);
		//----------------------------------------------------------
		$test_name = 'Baja Lugar: Dado de baja';

       	$nombre = "lugardetesting";
        $data = Array();

		$url = 'http://localhost/Lugar/baja?nombre='.$nombre;

		curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $error); // String position
		$expected_result = 44;
		
		echo $this->unit->run($test, $expected_result, $test_name);
		//----------------------------------------------------------
		$test_name = 'Alta Metodo de pago: OK';

        $nombre     = "metododepagotesting";
        $descripcion = "sadasdasda";
        $imagen = null;
		$data = Array(
					'nombre' => $nombre,
					'descripcion' => $descripcion,
					'imagen'   => $imagen
					
				);
	    $url = 'http://localhost/Metodopago/alta';

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $exito);
		$expected_result = 41;
		echo $this->unit->run($test, $expected_result, $test_name);
		//----------------------------------------------------------
		$test_name = 'Baja Metodo de pago: Ok';

       	$nombre = "metododepagotesting";
        $data = Array();

		$url = 'http://localhost/Metodopago/baja?nombre='.$nombre;

		curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $exito); // String position
		$expected_result = 41;
		
		echo $this->unit->run($test, $expected_result, $test_name);
		//----------------------------------------------------------
		$test_name = 'Baja Metodo de pago: Dado de baja';

       	$nombre = "metododepagotesting";
        $data = Array();

		$url = 'http://localhost/Metodopago/baja?nombre='.$nombre;

		curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $error); // String position
		$expected_result = 44;
		
		echo $this->unit->run($test, $expected_result, $test_name);
		//----------------------------------------------------------
		$cliente = [
			'mail'     => "correodetestins@correodetesting.com",
			'nombre'   => "nombre",
			'apellido' => "apellido",
			'telefono' => "78948548",
			'ciudad'   => "ciudad",
			'calle'    => "calle",
			'barrio'   => "barrio",
			'ci'       => "123321123",
			'hash' => "HASH"
		];
		$this->db->insert("CLIENTE", $cliente);

		$this->db->where ('mail', "correodetestins@correodetesting.com");
       	$resultado = $this->db->get("CLIENTE");
       	$cliente = $resultado->row_array();

       	$idusuario = $cliente['idusuario'];

		$test_name = 'Modificar cliente: OK';

		$data = Array(
					'idusuario' => $idusuario,
					'clave' => "nombre",
					'valor'   => "nuevonombre"
					
				);
	    $url = 'http://localhost/Cliente/modificarCliente';

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $exito);
		$expected_result = 41;
		echo $this->unit->run($test, $expected_result, $test_name);
		//----------------------------------------------------------
		$test_name = 'Baja Cliente: Ok';

		$url = 'http://localhost/Cliente/baja?idusuario='.$idusuario;

		curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $exito); // String position
		$expected_result = 41;
		
		echo $this->unit->run($test, $expected_result, $test_name);
		//----------------------------------------------------------
		$test_name = 'Alta logica Cliente: Ok';

		$data = array('idusuario' => $idusuario );
		$url = 'http://localhost/Cliente/altalogica';

		curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $exito); // String position
		$expected_result = 41;
		
		echo $this->unit->run($test, $expected_result, $test_name);
		//----------------------------------------------------------
		
		$test_name = 'Alta logica empleado: Ok';

       	$mail = "correoprueba@correoprueba.com";
        $data = Array();
		$url = 'http://localhost/Empleado/altalogica?mailempleado='.$mail;

		curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $exito); // String position
		$expected_result = 41;
		
		echo $this->unit->run($test, $expected_result, $test_name);
		//echo $response;
        //----------------------------------------------------------
		$test_name = 'Modificar empleado: Ok';

       	$this->db->where ('mail', "correoprueba@correoprueba.com");
       	$resultado = $this->db->get("EMPLEADO");
       	$empleado = $resultado->row_array();
       	$idusuario = $empleado['idusuario'];
        $data = Array();
		$data = Array(
					'idusuario' => $idusuario,
					'clave' => "tipo",
					'valor'   => 2
					
				);
	    $url = 'http://localhost/Empleado/modificarempleado';

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $exito);
		$expected_result = 41;
		echo $this->unit->run($test, $expected_result, $test_name);
		//----------------------------------------------------------
		$this->db->query('UPDATE EVENTO SET baja = 0 WHERE nombre = "eventoparatesting"');
		$this->db->where ('nombre', "eventoparatesting");
       	$resultado = $this->db->get("EVENTO");
       	$evento = $resultado->row_array();
       	$idevento = $evento['idevento'];
		$test_name = 'Modificar evento: Ok';
		$data = Array(
					'idevento' => $idevento,
					'clave' => "prioridad",
					'valor'   => 40
					
				);
	    $url = 'http://localhost/Evento/modificarevento';
	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_POST, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($curl);
        $error   = "Error"; // 44
        $exito   = "Exito"; // 41
        $test = strpos($response, $exito);
		$expected_result = 41;

		echo $this->unit->run($test, $expected_result, $test_name);
		//----------------------------------------------------------
        curl_close($curl); //cierro la sesion
        //----------------------------------------------------------
    }

        public function httpPost($url, $data)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

}

<?php
class Test2 extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Menu_model');   
    }

    //Antes de poder hacer el controlador de evento es necesario tener el controlador de Categoria y Lugar porque son necesarios para registrar un evento.


    public function index() {

        $idevento = 26;
        $data = [
            'idevento' =>  $idevento,
            'comidas' => $this->Menu_model->buscar($idevento,"")
        ];
        $this->load->view('Test2',$data);
    }

    

        public function buscar() {

        
        $buscar = $this->input->post("buscar");
        $idevento = $this->input->post("idevento");
        $numeropagina = $this->input->post("nropagina");
        $cantidad = $this->input->post("cantidad");        
        $inicio = ($numeropagina -1)*$cantidad;

        $data = array(
            "menus" => $this->Menu_model->buscar($idevento,$buscar,$inicio,$cantidad),
            "totalregistros" => count($this->Menu_model->buscar($idevento,$buscar)),
            "cantidad" =>$cantidad
            
        );
        echo json_encode($data);
    }

}

<?php
class Tipoticket extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Tipoticket_model');
    }

    //Antes de poder hacer el controlador de tipoticket es necesario tener el controlador de evento, para poder registrar los tickets asociados a los eventos.


    public function index() {
        // TODO: que cargue la vista de tipoticket, para poder Añadir/borrar/modificar sin tener que login
    }

    public function alta() {
        // TODO: Alta de tipoticket (requiere asociarle un evento y su hora) [La suma de "Cantidad" de los tipoticket asociados a un evento debe ser menor o igual a la cantidad de plazas del lugar asociado al evento [Debe revisarse que sea el resultado antes de realizar la operacion]]
    }

    public function baja() {
        // TODO: Da de baja un tipoticket (pide el id del tipoticket). [Dar de baja un tipoticket dejaria sin efecto todos sus tickets asociados hasta que se le de de alta logica (lo que significa que no se pueden validar)]
    }

    public function altalogica() {
       // TODO: Da alta logica de un tipoticket que haya sido dado de baja, pide el id [La suma de "Cantidad" de los tipoticket asociados a un evento debe ser menor o igual a la cantidad de plazas del lugar asociado al evento [Debe revisarse que sea el resultado antes de realizar la operacion]]
    }

    public function modificar() {
        // TODO: Modifica los datos de tipoticket
    }

    public function buscarporid() {
        // TODO: busca un tipodeticket por su id
    }

    public function buscaporevento() {
        // TODO: lista tipodeticket por idevento que correspondan
    }

}

<?php
class Saldo extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Saldo_model');
    }

    public function index() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $this->load->view('saldo');
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function buscar() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $buscar = $this->input->post("buscar");
        $fecha = $this->input->post("fecha");
        $numeropagina = $this->input->post("nropagina");
        $cantidad = $this->input->post("cantidad");        
        $procesados = $this->input->post("procesados");        
        $inicio = ($numeropagina -1)*$cantidad;

        $data = array(
            "saldos" => $this->Saldo_model->buscar($buscar,$fecha,$procesados,$inicio,$cantidad),
            "totalregistros" => count($this->Saldo_model->buscar($buscar,$fecha,$procesados)),
            "cantidad" => $cantidad
            
        );
        
        echo json_encode($data);
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function aprobar() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
    	$idrecarga = $this->input->get("idrecarga");
    	$aprobado = $this->Saldo_model->aprobado($idrecarga);

    	if ($aprobado == 1) {
    		if ( $this->Saldo_model->aprobar($idrecarga) ) {
                    $data = [
                        'info'     => 'La recarga: '.$idrecarga.' fue aprobada con exito.',
                        'redirect' => '/saldo'
                    ];
                    $this->load->view('success', $data );
                } 
                else {
                    //Cargo un array con los errores
                    $errores = ['Error' => 'Error al intentar aprobar la recarga: '.$idrecarga.'.'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/saldo',
                        'errores'  => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
    	}else {
    		$errores = ['Error' => 'Al parecer la recarga ya esta procesada'];
				//Cargo el array de datos a la vista de errores
        	$data = array(
				//Esto me sirve para saber a donde tiene que volver la plantilla de error
        		'redirect' => '/saldo',
        		'errores'  => $errores
        	);
				//Envio los datos a la vista de errores
        	$this->load->view('Error', $data );
    	}
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function rechazar() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
    	$idrecarga = $this->input->get("idrecarga");
    	$aprobado = $this->Saldo_model->aprobado($idrecarga);

    	if ($aprobado == 1) {
    		if ( $this->Saldo_model->rechazar($idrecarga) ) {
                    $data = [
                        'info'     => 'La recarga: '.$idrecarga.' fue rechazada con exito.',
                        'redirect' => '/saldo'
                    ];
                    $this->load->view('success', $data );
                } 
                else {
                    //Cargo un array con los errores
                    $errores = ['Error' => 'Error al intentar rechazar la recarga: '.$idrecarga.'.'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/saldo',
                        'errores'  => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
    	}else {
    		$errores = ['Error' => 'Al parecer la recarga ya esta procesada'];
				//Cargo el array de datos a la vista de errores
        	$data = array(
				//Esto me sirve para saber a donde tiene que volver la plantilla de error
        		'redirect' => '/saldo',
        		'errores'  => $errores
        	);
				//Envio los datos a la vista de errores
        	$this->load->view('Error', $data );
    	}
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }
}
<?php
class Chat extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Chat_model');
    }

    public function index() {
        $permitidos = array(5,2);
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
                    $this->load->view('Chatview');
            }else{
               $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                    //Envio los datos a la vista de errores
                $this->load->view('Error', $data ); 
            }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/',
                'errores' => $errores
            );
            //Envio los datos a la vista de errores
             $this->load->view('Error', $data );
        }
    }

    public function SalasDisponibles() {
        $permitidos = array(5,2);
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
                $numeropagina = $this->input->post("nropagina");
                $cantidad = $this->input->post("cantidad");
                $inicio = ($numeropagina -1)*$cantidad;
                $salas = $this->Chat_model->SalasDisponibles($inicio,$cantidad);
                $data = array(
                        "salas" => $salas,
                        "ahora" => date('Y-m-d H:i:s'),
                        "totalregistros" => count($this->Chat_model->SalasDisponibles()),
                        "inicio" => $inicio,
                        "cantidad" => $cantidad
                    );

                echo json_encode($data);
            }else{
               $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                echo json_encode(array("exito" => false,"errores" => $errores)); 
            }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                echo json_encode(array("exito" => false,"errores" => $errores));
        }
    }

    public function SalasActivas() {
        $permitidos = array(5,2);
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
                $cantidad = $this->input->post("cantidad");
                $numeropagina = $this->input->post("nropagina");
                $inicio = ($numeropagina -1)*$cantidad;
                $salas = $this->Chat_model->SalasActivas($inicio,$cantidad);
                foreach ($salas as $index => $sala) {
                    foreach ($sala as $key => $value) {
                        $idsala = $sala['idsala'];
                        $ultimomensaje = $this->Chat_model->ultimoMensaje($idsala);
                        $nombredeusuario = $this->Chat_model->nombreusuario($idsala);
                    }
                	$sala['ultimomensaje'] = $ultimomensaje;
                    $sala['nombreusuario'] = $nombredeusuario;
                    $salas[$index] = $sala;
                }
                $data = array(
                        "salas" => $salas,
                        "ahora" => date('Y-m-d H:i:s'),
                        "totalregistros" => count($this->Chat_model->SalasActivas())
                    );
                echo json_encode($data);

            }else{
               $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                echo json_encode(array("exito" => false,"errores" => $errores)); 
            }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
            echo json_encode(array("exito" => false,"errores" => $errores));
        }
    }

    public function ElegirSala() {
        $permitidos = array(5,2);
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
                $idsala = $this->input->post("idsala");
                if ($this->Chat_model->estaDisponible($idsala)){
                    if ($this->Chat_model->ElegirSala($idsala)){
                        echo json_encode(array("exito" => true));
                    }else{
                        $errores["ERROR"] = "Error al intentar elegir la sala ".$idsala;
                        echo json_encode(array("exito" => false,"errores" => $errores));
                    }
                }else{
                    $errores["ERROR"] = "Al parecer la sala ".$idsala." no esta disponible";
                    echo json_encode(array("exito" => false,"errores" => $errores));
                }
            }else{
               $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                echo json_encode(array("exito" => false,"errores" => $errores));
            }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                echo json_encode(array("exito" => false,"errores" => $errores));
        } 
    }

    public function CerrarSala() {
        $permitidos = array(5,2);
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
            	$idsala = $this->input->post("idsala");
            	if($this->Chat_model->pertenezco($idsala)){
            		$cerrarsala = $this->Chat_model->CerrarSala($idsala);
		    		if($cerrarsala){
			    		echo json_encode(array("exito" => true,"info" => "La sala se cerro con exito."));
			    	}else{
			    		$errores["ERROR"] = "Error al intentar cerrar la Sala";
			            echo json_encode(array("exito" => false,"errores" => $errores));
			    	}
            	}else{
            		$errores = ['Error' => 'Al parecer no perteneces a esta sala.'];
                    echo json_encode(array("exito" => false,"errores" => $errores));
            	}
            }else{
               $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                echo json_encode(array("exito" => false,"errores" => $errores));
            }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
            echo json_encode(array("exito" => false,"errores" => $errores));
        }
    }

    public function ObtenerMensajes() {
        $permitidos = array(5,2);
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
            	$idsala = $this->input->post("idsala");
            	if($this->Chat_model->pertenezco($idsala)){
                    if (!$this->Chat_model->estaCerrada($idsala) && !$this->Chat_model->expiro($idsala)){
                		$mensajes = $this->Chat_model->ObtenerMensajes($idsala);
                		echo json_encode(array("exito" => true,"mensajes" => $mensajes));
                    }else{
                        $errores = ['Error' => 'Al parecer la sala fue cerrada o expiro.'];
                        echo json_encode(array("exito" => false,"errores" => $errores));
                    }
            	}else{
            		$errores = ['Error' => 'Al parecer no perteneces a esta sala.'];
                    echo json_encode(array("exito" => false,"errores" => $errores));
            	}
            }else{
               $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                echo json_encode(array("exito" => false,"errores" => $errores)); 
            }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
            echo json_encode(array("exito" => false,"errores" => $errores));
        }
    }
    ///No se pueden enviar ni obtener mensajes de una sala cerrada/expirada, si se peude consultar el estado
    public function estadodelaSala() {
        $permitidos = array(5,2);
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
            	$idsala = $this->input->post("idsala");
            	if($this->Chat_model->pertenezco($idsala)){
            		$estado = $this->Chat_model->estadodelaSala($idsala);
                    $expiro = $this->Chat_model->expiro($idsala);
            		echo json_encode(array("estado" => $estado,"expiro" => $expiro));
            	}else{
            		$errores = ['Error' => 'Al parecer no perteneces a esta sala.'];
                    echo json_encode(array("exito" => false,"errores" => $errores));
            	}
            }else{
               $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                echo json_encode(array("exito" => false,"errores" => $errores));
            }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
            echo json_encode(array("exito" => false,"errores" => $errores));
        }
    }

    public function EnviarMensaje() {
        $permitidos = array(5,2);
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
            	$idsala = $this->input->post("idsala");
            	if($this->Chat_model->pertenezco($idsala)){
                    if (!$this->Chat_model->estaCerrada($idsala) && !$this->Chat_model->expiro($idsala)){
                        $mensaje = $this->input->post("mensaje");
                        $idusuario = $this->session->userdata('idusuario');
                        $mensaje = array(
                            "idsala" => $idsala,
                            "idautor" => $idusuario,
                            "tipoautor" => "EMPLEADO",
                            "mensaje" => $mensaje
                        );
                        $mensaje = $this->Chat_model->EnviarMensaje($mensaje);
                        if ($mensaje) {
                            echo json_encode(array("exito" => true));
                        }else{
                            $errores["ERROR"] = "Error al intentar enviar el mensaje";
                            echo json_encode(array("exito" => false,"errores" => $errores));
                        }
                    }else{
                        $errores = ['Error' => 'Al parecer la sala fue cerrada o expiro.'];
                        echo json_encode(array("exito" => false,"errores" => $errores));
                    }
            	}else{
            		$errores = ['Error' => 'Al parecer no perteneces a esta sala '.$idsala];
                    echo json_encode(array("exito" => false,"errores" => $errores));
            	}
            }else{
               $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                echo json_encode(array("exito" => false,"errores" => $errores)); 
            }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
            echo json_encode(array("exito" => false,"errores" => $errores));
        }
    }

    public function iralchat() {
        $permitidos = array(5,2);
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
            	$idsala = $this->input->get("idsala");
            	if($this->Chat_model->pertenezco($idsala)){
            		$idempleado = $this->session->userdata('idusuario');
            		$data = [
            				'idsala' => $idsala,
            				'idempleado' => $idempleado
            			];
            		$this->load->view('chatAbierto',$data);
            	}else{
            		$errores = ['Error' => 'Al parecer no perteneces a esta sala.'];
                    $data = array(
                        'redirect' => '/',
                        'errores' => $errores
                    );
                    $this->load->view('Error', $data );
            	}
            }else{
               $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                $data = array(
                    'redirect' => '/',
                    'errores' => $errores
                );
                $this->load->view('Error', $data ); 
            }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
            $data = array(
                'redirect' => '/',
                'errores' => $errores
            );
             $this->load->view('Error', $data );
        }
    }

    public function SalasCerradas() {
        $permitidos = array(5,2);
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
            	$numeropagina = $this->input->post("nropagina");
                $cantidad = $this->input->post("cantidad");
                $inicio = ($numeropagina -1)*$cantidad;
                $salas = $this->Chat_model->SalasCerradas($inicio,$cantidad);
                $data = array(
                        "salas" => $salas,
                        "ahora" => date('Y-m-d H:i:s'),
                        "totalregistros" => count($this->Chat_model->SalasCerradas())
                    );
                echo json_encode($data);
            }else{
               $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                echo json_encode(array("exito" => false,"errores" => $errores)); 
            }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
            echo json_encode(array("exito" => false,"errores" => $errores));
        }
    }

    public function ObtenerMensajesSalaCerrada() {
        $permitidos = array(5,2);
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
                $idsala = $this->input->get("idsala");
                if ($this->Chat_model->pertenezco($idsala)) {
                    if ($this->Chat_model->expiro($idsala) ||  $this->Chat_model->estaCerrada($idsala)){
                        $data = [
                            "idsala" => $idsala,
                            "mensajes" => $this->Chat_model->ObtenerMensajesSalaCerrada($idsala)
                        ];
                        $this->load->view('chatCerrado',$data);
                    }else{
                        $errores = ['Error' => 'Al parecer la sala no expiro y no esta cerrada.'];
                        echo json_encode(array("exito" => false,"errores" => $errores));
                    }  
                }else{
                    $errores = ['Error' => 'Al parecer no perteneces a esta sala.'];
                    echo json_encode(array("exito" => false,"errores" => $errores));
                } 
            }else{
               $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                echo json_encode(array("exito" => false,"errores" => $errores)); 
            }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
            echo json_encode(array("exito" => false,"errores" => $errores));
        }
    }
}
<?php
class Paquete extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Paquete_model');
        $this->load->model('Metodopago_model');
        $this->load->model('Evento_model');
        $this->load->model('Tipoticket_model');
        $this->load->model('Menu_model');
        
    }


    public function agregarMetodoPago() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idevento = $this->input->post("idevento");
        $idpaquete = $this->input->post("idpaquete");
        $metodopago = $this->input->post("metodopago");

        if (!$this->Evento_model->existeIdEvento($idevento) || $this->Evento_model->dadoDeBaja($idevento)) {
                //Cargo un array con los errores
            $errores = ['Error' => 'El evento no existe o esta dado de baja '.$idevento.'.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/evento',
                'errores'  => $errores
            );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        else{
            if (!$this->Paquete_model->existe($idpaquete) || $this->Paquete_model->dadoDeBaja($idpaquete)){
                    //Cargo un array con los errores
                    $errores = ['Error' => 'El paquete no existe o esta dado de baja'.$idpaquete.'.'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/evento/modificar/?idevento='.$idevento,
                        'errores'  => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
            }
            else{
                if (!$this->Metodopago_model->existe($metodopago) || $this->Metodopago_model->dadoDeBajaId($metodopago)){
                    //Cargo un array con los errores
                    $errores = ['Error' => 'El metodo de pago no existe o esta dado de baja'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/evento/modificar/?idevento='.$idevento,
                        'errores'  => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
                else{
                    if ($this->Paquete_model->agregarMetodopago($idpaquete,$metodopago)) {
                        $data = [
                            'info'     => 'El metodo de pago id: '.$metodopago.' fue fue agregado con exito al paquete id: '.$idpaquete.'.',
                            'redirect' => '/evento/paquete/?idpaquete='.$idpaquete
                        ];
                        $this->load->view('success', $data );
                    }
                    else{
                        $errores = ['Error' => 'Error al agregar el metodo de pago al paquete'];
                            //Cargo el array de datos a la vista de errores
                            $data = array(
                                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                                'redirect' => '/evento/paquete/?idpaquete='.$idpaquete,
                                'errores'  => $errores
                            );
                            //Envio los datos a la vista de errores
                        $this->load->view('Error', $data );
                    }
                } 
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }

    }

    public function listarmetodosdepago() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $buscar = $this->input->post("buscar");
        $idpaquete = $this->input->post("idpaquete");
        $numeropagina = $this->input->post("nropagina");
        $cantidad = $this->input->post("cantidad");        
        $inicio = ($numeropagina -1)*$cantidad;

        $data = array(
            "metodospago" => $this->Paquete_model->buscarMetodopago($idpaquete,$buscar,$inicio,$cantidad),
            "totalregistros" => count($this->Paquete_model->buscarMetodopago($idpaquete,$buscar)),
            "cantidad" =>$cantidad
                
        );
        echo json_encode($data);
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function borrarmetodopago() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idpaquete = $this->input->get("idpaquete");
        $idmetodopago = $this->input->get("idmetodopago");

        if (!$this->Metodopago_model->existe($idmetodopago) || $this->Metodopago_model->dadoDeBajaId($idmetodopago)){
                    //Cargo un array con los errores
            $errores = ['Error' => 'El metodo de pago no existe o esta dado de baja'];
                    //Cargo el array de datos a la vista de errores
            $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/evento',
                'errores'  => $errores
            );
                    //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        else{
            if (!$this->Paquete_model->existe($idpaquete) || $this->Paquete_model->dadoDeBaja($idpaquete)){
                    //Cargo un array con los errores
                $errores = ['Error' => 'El paquete no existe o esta dado de baja'.$idpaquete.'.'];
                    //Cargo el array de datos a la vista de errores
                $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/evento',
                    'errores'  => $errores
                );
                    //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
            }
            else{
                if ($this->Paquete_model->borrarMetodopago($idpaquete,$idmetodopago)) {
                    $data = [
                        'info'     => 'El metodo de pago id: '.$idmetodopago.' fue fue eliminado con exito del paquete id: '.$idpaquete.'.',
                        'redirect' => '/evento/paquete/?idpaquete='.$idpaquete
                    ];
                    $this->load->view('success', $data );
                }
                else{
                    $errores = ['Error' => 'Error al eliminar el metodo de pago del paquete'];
                            //Cargo el array de datos a la vista de errores
                    $data = array(
                                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/evento/paquete/?idpaquete='.$idpaquete,
                        'errores'  => $errores
                    );
                            //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function agregarTipoTicket() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idpaquete = $this->input->post("idpaquete");
        $descuento = $this->input->post("descuento");
        $cantidad = $this->input->post("cantidad");
        $tipoticket = $this->input->post("tipoticket");
        $idevento = $this->input->post("idevento");

        $errores = array();
        $errores = $this->Paquete_model->validarCantidad($cantidad);
        $errores += $this->Paquete_model->validarDescuento($descuento);

        if( count($errores) > 0) {
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/evento/paquete/?idpaquete='.$idpaquete,
                'errores' => $errores
            );
            $this->load->view('Error', $data );
        }
        else{
            if (!$this->Evento_model->existeIdEvento($idevento) || $this->Evento_model->dadoDeBaja($idevento)) {
                    //Cargo un array con los errores
                $errores = ['Error' => 'El evento no existe o esta dado de baja'.$idevento.'.'];
                    //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/evento',
                    'errores'  => $errores
                );
                    //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
            }
            else{
                if (!$this->Paquete_model->existe($idpaquete) || $this->Paquete_model->dadoDeBaja($idpaquete)){
                        //Cargo un array con los errores
                        $errores = ['Error' => 'El paquete no existe o esta dado de baja'.$idpaquete.'.'];
                        //Cargo el array de datos a la vista de errores
                        $data = array(
                            //Esto me sirve para saber a donde tiene que volver la plantilla de error
                            'redirect' => '/evento/modificar/?idevento='.$idevento,
                            'errores'  => $errores
                        );
                        //Envio los datos a la vista de errores
                        $this->load->view('Error', $data );
                }
                else{
                    if (!$this->Tipoticket_model->existe($tipoticket) || $this->Tipoticket_model->dadoDeBaja($tipoticket)){
                        //Cargo un array con los errores
                        $errores = ['Error' => 'El tipo de ticket no existe o esta dado de baja'];
                        //Cargo el array de datos a la vista de errores
                        $data = array(
                            //Esto me sirve para saber a donde tiene que volver la plantilla de error
                            'redirect' => '/evento/paquete/?idpaquete='.$idpaquete,
                            'errores'  => $errores
                        );
                        //Envio los datos a la vista de errores
                        $this->load->view('Error', $data );
                    }
                    else{
                        if ($this->Paquete_model->fecha($idpaquete) != $this->Tipoticket_model->fecha($tipoticket)) {
                            $errores = ['Error' => 'La fecha del tipo de ticket no corresponde con la fecha del paquete.'];
                                    //Cargo el array de datos a la vista de errores
                                    $data = array(
                                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                                        'redirect' => '/evento/paquete/?idpaquete='.$idpaquete,
                                        'errores'  => $errores
                                    );
                                    //Envio los datos a la vista de errores
                                $this->load->view('Error', $data );
                        }else{
                            if ($this->Paquete_model->agregarTipoTicket($idpaquete,$tipoticket,$cantidad,$descuento)) {
                                $data = [
                                    'info'     => 'El tipo de ticket id: '.$tipoticket.' fue fue agregado con exito al paquete id: '.$idpaquete.'.',
                                    'redirect' => '/evento/paquete/?idpaquete='.$idpaquete
                                ];
                                $this->load->view('success', $data );
                            }
                            else{
                                $errores = ['Error' => 'Error al agregar el tipo de ticket al paquete'];
                                    //Cargo el array de datos a la vista de errores
                                    $data = array(
                                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                                        'redirect' => '/evento/paquete/?idpaquete='.$idpaquete,
                                        'errores'  => $errores
                                    );
                                    //Envio los datos a la vista de errores
                                $this->load->view('Error', $data );
                            }
                        }
                    } 
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function listarTipoTickets() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $buscar = $this->input->post("buscar");
        $idpaquete = $this->input->post("idpaquete");
        $numeropagina = $this->input->post("nropagina");
        $cantidad = $this->input->post("cantidad");        
        $inicio = ($numeropagina -1)*$cantidad;

        $data = array(
            "tipotickets" => $this->Paquete_model->tipoticketsAsignados($idpaquete,$buscar,$inicio,$cantidad),
            "totalregistros" => count($this->Paquete_model->tipoticketsAsignados($idpaquete,$buscar)),
            "cantidad" =>$cantidad
                
        );
        echo json_encode($data);
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function modificarTipoticket() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idpaquete = $this->input->post("idpaquete");
        $idevento = $this->input->post("idevento");
        $idtipoticket = $this->input->post("idtipoticket");
        $clave = $this->input->post("clave");
        $valor = $this->input->post("valor");

        $errores = array();

        if ($clave == 'cantidad') {
            $errores = $this->Paquete_model->validarCantidad($valor);
        }
        elseif ($clave == 'descuento') {
            $errores = $this->Paquete_model->validarDescuento($valor);
        }
        else{
            $errores = ['CLAVE_INVALIDO' => 'El atributo que se desea modificar es invalido'];
        }

        if( count($errores) > 0) {
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/evento/paquete/?idpaquete='.$idpaquete,
                'errores' => $errores
            );
            $this->load->view('Error', $data );
        }
        else{
            if (!$this->Evento_model->existeIdEvento($idevento) || $this->Evento_model->dadoDeBaja($idevento)) {
                    //Cargo un array con los errores
                $errores = ['Error' => 'El evento no existe o esta dado de baja'.$idevento.'.'];
                    //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/evento',
                    'errores'  => $errores
                );
                    //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
            }
            else{
                if (!$this->Paquete_model->existe($idpaquete) || $this->Paquete_model->dadoDeBaja($idpaquete)){
                        //Cargo un array con los errores
                        $errores = ['Error' => 'El paquete no existe o esta dado de baja'.$idpaquete.'.'];
                        //Cargo el array de datos a la vista de errores
                        $data = array(
                            //Esto me sirve para saber a donde tiene que volver la plantilla de error
                            'redirect' => '/evento/modificar/?idevento='.$idevento,
                            'errores'  => $errores
                        );
                        //Envio los datos a la vista de errores
                        $this->load->view('Error', $data );
                }
                else{
                    if (!$this->Tipoticket_model->existe($idtipoticket) || $this->Tipoticket_model->dadoDeBaja($idtipoticket)){
                        //Cargo un array con los errores
                        $errores = ['Error' => 'El tipo de ticket no existe o esta dado de baja'];
                        //Cargo el array de datos a la vista de errores
                        $data = array(
                            //Esto me sirve para saber a donde tiene que volver la plantilla de error
                            'redirect' => '/evento/paquete/?idpaquete='.$idpaquete,
                            'errores'  => $errores
                        );
                        //Envio los datos a la vista de errores
                        $this->load->view('Error', $data );
                    }
                    else{
                        if ($clave == 'descuento' && $clave == 'cantidad'){
                            //Cargo un array con los errores
                            $errores = ['Error' => 'El atributo que se desea modificar es invalido: '.$clave.'.'];
                            //Cargo el array de datos a la vista de errores
                            $data = array(
                                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                                'redirect' => '/evento/paquete/?idpaquete='.$idpaquete,
                                'errores'  => $errores
                            );
                            //Envio los datos a la vista de errores
                            $this->load->view('Error', $data );
                        }
                        else{
                            if ($this->Paquete_model->modificartipoticket($idpaquete,$idtipoticket,$clave ,$valor)) {
                                $data = [
                                    'info'     => 'El tipo de ticket id: '.$idtipoticket.' modifico '.$clave.' a : '.$valor.'.',
                                    'redirect' => '/evento/paquete/?idpaquete='.$idpaquete
                                ];
                                $this->load->view('success', $data );
                            }
                            else{
                                $errores = ['Error' => 'Error al modificar el tipo de ticket.'];
                                    //Cargo el array de datos a la vista de errores
                                    $data = array(
                                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                                        'redirect' => '/evento/paquete/?idpaquete='.$idpaquete,
                                        'errores'  => $errores
                                    );
                                    //Envio los datos a la vista de errores
                                $this->load->view('Error', $data );
                            }
                        }
                    } 
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function borrartipoticket() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idpaquete = $this->input->get("idpaquete");
        $idtipoticket = $this->input->get("idtipoticket");

        if (!$this->Tipoticket_model->existe($idtipoticket) || $this->Tipoticket_model->dadoDeBaja($idtipoticket)){
                    //Cargo un array con los errores
            $errores = ['Error' => 'El tipo de ticket no existe o esta dado de baja'];
                    //Cargo el array de datos a la vista de errores
            $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/evento',
                'errores'  => $errores
            );
                    //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        else{
            if (!$this->Paquete_model->existe($idpaquete) || $this->Paquete_model->dadoDeBaja($idpaquete)){
                    //Cargo un array con los errores
                $errores = ['Error' => 'El paquete no existe o esta dado de baja'.$idpaquete.'.'];
                    //Cargo el array de datos a la vista de errores
                $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/evento',
                    'errores'  => $errores
                );
                    //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
            }
            else{
                if ($this->Paquete_model->borrarTipoTicket($idpaquete,$idtipoticket)) {
                    $data = [
                        'info'     => 'El tipo de ticket id: '.$idtipoticket.' fue fue eliminado con exito del paquete id: '.$idpaquete.'.',
                        'redirect' => '/evento/paquete/?idpaquete='.$idpaquete
                    ];
                    $this->load->view('success', $data );
                }
                else{
                    $errores = ['Error' => 'Error al eliminar el tipo de ticket del paquete'];
                            //Cargo el array de datos a la vista de errores
                    $data = array(
                                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/evento/paquete/?idpaquete='.$idpaquete,
                        'errores'  => $errores
                    );
                            //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function listarComidas() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $buscar = $this->input->post("buscar");
        $idpaquete = $this->input->post("idpaquete");
        $numeropagina = $this->input->post("nropagina");
        $cantidad = $this->input->post("cantidad");        
        $inicio = ($numeropagina -1)*$cantidad;

        $data = array(
            "comidas" => $this->Paquete_model->comidasAsignadas($idpaquete,$buscar,$inicio,$cantidad),
            "totalregistros" => count($this->Paquete_model->comidasAsignadas($idpaquete,$buscar)),
            "cantidad" =>$cantidad
                
        );
        echo json_encode($data);
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function borrarcomida() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idpaquete = $this->input->get("idpaquete");
        $idcomida = $this->input->get("idcomida");

        if (!$this->Paquete_model->existe($idpaquete) || $this->Paquete_model->dadoDeBaja($idpaquete)){
                    //Cargo un array con los errores
            $errores = ['Error' => 'El paquete no existe o esta dado de baja'.$idpaquete.'.'];
                    //Cargo el array de datos a la vista de errores
            $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/evento',
                'errores'  => $errores
            );
                    //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        else{
            if ($this->Paquete_model->borrarComida($idpaquete,$idcomida)) {
                $data = [
                    'info'     => 'La comida id: '.$idcomida.' fue fue eliminada con exito del paquete id: '.$idpaquete.'.',
                    'redirect' => '/evento/paquete/?idpaquete='.$idpaquete
                ];
                $this->load->view('success', $data );
            }
            else{
                $errores = ['Error' => 'Error al eliminar la comida del paquete'];
                            //Cargo el array de datos a la vista de errores
                $data = array(
                                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/evento/paquete/?idpaquete='.$idpaquete,
                    'errores'  => $errores
                );
                            //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function agregarComida() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idpaquete = $this->input->post("idpaquete");
        $descuento = $this->input->post("descuento");
        $cantidad = $this->input->post("cantidad");
        $idcomida = $this->input->post("idcomida");
        $idevento = $this->input->post("idevento");

        $errores = array();
        $errores = $this->Paquete_model->validarCantidad($cantidad);
        $errores += $this->Paquete_model->validarDescuento($descuento);

        if( count($errores) > 0) {
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/evento/paquete/?idpaquete='.$idpaquete,
                'errores' => $errores
            );
            $this->load->view('Error', $data );
        }
        else{
            if (!$this->Evento_model->existeIdEvento($idevento) || $this->Evento_model->dadoDeBaja($idevento)) {
                    //Cargo un array con los errores
                $errores = ['Error' => 'El evento no existe o esta dado de baja'.$idevento.'.'];
                    //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/evento',
                    'errores'  => $errores
                );
                    //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
            }
            else{
                if (!$this->Paquete_model->existe($idpaquete) || $this->Paquete_model->dadoDeBaja($idpaquete)){
                        //Cargo un array con los errores
                        $errores = ['Error' => 'El paquete no existe o esta dado de baja'.$idpaquete.'.'];
                        //Cargo el array de datos a la vista de errores
                        $data = array(
                            //Esto me sirve para saber a donde tiene que volver la plantilla de error
                            'redirect' => '/evento/modificar/?idevento='.$idevento,
                            'errores'  => $errores
                        );
                        //Envio los datos a la vista de errores
                        $this->load->view('Error', $data );
                }
                else{
                    if (!$this->Menu_model->comidaxidevento($idevento, $idcomida)){
                        //Cargo un array con los errores
                        $errores = ['Error' => 'La comida no esta registrada en el evento o esta dada de baja'];
                        //Cargo el array de datos a la vista de errores
                        $data = array(
                            //Esto me sirve para saber a donde tiene que volver la plantilla de error
                            'redirect' => '/evento/paquete/?idpaquete='.$idpaquete,
                            'errores'  => $errores
                        );
                        //Envio los datos a la vista de errores
                        $this->load->view('Error', $data );
                    }
                    else{
                        if ($this->Paquete_model->agregarComida($idpaquete,$idcomida,$cantidad,$descuento)) {
                            $data = [
                                'info'     => 'La comida id: '.$idcomida.' fue fue agregada con exito al paquete id: '.$idpaquete.'.',
                                'redirect' => '/evento/paquete/?idpaquete='.$idpaquete
                            ];
                            $this->load->view('success', $data );
                        }
                        else{
                            $errores = ['Error' => 'Error al agregar la comida al paquete'];
                                //Cargo el array de datos a la vista de errores
                                $data = array(
                                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                                    'redirect' => '/evento/paquete/?idpaquete='.$idpaquete,
                                    'errores'  => $errores
                                );
                                //Envio los datos a la vista de errores
                            $this->load->view('Error', $data );
                        }
                    } 
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function modificarComida() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idpaquete = $this->input->post("idpaquete");
        $idevento = $this->input->post("idevento");
        $idcomida = $this->input->post("idcomida");
        $clave = $this->input->post("clave");
        $valor = $this->input->post("valor");

        $errores = array();

        if ($clave == 'cantidad') {
            $errores = $this->Paquete_model->validarCantidad($valor);
        }
        elseif ($clave == 'descuento') {
            $errores = $this->Paquete_model->validarDescuento($valor);
        }
        else{
            $errores = ['CLAVE_INVALIDO' => 'El atributo que se desea modificar es invalido'];
        }

        if( count($errores) > 0) {
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/evento/paquete/?idpaquete='.$idpaquete,
                'errores' => $errores
            );
            $this->load->view('Error', $data );
        }
        else{
            if (!$this->Evento_model->existeIdEvento($idevento) || $this->Evento_model->dadoDeBaja($idevento)) {
                    //Cargo un array con los errores
                $errores = ['Error' => 'El evento no existe o esta dado de baja'.$idevento.'.'];
                    //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/evento',
                    'errores'  => $errores
                );
                    //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
            }
            else{
                if (!$this->Paquete_model->existe($idpaquete) || $this->Paquete_model->dadoDeBaja($idpaquete)){
                        //Cargo un array con los errores
                        $errores = ['Error' => 'El paquete no existe o esta dado de baja'.$idpaquete.'.'];
                        //Cargo el array de datos a la vista de errores
                        $data = array(
                            //Esto me sirve para saber a donde tiene que volver la plantilla de error
                            'redirect' => '/evento/modificar/?idevento='.$idevento,
                            'errores'  => $errores
                        );
                        //Envio los datos a la vista de errores
                        $this->load->view('Error', $data );
                }
                else{
                    if (!$this->Menu_model->comidaxidevento($idevento, $idcomida)){
                        //Cargo un array con los errores
                        $errores = ['Error' => 'La comida no esta registrada en el evento o esta dada de baja'];
                        //Cargo el array de datos a la vista de errores
                        $data = array(
                            //Esto me sirve para saber a donde tiene que volver la plantilla de error
                            'redirect' => '/evento/paquete/?idpaquete='.$idpaquete,
                            'errores'  => $errores
                        );
                        //Envio los datos a la vista de errores
                        $this->load->view('Error', $data );
                    }
                    else{
                        if ($clave == 'descuento' && $clave == 'cantidad'){
                            //Cargo un array con los errores
                            $errores = ['Error' => 'El atributo que se desea modificar es invalido: '.$clave.'.'];
                            //Cargo el array de datos a la vista de errores
                            $data = array(
                                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                                'redirect' => '/evento/paquete/?idpaquete='.$idpaquete,
                                'errores'  => $errores
                            );
                            //Envio los datos a la vista de errores
                            $this->load->view('Error', $data );
                        }
                        else{
                            if ($this->Paquete_model->modificarComida($idpaquete,$idcomida,$clave ,$valor)) {
                                $data = [
                                    'info'     => 'La comida id: '.$idcomida.' modifico '.$clave.' a : '.$valor.'.',
                                    'redirect' => '/evento/paquete/?idpaquete='.$idpaquete
                                ];
                                $this->load->view('success', $data );
                            }
                            else{
                                $errores = ['Error' => 'Error al modificar la comida.'];
                                    //Cargo el array de datos a la vista de errores
                                    $data = array(
                                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                                        'redirect' => '/evento/paquete/?idpaquete='.$idpaquete,
                                        'errores'  => $errores
                                    );
                                    //Envio los datos a la vista de errores
                                $this->load->view('Error', $data );
                            }
                        }
                    } 
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function modificarimagen() {
    	$permitidos = array(5,4); 
    	if ($this->session->userdata('logeado') == TRUE) {
    		if(in_array($this->session->userdata('tipo'), $permitidos)){
    			$idpaquete = $this->input->post("idpaquete");
    			$idevento = $this->input->post('idevento');
    			$clave = "imagen";
    			$errores = array();
    			$imagen = $_FILES['imagen'];
        if($valor = $this->subirimagen($imagen,"paqueteimagen")){ //Intento subir la foto
		}else{ //Si no lo puedo subir agrego el error
			$errores['ERROR_CARGA'] = "Hubo algun error en la carga de la imagen";//$this->upload->display_errors();
		}
		if( count($errores) > 0) {
			$data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
				'redirect' => '/evento/paquete/?idpaquete='.$idpaquete,
				'errores' => $errores
			);
			$this->load->view('Error', $data );
		}
		else{
			if (!$this->Evento_model->existeIdEvento($idevento) || $this->Evento_model->dadoDeBaja($idevento)) {
                    //Cargo un array con los errores
				$errores = ['Error' => 'El evento no existe o esta dado de baja'.$idevento.'.'];
                    //Cargo el array de datos a la vista de errores
				$data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
					'redirect' => '/evento',
					'errores'  => $errores
				);
                    //Envio los datos a la vista de errores
				$this->load->view('Error', $data );
			}
			else{
				if ($this->Paquete_model->modificarimagen($idpaquete,$clave ,$valor)) {
					$data = [
						'info'     => 'El paquete: '.$idpaquete.' cambio la imagen correctamente.',
						'redirect' => '/evento/paquete/?idpaquete='.$idpaquete
					];
					$this->load->view('success', $data );
				}
				else{
					$errores = ['Error' => 'Error al modificar la imagen.'];
                                    //Cargo el array de datos a la vista de errores
					$data = array(
                                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
						'redirect' => '/evento/paquete/?idpaquete='.$idpaquete,
						'errores'  => $errores
					);
                                    //Envio los datos a la vista de errores
					$this->load->view('Error', $data );
				}
			}
		}
	}else{
		$errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
		$data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
			'redirect' => '/',
			'errores' => $errores
		);
                //Envio los datos a la vista de errores
		$this->load->view('Error', $data );
	}
}else{
	$errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
	$data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
		'redirect' => '/',
		'errores' => $errores
	);
                //Envio los datos a la vista de errores
	$this->load->view('Error', $data );
}
}

    public function subirimagen($file,$directorio) {

        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
                $options = array(
                    CURLOPT_URL => 'http://192.168.21.66/imagenes/insertarfoto',
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_POST => TRUE,
                    CURLOPT_POSTFIELDS => array(
                        'directorio' => $directorio,
                        'imagen' => curl_file_create(realpath($file['tmp_name']),$file['type'],$file['name'])
                    )
                );
                $ch = curl_init();
                curl_setopt_array($ch, $options);
                $content = curl_exec($ch);
                $info = curl_getinfo($ch);
                $error = curl_error($ch);
                $error .= " error Nro ".curl_errno($ch);
                curl_close($ch);
                if ($content === false) {
                    $js_code = 'confirm(' . json_encode("Error al intentar subir la imagen: ".$error, JSON_HEX_TAG) . ');';
                    $js_code = '<script>' . $js_code . '</script>';
                    echo $js_code;
                    return false;
                }
                $contenido = json_decode($content);
                $exito = false;
                $nombre = "";
                foreach ($contenido as $clave => $valor) {
                    if($clave == 'exito'){$exito = $valor;}
                    if($clave == 'nombre'){$nombre = $valor;}
                }
                if($exito){
                    return $nombre;
                }else{
                    return NULL;
                }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

}

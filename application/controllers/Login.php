<?php
class Login extends CI_Controller {

    public function index() {
        if ($this->session->userdata('logeado') == TRUE)
            $this->load->view('empleado');
        else
            $this->load->view('login');
    }

    private function iniciarSesion($mail, $password) {
        $this->load->model('Empleado_model');

        $empleado = $this->Empleado_model->buscarPorMail($mail);

        if (!$empleado)
            return false;

		if ( !password_verify($password, $empleado['password']) ) {
            return false;
		}

        $session = array (
            'idusuario' => $empleado['idusuario'],
            'mail'      => $empleado['mail'],
            'tipo'      => $empleado['tipo'],
            'logeado'   => TRUE
        );
        $this->session->set_userdata($session);

        return true;
    }

    public function loginpost() {
        $mail     = $this->input->post('mail');
        $password = $this->input->post('password');

        if ( $this->iniciarSesion($mail, $password) ) {
            header('location: /');
        } else {
            //Cargo un array con los errores
            $errores = ['Error' => 'Parametros de inicio de sesion incorrectos'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/',
                'errores' => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
    }

    public function asd() {
        echo "asasd";
    }

    public function logout() {
        $this->session->sess_destroy();
		header('location: /');
    }

}

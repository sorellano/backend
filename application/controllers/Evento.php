<?php
class Evento extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Evento_model');
        $this->load->model('Lugar_model');
        $this->load->model('Categoria_model');
        $this->load->model('Comida_model');
        $this->load->model('Menu_model');
        $this->load->model('Fechas_model');
        $this->load->model('Tipoticket_model');
        $this->load->model('Paquete_model');
        $this->load->library('pagination');
        $this->load->helper('file');
    }

    public function index() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $data = [
            'categorias' => $this->Categoria_model->listar(),
            'lugares' => $this->Lugar_model->listar()
        ];
        $this->load->view('Evento', $data);
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }}else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function alta() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $evento = [
            'nombre' => $this->input->post('nombre'),
            'prioridad'     => $this->input->post('prioridad'),
            'descripcion'     => $this->input->post('descripcion'),
            'idlugar' => $this->input->post('lugar'),
            'idcategoria' => $this->input->post('categoria')
        ];
        
        $errores = $this->Evento_model->validarRegistro($evento);

        if(isset($_FILES['imagen'])){
            $imagen = $_FILES['imagen'];
            if ($imagen['error']==0){  //checkeo si existe la foto o no, error 4 significa que no hay nada, 0 significa que esta correcto, dentro del array "FILES" que contiene todos los archivos enviados por POST

                if($evento['imagen'] = $this->subirimagen($imagen,'imagenevento')){ //Intento subir la foto

                } else{ //Si no lo puedo subir agrego el error
                    $errores['ERROR_CARGA'] = "Hubo algun error en la carga de la imagen";//$this->upload->display_errors();
                }
            }
        }

        if ( count($errores) > 0) {
            $data = array(
                'redirect' => '/evento',
                'errores' => $errores
            );
            $this->load->view('Error', $data );
        }
        else{
            if ( $this->Evento_model->alta($evento) ) {
                $data = [
                    'info'     => 'Evento registrado correctamente ',
                    'redirect' => '/evento'
                ];
                $this->load->view('success', $data);
            }
            else {
                     //Cargo un array con los errores
                $errores = ['Error' => 'Error al intentar registrar el evento'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/evento',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
                }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function subirimagen($file,$directorio) {

        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
                $options = array(
                    CURLOPT_URL => 'http://192.168.21.66/imagenes/insertarfoto',
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_POST => TRUE,
                    CURLOPT_POSTFIELDS => array(
                        'directorio' => $directorio,
                        'imagen' => curl_file_create(realpath($file['tmp_name']),$file['type'],$file['name'])
                    )
                );
                $ch = curl_init();
                curl_setopt_array($ch, $options);
                $content = curl_exec($ch);
                $info = curl_getinfo($ch);
                $error = curl_error($ch);
                $error .= " error Nro ".curl_errno($ch);
                curl_close($ch);
                if ($content === false) {
                    $js_code = 'confirm(' . json_encode("Error al intentar subir la imagen: ".$error, JSON_HEX_TAG) . ');';
                    $js_code = '<script>' . $js_code . '</script>';
                    echo $js_code;
                    return false;
                }
                $contenido = json_decode($content);
                $exito = false;
                $nombre = "";
                foreach ($contenido as $clave => $valor) {
                    if($clave == 'exito'){$exito = $valor;}
                    if($clave == 'nombre'){$nombre = $valor;}
                }
                if($exito){
                    return $nombre;
                }else{
                    return NULL;
                }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function modificar() { 
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        //Este modificar es para cargar la vista de modificar, el metodo modificarevento es para cambiar los datos, hay que modificar el codigo para el caso en el cual no se encuentra el evento dado.
        $idevento = $this->input->get('idevento');

        if (!$this->Evento_model->existeIdEvento($idevento) || $this->Evento_model->dadoDeBaja($idevento)) {
				//Cargo un array con los errores
        	$errores = ['Error' => 'El evento no existe o esta dado de baja'];
				//Cargo el array de datos a la vista de errores
        	$data = array(
				//Esto me sirve para saber a donde tiene que volver la plantilla de error
        		'redirect' => '/evento',
        		'errores'  => $errores
        	);
				//Envio los datos a la vista de errores
        	$this->load->view('Error', $data );
        }
        else{
        	$evento = $this->Evento_model->buscarPorId($idevento);

        	$lugar = $this->Lugar_model->buscarPorId($evento['idlugar']);
        	$categoria = $this->Categoria_model->buscarPorId($evento['idcategoria']);

        	$data = [
        		'idevento'     => $idevento,
        		'evento'       => $evento,
            	'lugar'        => $lugar['nombre'], //lugar actual del evento
            	'categoria'    => $categoria['nombre'], //categoria actual del evento
            	'categorias'   => $this->Categoria_model->listar(), //todas las categorias
            	'lugares'      => $this->Lugar_model->listar(), //todos los lugares
            	'comidas'      => $this->Menu_model->noAsignadas($idevento), //todas las comidas no asignadas
            	//'asignadas'    => $this->Menu_model->Asignadas($idevento), //todas las comidas asignadas
            	'fechas'       => $this->Fechas_model->buscarPorIdevento($idevento)
        	];
        	$this->load->view('ModificarEvento',$data);
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }        
    }

    public function buscar() { 
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $buscar = $this->input->post("buscar");
        $numeropagina = $this->input->post("nropagina");
        $cantidad = $this->input->post("cantidad");
        $baja = $this->input->post("baja");
        $vigentes = $this->input->post("vigentes");
        
        $inicio = ($numeropagina -1)*$cantidad;
        $data = array(
            "eventos" => $this->Evento_model->buscar($buscar,$baja,$vigentes,$inicio,$cantidad),
            "totalregistros" => count($this->Evento_model->buscar($buscar,$baja,$vigentes)),
            "cantidad" => $cantidad
            
        );
        echo json_encode($data);
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function baja() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idevento = $this->input->get('idevento');

        if (!$this->Evento_model->existeIdEvento($idevento)) {
            //Cargo un array con los errores
            $errores = ['Error' => 'El evento que intenta eliminar no existe'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/evento',
                'errores' => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        else{
            if ($this->Evento_model->dadoDeBaja($idevento)) {
            //Cargo un array con los errores
            $errores = ['Error' => 'El evento que intenta eliminar ya esta dado de baja'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/evento',
                'errores' => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
            }
            else{
                if ( $this->Evento_model->darDeBaja($idevento) ) {
                    $data = [
                        'info'     => 'El evento: '.$idevento.' fue dado de baja con exito.',
                        'redirect' => '/evento'
                    ];
                    $this->load->view('success', $data );
                } 
                else {
                    //Cargo un array con los errores
                    $errores = ['Error' => 'Error al intentar dar de baja el evento'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/evento',
                        'errores'  => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function altalogica() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
       $idevento = $this->input->get('idevento');

       if (!$this->Evento_model->existeIdEvento($idevento)) {
            //Cargo un array con los errores
            $errores = ['Error' => 'El evento que intenta dar de alta no existe'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/evento',
                'errores'  => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        else{
            if (!$this->Evento_model->dadoDeBaja($idevento)) {
                //Cargo un array con los errores
                    $errores = ['Error' => 'El evento ya esta dado de alta'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/evento',
                        'errores'  => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
            }
            else{
                if ( $this->Evento_model->altalogica($idevento) ) {
                    $data = [
                        'info'     => 'El evento: '.$idevento.' fue dado de alta logica con exito.',
                        'redirect' => '/evento'
                    ];
                    $this->load->view('success', $data );
                } 
                else {
                    //Cargo un array con los errores
                    $errores = ['Error' => 'Error al intentar dar de alta logica el evento'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/evento',
                        'errores'  => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }
    
    public function modificarevento() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        //este modificarevento es para cambiar los datos, para cargar la vista se utiliza la funcion "modificar"
        $idevento = $this->input->post('idevento');
        $clave = $this->input->post('clave');
        $valor = $this->input->post('valor');

        if (!$this->Evento_model->existeIdEvento($idevento)) {
            //Cargo un array con los errores
            $errores = ['Error' => 'El evento que intenta modificar no existe'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/evento',
                'errores'  => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        else{
            if ($this->Evento_model->dadoDeBaja($idevento)) {
                //Cargo un array con los errores
                $errores = ['Error' => 'El evento que intenta modificar esta dado de baja'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/evento',
                    'errores'  => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
            }
            else{
                $errores = array();
                if ($clave == 'nombre')
                $errores = $this->Evento_model->validarNombre($valor);
                if ($clave == 'descripcion')
                $errores = $this->Evento_model->validarDescripcion($valor);
                if ($clave == 'prioridad')
                $errores = $this->Evento_model->validarPrioridad($valor);
                if ($clave == 'idcategoria')
                $errores = $this->Evento_model->validarCategoria($valor);
                if ($clave == 'idlugar')
                $errores = $this->Evento_model->validarLugar($valor);

                if( count($errores) > 0) {
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/evento/modificar/?idevento='.$idevento,
                    'errores' => $errores
                );
                $this->load->view('Error', $data );
                }
                else{
                    if ($this->Evento_model->modificarEvento( $idevento,$clave,$valor) ) {
                        $data = [
                            'info'     => 'Se modifico:'.$clave.' del evento:'.$idevento.' por:'.$valor.'.',
                            'redirect' => '/evento/modificar/?idevento='.$idevento
                        ];
                        $this->load->view('success', $data );
                    } else {
                        //Cargo un array con los errores
                        $errores = ['Error' => 'Error al intentar modificar el evento'];
                        //Cargo el array de datos a la vista de errores
                        $data = array(
                            //Esto me sirve para saber a donde tiene que volver la plantilla de error
                            'redirect' => '/evento/modificar/?idevento='.$idevento,
                            'errores'  => $errores
                        );
                        //Envio los datos a la vista de errores
                        $this->load->view('Error', $data );
                    }
                }
                
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function modificarimagen() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idevento = $this->input->post('idevento');
        $clave = 'imagen';

        if (!$this->Evento_model->existeIdEvento($idevento)) {
            //Cargo un array con los errores
            $errores = ['Error' => 'El evento que intenta modificar no existe'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/evento',
                'errores'  => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        else{
            if ($this->Evento_model->dadoDeBaja($idevento)) {
                //Cargo un array con los errores
                $errores = ['Error' => 'El evento esta dado de baja'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/evento',
                    'errores'  => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
            }
            else{

                $imagen = $_FILES['imagen'];
                $errores = array();
                if($valor = $this->subirimagen($imagen,'imagenevento')){ //Intento subir la foto
                } else{ //Si no lo puedo subir agrego el error
                    $errores['ERROR_CARGA'] = "Hubo algun error en la carga de la imagen";//$this->upload->display_errors();
                }
                if( count($errores) > 0) {
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/evento/modificar/?idevento='.$idevento,
                        'errores'  => $errores
                    );
                    $this->load->view('Error', $data );
                }
                else{
                    if ($this->Evento_model->modificarEvento( $idevento,$clave,$valor) ) {
                        $data = [
                            'info'     => 'Se modifico:'.$clave.' del evento:'.$idevento.' por:'.$valor.'.',
                            'redirect' => '/evento/modificar/?idevento='.$idevento
                        ];
                        $this->load->view('success', $data );
                    } else {
                        //Cargo un array con los errores
                        $errores = ['Error' => 'Error al intentar modificar el evento'];
                        //Cargo el array de datos a la vista de errores
                        $data = array(
                            //Esto me sirve para saber a donde tiene que volver la plantilla de error
                            'redirect' => '/evento/modificar/?idevento='.$idevento,
                            'errores'  => $errores
                        );
                        //Envio los datos a la vista de errores
                        $this->load->view('Error', $data );
                    }
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function agregarcomida() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idevento = $this->input->post('idevento');
        $idcomida = $this->input->post('comida');
        $precio = $this->input->post('precio');

        $errores = array();
        $errores = $this->Menu_model->validarPrecio($precio);
        if (count($errores) > 0) {
            $data = [
                'redirect' => '/evento/modificar/?idevento='.$idevento,
                'errores'  => $errores
            ];                
            $this->load->view('Error', $data );
        }
        else{
            $comida = [
                'idevento' => $idevento,
                'idcomida' => $idcomida,
                'precio'   => $precio
            ];

            if (!$this->Evento_model->existeIdEvento($idevento) && !$this->Evento_model->dadoDeBaja($idevento)) {
            //Cargo un array con los errores
            $errores = ['Error' => 'El evento no existe o esta dado de baja'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/evento',
                'errores'  => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
            }
            else{
                if($this->Menu_model->dadoDeBaja($idcomida,$idevento)){

                    $data = [
                            'info'     => 'Esa comida fue previamente dada de alta, desea realizar un alta logica? (Mantiene el precio anterior a la baja)',
                            'redirect' => '/evento/altalogicacomida', //Este es para aceptar
                            'regresar' => '/evento/modificar/?idevento='.$idevento, //Este para volver para atras
                            'id'       => $idevento, //El 'id' en la vista de alta logica es la info que luego se va a utilizar para realizar el alta logica
                            'id2'      => $idcomida
                        ];
                        $this->load->view('altalogica2ids', $data);

                }
                else{
                	if ( $this->Comida_model->existe($idcomida) && !$this->Comida_model->dadaDeBajaId($idcomida)) {
                		if ( $this->Menu_model->agregarComida($comida) ) { 
                        $data = [
                            'info'     => 'Comida agregada correctamente ',
                            'redirect' => '/evento/modificar/?idevento='.$idevento
                        ];
                        $this->load->view('success', $data);
	                    }
	                    else{
	                             //Cargo un array con los errores
	                        $errores = ['Error' => 'Error al intentar agregar la comida al evento'];
	                        //Cargo el array de datos a la vista de errores
	                        $data = array(
	                            //Esto me sirve para saber a donde tiene que volver la plantilla de error
	                            'redirect' => '/evento/modificar/?idevento='.$idevento,
	                            'errores'  => $errores
	                        );
	                        //Envio los datos a la vista de errores
	                        $this->load->view('Error', $data );
	                    }
                	}
                	else{
                		     //Cargo un array con los errores
	                        $errores = ['Error' => 'La comida no existe o esta dada de baja.'];
	                        //Cargo el array de datos a la vista de errores
	                        $data = array(
	                            //Esto me sirve para saber a donde tiene que volver la plantilla de error
	                            'redirect' => '/evento/modificar/?idevento='.$idevento,
	                            'errores'  => $errores
	                        );
	                        //Envio los datos a la vista de errores
	                        $this->load->view('Error', $data );
                	}
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function modificarprecio() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idevento = $this->input->post('idevento');
        $idcomida = $this->input->post('idcomida');
        $precio = $this->input->post('precio');

        $errores = array();

        $errores = $this->Menu_model->validarPrecio($precio);

        if (count($errores) > 0) {
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/evento/modificar/?idevento='.$idevento,
                    'errores'  => $errores
                );
                $this->load->view('Error', $data );
        }
        else{
        	if (!$this->Evento_model->existeIdEvento($idevento) && !$this->Evento_model->dadoDeBaja($idevento)) {
            //Cargo un array con los errores
            $errores = ['Error' => 'El evento no existe o esta dado de baja'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/evento',
                'errores'  => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
            }
            else{
	        	if ( $this->Comida_model->existe($idcomida) && !$this->Comida_model->dadaDeBajaId($idcomida)) {
		            if ($this->Menu_model->cambiarPrecio($idevento,$idcomida,$precio)) {
		                $data = [
		                        'info'     => 'El precio '.$precio.' fue aplicado a la comida '.$idcomida.' del evento '.$idevento.' correctamente.',
		                        'redirect' => '/evento/modificar/?idevento='.$idevento
		                    ];
		                    $this->load->view('success', $data);
		            }
		            else{
		                $errores = ['Error' => 'Error al intentar cambiar el precio de la comida del evento'];
		                    //Cargo el array de datos a la vista de errores
		                    $data = array(
		                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
		                        'redirect' => '/evento/modificar/?idevento='.$idevento,
		                        'errores'  => $errores
		                    );
		                    //Envio los datos a la vista de errores
		                    $this->load->view('Error', $data );
		            }
	        	}else{
	        		$errores = ['Error' => 'La comida no existe o esta dada de baja.'];
		                        //Cargo el array de datos a la vista de errores
		            $data = array(
		                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
		                    'redirect' => '/evento/modificar/?idevento='.$idevento,
		                    'errores'  => $errores
		                    );
		            //Envio los datos a la vista de errores
		            $this->load->view('Error', $data );
	        	}
	        }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function altalogicacomida() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
    	$idevento = $this->input->post('id');
    	$idcomida = $this->input->post('id2');

    	if (!$this->Evento_model->existeIdEvento($idevento) && !$this->Evento_model->dadoDeBaja($idevento)) {
            //Cargo un array con los errores
    		$errores = ['Error' => 'El evento no existe o esta dado de baja'];
            //Cargo el array de datos a la vista de errores
    		$data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
    			'redirect' => '/evento',
    			'errores'  => $errores
    		);
            //Envio los datos a la vista de errores
    		$this->load->view('Error', $data );
    	}
    	else{
    		if ( $this->Comida_model->existe($idcomida) && !$this->Comida_model->dadaDeBajaId($idcomida)) {
    			if($this->Menu_model->altalogica($idevento,$idcomida)){
    				$data = [
    					'info'     => 'La comida: '.$idcomida.' fue dada de alta logica con exito en el menu del evento '.$idevento.'.',
    					'redirect' => '/evento/modificar/?idevento='.$idevento
    				];
    				$this->load->view('success', $data );
    			}
    			else{
    				$errores = ['Error' => 'Error al intentar dar de alta logica la comida en el evento'];
			                    //Cargo el array de datos a la vista de errores
    				$data = array(
			                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
    					'redirect' => '/evento/modificar/?idevento='.$idevento,
    					'errores'  => $errores
    				);
			                    //Envio los datos a la vista de errores
    				$this->load->view('Error', $data );
    			}
    		}else{
    			$errores = ['Error' => 'La comida no existe o esta dada de baja.'];
			        //Cargo el array de datos a la vista de errores
    			$data = array(
			        	//Esto me sirve para saber a donde tiene que volver la plantilla de error
    				'redirect' => '/evento/modificar/?idevento='.$idevento,
    				'errores'  => $errores
    			);
			        //Envio los datos a la vista de errores
    			$this->load->view('Error', $data );
    		}
    	}
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function borrarcomida() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
    	$idevento = $this->input->get('idevento');
    	$idcomida = $this->input->get('idcomida');

    	if (!$this->Evento_model->existeIdEvento($idevento) && !$this->Evento_model->dadoDeBaja($idevento)) {
				//Cargo un array con los errores
    		$errores = ['Error' => 'El evento no existe o esta dado de baja'];
				//Cargo el array de datos a la vista de errores
    		$data = array(
					//Esto me sirve para saber a donde tiene que volver la plantilla de error
    			'redirect' => '/evento',
    			'errores'  => $errores
    		);
				//Envio los datos a la vista de errores
    		$this->load->view('Error', $data );
    	}
    	else{
    		if ( $this->Comida_model->existe($idcomida) && !$this->Comida_model->dadaDeBajaId($idcomida)) {
    			if ($this->Menu_model->dadoDeBaja($idcomida,$idevento)){
    				$errores = ['Error' => 'La comida que intenta dar de baja ya esta dada de baja'];
	                    //Cargo el array de datos a la vista de errores
    				$data = array(
	                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
    					'redirect' => '/evento/modificar/?idevento='.$idevento,
    					'errores'  => $errores
    				);
	                    //Envio los datos a la vista de errores
    				$this->load->view('Error', $data );
    			}
    			else{
    				if ($this->Menu_model->darDeBaja($idevento,$idcomida)) {
    					$data = [
    						'info'     => 'La comida: '.$idcomida.' fue dada de baja con exito en el menu del evento '.$idevento.'.',
    						'redirect' => '/evento/modificar/?idevento='.$idevento
    					];
    					$this->load->view('success', $data );
    				}
    				else{
    					$errores = ['Error' => 'Error al intentar dar de baja la comida del evento'];
                    //Cargo el array de datos a la vista de errores
    					$data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
    						'redirect' => '/evento/modificar/?idevento='.$idevento,
    						'errores'  => $errores
    					);
                    //Envio los datos a la vista de errores
    					$this->load->view('Error', $data );
    				}
    			}
    		}
    		else{
    			$errores = ['Error' => 'La comida no existe o esta dada de baja.'];
			        //Cargo el array de datos a la vista de errores
    			$data = array(
			        	//Esto me sirve para saber a donde tiene que volver la plantilla de error
    				'redirect' => '/evento/modificar/?idevento='.$idevento,
    				'errores'  => $errores
    			);
			        //Envio los datos a la vista de errores
    			$this->load->view('Error', $data );
    		}
    	}
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function listarcomidas() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
                $buscar = $this->input->post("buscar");
                $idevento = $this->input->post("idevento");
                $numeropagina = $this->input->post("nropagina");
                $cantidad = $this->input->post("cantidad");        
                $inicio = ($numeropagina -1)*$cantidad;

                $data = array(
                    "menus" => $this->Menu_model->buscar($idevento,$buscar,$inicio,$cantidad),
                    "totalregistros" => count($this->Menu_model->buscar($idevento,$buscar)),
                    "cantidad" =>$cantidad

                );
                echo json_encode($data);
            }else{
                $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
            }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/',
                'errores' => $errores
            );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }	
    }

    public function agregarfecha(){
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idevento = $this->input->post('idevento');
        $fecha = $this->input->post('date');
        $hora = $this->input->post('time');

        $datetime = $fecha.' '.$hora;

        $funcion = [
        	'idevento'     => $idevento,
        	'fecha'        => $datetime
        ];

        $errores = $this->Fechas_model->validarFecha($fecha);

        if (count($errores) > 0) {
        	$data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
        		'redirect' => '/evento/modificar/?idevento='.$idevento,
        		'errores' => $errores
        	);
        	$this->load->view('Error', $data );
        }
        else{
        	if (!$this->Evento_model->existeIdEvento($idevento) && !$this->Evento_model->dadoDeBaja($idevento)) {
					//Cargo un array con los errores
        		$errores = ['Error' => 'El evento que intenta dar de alta no existe o esta dado de baja'];
					//Cargo el array de datos a la vista de errores
        		$data = array(
					//Esto me sirve para saber a donde tiene que volver la plantilla de error
        			'redirect' => '/evento',
        			'errores'  => $errores
        		);
					//Envio los datos a la vista de errores
        		$this->load->view('Error', $data );
        	}
        	else{
        		if ($this->Fechas_model->existe($idevento,$datetime)) {
        			$errores = ['Error' => 'La fecha que desea ingresar ya existe en la base de datos'];
                    //Cargo el array de datos a la vista de errores
        			$data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
        				'redirect' => '/evento/modificar/?idevento='.$idevento,
        				'errores' => $errores
        			);
                    //Envio los datos a la vista de errores
        			$this->load->view('Error', $data );
        		}
        		else{
        			if ($this->Fechas_model->agregar($funcion)) {
        				$data = [
        					'info'     => 'La fecha '.$datetime.' fue agregada correctamente al evento '.$idevento.'.',
        					'redirect' => '/evento/modificar/?idevento='.$idevento
        				];
        				$this->load->view('success', $data );
        			}
        			else{
        				$errores = ['Error' => 'Error al intentar registrar la fecha en la base de datos'];
                    //Cargo el array de datos a la vista de errores
        				$data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
        					'redirect' => '/evento/modificar/?idevento='.$idevento,
        					'errores' => $errores
        				);
                    //Envio los datos a la vista de errores
        				$this->load->view('Error', $data );
        			}
        		}
        	}
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function borrarfecha(){
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
    	$idevento = $this->input->post('idevento');
    	$funcion = $this->input->post('funcion');

    	if (!$this->Evento_model->existeIdEvento($idevento) && !$this->Evento_model->dadoDeBaja($idevento)) {
    		$errores = ['Error' => 'El evento no existe o esta dado de baja'];
    		$data = array(
    			'redirect' => '/evento',
    			'errores'  => $errores
    		);
    		$this->load->view('Error', $data );
    	}
    	else{
    		if ($this->Fechas_model->borrar($idevento,$funcion)) {
    			$data = [
    				'info'     => 'La fecha '.$funcion.' fue eliminada correctamente del evento '.$idevento.'.',
    				'redirect' => '/evento/modificar/?idevento='.$idevento
    			];
    			$this->load->view('success', $data );
    		}
    		else{
    			$errores = [
    				'Error' => 'Error al intentar dar de baja la fecha'
    			];
    			$data = array(
    				'redirect' => '/evento/modificar/?idevento='.$idevento,
    				'errores' => $errores
    			);
    			$this->load->view('Error', $data );
    		}
    	}
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function listarfechas(){
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
	    $idevento = $this->input->post("idevento");
	    $numeropagina = $this->input->post("nropagina");
	    $cantidad = $this->input->post("cantidad");        
	    $inicio = ($numeropagina -1)*$cantidad;

	    $data = array(
	        "fechas" => $this->Fechas_model->listar($idevento,$inicio,$cantidad),
	        "totalregistros" => count($this->Fechas_model->listar($idevento)),
	        "cantidad" =>$cantidad
	            
	    );
	    echo json_encode($data);
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }       
    }

    public function agregarTipoTicket(){
    	$permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
               $idevento = $this->input->post("idevento");
               $tipoticket = [
                  'idevento' => $idevento,
                  'fecha'    => $this->input->post("fecha"),
                  'precio'   => $this->input->post("precio"),    	
                  'numerado' => $this->input->post("numerado"),
                  'cantidad' => $this->input->post("cantidad"),
                  'nombre'   => $this->input->post("nombre")
              ];
              $errores = $this->Tipoticket_model->validarRegistro($tipoticket);
              $imagen = $_FILES['imagen'];
                if ($imagen['error']==0){
                        //checkeo si existe la foto o no, error 4 significa que no hay nada, 0 significa que esta correcto, dentro del array "FILES" que contiene todos los archivos enviados por POST
                    if($tipoticket['referencia'] = $this->subirimagen($imagen,"referenciatipoticket")){
                        //Intento subir la foto
                    }else{ //Si no lo puedo subir agrego el error
                    	$$errores['ERROR_CARGA'] = "Hubo algun error en la carga de la imagen";//$this->upload->display_errors();
                        }
                }
            if ( count($errores) > 0) {
        	$data = array(
        		'redirect' => '/evento/modificar/?idevento='.$idevento,
        		'errores' => $errores
        	);
        	$this->load->view('Error', $data );
            }
            else{
                if (!$this->Evento_model->existeIdEvento($idevento) || $this->Evento_model->dadoDeBaja($idevento)   ) {
    				//Cargo un array con los errores
            		$errores = ['Error' => 'El evento no existe o esta dado de baja'];
    				//Cargo el array de datos a la vista de errores
            		$data = array(
    				//Esto me sirve para saber a donde tiene que volver la plantilla de error
            			'redirect' => '/evento',
            			'errores'  => $errores
            		);
    				//Envio los datos a la vista de errores
            		$this->load->view('Error', $data );
                }
        	   else{if ($this->Tipoticket_model->agregar($tipoticket)){
            		$data = [
            			'info'     => 'La el tipo de ticket se registro correctamente.',
            			'redirect' => '/evento/modificar/?idevento='.$idevento
            		];
            		$this->load->view('success', $data );
        	   }else{
                	$errores = [
                		'Error' => 'Error al intentar dar de alta el tipo de ticket'
                	];
                	$data = array(
                		'redirect' => '/evento/modificar/?idevento='.$idevento,
                		'errores' => $errores
                	);
                	$this->load->view('Error', $data );
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                        //Cargo el array de datos a la vista de errores
            $data = array(
                            //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/',
                'errores' => $errores
            );
                        //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                        //Cargo el array de datos a la vista de errores
            $data = array(
                            //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/',
                'errores' => $errores
            );
                        //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
    }

    public function listarTipoTickets(){
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
	    $buscar = $this->input->post("buscar");
	    $idevento = $this->input->post("idevento");
	    $numeropagina = $this->input->post("nropagina");
	    $cantidad = $this->input->post("cantidad");        
	    $inicio = ($numeropagina -1)*$cantidad;

	    $data = array(
	        "tipotickets" => $this->Tipoticket_model->listar($idevento,$buscar,$inicio,$cantidad),
	        "totalregistros" => count($this->Tipoticket_model->listar($idevento,$buscar)),
	        "cantidad" =>$cantidad
	            
	    );
	    echo json_encode($data);
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }     
    }

    public function tipoTicket(){
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
    	$idtipoticket = $this->input->get("tipoticket");

    	if (!$this->Tipoticket_model->existe($idtipoticket) || $this->Tipoticket_model->dadoDeBaja($idtipoticket)) {
				//Cargo un array con los errores
    		$errores = ['Error' => 'El tipo de ticket no existe o esta dado de baja'];
				//Cargo el array de datos a la vista de errores
    		$data = array(
				//Esto me sirve para saber a donde tiene que volver la plantilla de error
    			'redirect' => '/evento',
    			'errores'  => $errores
    		);
				//Envio los datos a la vista de errores
    		$this->load->view('Error', $data );
    	}
    	else{
    		$tipoticket = $this->Tipoticket_model->buscarPorId($idtipoticket);
    		$idevento = $tipoticket['idevento'];
    		if (!$this->Evento_model->existeIdEvento($idevento) || $this->Evento_model->dadoDeBaja($idevento)) {
				//Cargo un array con los errores
    			$errores = ['Error' => 'El evento no existe o esta dado de baja'];
				//Cargo el array de datos a la vista de errores
    			$data = array(
				//Esto me sirve para saber a donde tiene que volver la plantilla de error
    				'redirect' => '/evento',
    				'errores'  => $errores
    			);
				//Envio los datos a la vista de errores
    			$this->load->view('Error', $data );
    		}
    		else{
    			$data =['tipoticket' => $tipoticket];
    			$this->load->view('modificartipoticket',$data);
    		}
    		
    	}
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function modificarTipoTicket(){
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
    	$idtipoticket = $this->input->post('idtipoticket');
        $idevento = $this->input->post('idevento');
        $clave = $this->input->post('clave');
        $valor = $this->input->post('valor');

        if (!$this->Evento_model->existeIdEvento($idevento) || $this->Evento_model->dadoDeBaja($idevento)) {
				//Cargo un array con los errores
        	$errores = ['Error' => 'El evento no existe o esta dado de baja'];
				//Cargo el array de datos a la vista de errores
        	$data = array(
				//Esto me sirve para saber a donde tiene que volver la plantilla de error
        		'redirect' => '/evento',
        		'errores'  => $errores
        	);
				//Envio los datos a la vista de errores
        	$this->load->view('Error', $data );
        }
        else{        
	        if (!$this->Tipoticket_model->existe($idtipoticket) || $this->Tipoticket_model->dadoDeBaja($idtipoticket)) {
	            //Cargo un array con los errores
	            $errores = ['Error' => 'El tipo de ticket no existe o esta dado de baja'];
	            //Cargo el array de datos a la vista de errores
	            $data = array(
	                //Esto me sirve para saber a donde tiene que volver la plantilla de error
	                'redirect' => '/evento/modificar/?idevento='.$idevento,
	                'errores'  => $errores
	            );
	            //Envio los datos a la vista de errores
	            $this->load->view('Error', $data );
	        }
	        else{
	            $errores = array();
	            if ($clave == 'nombre')
	            $errores = $this->Tipoticket_model->validarNombre($valor);
	            if ($clave == 'precio')
	            $errores = $this->Tipoticket_model->validarPrecio($valor);

	            if( count($errores) > 0) {
	            $data = array(
	                //Esto me sirve para saber a donde tiene que volver la plantilla de error
	                'redirect' => '/evento/tipoTicket/?tipoticket='.$idtipoticket,
	                'errores' => $errores
	            );
	            $this->load->view('Error', $data );
	            }
	            else{
	                if ($this->Tipoticket_model->modificarTipoTicket($idtipoticket,$clave,$valor) ) {
	                    $data = [
	                        'info'     => 'Se modifico : '.$clave.' del tipo de ticket : '.$idtipoticket.' por : '.$valor.'.',
	                        'redirect' => '/evento/tipoTicket/?tipoticket='.$idtipoticket
	                    ];
	                    $this->load->view('success', $data );
	                } else {
	                    //Cargo un array con los errores
	                    $errores = ['Error' => 'Error al intentar modificar el tipo de ticket'];
	                    //Cargo el array de datos a la vista de errores
	                    $data = array(
	                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
	                        'redirect' => '/evento/tipoTicket/?tipoticket='.$idtipoticket,
	                        'errores'  => $errores
	                    );
	                    //Envio los datos a la vista de errores
	                    $this->load->view('Error', $data );
	                }
	            }        
	        }
    	}
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function modificarReferencia(){
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
    	$idtipoticket = $this->input->post('idtipoticket');
    	$idevento = $this->input->post('idevento');
    	$clave = 'referencia';

    	if (!$this->Evento_model->existeIdEvento($idevento) || $this->Evento_model->dadoDeBaja($idevento)) {
				//Cargo un array con los errores
    		$errores = ['Error' => 'El evento no existe o esta dado de baja'];
				//Cargo el array de datos a la vista de errores
    		$data = array(
				//Esto me sirve para saber a donde tiene que volver la plantilla de error
    			'redirect' => '/evento',
    			'errores'  => $errores
    		);
				//Envio los datos a la vista de errores
    		$this->load->view('Error', $data );
    	}
    	else{        
    		if (!$this->Tipoticket_model->existe($idtipoticket) || $this->Tipoticket_model->dadoDeBaja($idtipoticket)) {
	            //Cargo un array con los errores
    			$errores = ['Error' => 'El tipo de ticket no existe o esta dado de baja'];
	            //Cargo el array de datos a la vista de errores
    			$data = array(
	                //Esto me sirve para saber a donde tiene que volver la plantilla de error
    				'redirect' => '/evento/modificar/?idevento='.$idevento,
    				'errores'  => $errores
    			);
	            //Envio los datos a la vista de errores
    			$this->load->view('Error', $data );
    		}
    		else{
    			$imagen = $_FILES['imagen'];
    			$errores = array();
		            if($valor = $this->subirimagen($imagen,"referenciatipoticket")){ //Intento subir la foto
		            } else{ //Si no lo puedo subir agrego el error
		            	$errores['ERROR_CARGA'] = "Hubo algun error en la carga de la imagen";//$this->upload->display_errors();
		            }
		            if( count($errores) > 0) {
		            	$data = array(
		                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
		            		'redirect' => '/evento/tipoTicket/?tipoticket='.$idtipoticket,
		            		'errores'  => $errores
		            	);
		            	$this->load->view('Error', $data );
		            }
		            else{
		            	if ($this->Tipoticket_model->modificarTipoTicket($idtipoticket,$clave,$valor) ) {
		            		$data = [
		            			'info'     => 'Se modifico : '.$clave.' del tipo de ticket : '.$idtipoticket.' por : '.$valor.'.',
		            			'redirect' => '/evento/tipoTicket/?tipoticket='.$idtipoticket
		            		];
		            		$this->load->view('success', $data );
		            	} else {
		                    //Cargo un array con los errores
		            		$errores = ['Error' => 'Error al intentar modificar el tipo de ticket'];
		                    //Cargo el array de datos a la vista de errores
		            		$data = array(
		                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
		            			'redirect' => '/evento/tipoTicket/?tipoticket='.$idtipoticket,
		            			'errores'  => $errores
		            		);
		                    //Envio los datos a la vista de errores
		            		$this->load->view('Error', $data );
		            	}
		            }
		        }
		    }
            }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
		}

    public function borrartipoticket() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idtipoticket = $this->input->get('idtipoticket');
        $idevento = $this->input->get('idevento');
        
        if (!$this->Evento_model->existeIdEvento($idevento) || $this->Evento_model->dadoDeBaja($idevento)) {
				//Cargo un array con los errores
        	$errores = ['Error' => 'El evento no existe o esta dado de baja'];
				//Cargo el array de datos a la vista de errores
        	$data = array(
				//Esto me sirve para saber a donde tiene que volver la plantilla de error
        		'redirect' => '/evento',
        		'errores'  => $errores
        	);
				//Envio los datos a la vista de errores
        	$this->load->view('Error', $data );
        }
        else{
	        if (!$this->Tipoticket_model->existe($idtipoticket) || $this->Tipoticket_model->dadoDeBaja($idtipoticket)){
	            //Cargo un array con los errores
	            $errores = ['Error' => 'El tipo de ticket que intenta modificar no existe o ya esta dado de baja'];
	            //Cargo el array de datos a la vista de errores
	            $data = array(
	                //Esto me sirve para saber a donde tiene que volver la plantilla de error
	                'redirect' => '/evento/modificar/?idevento='.$idevento,
	                'errores'  => $errores
	            );
	            //Envio los datos a la vista de errores
	            $this->load->view('Error', $data );
	        }
	        else{
	            if ($this->Tipoticket_model->darDeBaja($idtipoticket)) {
	                $data = [
	                    'info'     => 'El tipo de ticket: '.$idtipoticket.' fue dada de baja con exito del evento '.$idevento.'.',
	                    'redirect' => '/evento/modificar/?idevento='.$idevento
	                ];
	                $this->load->view('success', $data );
	            }
	            else{
	                $errores = ['Error' => 'Error al intentar dar de baja el tipo de ticket del evento'];
	                    //Cargo el array de datos a la vista de errores
	                    $data = array(
	                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
	                        'redirect' => '/evento/modificar/?idevento='.$idevento,
	                        'errores'  => $errores
	                    );
	                    //Envio los datos a la vista de errores
	                $this->load->view('Error', $data );
	            }
	        }
   		}
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function altalogicatipoticket() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idtipoticket = $this->input->post('idtipoticket');
        $idevento = $this->input->post('idevento');


        if (!$this->Evento_model->existeIdEvento($idevento) || $this->Evento_model->dadoDeBaja($idevento)) {
				//Cargo un array con los errores
        	$errores = ['Error' => 'El evento no existe o esta dado de baja'];
				//Cargo el array de datos a la vista de errores
        	$data = array(
				//Esto me sirve para saber a donde tiene que volver la plantilla de error
        		'redirect' => '/evento',
        		'errores'  => $errores
        	);
				//Envio los datos a la vista de errores
        	$this->load->view('Error', $data );
        }
        else{
	        if (!$this->Tipoticket_model->existe($idtipoticket) || !$this->Tipoticket_model->dadoDeBaja($idtipoticket)){
	            //Cargo un array con los errores
	            $errores = ['Error' => 'El tipo de ticket que intenta modificar no existe o esta no dado de baja'];
	            //Cargo el array de datos a la vista de errores
	            $data = array(
	                //Esto me sirve para saber a donde tiene que volver la plantilla de error
	                'redirect' => '/evento/modificar/?idevento='.$idevento,
	                'errores'  => $errores
	            );
	            //Envio los datos a la vista de errores
	            $this->load->view('Error', $data );
	        }
	        else{
	            if($this->Tipoticket_model->altalogica($idtipoticket,$idevento)){
	                $data = [
	                        'info'     => 'El tipo de ticket : '.$idtipoticket.' fue dado de alta logica con exito en el evento '.$idevento.'.',
	                        'redirect' => '/evento/modificar/?idevento='.$idevento
	                    ];
	                    $this->load->view('success', $data );
	            }
	            else{
	                $errores = ['Error' => 'Error al intentar dar de alta logica el tipo de ticket del evento'];
	                        //Cargo el array de datos a la vista de errores
	                        $data = array(
	                            //Esto me sirve para saber a donde tiene que volver la plantilla de error
	                            'redirect' => '/evento/modificar/?idevento='.$idevento,
	                            'errores'  => $errores
	                        );
	                        //Envio los datos a la vista de errores
	                        $this->load->view('Error', $data );
	            }
	        }
	    }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function agregarPaquete(){
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idevento = $this->input->post("idevento");
        $paquete = [
            'idevento' => $idevento,
            'fecha'    => $this->input->post("fecha"),
            'nombre'   => $this->input->post("nombre")
        ];
        $errores = $this->Paquete_model->validarNombre($paquete['nombre']);

        $imagen = $_FILES['imagen'];
        if ($imagen['error']==0){  //checkeo si existe la foto o no, error 4 significa que no hay nada, 0 significa que esta correcto, dentro del array "FILES" que contiene todos los archivos enviados por POST

            if($paquete['imagen'] = $this->subirimagen($imagen,'paqueteimagen')){ //Intento subir la foto

            } else{ //Si no lo puedo subir agrego el error
                $errores['ERROR_CARGA'] = "Hubo algun error en la carga de la imagen";//$this->upload->display_errors();
            }
        }

        if ( count($errores) > 0) {
            $data = array(
                'redirect' => '/evento/modificar/?idevento='.$idevento,
                'errores' => $errores
            );
            $this->load->view('Error', $data );
        }
        else{
        	if (!$this->Evento_model->existeIdEvento($idevento) || $this->Evento_model->dadoDeBaja($idevento)) {
				//Cargo un array con los errores
        		$errores = ['Error' => 'El evento no existe o esta dado de baja'];
				//Cargo el array de datos a la vista de errores
        		$data = array(
				//Esto me sirve para saber a donde tiene que volver la plantilla de error
        			'redirect' => '/evento',
        			'errores'  => $errores
        		);
				//Envio los datos a la vista de errores
        		$this->load->view('Error', $data );
        	}
        	else{
	            if ($this->Paquete_model->agregar($paquete)) {
	                $data = [
	                    'info'     => 'El paquete se registro correctamente.',
	                    'redirect' => '/evento/modificar/?idevento='.$idevento
	                ];
	                $this->load->view('success', $data );
	            }
	            else{
	                $errores = [
	                    'Error' => 'Error al intentar dar de alta el paquete'
	                ];
	                $data = array(
	                    'redirect' => '/evento/modificar/?idevento='.$idevento,
	                    'errores' => $errores
	                );
	                $this->load->view('Error', $data );
	            }
	        }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function listarPaquetes(){
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $buscar = $this->input->post("buscar");
        $idevento = $this->input->post("idevento");
        $numeropagina = $this->input->post("nropagina");
        $cantidad = $this->input->post("cantidad");        
        $inicio = ($numeropagina -1)*$cantidad;

        $data = array(
            "paquetes" => $this->Paquete_model->listar($idevento,$buscar,$inicio,$cantidad),
            "totalregistros" => count($this->Paquete_model->listar($idevento,$buscar)),
            "cantidad" =>$cantidad
                
        );
        echo json_encode($data);  
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }     
    }


    public function borrarpaquete() {
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idpaquete = $this->input->get('idpaquete');
        $idevento = $this->input->get('idevento');
        
        if (!$this->Evento_model->existeIdEvento($idevento) || $this->Evento_model->dadoDeBaja($idevento)) {
				//Cargo un array con los errores
        	$errores = ['Error' => 'El evento no existe o esta dado de baja'];
				//Cargo el array de datos a la vista de errores
        	$data = array(
				//Esto me sirve para saber a donde tiene que volver la plantilla de error
        		'redirect' => '/evento',
        		'errores'  => $errores
        	);
				//Envio los datos a la vista de errores
        	$this->load->view('Error', $data );
        }
        else{
	        if (!$this->Paquete_model->existe($idpaquete) || $this->Paquete_model->dadoDeBaja($idpaquete)){
	            //Cargo un array con los errores
	            $errores = ['Error' => 'El paquete que intenta borrar no existe o esta dado de baja'];
	            //Cargo el array de datos a la vista de errores
	            $data = array(
	                //Esto me sirve para saber a donde tiene que volver la plantilla de error
	                'redirect' => '/evento/modificar/?idevento='.$idevento,
	                'errores'  => $errores
	            );
	            //Envio los datos a la vista de errores
	            $this->load->view('Error', $data );
	        }
	        else{
	            if ($this->Paquete_model->darDeBaja($idpaquete)) {
	                $data = [
	                    'info'     => 'El paquete: '.$idpaquete.' fue dado de baja con exito del evento '.$idevento.'.',
	                    'redirect' => '/evento/modificar/?idevento='.$idevento
	                ];
	                $this->load->view('success', $data );
	            }
	            else{
	                $errores = ['Error' => 'Error al intentar dar de baja el paquete'];
	                    //Cargo el array de datos a la vista de errores
	                    $data = array(
	                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
	                        'redirect' => '/evento/modificar/?idevento='.$idevento,
	                        'errores'  => $errores
	                    );
	                    //Envio los datos a la vista de errores
	                $this->load->view('Error', $data );
	            }
	        }
	    }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function paquete(){
        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
    	$idpaquete = $this->input->get("idpaquete");

    	if (!$this->Paquete_model->existe($idpaquete) || $this->Paquete_model->dadoDeBaja($idpaquete)) {
				//Cargo un array con los errores
    		$errores = ['Error' => 'El paquete no existe o esta dado de baja'];
				//Cargo el array de datos a la vista de errores
    		$data = array(
				//Esto me sirve para saber a donde tiene que volver la plantilla de error
    			'redirect' => '/evento',
    			'errores'  => $errores
    		);
				//Envio los datos a la vista de errores
    		$this->load->view('Error', $data );
    	}
    	else{
    		$paquete = $this->Paquete_model->buscarPorId($idpaquete);
    		$idevento = $paquete['idevento'];
    		if (!$this->Evento_model->existeIdEvento($idevento) || $this->Evento_model->dadoDeBaja($idevento)) {
				//Cargo un array con los errores
    			$errores = ['Error' => 'El evento no existe o esta dado de baja'];
				//Cargo el array de datos a la vista de errores
    			$data = array(
				//Esto me sirve para saber a donde tiene que volver la plantilla de error
    				'redirect' => '/evento',
    				'errores'  => $errores
    			);
				//Envio los datos a la vista de errores
    			$this->load->view('Error', $data );
    		}
    		else{
    			$data =[
                    'paquete'     => $paquete,
                    'metodospago'     => $this->Paquete_model->metodospagoNoAsignados($idpaquete),
                    'comidas'     => $this->Paquete_model->comidasNoAsignadas($idpaquete),
                    'tipotickets' => $this->Paquete_model->tipoticketsnoAsignados($idpaquete)
                    ];
    			$this->load->view('paquete',$data);
    		}
    	}
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

}

<?php
class Categoria extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Categoria_model');
    }

    public function index() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $this->load->view('categoria');
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function alta() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $categoria = [
            'nombre' => $this->input->post('nombre'),
            'descripcion'     => $this->input->post('descripcion')
        ];


        if(isset($_FILES['imagen'])){
            $imagen = $_FILES['imagen'];
            if ($imagen['error']==0){  //checkeo si existe la foto o no, error 4 significa que no hay nada, 0 significa que esta correcto, dentro del array "FILES" que contiene todos los archivos enviados por POST
                if($categoria['icono'] = $this->subirimagen($imagen,'categoriaicono')){ //Intento subir la foto

                } else{ //Si no lo puedo subir agrego el error
                    $errores['ERROR_CARGA'] = "Hubo algun error en la carga de la imagen";//$this->upload->display_errors();
                }
            }
        }

        $errores = $this->Categoria_model->validarRegistro($categoria);
        //Si hay errores devuelve los devuelve
        if ( count($errores) > 0) {
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/categoria',
                'errores' => $errores
            );
            $this->load->view('Error', $data );
        }
        else{
            if($this->Categoria_model->existeNombre($categoria['nombre'])){

                //if ($this->Categoria_model->dadaDeBaja($categoria['nombre'])) {
                    /*$data = [
                        'info'     => 'Ya existe una categoria con ese nombre, desea realizar un alta logica?',
                        'redirect' => '/categoria/altalogica', //Este es para aceptar
                        'regresar' => '/categoria', //Este para volver para atras
                        'id'     => $categoria['nombre'] //El 'id' en la vista de alta logica es la info que luego se va a utilizar para realizar el alta logica
                    ];
                    $this->load->view('altalogica', $data);
                }
                else{*/
                    $errores = ['Error' => 'Ya existe una categoria con ese nombre'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/categoria',
                        'errores' => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
               // }
            }
            else{
                if ( $this->Categoria_model->insertar($categoria) ) {
                    $data = [
                        'info'     => 'Categoria ingresada correctamente',
                        'redirect' => '/categoria'
                    ];
                    $this->load->view('success', $data);
                } 
                else {
                    //Cargo un array con los errores
                    $errores = ['Error' => 'Error al intentar ingresar la categoria'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/categoria',
                        'errores' => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function altalogica() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $nombre = $this->input->get('nombre');
        if ( $this->Categoria_model->altalogica($nombre) ) {
            $data = [
                'info'     => 'La categoria: '.$nombre.' fue dada de alta logica con exito.',
                'redirect' => '/categoria'
            ];
            $this->load->view('success', $data );
        } 
        else {
            //Cargo un array con los errores
            $errores = ['Error' => 'Error al intentar dar de alta logica la categoria'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/categoria',
                'errores' => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function baja() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $nombre = $this->input->get('nombre');
        if ($this->Categoria_model->existeNombre($nombre)) {
            if ($this->Categoria_model->dadaDeBaja($nombre)) {
                //Cargo un array con los errores
                $errores = ['Error' => 'La categoria ya esta dada de baja'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/categoria',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
            }
            else{
                if (!$this->Categoria_model->predeterminada($nombre)) {
                    if ( $this->Categoria_model->darDeBaja($nombre) ) {
                        $data = [
                            'info'     => 'La categoria: '.$nombre.' fue dada de baja con exito.',
                            'redirect' => '/categoria'
                        ];
                        $this->load->view('success', $data );
                    } 
                    else {
                        //Cargo un array con los errores
                        $errores = ['Error' => 'Error al intentar dar de baja la categoria'];
                        //Cargo el array de datos a la vista de errores
                        $data = array(
                            //Esto me sirve para saber a donde tiene que volver la plantilla de error
                            'redirect' => '/empleado',
                            'errores' => $errores
                        );
                        //Envio los datos a la vista de errores
                        $this->load->view('Error', $data );
                    }
                }else{
                    //Cargo un array con los errores
                    $errores = ['Error' => 'La categoria que se intenta dar de baja es predeterminada.'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/empleado',
                       'errores' => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }

            }
        }
        else{
            //Cargo un array con los errores
            $errores = ['Error' => 'No existe una categoria con ese nombre, '.$nombre];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/categoria',
                'errores' => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function modificar() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $nombre = $this->input->post('nombre');
        $clave = $this->input->post('clave');
        $valor = $this->input->post('valor');

        if ($clave == 'nombre'){
            $errores = $this->Categoria_model->validarNombre($this->input->post('valor'));
            if ($this->Categoria_model->existeNombre($valor)) {
                $errores['NOMBRE_DUPLICADO'] = "El nombre ya existe dentro de la base de datos";
            }
        }

        if ($clave == 'descripcion')
            $errores = $this->Categoria_model->validarDescripcion($this->input->post('valor'));

        if( count($errores) > 0) {
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/categoria',
                'errores' => $errores
            );
            $this->load->view('Error', $data );
        }
        else{
            if (!$this->Categoria_model->buscarPorNombre($nombre)) {
                //Cargo un array con los errores
                $errores = ['Error' => 'No existe una categoria con ese nombre'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/categoria',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
            }
            else{
                if ($this->Categoria_model->actualizar( $nombre,$clave,$valor) ) {
                    $data = [
                        'info'     => 'Se modifico: '.$clave.' de la categoria: '.$nombre.' por: '.$valor.'.',
                        'redirect' => '/categoria'
                    ];
                    $this->load->view('success', $data );
                } 
                else {
                    //Cargo un array con los errores
                    $errores = ['Error' => 'Error al intentar modificar la categoria'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/categoria',
                        'errores' => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function modificarImagen() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $nombre = $this->input->post('nombre');
        $clave = 'icono';

        if (!$this->Categoria_model->buscarPorNombre($nombre)) {
                //Cargo un array con los errores
                $errores = ['Error' => 'No existe una categoria con ese nombre'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/categoria/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
            }
        else{

            	$imagen = $_FILES['imagen'];
                $errores = array();
                if($valor = $this->subirimagen($imagen,'categoriaicono')){ //Intento subir la foto
                } else{ //Si no lo puedo subir agrego el error
                    $errores['ERROR_CARGA'] = "Hubo algun error en la carga de la imagen";//$this->upload->display_errors();
                }
                if( count($errores) > 0) {
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/categoria/modificarcategoria/?nombre='.$nombre,
                        'errores'  => $errores
                    );
                    $this->load->view('Error', $data );
                }
                else{
	                if ($this->Categoria_model->actualizar( $nombre,$clave,$valor) ) {
	                    $data = [
	                        'info'     => 'Se modifico: '.$clave.' de la categoria: '.$nombre.' por: '.$valor.'.',
	                        'redirect' => '/categoria/modificarcategoria/?nombre='.$nombre
	                    ];
	                    $this->load->view('success', $data );
	                } 
	                else {
	                    //Cargo un array con los errores
	                    $errores = ['Error' => 'Error al intentar modificar la categoria'];
	                    //Cargo el array de datos a la vista de errores
	                    $data = array(
	                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
	                        'redirect' => '/categoria/modificarcategoria/?nombre='.$nombre,
	                        'errores' => $errores
	                    );
	                    //Envio los datos a la vista de errores
	                    $this->load->view('Error', $data );
	                }
                }
       	}
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function buscar() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $buscar = $this->input->post("buscar");
        $numeropagina = $this->input->post("nropagina");
        $cantidad = $this->input->post("cantidad");
        $baja = $this->input->post("baja");
        
        $inicio = ($numeropagina -1)*$cantidad;
        $data = array(
            "categorias" => $this->Categoria_model->buscar($buscar,$baja,$inicio,$cantidad),
            "totalregistros" => count($this->Categoria_model->buscar($buscar,$baja)),
            "cantidad" =>$cantidad
            
        );
        echo json_encode($data);
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function modificarCategoria() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
                $nombre = $this->input->get("nombre");
                if ($this->Categoria_model->existeNombre($nombre) && !$this->Categoria_model->dadaDeBaja($nombre)){
                    $data = array(
                        "categoria" => $this->Categoria_model->buscarPorNombre($nombre)            
                    );
                    $this->load->view('modificarcategoria', $data );
                }else{
                    $errores = ['Error' => 'La categoria no existe o esta dada de baja '.$nombre];
                        //Cargo el array de datos a la vista de errores
                    $data = array(
                            //Esto me sirve para saber a donde tiene que volver la plantilla de error
                            'redirect' => '/categoria',
                            'errores' => $errores
                        );
                        //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function subirimagen($file,$directorio) {

        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
                $options = array(
                    CURLOPT_URL => 'http://192.168.21.66/imagenes/insertarfoto',
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_POST => TRUE,
                    CURLOPT_POSTFIELDS => array(
                        'directorio' => $directorio,
                        'imagen' => curl_file_create(realpath($file['tmp_name']),$file['type'],$file['name'])
                    )
                );
                $ch = curl_init();
                curl_setopt_array($ch, $options);
                $content = curl_exec($ch);
                $info = curl_getinfo($ch);
                $error = curl_error($ch);
                $error .= " error Nro ".curl_errno($ch);
                curl_close($ch);
                if ($content === false) {
                    $js_code = 'confirm(' . json_encode("Error al intentar subir la imagen: ".$error, JSON_HEX_TAG) . ');';
                    $js_code = '<script>' . $js_code . '</script>';
                    echo $js_code;
                    return false;
                }
                $contenido = json_decode($content);
                $exito = false;
                $nombre = "";
                foreach ($contenido as $clave => $valor) {
                    if($clave == 'exito'){$exito = $valor;}
                    if($clave == 'nombre'){$nombre = $valor;}
                }
                if($exito){
                    return $nombre;
                }else{
                    return NULL;
                }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }


}

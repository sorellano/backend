<?php

class Informes extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->library('Ciqrcode');
        $this->load->model('Informes_model');
	}

    public function index(){
        $permitidos = array(1,4,5);
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
            $this->load->view('informes');
            }else{
                $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                    //Cargo el array de datos a la vista de errores
                $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/',
                        'errores' => $errores
                    );
                    //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
            }
        }else{
                $errores = ['Error' => 'Debe iniciar sesion para acceder a los informes.'];
                    //Cargo el array de datos a la vista de errores
                $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/',
                        'errores' => $errores
                    );
                    //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
            }
    }

    public function generarinforme(){
        $permitidos = array(1,4,5);
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
                $fechainicio = $this->input->get("fechainicio")." 00:00:01";
                $fechafin = $this->input->get("fechafin")." 23:58:58";
                $cantidad = $this->input->get("cantidad");
                if($fechainicio < $fechafin){
                	$this->informe($fechainicio,$fechafin,$cantidad);
                }else{
                	$errores = ['Error' => 'La fecha de inicio no puede ser mayor a la fecha de fin del informe de ventas.'];
	                    //Cargo el array de datos a la vista de errores
	                $data = array(
	                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
	                        'redirect' => '/informes',
	                        'errores' => $errores
	                    );
	                    //Envio los datos a la vista de errores
	                $this->load->view('Error', $data );
                }
            }else{
                $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                    //Cargo el array de datos a la vista de errores
                $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/informes',
                        'errores' => $errores
                    );
                    //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
            }
        }else{
                $errores = ['Error' => 'Debe iniciar sesion para acceder a los informes.'];
                    //Cargo el array de datos a la vista de errores
                $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/',
                        'errores' => $errores
                    );
                    //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
            }
        
    }

    public function informe($fechainicio,$fechafin,$cantidad){
        $permitidos = array(1,4,5);
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
                $data = [
                    "fechainicio" => $fechainicio,
                    "fechafin"    => $fechafin,
                    "cantidad"    => $cantidad,
                    "eventosmasvendidos"    => $this->Informes_model->eventosmasvendidos($fechainicio,$fechafin,$cantidad),
                    "eventosmenosvendidos"    => $this->Informes_model->eventosmenosvendidos($fechainicio,$fechafin,$cantidad),
                    "promedioventasevento"    => $this->Informes_model->promedioventasevento($fechainicio,$fechafin),
                    "eventosconmasganancia"    => $this->Informes_model->eventosconmasganancia($fechainicio,$fechafin,$cantidad),
                    "eventosconmenosganancia"    => $this->Informes_model->eventosconmenosganancia($fechainicio,$fechafin,$cantidad),
                    "promediogananciasevento"    => $this->Informes_model->promediogananciasevento($fechainicio,$fechafin),
                    "tipoticketmasvendido"    => $this->Informes_model->tipoticketmasvendido($fechainicio,$fechafin,$cantidad),
                    "tipoticketmenosvendido"    => $this->Informes_model->tipoticketmenosvendido($fechainicio,$fechafin,$cantidad),
                    "ticketsconmayorganancia"    => $this->Informes_model->ticketsconmayorganancia($fechainicio,$fechafin,$cantidad),
                    "ticketsconmenorganancia"    => $this->Informes_model->ticketsconmenorganancia($fechainicio,$fechafin,$cantidad),
                    "promediogananciatipoticket"    => $this->Informes_model->promediogananciatipoticket($fechainicio,$fechafin),
                    "lugaresmasconcurridos"    => $this->Informes_model->lugaresmasconcurridos($fechainicio,$fechafin,$cantidad),
                    "lugaresmenosconcurridos"    => $this->Informes_model->lugaresmenosconcurridos($fechainicio,$fechafin,$cantidad),
                    "comidasmasvendidas"    => $this->Informes_model->comidasmasvendidas($fechainicio,$fechafin,$cantidad),
                    "comidasmenosvendidas"    => $this->Informes_model->comidasmenosvendidas($fechainicio,$fechafin,$cantidad),
                    "promedioventasdecomidas"    => $this->Informes_model->promedioventasdecomidas($fechainicio,$fechafin),
                    "comidaconmasganancia"    => $this->Informes_model->comidaconmasganancia($fechainicio,$fechafin,$cantidad),
                    "comidaconmenosganancia"    => $this->Informes_model->comidaconmenosganancia($fechainicio,$fechafin,$cantidad),
                    "promediodegananciaporcomida"    => $this->Informes_model->promediodegananciaporcomida($fechainicio,$fechafin),
                    "paquetemasvendido"    => $this->Informes_model->paquetemasvendido($fechainicio,$fechafin,$cantidad),
                    "paquetemenosvendido"    => $this->Informes_model->paquetemenosvendido($fechainicio,$fechafin,$cantidad),
                    "promedioventasdepaquetes"    => $this->Informes_model->promedioventasdepaquetes($fechainicio,$fechafin),
                    "metodosdepagomasutilizados"    => $this->Informes_model->metodosdepagomasutilizados($fechainicio,$fechafin,$cantidad),
                    "metodosdepagomenosutilizados"    => $this->Informes_model->metodosdepagomenosutilizados($fechainicio,$fechafin,$cantidad),
                    "promediocargasaldo"    => $this->Informes_model->promediocargasaldo($fechainicio,$fechafin)
                ];
                $this->load->view("informemodelo",$data);
                $html = $this->output->get_output();
            	// Load pdf library
                $this->load->library('pdf');        
                // Load HTML content
                $this->dompdf->loadHtml($html);       
                // (Optional) Setup the paper size and orientation
                $this->dompdf->setPaper('A4', 'landscape');      
                // Render the HTML as PDF
                $this->dompdf->render();
                // Output the generated PDF (1 = download and 0 = preview)
                $this->dompdf->stream("Informe", array("Attachment"=>0));
        }
        }
    }
}



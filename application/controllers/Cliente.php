<?php
class Cliente extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Cliente_model');
    }

    public function index() {
            $permitidos = array(5,2); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $this->load->view('Cliente');
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function buscar() {
        $permitidos = array(5,2); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $buscar = $this->input->post("buscar");
        $numeropagina = $this->input->post("nropagina");
        $cantidad = $this->input->post("cantidad");
        $baja = $this->input->post("baja");
        
        $inicio = ($numeropagina -1)*$cantidad;
        $data = array(
            "clientes" => $this->Cliente_model->buscar($buscar,$baja,$inicio,$cantidad),
            "totalregistros" => count($this->Cliente_model->buscar($buscar,$baja)),
            "cantidad" =>$cantidad
            
        );
        echo json_encode($data);
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function modificar() {
        $permitidos = array(5,2); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idusuario = $this->input->get('idusuario');

        if ($this->Cliente_model->existeIdCliente($idusuario) /*&& !$this->Cliente_model->dadoDeBaja($idusuario)*/){
            $cliente = $this->Cliente_model->buscarPorId($idusuario);

            if($cliente['baja']){$baja = "Si";}
            else{$baja = "No";}

            $data = [
                'idusuario'     => $cliente['idusuario'],
                'mail'       => $cliente['mail'],
                'baja'       => $baja,
                'nombre'       => $cliente['nombre'],
                'apellido'       => $cliente['apellido'],
                'telefono'       => $cliente['telefono'],
                'ciudad'       => $cliente['ciudad'],
                'calle'       => $cliente['calle'],
                'barrio'       => $cliente['barrio'],
                'ci'       => $cliente['ci'],
                'saldo'       => $cliente['saldo'],
            ];
            $this->load->view('ModificarCliente',$data);
        }else{
        //Cargo un array con los errores
            $errores = ['Error' => 'El cliente no existe o esta dado de baja '.$idusuario.'.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/cliente',
                'errores'  => $errores
            );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }
    

    public function altalogica() {  
        $permitidos = array(5,2); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idusuario = $this->input->post('idusuario');

       if (!$this->Cliente_model->existeIdCliente($idusuario)) {
            //Cargo un array con los errores
            $errores = ['Error' => 'El usuario que intenta dar de alta no existe'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/cliente',
                'errores'  => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        else{
            if (!$this->Cliente_model->dadoDeBaja($idusuario)) {
                //Cargo un array con los errores
                    $errores = ['Error' => 'El cliente ya esta dado de alta'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/cliente',
                        'errores'  => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
            }
            else{
                if ( $this->Cliente_model->altalogica($idusuario) ) {
                    $data = [
                        'info'     => 'El cliente: '.$idusuario.' fue dado de alta logica con exito.',
                        'redirect' => '/cliente'
                    ];
                    $this->load->view('success', $data );
                } 
                else {
                    //Cargo un array con los errores
                    $errores = ['Error' => 'Error al intentar dar de alta logica el cliente'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/cliente',
                        'errores'  => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function baja() {
        $permitidos = array(5,2); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idusuario = $this->input->post('idusuario');

        if (!$this->Cliente_model->existeIdCliente($idusuario)) {
            //Cargo un array con los errores
            $errores = ['Error' => 'El cliente que intenta eliminar no existe'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/cliente',
                'errores' => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        else{
            if ($this->Cliente_model->dadoDeBaja($idusuario)) {
            //Cargo un array con los errores
            $errores = ['Error' => 'El cliente que intenta eliminar ya esta dado de baja'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/cliente',
                'errores' => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
            }
            else{
                if ( $this->Cliente_model->darDeBaja($idusuario) ) {
                    $data = [
                        'info'     => 'El cliente: '.$idusuario.' fue dado de baja con exito.',
                        'redirect' => '/cliente'
                    ];
                    $this->load->view('success', $data );
                } 
                else {
                    //Cargo un array con los errores
                    $errores = ['Error' => 'Error al intentar dar de baja el cliente'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/cliente',
                        'errores'  => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function modificarCliente() { 
        $permitidos = array(5,2); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idusuario = $this->input->post('idusuario');
        $clave = $this->input->post('clave');
        $valor = $this->input->post('valor');

        if ($this->Cliente_model->dadoDeBaja($idusuario)) {
            //Cargo un array con los errores
            $errores = ['Error' => 'El cliente que intenta modificar no existe o esta dado de baja'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/cliente',
                'errores'  => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        else{
            $errores = array();
            if ($clave == 'nombre')
            $errores = $this->Cliente_model->validarNombre($valor);
            if ($clave == 'apellido')
            $errores = $this->Cliente_model->validarApellido($valor);
            if ($clave == 'telefono')
            $errores = $this->Cliente_model->validarTelefono($valor);
            if ($clave == 'ciudad')
            $errores = $this->Cliente_model->validarCiudad($valor);
            if ($clave == 'barrio')
            $errores = $this->Cliente_model->validarBarrio($valor);
            if ($clave == 'calle')
            $errores = $this->Cliente_model->validarCalle($valor);

            if( count($errores) > 0) {
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/cliente/modificar/?idusuario='.$idusuario,
                'errores' => $errores
            );
            $this->load->view('Error', $data );
            }
            else{
                if ($this->Cliente_model->modificarCliente( $idusuario,$clave,$valor) ) {
                    $data = [
                        'info'     => 'Se modifico:'.$clave.' del cliente:'.$idusuario.' por:'.$valor.'.',
                        'redirect' => '/cliente/modificar/?idusuario='.$idusuario
                    ];
                    $this->load->view('success', $data );
                } else {
                    //Cargo un array con los errores
                    $errores = ['Error' => 'Error al intentar modificar el cliente'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/cliente/modificar/?idusuario='.$idusuario,
                        'errores'  => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
            }
            
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }

    }

}
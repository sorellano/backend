<?php
class Metodopago extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Metodopago_model');
    }

    public function index() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
         $this->load->view('metodopago');
         }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function alta() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $metodopago = [
            'nombre' => $this->input->post('nombre'),
            'descripcion'     => $this->input->post('descripcion')
        ];
        if(isset($_FILES['imagen'])){
            $imagen = $_FILES['imagen'];
            if ($imagen['error']==0){  //checkeo si existe la foto o no, error 4 significa que no hay nada, 0 significa que esta correcto, dentro del array "FILES" que contiene todos los archivos enviados por POST
                if($metodopago['logo'] = $this->subirimagen($imagen,'logometodopago')){ //Intento subir la foto

                } else{ //Si no lo puedo subir agrego el error
                    $errores['ERROR_CARGA'] = "Hubo algun error en la carga de la imagen";//$this->upload->display_errors();
                }
            }
        }
        $errores = $this->Metodopago_model->validarRegistro($metodopago);
        //Si hay errores devuelve los devuelve
        if ( count($errores) > 0) {
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/metodopago',
                'errores' => $errores
            );
            $this->load->view('Error', $data );
        }
        else{
            if($this->Metodopago_model->existeNombre($metodopago['nombre'])){

                //if ($this->Metodopago_model->dadoDeBaja($metodopago['nombre'])) {
                    /*$data = [
                        'info'     => 'Ya existe un metodopago con ese nombre, desea realizar un alta logica?',
                        'redirect' => '/metodopago/altalogica', //Este es para aceptar
                        'regresar' => '/metodopago', //Este para volver para atras
                        'id'     => $metodopago['nombre'] //El 'id' en la vista de alta logica es la info que luego se va a utilizar para realizar el alta logica
                    ];
                    $this->load->view('altalogica', $data);
                }
                else{*/
                    $errores = ['Error' => 'Ya existe un metodopago con ese nombre'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/metodopago',
                        'errores' => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                //}
            }
            else{
                if ( $this->Metodopago_model->alta($metodopago) ) {
                    $data = [
                        'info'     => 'metodopago ingresado correctamente',
                        'redirect' => '/metodopago'
                    ];
                    $this->load->view('success', $data);
                } 
                else {
                    //Cargo un array con los errores
                    $errores = ['Error' => 'Error al intentar ingresar la metodopago'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/metodopago',
                        'errores' => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function altalogica() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $nombre = $this->input->get('nombre');
        if ( $this->Metodopago_model->altalogica($nombre) ) {
            $data = [
                'info'     => 'El metodo de pago: '.$nombre.' fue dado de alta logica con exito.',
                'redirect' => '/metodopago'
            ];
            $this->load->view('success', $data );
        } 
        else {
            //Cargo un array con los errores
            $errores = ['Error' => 'Error al intentar dar de alta logica el metodo de pago'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/metodopago',
                'errores' => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function baja() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $nombre = $this->input->get('nombre');
        if ($this->Metodopago_model->existeNombre($nombre)) {
            if ($this->Metodopago_model->dadoDeBaja($nombre)) {
                //Cargo un array con los errores
                $errores = ['Error' => 'El metodo de pago ya esta dado de baja'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/metodopago',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
            }
            else{
                if ( $this->Metodopago_model->darDeBaja($nombre) ) {
                    $data = [
                        'info'     => 'El metodo de pago: '.$nombre.' fue dado de baja con exito.',
                        'redirect' => '/metodopago'
                    ];
                    $this->load->view('success', $data );
                } 
                else {
                    //Cargo un array con los errores
                    $errores = ['Error' => 'Error al intentar dar de baja el metodo de pago'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/metodopago',
                        'errores' => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
            }
        }
        else{
            //Cargo un array con los errores
            $errores = ['Error' => 'No existe un metodo de pago con ese nombre'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/metodopago',
                'errores' => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }
    
    public function modificar() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $nombre = $this->input->post('nombre');
        $clave = $this->input->post('clave');
        $valor = $this->input->post('valor');

        if ($clave == 'nombre'){
            $errores = $this->Metodopago_model->validarNombre($this->input->post('valor'));
            if ($this->Metodopago_model->existeNombre($valor)) {
                $errores['NOMBRE_DUPLICADO'] = "El nombre ya existe dentro de la base de datos";
            }
        }
        

        if ($clave == 'descripcion')
            $errores = $this->Metodopago_model->validarDescripcion($this->input->post('valor'));

        if( count($errores) > 0) {
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/metodopago',
                'errores' => $errores
            );
            $this->load->view('Error', $data );
        }
        else{
            if (!$this->Metodopago_model->buscarPorNombre($nombre)) {
                //Cargo un array con los errores
                $errores = ['Error' => 'No existe un metodo de pago con ese nombre'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/metodopago',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
            }
            else{
                if ($this->Metodopago_model->actualizar( $nombre,$clave,$valor) ) {
                    $data = [
                        'info'     => 'Se modifico: '.$clave.' del metodo de pago: '.$nombre.' por: '.$valor.'.',
                        'redirect' => '/metodopago'
                    ];
                    $this->load->view('success', $data );
                } 
                else {
                    //Cargo un array con los errores
                    $errores = ['Error' => 'Error al intentar modificar el metodo de pago'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/metodopago',
                        'errores' => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function modificarlogo() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
                $nombre = $this->input->post('nombre');
                $clave = "logo";
                if (!$this->Metodopago_model->buscarPorNombre($nombre)) {
                //Cargo un array con los errores
                    $errores = ['Error' => 'No existe un metodo de pago con ese nombre'];
                //Cargo el array de datos a la vista de errores
                    $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/metodopago',
                        'errores' => $errores
                    );
                //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
                else{
                    $imagen = $_FILES['imagen'];
                    $errores = array();
                if($valor = $this->subirimagen($imagen,'logometodopago')){ //Intento subir la foto
                } else{ //Si no lo puedo subir agrego el error
                    $errores['ERROR_CARGA'] = "Hubo algun error en la carga de la imagen";//$this->upload->display_errors();
                }
                if( count($errores) > 0) {
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/metodopago/modificarmetodopago/?nombre='.$nombre,
                        'errores'  => $errores
                    );
                    $this->load->view('Error', $data );
                }
                else{
                    if ($this->Metodopago_model->actualizar( $nombre,$clave,$valor) ) {
                        $data = [
                            'info'     => 'Se modifico: '.$clave.' del metodo de pago: '.$nombre.' por: '.$valor.'.',
                            'redirect' => '/metodopago/modificarmetodopago/?nombre='.$nombre
                        ];
                        $this->load->view('success', $data );
                    } 
                    else {
                        //Cargo un array con los errores
                        $errores = ['Error' => 'Error al intentar modificar el metodo de pago'];
                        //Cargo el array de datos a la vista de errores
                        $data = array(
                            //Esto me sirve para saber a donde tiene que volver la plantilla de error
                            'redirect' => '/metodopago/modificarmetodopago/?nombre='.$nombre,
                            'errores' => $errores
                        );
                        //Envio los datos a la vista de errores
                        $this->load->view('Error', $data );
                    }
                }
            }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/',
                'errores' => $errores
            );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
    }else{
        $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
        $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
            'redirect' => '/',
            'errores' => $errores
        );
                //Envio los datos a la vista de errores
        $this->load->view('Error', $data );
    }
}

    public function buscar() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $buscar = $this->input->post("buscar");
        $numeropagina = $this->input->post("nropagina");
        $cantidad = $this->input->post("cantidad");
        $baja = $this->input->post("baja");
        
        $inicio = ($numeropagina -1)*$cantidad;
        $data = array(
            "metodospago" => $this->Metodopago_model->buscar($buscar,$baja,$inicio,$cantidad),
            "totalregistros" => count($this->Metodopago_model->buscar($buscar,$baja)),
            "cantidad" =>$cantidad
            
        );
        echo json_encode($data);
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function modificarMetodopago() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){

        $nombre = $this->input->get("nombre");

        if ($this->Metodopago_model->existeNombre($nombre) && !$this->Metodopago_model->dadoDeBaja($nombre) ){
            
            $data = array(
                 "metodopago" => $this->Metodopago_model->buscarPorNombre($nombre)            
            );
            $this->load->view('modificarmetodopago', $data );
        }else{
        //Cargo un array con los errores
            $errores = ['Error' => 'El metodo de pago no existe o esta dado de baja '.$nombre.'.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/metodopago',
                'errores'  => $errores
            );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }

        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function subirimagen($file,$directorio) {

        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
                $options = array(
                    CURLOPT_URL => 'http://192.168.21.66/imagenes/insertarfoto',
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_POST => TRUE,
                    CURLOPT_POSTFIELDS => array(
                        'directorio' => $directorio,
                        'imagen' => curl_file_create(realpath($file['tmp_name']),$file['type'],$file['name'])
                    )
                );
                $ch = curl_init();
                curl_setopt_array($ch, $options);
                $content = curl_exec($ch);
                $info = curl_getinfo($ch);
                $error = curl_error($ch);
                $error .= " error Nro ".curl_errno($ch);
                curl_close($ch);
                if ($content === false) {
                    $js_code = 'confirm(' . json_encode("Error al intentar subir la imagen: ".$error, JSON_HEX_TAG) . ');';
                    $js_code = '<script>' . $js_code . '</script>';
                    echo $js_code;
                    return false;
                }
                $contenido = json_decode($content);
                $exito = false;
                $nombre = "";
                foreach ($contenido as $clave => $valor) {
                    if($clave == 'exito'){$exito = $valor;}
                    if($clave == 'nombre'){$nombre = $valor;}
                }
                if($exito){
                    return $nombre;
                }else{
                    return NULL;
                }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

}
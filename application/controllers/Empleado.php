<?php
class Empleado extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Empleado_model');
    }

    public function index() {
        if ($this->session->userdata('logeado') == TRUE) {
        $this->load->view('empleado');
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function empleados() {
        $permitidos = array(5); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
            $this->load->view('empleados');
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function registrar() {
        $permitidos = array(5); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){

        $empleado = [
            'username' => $this->input->post('username'),
            'mail'     => $this->input->post('mail'),
            'tipo'     => $this->input->post('tipo'),

            'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT)
        ];


        $errores = $this->Empleado_model->validarRegistro($empleado);
        //Si hay errores devuelve los devuelve
        if ( count($errores) > 0) {
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/empleado',
                'errores' => $errores
            );
            $this->load->view('Error', $data );
        }
        else{
            if ($this->Empleado_model->buscarPorMail($empleado['mail'])) {

                $auxiliar = $this->Empleado_model->buscarPorMail($empleado['mail']);
                $idusuario = $auxiliar['idusuario'];

                if ($this->Empleado_model->dadoDeBaja($idusuario)) {
                    /*$data = [
                        'info'     => 'Ya existe un empleado con ese email, desea realizar un alta logica?',
                        'redirect' => '/empleado/altalogica', //Este es para aceptar
                        'regresar' => '/empleado/empleados', //Este para volver para atras
                        'id'     => $empleado['mail'] //El 'id' en la vista de alta logica es la info que luego se va a utilizar para realizar el alta logica
                    ];
                    $this->load->view('altalogica', $data);
                }
                else{*/
                    $errores = ['Error' => 'Ya existe un empleado con ese mail'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/empleado/empleados',
                        'errores' => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
            }
            else{
                if ( $this->Empleado_model->registrar($empleado) ) {
                    $data = [
                        'info'     => 'Usuario registrado correctamente',
                        'redirect' => '/empleado/empleados'
                    ];
                    $this->load->view('success', $data);
                } 
                else {
                     //Cargo un array con los errores
                $errores = ['Error' => 'Error al intentar registrar el usuario'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/empleado/empleados',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function modificarempleado() {
        $permitidos = array(5); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idusuario = $this->input->post('idusuario');
        $clave     = $this->input->post('clave'); //Las claves son tipo,username,password

        if (!$this->Empleado_model->existeIdUsuario($idusuario)) {
            //Cargo un array con los errores
            $errores = ['Error' => 'El usuario que intenta modificar no existe'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/empleado',
                'errores' => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        else{
            //Valido dependiendo del tipo de campo el valor ingresado
            if ($clave == 'password')
            $errores = $this->Empleado_model->validarpassword($this->input->post('valor'));

            if ($clave == 'username')
            $errores = $this->Empleado_model->validarusername($this->input->post('valor'));

            if ($clave == 'tipo')
            $errores = $this->Empleado_model->validartipo($this->input->post('valor'));

            if( count($errores) > 0) {
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/empleado/empleados',
                'errores' => $errores
            );
            $this->load->view('Error', $data );
            }
            else{
                if ($clave == 'password') //si es pasword tomo el valor y lo hasheo
                    $valor = password_hash($this->input->post('valor'), PASSWORD_BCRYPT);
                else
                    $valor = $this->input->post('valor'); //sino solamente lo tomo y lo cargo en la variable

                if ($this->Empleado_model->modificarEmpleado( $idusuario,$clave,$valor) ) {
                    $data = [
                        'info'     => 'Se modifico:'.$clave.' del usuario:'.$idusuario.' por:'.$valor.'.',
                        'redirect' => '/empleado/empleados'
                    ];
                    $this->load->view('success', $data );
                } else {
                    //Cargo un array con los errores
                    $errores = ['Error' => 'Error al intentar modificar el usuario'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/empleado/empleados',
                        'errores' => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }


    public function baja() {

        $permitidos = array(5); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $idusuario = $this->input->get('idusuario');

        if (!$this->Empleado_model->existeIdUsuario($idusuario)) {
            //Cargo un array con los errores
            $errores = ['Error' => 'El usuario que intenta eliminar no existe'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/empleado/empleados',
                'errores' => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        else{
            if ($this->Empleado_model->dadoDeBaja($idusuario)) {
            //Cargo un array con los errores
            $errores = ['Error' => 'El usuario que intenta eliminar ya esta dado de baja'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/empleado/empleados',
                'errores' => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
            }
            else{
                if ( $this->Empleado_model->darDeBaja($idusuario) ) {
                    $data = [
                        'info'     => 'El usuario: '.$idusuario.' fue dado de baja con exito.',
                        'redirect' => '/empleado/empleados'
                    ];
                    $this->load->view('success', $data );
                } 
                else {
                    //Cargo un array con los errores
                    $errores = ['Error' => 'Error al intentar dar de baja el empleado'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/empleado/empleados',
                        'errores' => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
        
    }

    public function altalogica(){
        $permitidos = array(5); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $mailempleado = $this->input->get('mailempleado');

        if ( $this->Empleado_model->altalogica($mailempleado) ) {
            $data = [
                'info'     => 'El usuario: '.$mailempleado.' fue dado de alta logica con exito.',
                'redirect' => '/empleado/empleados'
            ];
            $this->load->view('success', $data );
        } 
        else {
            //Cargo un array con los errores
            $errores = ['Error' => 'Error al intentar dar de alta logica el empleado'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/empleado/empleados',
                'errores' => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function buscar() { 
        $permitidos = array(5); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $buscar = $this->input->post("buscar");
        $numeropagina = $this->input->post("nropagina");
        $cantidad = $this->input->post("cantidad");
        $baja = $this->input->post("baja");
        
        $inicio = ($numeropagina -1)*$cantidad;
        $data = array(
            "empleados" => $this->Empleado_model->buscar($buscar,$baja,$inicio,$cantidad),
            "totalregistros" => count($this->Empleado_model->buscar($buscar,$baja)),
            "cantidad" =>$cantidad
            
        );
        echo json_encode($data);
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function modificar() {
        $permitidos = array(5); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $mail = $this->input->get("mail");

        $data = array(
            "empleado" => $this->Empleado_model->buscarPorMail($mail)            
        );
        $this->load->view('modificarempleado', $data );
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }
}

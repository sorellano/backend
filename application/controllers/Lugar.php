<?php
class Lugar extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Lugar_model');
    }

    public function index() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $this->load->view('lugar');
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function alta() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $lugar = [
            'nombre'          => $this->input->post('nombre'),
            'plazas'          => $this->input->post('plazas'),
            'direccion'       => $this->input->post('direccion'),
            'web'             => $this->input->post('web'),
            'descripcion'     => $this->input->post('descripcion'),
        ];

        if(isset($_FILES['plano'])){
            $imagen = $_FILES['plano'];
            if ($imagen['error']==0){  //checkeo si existe la foto o no, error 4 significa que no hay nada, 0 significa que esta correcto, dentro del array "FILES" que contiene todos los archivos enviados por POST
                if($lugar['plano'] = $this->subirimagen($imagen,'planolugar')){ //Intento subir la foto

                } else{ //Si no lo puedo subir agrego el error
                    $errores['ERROR_CARGA'] = "Hubo algun error en la carga de la imagen";//$this->upload->display_errors();
                }
            }
        }
        if(!isset($lugar['plano'])){$lugar['plano']="";} 

        $errores = $this->Lugar_model->validarInformacion($lugar);
        //Si hay errores devuelve los devuelve
        if ( count($errores) > 0) {
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/lugar',
                'errores' => $errores
            );
            $this->load->view('Error', $data );
        }
        else{
            if ($this->Lugar_model->existeNombre($lugar['nombre'])) {

                if ($this->Lugar_model->dadoDeBaja($lugar['nombre'])) {
                    /*$data = [
                        'info'     => 'Ya existe un lugar con ese nombre, desea realizar un alta logica?',
                        'redirect' => '/lugar/altalogica', //Este es para aceptar
                        'regresar' => '/lugar', //Este para volver para atras
                        'id'     => $lugar['nombre'] //El 'id' en la vista de alta logica es la info que luego se va a utilizar para realizar el alta logica
                    ];
                    $this->load->view('altalogica', $data);
                }
                else{*/
                    $errores = ['Error' => 'Ya existe un lugar con ese nombre'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/lugar',
                        'errores' => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data ); 
                }
            }
            else{
                if ( $this->Lugar_model->alta($lugar) ) {
                    $data = [
                        'info'     => 'Lugar registrado correctamente',
                        'redirect' => '/lugar'
                    ];
                    $this->load->view('success', $data);
                } 
                else {
                     //Cargo un array con los errores
                $errores = ['Error' => 'Error al intentar registrar el lugar'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/lugar',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
                } 
            }
            }}else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
        }

        
    

    public function altalogica() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $nombre = $this->input->get('nombre');

        if ( $this->Lugar_model->altalogica($nombre) ) {
            $data = [
                'info'     => 'El lugar: '.$nombre.' fue dado de alta logica con exito.',
                'redirect' => '/lugar'
            ];
            $this->load->view('success', $data );
        } 
        else {
            //Cargo un array con los errores
            $errores = ['Error' => 'Error al intentar dar de alta logica el lugar'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/lugar',
                'errores' => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function baja() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $nombre = $this->input->get('nombre');

        if (!$this->Lugar_model->existeNombre($nombre)) {
            //Cargo un array con los errores
            $errores = ['Error' => 'El lugar que intenta eliminar no existe'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/lugar',
                'errores' => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        else{
            if ($this->Lugar_model->dadoDeBaja($nombre)) {
            //Cargo un array con los errores
            $errores = ['Error' => 'El lugar que intenta eliminar ya esta dado de baja'];
            //Cargo el array de datos a la vista de errores
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/lugar',
                'errores' => $errores
            );
            //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
            }
            else{
                if ( $this->Lugar_model->darDeBaja($nombre) ) {
                    $data = [
                        'info'     => 'El lugar: '.$nombre.' fue dado de baja con exito.',
                        'redirect' => '/lugar'
                    ];
                    $this->load->view('success', $data );
                } 
                else {
                    //Cargo un array con los errores
                    $errores = ['Error' => 'Error al intentar dar de baja el lugar'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/lugar',
                        'errores' => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function modificar() {

        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){

        $nombre = $this->input->post('nombre');
        $clave = $this->input->post('clave');
        $valor = $this->input->post('valor');

        if ($clave == 'nombre'){
            $errores = $this->Lugar_model->validarnombre($this->input->post('valor'));
            if ($this->Lugar_model->existeNombre($valor)) {
                $errores['NOMBRE_DUPLICADO'] = "El nombre ya existe dentro de la base de datos";
            }
        }

        if ($clave == 'plazas')
            $errores = $this->Lugar_model->validarplazas($this->input->post('valor'));

        if ($clave == 'direccion')
            $errores = $this->Lugar_model->validardireccion($this->input->post('valor'));

        if ($clave == 'web')
            $errores = $this->Lugar_model->validarweb($this->input->post('valor'));

        if ($clave == 'descripcion')
            $errores = $this->Lugar_model->validardescripcion($this->input->post('valor'));

        if( count($errores) > 0) {
            $data = array(
                //Esto me sirve para saber a donde tiene que volver la plantilla de error
                'redirect' => '/lugar',
                'errores' => $errores
            );
            $this->load->view('Error', $data );
        }
        else{
            if (!$this->Lugar_model->buscarPorNombre($nombre)) {
                //Cargo un array con los errores
                $errores = ['Error' => 'No existe un lugar con ese nombre'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/lugar',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
            }
            else{
                if ($this->Lugar_model->modificar( $nombre,$clave,$valor) ) {
                    $data = [
                        'info'     => 'Se modifico: '.$clave.' del lugar: '.$nombre.' por: '.$valor.'.',
                        'redirect' => '/lugar'
                    ];
                    $this->load->view('success', $data );
                } 
                else {
                    //Cargo un array con los errores
                    $errores = ['Error' => 'Error al intentar modificar la categoria'];
                    //Cargo el array de datos a la vista de errores
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/lugar',
                        'errores' => $errores
                    );
                    //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
            }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function modificarplano() {

        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){

        $nombre = $this->input->post('nombre');
        $clave = "plano";

            if (!$this->Lugar_model->buscarPorNombre($nombre)) {
                //Cargo un array con los errores
                $errores = ['Error' => 'No existe un lugar con ese nombre'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/lugar',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
            }
            else{

                $imagen = $_FILES['imagen'];
                $errores = array();
                if($valor = $this->subirimagen($imagen,'planolugar')){ //Intento subir la foto
                } else{ //Si no lo puedo subir agrego el error
                    $errores['ERROR_CARGA'] = "Hubo algun error en la carga de la imagen";//$this->upload->display_errors();
                }
                if( count($errores) > 0) {
                    $data = array(
                        //Esto me sirve para saber a donde tiene que volver la plantilla de error
                        'redirect' => '/lugar/modificarLugar/?nombre='.$nombre,
                        'errores'  => $errores
                    );
                    $this->load->view('Error', $data );
                }
                else{
                        if ($this->Lugar_model->modificar( $nombre,$clave,$valor) ) {
                        $data = [
                            'info'     => 'Se modifico: '.$clave.' del lugar: '.$nombre.' por: '.$valor.'.',
                            'redirect' => '/lugar/modificarLugar/?nombre='.$nombre
                        ];
                        $this->load->view('success', $data );
                    } 
                    else {
                        //Cargo un array con los errores
                        $errores = ['Error' => 'Error al intentar modificar la categoria'];
                        //Cargo el array de datos a la vista de errores
                        $data = array(
                            //Esto me sirve para saber a donde tiene que volver la plantilla de error
                            'redirect' => '/lugar',
                            'errores' => $errores
                        );
                        //Envio los datos a la vista de errores
                        $this->load->view('Error', $data );
                    }
                }
        }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function buscar() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
        $buscar = $this->input->post("buscar");
        $numeropagina = $this->input->post("nropagina");
        $cantidad = $this->input->post("cantidad");
        $baja = $this->input->post("baja");
        
        $inicio = ($numeropagina -1)*$cantidad;
        $data = array(
            "lugares" => $this->Lugar_model->buscar($buscar,$baja,$inicio,$cantidad),
            "totalregistros" => count($this->Lugar_model->buscar($buscar,$baja)),
            "cantidad" =>$cantidad
            
        );
        echo json_encode($data);
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function modificarLugar() {
        $permitidos = array(5,1); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
                $nombre = $this->input->get("nombre");

                if ($this->Lugar_model->existeNombre($nombre) && !$this->Lugar_model->dadoDeBaja($nombre)){
                    $data = array(
                        "lugar" => $this->Lugar_model->buscarPorNombre($nombre)            
                    );
                    $this->load->view('modificarlugar', $data );            
                }else{
                    $errores = ['Error' => 'El lugar no existe o esta dado de baja '.$nombre];
                        //Cargo el array de datos a la vista de errores
                    $data = array(
                            //Esto me sirve para saber a donde tiene que volver la plantilla de error
                            'redirect' => '/lugar',
                            'errores' => $errores
                        );
                        //Envio los datos a la vista de errores
                    $this->load->view('Error', $data );
                }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

    public function subirimagen($file,$directorio) {

        $permitidos = array(5,4); 
        if ($this->session->userdata('logeado') == TRUE) {
            if(in_array($this->session->userdata('tipo'), $permitidos)){
                $options = array(
                    CURLOPT_URL => 'http://192.168.21.66/imagenes/insertarfoto',
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_POST => TRUE,
                    CURLOPT_POSTFIELDS => array(
                        'directorio' => $directorio,
                        'imagen' => curl_file_create(realpath($file['tmp_name']),$file['type'],$file['name'])
                    )
                );
                $ch = curl_init();
                curl_setopt_array($ch, $options);
                $content = curl_exec($ch);
                $info = curl_getinfo($ch);
                $error = curl_error($ch);
                $error .= " error Nro ".curl_errno($ch);
                curl_close($ch);
                if ($content === false) {
                    $js_code = 'confirm(' . json_encode("Error al intentar subir la imagen: ".$error, JSON_HEX_TAG) . ');';
                    $js_code = '<script>' . $js_code . '</script>';
                    echo $js_code;
                    return false;
                }
                $contenido = json_decode($content);
                $exito = false;
                $nombre = "";
                foreach ($contenido as $clave => $valor) {
                    if($clave == 'exito'){$exito = $valor;}
                    if($clave == 'nombre'){$nombre = $valor;}
                }
                if($exito){
                    return $nombre;
                }else{
                    return NULL;
                }
        }else{
            $errores = ['Error' => 'El usuario no tiene permisos sobre la seccion elegida.'];
                //Cargo el array de datos a la vista de errores
            $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
            $this->load->view('Error', $data );
        }
        }else{
            $errores = ['Error' => 'El usuario no ha iniciado sesion en el sistema.'];
                //Cargo el array de datos a la vista de errores
                $data = array(
                    //Esto me sirve para saber a donde tiene que volver la plantilla de error
                    'redirect' => '/',
                    'errores' => $errores
                );
                //Envio los datos a la vista de errores
                $this->load->view('Error', $data );
        }
    }

}
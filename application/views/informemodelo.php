<!DOCTYPE html>
<html>
<head>
    <title>Informe de ventas</title>
    <link href="/assets/css/bootstrap.css" rel="stylesheet"/>
    <link rel="icon" href="/assets/img/favicon.ico" type="image/png">
    <?php
        include ("/var/www/html/backend/application/libraries/chart/class/pData.class.php");
        include ("/var/www/html/backend/application/libraries/chart/class/pDraw.class.php");
        include ("/var/www/html/backend/application/libraries/chart/class/pImage.class.php");
        function graficar($nombreimagen,$valores,$nombrevalores,$elementos,$nombrelementos,$titulo){
            $MyData = new pData();
            $MyData->addPoints($valores,$nombrevalores);
            $MyData->setAxisName(0,$nombrevalores);
            $MyData->addPoints($elementos,$nombrelementos);
            $MyData->setSerieDescription($nombrelementos,$nombrelementos);
            $MyData->setAbscissa($nombrelementos);
            /* Create the pChart object */
            //largo x alto tamaño de imagen
            $largo = 1000;
            $alto = 500;
            $myPicture = new pImage($largo,$alto,$MyData);
            $myPicture->drawGradientArea(0,0,$largo,$alto,DIRECTION_VERTICAL,array("StartR"=>240,"StartG"=>240,"StartB"=>240,"EndR"=>180,"EndG"=>180,"EndB"=>180,"Alpha"=>100));
            $myPicture->drawGradientArea(0,0,$largo,$alto,DIRECTION_HORIZONTAL,array("StartR"=>240,"StartG"=>240,"StartB"=>240,"EndR"=>180,"EndG"=>180,"EndB"=>180,"Alpha"=>20));
            $myPicture->setFontProperties(array("FontName"=>'/var/www/html/backend/assets/fonts/verdana.ttf',"FontSize"=>7));

            /* Write the chart title */  
            $myPicture->setFontProperties(array("FontName"=>"/var/www/html/backend/assets/fonts/Forgotte.ttf","FontSize"=>11));
            $myPicture->drawText(160,39,$titulo,array("FontSize"=>20,"Align"=>TEXT_ALIGN_BOTTOMMIDDLE));

            /* Add a border to the picture */ 
            $myPicture->drawRectangle(0,0,$largo-1,$alto-1,array("R"=>0,"G"=>0,"B"=>0)); 

            /* Draw the chart scale */
            $myPicture->setGraphArea(100,70,960,490); //padding left and top, then tamaño x y tamaño y
            $myPicture->drawScale(array("CycleBackground"=>TRUE,"DrawSubTicks"=>TRUE,"GridR"=>0,"GridG"=>0,"GridB"=>0,"GridAlpha"=>10, "Pos"=>SCALE_POS_TOPBOTTOM, "RemoveYAxis"=>TRUE)); 
            
            /* Turn on shadow computing */ 
            $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));

            /* Draw the chart */ 
            $myPicture->drawBarChart(array("DisplayPos"=>LABEL_POS_INSIDE,"DisplayValues"=>TRUE,"Rounded"=>TRUE,"Surrounding"=>30));

            /* Write the legend */ 
            $myPicture->drawLegend(500,29,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_HORIZONTAL,"FontSize"=>15));
            
            /* Render the picture (choose the best way) */
            $myPicture->render("/var/www/html/backend/imagenes/graficas/".$nombreimagen.".png");
        }
    ?>
</head>
<body>
  <div class="wrapper">
    <h1>Informe de estadisticas </h1>
    <br>
    Para la fecha: <?php echo($fechainicio);?> hasta <?php echo($fechafin);?>, una cantidad de <?php echo($cantidad);?> articulos por categoria.
    <br>
    <br>
    <br><br>
    <?php
        $titulo = "Top eventos con mas ventas:";
        $nombreimagen = "topmasvendidos";
        $nombrevalores = 'ventas';
        $nombrelementos = 'nombre';
        $valores = array();
        $elementos = array();
        foreach ($eventosmasvendidos as $index => $arrayvendidos) {
            array_push($valores, $arrayvendidos[$nombrevalores]);
            array_push($elementos, $arrayvendidos[$nombrelementos]);
        }
        if (empty($valores)) {
            array_push($valores, 0);
            array_push($elementos, "");
        }
        graficar($nombreimagen,$valores,$nombrevalores,$elementos,$nombrelementos,$titulo);
        echo '<IMG SRC=/var/www/html/backend/imagenes/graficas/'.$nombreimagen.'.png>';
    ?>
    
    <br><br>
    <br><br>
    <?php
        //print("<pre>".print_r($eventosmenosvendidos,true)."</pre>");
        //$eventosmenosvendidos
        $titulo = "Top eventos con menos ventas: ";
        $nombreimagen = "eventosmenosvendidos";
        $nombrevalores = 'ventas';
        $nombrelementos = 'nombre';
        $valores = array();
        $elementos = array();
        foreach ($eventosmenosvendidos as $index => $arrayvendidos) {
            array_push($valores, $arrayvendidos[$nombrevalores]);
            array_push($elementos, $arrayvendidos[$nombrelementos]);
        }
        if (empty($valores)) {
            array_push($valores, 0);
            array_push($elementos, "");
        }
        graficar($nombreimagen,$valores,$nombrevalores,$elementos,$nombrelementos,$titulo);
        echo '<IMG SRC=/var/www/html/backend/imagenes/graficas/'.$nombreimagen.'.png>';
    ?>
    <br><br>
    Promedio de ventas por evento: <?=$promedioventasevento;?>
    <br><br>
    <br><br>
    <?php
    	//print("<pre>".print_r($eventosconmasganancia,true)."</pre>");
        $titulo = "Top eventos con mayor ganancia: ";
        $nombreimagen = "eventosconmasganancia";
        $nombrevalores = 'ganancia';
        $nombrelementos = 'nombre';
        $valores = array();
        $elementos = array();
        foreach ($eventosconmasganancia as $index => $arrayvendidos) {
            array_push($valores, $arrayvendidos[$nombrevalores]);
            array_push($elementos, $arrayvendidos[$nombrelementos]);
        }
        if (empty($valores)) {
            array_push($valores, 0);
            array_push($elementos, "");
        }
        graficar($nombreimagen,$valores,$nombrevalores,$elementos,$nombrelementos,$titulo);
        echo '<IMG SRC=/var/www/html/backend/imagenes/graficas/'.$nombreimagen.'.png>';
    ?>
    <br><br>
    
    <br><br>
    <?php
    	//print("<pre>".print_r($eventosconmenosganancia,true)."</pre>");
        $titulo = "Top eventos con menor ganancia: ";
        $nombreimagen = "eventosconmenosganancia";
        $nombrevalores = 'ganancia';
        $nombrelementos = 'nombre';
        $valores = array();
        $elementos = array();
        foreach ($eventosconmenosganancia as $index => $arrayvendidos) {
            array_push($valores, $arrayvendidos[$nombrevalores]);
            array_push($elementos, $arrayvendidos[$nombrelementos]);
        }
        if (empty($valores)) {
            array_push($valores, 0);
            array_push($elementos, "");
        }
        graficar($nombreimagen,$valores,$nombrevalores,$elementos,$nombrelementos,$titulo);
        echo '<IMG SRC=/var/www/html/backend/imagenes/graficas/'.$nombreimagen.'.png>';
    ?>
    <br><br>
    Promedio de ganancias por evento: <?=$promediogananciasevento;?>
    <br><br>
    <br><br>
    <?php
    	//print("<pre>".print_r($tipoticketmasvendido,true)."</pre>");
        $titulo = "Top tipoticket mas vendido: ";
        $nombreimagen = "tipoticketmasvendido";
        $nombrevalores = 'vendidas';
        $nombrelementos = 'nombre';
        $valores = array();
        $elementos = array();
        foreach ($tipoticketmasvendido as $index => $arrayvendidos) {
            array_push($valores, $arrayvendidos[$nombrevalores]);
            array_push($elementos, $arrayvendidos[$nombrelementos]);
        }
        if (empty($valores)) {
            array_push($valores, 0);
            array_push($elementos, "");
        }
        graficar($nombreimagen,$valores,$nombrevalores,$elementos,$nombrelementos,$titulo);
        echo '<IMG SRC=/var/www/html/backend/imagenes/graficas/'.$nombreimagen.'.png>';
    ?>
    <br><br>
    <br><br>
    <?php
    	//print("<pre>".print_r($tipoticketmenosvendido,true)."</pre>");
        $titulo = "Top tipoticket menos vendido: ";
        $nombreimagen = "tipoticketmenosvendido";
        $nombrevalores = 'vendidas';
        $nombrelementos = 'nombre';
        $valores = array();
        $elementos = array();
        foreach ($tipoticketmenosvendido as $index => $arrayvendidos) {
            array_push($valores, $arrayvendidos[$nombrevalores]);
            array_push($elementos, $arrayvendidos[$nombrelementos]);
        }
        if (empty($valores)) {
            array_push($valores, 0);
            array_push($elementos, "");
        }
        graficar($nombreimagen,$valores,$nombrevalores,$elementos,$nombrelementos,$titulo);
        echo '<IMG SRC=/var/www/html/backend/imagenes/graficas/'.$nombreimagen.'.png>';
    ?>
    <br><br>
    <br><br>
    <?php
    	//print("<pre>".print_r($ticketsconmayorganancia,true)."</pre>");
        $titulo = "Top tipoticket con mayor ganancia: ";
        $nombreimagen = "ticketsconmayorganancia";
        $nombrevalores = 'ganancia';
        $nombrelementos = 'nombre';
        $valores = array();
        $elementos = array();
        foreach ($ticketsconmayorganancia as $index => $arrayvendidos) {
            array_push($valores, $arrayvendidos[$nombrevalores]);
            array_push($elementos, $arrayvendidos[$nombrelementos]);
        }
        if (empty($valores)) {
            array_push($valores, 0);
            array_push($elementos, "");
        }
        graficar($nombreimagen,$valores,$nombrevalores,$elementos,$nombrelementos,$titulo);
        echo '<IMG SRC=/var/www/html/backend/imagenes/graficas/'.$nombreimagen.'.png>';
    ?>
    <br><br>
    <br><br>
    <?php
    	//print("<pre>".print_r($ticketsconmenorganancia,true)."</pre>");
        $titulo = "Top tipoticket con menor ganancia: ";
        $nombreimagen = "ticketsconmenorganancia";
        $nombrevalores = 'ganancia';
        $nombrelementos = 'nombre';
        $valores = array();
        $elementos = array();
        foreach ($ticketsconmenorganancia as $index => $arrayvendidos) {
            array_push($valores, $arrayvendidos[$nombrevalores]);
            array_push($elementos, $arrayvendidos[$nombrelementos]);
        }
        if (empty($valores)) {
            array_push($valores, 0);
            array_push($elementos, "");
        }
        graficar($nombreimagen,$valores,$nombrevalores,$elementos,$nombrelementos,$titulo);
        echo '<IMG SRC=/var/www/html/backend/imagenes/graficas/'.$nombreimagen.'.png>';
    ?>
    <br><br>
    Promedio de ganancias por top de tickets: <?=$promediogananciatipoticket?>
    <br><br>
    <br><br>
    <?php
    	//print("<pre>".print_r($lugaresmasconcurridos,true)."</pre>");
        $titulo = "Lugares mas concurridos: ";
        $nombreimagen = "lugaresmasconcurridos";
        $nombrevalores = 'asistencias';
        $nombrelementos = 'nombrelugar';
        $valores = array();
        $elementos = array();
        foreach ($lugaresmasconcurridos as $index => $arrayvendidos) {
            array_push($valores, $arrayvendidos[$nombrevalores]);
            array_push($elementos, $arrayvendidos[$nombrelementos]);
        }
        if (empty($valores)) {
            array_push($valores, 0);
            array_push($elementos, "");
        }
        graficar($nombreimagen,$valores,$nombrevalores,$elementos,$nombrelementos,$titulo);
        echo '<IMG SRC=/var/www/html/backend/imagenes/graficas/'.$nombreimagen.'.png>';
    ?>
    <br><br>
    <br><br>
    <?php
    	//print("<pre>".print_r($lugaresmenosconcurridos,true)."</pre>");
        $titulo = "Lugares menos concurridos: ";
        $nombreimagen = "lugaresmenosconcurridos";
        $nombrevalores = 'asistencias';
        $nombrelementos = 'nombrelugar';
        $valores = array();
        $elementos = array();
        foreach ($lugaresmenosconcurridos as $index => $arrayvendidos) {
            array_push($valores, $arrayvendidos[$nombrevalores]);
            array_push($elementos, $arrayvendidos[$nombrelementos]);
        }
        if (empty($valores)) {
            array_push($valores, 0);
            array_push($elementos, "");
        }
        graficar($nombreimagen,$valores,$nombrevalores,$elementos,$nombrelementos,$titulo);
        echo '<IMG SRC=/var/www/html/backend/imagenes/graficas/'.$nombreimagen.'.png>';
    ?>
    <br><br>    
    <br><br>
    <?php
        //print("<pre>".print_r($comidasmasvendidas,true)."</pre>");
        $titulo = "Comidas mas vendidas: ";
        $nombreimagen = "comidasmasvendidas";
        $nombrevalores = 'compras';
        $nombrelementos = 'nombrecomida';
        $valores = array();
        $elementos = array();
        foreach ($comidasmasvendidas as $index => $arrayvendidos) {
            array_push($valores, $arrayvendidos[$nombrevalores]);
            array_push($elementos, $arrayvendidos[$nombrelementos]);
        }
        if (empty($valores)) {
            array_push($valores, 0);
            array_push($elementos, "");
        }
        graficar($nombreimagen,$valores,$nombrevalores,$elementos,$nombrelementos,$titulo);
        echo '<IMG SRC=/var/www/html/backend/imagenes/graficas/'.$nombreimagen.'.png>';
    ?>
    <br><br>
    <br><br>
    <?php
    	//print("<pre>".print_r($comidasmenosvendidas,true)."</pre>");
        $titulo = "Comidas menos vendidas: ";
        $nombreimagen = "comidasmenosvendidas";
        $nombrevalores = 'compras';
        $nombrelementos = 'nombrecomida';
        $valores = array();
        $elementos = array();
        foreach ($comidasmenosvendidas as $index => $arrayvendidos) {
            array_push($valores, $arrayvendidos[$nombrevalores]);
            array_push($elementos, $arrayvendidos[$nombrelementos]);
        }
        if (empty($valores)) {
            array_push($valores, 0);
            array_push($elementos, "");
        }
        graficar($nombreimagen,$valores,$nombrevalores,$elementos,$nombrelementos,$titulo);
        echo '<IMG SRC=/var/www/html/backend/imagenes/graficas/'.$nombreimagen.'.png>';
    ?>
    <br><br>
    Promedio ventas de comidas: <?=$promedioventasdecomidas?>
    <br><br>
    <br><br>
    <?php
    	//print("<pre>".print_r($comidaconmasganancia,true)."</pre>");
        $titulo = "Comida con mayor ganancia: ";
        $nombreimagen = "comidaconmasganancia";
        $nombrevalores = 'ganancia';
        $nombrelementos = 'nombrecomida';
        $valores = array();
        $elementos = array();
        foreach ($comidaconmasganancia as $index => $arrayvendidos) {
            array_push($valores, $arrayvendidos[$nombrevalores]);
            array_push($elementos, $arrayvendidos[$nombrelementos]);
        }
        if (empty($valores)) {
            array_push($valores, 0);
            array_push($elementos, "");
        }
        graficar($nombreimagen,$valores,$nombrevalores,$elementos,$nombrelementos,$titulo);
        echo '<IMG SRC=/var/www/html/backend/imagenes/graficas/'.$nombreimagen.'.png>';
    ?>	
    <br><br>
    <br><br>
    <?php
    	//print("<pre>".print_r($comidaconmenosganancia,true)."</pre>");
        $titulo = "Comida con menor ganancia: ";
        $nombreimagen = "comidaconmenosganancia";
        $nombrevalores = 'ganancia';
        $nombrelementos = 'nombrecomida';
        $valores = array();
        $elementos = array();
        foreach ($comidaconmenosganancia as $index => $arrayvendidos) {
            array_push($valores, $arrayvendidos[$nombrevalores]);
            array_push($elementos, $arrayvendidos[$nombrelementos]);
        }
        if (empty($valores)) {
            array_push($valores, 0);
            array_push($elementos, "");
        }
        graficar($nombreimagen,$valores,$nombrevalores,$elementos,$nombrelementos,$titulo);
        echo '<IMG SRC=/var/www/html/backend/imagenes/graficas/'.$nombreimagen.'.png>';
    ?>
    <br><br>
    Promedio de ganancia por comida: <?=$promediodegananciaporcomida?>
    <br><br>
    <br><br>
    <?php
    	//print("<pre>".print_r($paquetemasvendido,true)."</pre>");
        $titulo = "Paquete mas vendidos: ";
        $nombreimagen = "paquetemasvendido";
        $nombrevalores = 'compras';
        $nombrelementos = 'nombrepaquete';
        $valores = array();
        $elementos = array();
        foreach ($paquetemasvendido as $index => $arrayvendidos) {
            array_push($valores, $arrayvendidos[$nombrevalores]);
            array_push($elementos, $arrayvendidos[$nombrelementos]);
        }
        if (empty($valores)) {
            array_push($valores, 0);
            array_push($elementos, "");
        }
        graficar($nombreimagen,$valores,$nombrevalores,$elementos,$nombrelementos,$titulo);
        echo '<IMG SRC=/var/www/html/backend/imagenes/graficas/'.$nombreimagen.'.png>';
    ?>
    <br><br>  
    <br><br>
    <?php
    	//print("<pre>".print_r($paquetemenosvendido,true)."</pre>");
        $titulo = "Paquete menos vendidos: ";
        $nombreimagen = "paquetemenosvendido";
        $nombrevalores = 'compras';
        $nombrelementos = 'nombrepaquete';
        $valores = array();
        $elementos = array();
        foreach ($paquetemenosvendido as $index => $arrayvendidos) {
            array_push($valores, $arrayvendidos[$nombrevalores]);
            array_push($elementos, $arrayvendidos[$nombrelementos]);
        }
        if (empty($valores)) {
            array_push($valores, 0);
            array_push($elementos, "");
        }
        graficar($nombreimagen,$valores,$nombrevalores,$elementos,$nombrelementos,$titulo);
        echo '<IMG SRC=/var/www/html/backend/imagenes/graficas/'.$nombreimagen.'.png>';
    ?>
    <br><br>
    Promedio de ventas de paquetes: <?=$promedioventasdepaquetes?>
    
    <br><br>
    <br><br>
    <?php
    	//print("<pre>".print_r($metodosdepagomasutilizados,true)."</pre>");
        $titulo = "Metodos de pago mas utilizados: ";
        $nombreimagen = "metodosdepagomasutilizados";
        $nombrevalores = 'transacciones';
        $nombrelementos = 'nombremetodopago';
        $valores = array();
        $elementos = array();
        foreach ($metodosdepagomasutilizados as $index => $arrayvendidos) {
            array_push($valores, $arrayvendidos[$nombrevalores]);
            array_push($elementos, $arrayvendidos[$nombrelementos]);
        }
        if (empty($valores)) {
            array_push($valores, 0);
            array_push($elementos, "");
        }
        graficar($nombreimagen,$valores,$nombrevalores,$elementos,$nombrelementos,$titulo);
        echo '<IMG SRC=/var/www/html/backend/imagenes/graficas/'.$nombreimagen.'.png>';
    ?>
    <br><br>
    <br><br>
    <?php
    	//print("<pre>".print_r($metodosdepagomenosutilizados,true)."</pre>");
        $titulo = "Metodos de pago menos utilizados: ";
        $nombreimagen = "metodosdepagomenosutilizados";
        $nombrevalores = 'transacciones';
        $nombrelementos = 'nombremetodopago';
        $valores = array();
        $elementos = array();
        foreach ($metodosdepagomenosutilizados as $index => $arrayvendidos) {
            array_push($valores, $arrayvendidos[$nombrevalores]);
            array_push($elementos, $arrayvendidos[$nombrelementos]);
        }
        if (empty($valores)) {
            array_push($valores, 0);
            array_push($elementos, "");
        }
        graficar($nombreimagen,$valores,$nombrevalores,$elementos,$nombrelementos,$titulo);
        echo '<IMG SRC=/var/www/html/backend/imagenes/graficas/'.$nombreimagen.'.png>';
    ?>
    <br><br>

    Promedio de cargas de saldo por usuario: <?=$promediocargasaldo?>
    <br><br>


  </div>

</body>
</html>
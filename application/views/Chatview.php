<!DOCTYPE html>
<html>
<head>
  <title>Chat</title>
  <link href="/assets/css/bootstrap.css" rel="stylesheet"/>
  <link rel="icon" href="/assets/img/favicon.ico" type="image/png">
</head>
<body>
	<!--<h1 class="card-title">Vista del chat backend</h1>-->
	<!------------------------------- nav ------------------------------->

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="/">Tickasur</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">Menú
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto menuprincipal">
            </ul>
            <form class="form-inline my-2 my-lg-0">
              <a href="/login/logout" class="btn btn-outline-light my-2 my-sm-0" >Logout</a>
            </form>
          </div>
        </nav>

        <!------------------------------- nav ------------------------------->

        <div class="row">  <!--- row--->
        <div class="col-md-6">
		<br>
        <h1 style="text-align: center;">Salas abiertas</h1>
        <br>
		<p> <br>
		<div class="table-responsive">
			<table id="tbsalasabiertas" class="table table-striped table-bordered">
				<thead>
					<tr>
						<th scope="col">IdSala</th>
						<th scope="col">Nombre de usuario</th>
						<th scope="col">Ultimo Mensaje</th>
						<th scope="col">Ir al chat</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		<div class="paginaciontablasabiertas" id="paginacion-estilo">
		</div>
	</div>

	<br><br><br>
	<div class="col-md-6">
		<br>
        <h1 style="text-align: center;">Salas disponibles</h1>
        <br>
		<p> <br>
		<div class="table-responsive">
			<table id="tbsalasdisponibles" class="table table-striped table-bordered">
				<thead>
					<tr>
						<th scope="col">IdSala</th>
						<th scope="col">Idusuario</th>
						<th scope="col">Elegir</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		<div class="paginaciontablasdisponibles" id="paginacion-estilo">
		</div>
	</div>
	</div> <!--- row--->


	<br><br><br>
	<div class="col">

		<h2 style="text-align: center;">Mis salas cerradas</h2>
		<p> <br>
		<div class="table-responsive">
			<table id="tbsalascerradas" class="table table-striped table-bordered">
				<thead>
					<tr>
						<th scope="col">IdSala</th>
						<th scope="col">Idusuario</th>
						<th scope="col">Ver Mensajes</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		<div class="paginaciontablascerradas" id="paginacion-estilo">
		</div>
	</div>
	<script src="<?php echo base_url();?>assets/js/jquery-1.12.4.js"></script>
	<script>
		function limpiarIntervalos(intervals){
			intervals.forEach(clearInterval);
		}
	</script>
	<script src="<?php echo base_url();?>assets/js/salasAbiertas.js"></script>
	<script src="<?php echo base_url();?>assets/js/salasDisponibles.js"></script>
	<script src="<?php echo base_url();?>assets/js/salasCerradas.js"></script>
	<script>
      var tipousuario = <?php echo $this->session->userdata('tipo'); ?>;
      var vista = "chat";
    </script>
    <script src="<?php echo base_url();?>assets/js/menuprincipal.js"></script>
</body>
</html>
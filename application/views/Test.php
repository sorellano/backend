<!DOCTYPE html>
<html>
<head>
  <title>Test</title>
  <link href="/assets/css/bootstrap.css" rel="stylesheet"/>
</head>
<body>
  <div class="wrapper">
    <div class="content">
      <h1 class="tituloLogin">Panel de test</h1> <br>
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-12" id="cuadroA">
              <h2 class="tituloLogin">Titulo</h2>
              <br>
              <form class="form" method="POST" action="/test">

                <select required> <!-- Aca lleno la lista de opciones con las categorias disponibles-->
                  <?php echo "<br>"; 
  	 				        foreach ($categorias as $categoria) { 	
  		              	echo '<option value="';
  		              	echo $categoria['idcategoria'];
  		              	echo '">';
  		              	echo $categoria['nombre'];
  		              	echo '</option>';
                    }
                  ?>
                </select>
	            <br>
	            <br>
              	<!-- Aca empieza la parte del multiple data time picker -->

              	<div id="fechas">

              		<div id="fecha1">
	              		Fecha 1: <input type="date" name="date" min="<?=date('Y')?>-<?=date('m')?>-<?=date('d')?>" required>, Hora: <input type="time" name="time" value="12:00" required>
                    <?=date('Y')?>-<?=date('m')?>-<?=date('d')?>
            
	              		<!-- Borrado porque no queremos eliminar la primer fecha del evento <button type="button" onclick="javascript:removeElement('fecha1');return false;">Remove</button> -->
              		</div>
              	</div>
              		
              	<button type="button" onclick="javascript:addFile();return false;"> Añadir fecha</button>
              		
				        <br>
              	<br>

              	Asd: <input type="checkbox" id="chbx" onchange="javascript:checking('chbx')">
              	
              	<br>
              	<br>
                <input id="cantidadfechas" name="cantidadfechas" type="hidden" />

                <button type="submit" onclick="javascript:sendcantidad();" class="btn btn-success btn-block" id="ingresar-button">Ingresar</button>
              </form>

              <form class="form" method="POST" action="/test/subirImagen" enctype='multipart/form-data'>
              <br>
              Imagen: <input type="file" id="imagen" name="imagen" accept=".jpg,.jpeg" size="3MB">
              <br>
              <button type="submit" class="btn btn-success btn-block" id="ingresar-button">Subir</button>
               </form>

              <?php foreach($comidas as $comida){?>
              <?='<br>';?>
              <?=$comida['nombre'];?>
              <?='<br>';?>
              <?=$comida['descripcion'];?>
              <?='<br>';?>
              <?php }?>
              <?=$paginacion;?>

          </div>
        </div>
      </div> <br>
      <h1 class="tituloLogin"><a href="/empleado">EMPLEADO <a></button></h1>
    </div>
  </div>
</body>
<script type="text/javascript">

    function removeElement(id) {
	    var elem = document.getElementById(id);
	    return elem.parentNode.removeChild(elem);
	}

	var fechaId = 1; // used by the addFile() function to keep track of IDs
    function addFile() {
        fechaId++; // increment fileId to get a unique ID for the new element

        var nombre= 'Fecha '+fechaId; //Para identificar que fecha es en el body

        var borrar= '\'fecha'+fechaId+'\''; //Para identificar cual linea borra en el remove element

        var html = nombre + ': <input type="date" name="date" min="<?=date('Y-m-d');?>" >, Hora: <input type="time" name="time" value="12:00" >'+
               '<button type="button" onclick="javascript:removeElement('+borrar+');return false;">Remove</button>'; 

        addElement('fechas', 'div', 'fecha'+fechaId, html);
    }

    function addElement(parentId, elementTag, elementId, html){
    	// Adds an element to the document
    	var p = document.getElementById(parentId);
    	var newElement = document.createElement(elementTag);
   		newElement.setAttribute('id', elementId);
    	newElement.innerHTML = html;
    	p.appendChild(newElement);
    }

     function sendcantidad(){
        document.getElementById('cantidadfechas').value = fechaId;
      }

      function checking(id){

      var checkbox = document.getElementById(id);
      if(checkbox.checked == true ){
       alert("Checked!");
      }
      else {
        alert("UnChecked!");
      }

    }
   

</script>

</html>    		
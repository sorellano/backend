<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Chat <?=$idsala?></title>
        <link href="/assets/css/bootstrap.css" rel="stylesheet"/>
        <link href="/assets/css/estilo.css" rel="stylesheet"/>
    </head>

    <body>
        <!------------------------------- nav ------------------------------->

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="/">Tickasur</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">Menú
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto menuprincipal">
            </ul>
            <form class="form-inline my-2 my-lg-0">
              <a href="/login/logout" class="btn btn-outline-light my-2 my-sm-0" >Logout</a>
            </form>
          </div>
        </nav>
        <!------------------------------- nav ------------------------------->
        <div id="chatbox">
        </div>
    </body>
     <script>
        var idsala = <?=$idsala?>;
        var idemepleado = <?=$idempleado?>;
    </script>
    <script src="<?php echo base_url();?>assets/js/jquery-1.12.4.js"></script>
    <script src="<?php echo base_url();?>assets/js/salaabierta.js"></script>
    <script>
      var tipousuario = <?php echo $this->session->userdata('tipo'); ?>;
      var vista = "chat";
    </script>
    <script src="<?php echo base_url();?>assets/js/menuprincipal.js"></script>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<link href="/assets/css/bootstrap.css" rel="stylesheet"/>
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
</head>
<body>
    <div >
    	<br>
    	<br>
    	<div class="col-12" id="loginprueba">
	    	<div class="titulo-login"> <br>
	    		<h1 class="tituloLogin">Bienvenidos al Backend de Ticketsur  </h1>
	    	</div>
	        <br> <br>
	        <div class="contenido-login">
	            <form class="form" method="POST" action="/login/loginpost">
	                <input class="form-control" type="mail" placeholder="Ingrese su e-mail" id="mail" name="mail" required><br>
	                <input class="form-control" type="password" placeholder="Ingrese su password" id="password" name="password" required> <br> <br>
	                <button type="submit" class="btn btn-info btn-block" id="login-button">Ingresar</button> <br>
	            </form>
	        </div>
	    </div>
    </div>
</body>
</html>
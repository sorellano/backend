<body>
    <h1 class="tituloLogin">Alta comida</h1>
    <br>
    <form class="form" method="POST" action="/comida/alta">

        <input class="form-control" type="text" placeholder="nombre" id="nombre" name="nombre" required>
        <br>
        <input class="form-control" type="text" placeholder="descripcion" id="descripcion" name="descripcion" required>
        <br>
        <button type="submit" class="btn btn-info btn-block" id="altacomida-button">Ingresar</button>
    </form>

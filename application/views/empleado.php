<!DOCTYPE html>
<html>
<head>
    <title>Empleado</title>
    <link href="/assets/css/bootstrap.css" rel="stylesheet"/>
    <link rel="icon" href="/assets/img/favicon.ico" type="image/png">
</head>
<body>
    <div class="wrapper">
        <!------------------------------- nav ------------------------------->

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="/">Tickasur</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">Menú
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto menuprincipal">
            </ul>
            <form class="form-inline my-2 my-lg-0">
              <a href="/login/logout" class="btn btn-outline-light my-2 my-sm-0" >Logout</a>
            </form>
          </div>
        </nav>

        <!------------------------------- nav ------------------------------->
        
        <div class="content">
            <div class="col-md-12"> <br>
                <div class="card border-dark text-white mb-3" style="background-color: #14161a;">
                    <div class="card-body">
                    <h1 class="card-title">Iniciaste sesion como empleado</h1>
                    </div>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="card border-dark text-white mb-3" style="background-color: #848486;">
                      <div class="card-body">
                        <h2 class="card-title">Tu ID es:</h2>
                        <h4 class="card-text"><?php echo $this->session->userdata('idusuario'); ?></h4>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="card border-dark text-white mb-3" style="background-color: #848486;">
                      <div class="card-body">
                        <h2 class="card-title">Tu mail es:</h2>
                        <h4 class="card-text"><?php echo $this->session->userdata('mail'); ?> </h4>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="card border-dark text-white mb-3" style="background-color: #848486;">
                      <div class="card-body">
                        <h2 class="card-title">Tu tipo de empleado es:</h2>
                        <h4 class="card-text">
                          <?php
                            switch ($this->session->userdata('tipo')){
                              case 5:
                                  echo "Root";
                              break;
                              case 4:
                                  echo "Organizador";
                              break;
                              case 3:
                                  echo "Validador";
                              break;
                              case 2:
                                  echo "Moderador";
                              break;
                              case 1:
                                  echo "Marketing";
                              break;
                              default:
                                  echo "Tipo no reconocido";
                              break;
                            }
                          ?> 
                        </h4>
                      </div>
                    </div>
                  </div>
                </div>
            </div>                  
        </div>       
    </div>
    <script src="<?php echo base_url();?>assets/js/jquery-1.12.4.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
    <script>
      var tipousuario = <?php echo $this->session->userdata('tipo'); ?>;
      var vista = "empleado";
    </script>
    <script src="<?php echo base_url();?>assets/js/menuprincipal.js"></script>
</body>
</html>
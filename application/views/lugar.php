<!DOCTYPE html>
<html>
<head>
  <title>Lugar</title>
  <link href="/assets/css/bootstrap.css" rel="stylesheet"/>
  <link rel="icon" href="/assets/img/favicon.ico" type="image/png">
</head>
<body>
  <div class="wrapper">
        <!------------------------------- nav ------------------------------->

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="/">Tickasur</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">Menú
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto menuprincipal">
            </ul>
            <form class="form-inline my-2 my-lg-0">
              <a href="/login/logout" class="btn btn-outline-light my-2 my-sm-0" >Logout</a>
            </form>
          </div>
        </nav>

        <!------------------------------- nav ------------------------------->
    <div class="content">
      <div class="col-md-12">
        <div class="row">
          <div class="col my-auto" id="cuadroB">
              <h2 class="tituloP">Alta de lugar</h2>
              <br>
              <form class="form" method="POST" action="/lugar/alta"  enctype='multipart/form-data'>
                  <input class="form-control" type="text" placeholder="nombre" id="nombre" name="nombre" required>
                  <br>
                  <input class="form-control" type="text" placeholder="plazas" id="plazas" name="plazas" required>
                  <br>
                  <input class="form-control" type="text" placeholder="direccion" id="direccion" name="direccion" required>
                  <br>
                  <div class="input-group">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" id="plano" name="plano" accept=".jpg,.jpeg,.png" size="3MB" aria-describedby="inputGroupFileAddon01">
                      <label class="custom-file-label" for="inputGroupFile01">Plano del sitio</label>
                    </div>
                  </div>
                  <br>
                  <input class="form-control" type="text" placeholder="Sitio web URL" id="web" name="web" required>
                  <br>
                  <input class="form-control" type="text" placeholder="descripcion" id="descripcion" name="descripcion" required>
                  <br>
                  <button type="submit" class="btn btn-info btn-block" id="altalugar-button">Ingresar</button> <br>
              </form>
          </div>
          <div class="col" id="cuadroA">
            <br>
            <h1>Panel de control de lugares</h1>
             <br>
            <p>
              <input type="text" class="form-control-md" name="busqueda" placeholder="Buscar lugar" />
              &nbsp;
              <strong>Cantidad a mostrar: </strong>
              <select name="cantidad" id="cantidad">
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
              </select>
              &nbsp;
              <strong>Mostrar dados de baja: </strong>
              <select name="baja" id="baja">                
                <option value="0">No</option>
                <option value="1">Si</option>
              </select>
              &nbsp;
              <button class="btn btn-default btn-sm"><big><a href="#" data-toggle="modal" data-target="#altalugar" style="text-decoration: none;color:black;">Alta lugar</a></big></button>
            </p>
            <div class="table-responsive">
              <table id="tblugares" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th scope="col">Idlugar</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Plazas</th>
                    <th scope="col">Direccion</th>
                    <th scope="col">Baja</th>
                    <th scope="col">Modificar</th>
                    <th scope="col">Dar de Baja/Alta</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <div class="paginacion" id="paginacion-estilo">
              
            </div>
          </div>
        </div>
      </div>
    </div> <!-- content -->

    <!------------------------------------------------------------------ MODALFADE-------------------------------------->
    <div id="altalugar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Alta Lugar</h3>
                    <button type="button" class="close font-weight-light" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form class="form" method="POST" action="/lugar/alta"  enctype='multipart/form-data'>
                  <input class="form-control" type="text" placeholder="nombre" id="nombre" name="nombre" required>
                  <br>
                  <input class="form-control" type="text" placeholder="plazas" id="plazas" name="plazas" required>
                  <br>
                  <input class="form-control" type="text" placeholder="direccion" id="direccion" name="direccion" required>
                  <br>
                  <div class="input-group">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" id="plano" name="plano" accept=".jpg,.jpeg,.png" size="3MB" aria-describedby="inputGroupFileAddon01">
                      <label class="custom-file-label" for="inputGroupFile01">Plano del sitio</label>
                    </div>
                  </div>
                  <br>
                  <input class="form-control" type="text" value="Sitio web URL" id="web" name="web">
                  <br>
                  <input class="form-control" type="text" value="descripcion" id="descripcion" name="descripcion">
                        <br>
                        <div class="modal-footer">
                          <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                          <button type="submit" class="btn btn-primary" id="altalugar-button">Ingresar</button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
        </div>

        <!------------------------------------------------------------------ MODALFADE-------------------------------------->

  </div>
  <script src="<?php echo base_url();?>assets/js/jquery-1.12.4.js"></script>
  <script src="<?php echo base_url();?>assets/js/buscadorlugares.js"></script>
  <script src="<?php echo base_url();?>assets/js/ocultarSidebar.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
  <script>
      var tipousuario = <?php echo $this->session->userdata('tipo'); ?>;
      var vista = "lugar";
    </script>
    <script src="<?php echo base_url();?>assets/js/menuprincipal.js"></script>
</body>
</html>
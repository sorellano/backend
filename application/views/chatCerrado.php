<!DOCTYPE html>
<html>
<head>
	<title>Chat cerrado <?=$idsala?></title>
	<link href="/assets/css/bootstrap.css" rel="stylesheet"/>
	<link href="/assets/css/estilo.css" rel="stylesheet"/>
	<link rel="icon" href="/assets/img/favicon.ico" type="image/png">
	<title>Chat <?=$idsala?></title>
</head>
<body>
	<!------------------------------- nav ------------------------------->

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="/">Tickasur</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">Menú
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarText">
			<ul class="navbar-nav mr-auto menuprincipal">
			</ul>
			<form class="form-inline my-2 my-lg-0">
				<a href="/login/logout" class="btn btn-outline-light my-2 my-sm-0" >Logout</a>
			</form>
		</div>
	</nav>

	<!------------------------------- nav ------------------------------->

	<h1 class="card-title" style="text-align: center;">Chat cerrado <?=$idsala?></h1>
	<hr>

	<div id="historial" class="historial">
		<?php
		foreach ($mensajes as $index => $mensaje) {
			echo "<br>".$mensaje['timestamp']." ";
			if($mensaje['tipoautor'] == 'CLIENTE'){
				echo "<font color='red'>";
				echo " Cliente: ";
				echo "</font>";
			}else{
				echo "<font color='blue'>";
				echo " Atencion cliente: ";
				echo "</font>";
			}
			echo "<br>".$mensaje['mensaje']."<br>";
		}
		?>
	</div>


	<script src="<?php echo base_url();?>assets/js/jquery-1.12.4.js"></script>
	<script>
		var tipousuario = <?php echo $this->session->userdata('tipo'); ?>;
		var vista = "chat";
	</script>
	<script src="<?php echo base_url();?>assets/js/menuprincipal.js"></script>
</body>
</html>
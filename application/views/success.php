<!DOCTYPE html>
<html>
<head>
	<title>Exito!</title>
	<link href="/assets/css/bootstrap.css" rel="stylesheet"/>
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
</head>
<body>
    <div class="pagina-login2">
    	<div class="col-12" id="loginprueba2">
	    	<div class="tituloLogin2"> <br>
	    		<h1 class="tituloLogin2">Exito &#10004;</h1>
	    	</div>
	    	<div class="tituloLogin2"> <br>
	    		<h1 class="tituloLogin2"><?=$info?></h1>
	    	</div>
	        <br>
	        <div class="tituloLogin2"> <br>
	    		<h1 class="tituloLogin2"><a href=" <?= $redirect ?> ">Regresar a la página</a> </h1>
	    	</div><br>
	    </div>
    </div>
</body>
</html>

<!DOCTYPE html>
<html>
<head>
  <title>Modificar paquete</title>
  <link href="/assets/css/bootstrap.css" rel="stylesheet"/>
  <link href="/assets/css/estilo.css" rel="stylesheet"/>
  <link rel="icon" href="/assets/img/favicon.ico" type="image/png">
</head>
<body>
  <div class="wrapper">
    <!------------------------------- nav ------------------------------->

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="/">Tickasur</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">Menú
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto menuprincipal">
        </ul>
        <form class="form-inline my-2 my-lg-0">
          <a href="/login/logout" class="btn btn-outline-light my-2 my-sm-0" >Logout</a>
        </form>
      </div>
    </nav>

    <!------------------------------- nav ------------------------------->

    <div class="content">
      <div class="col-md-12"> <br>
        <h1>Modificar paquete</h1>

        <div class="col-md-12">
          <div class="row">
            <br>
            <div class="col-sm my-auto">
              <h2>Datos del paquete</h2>
              <br>
              <h2 class="card-title">Id del paquete: <?=$paquete['idpaquete'];?></h2>
              <h2 class="card-title">Id del evento: <?=$paquete['idevento'];?></h2>
              <h2 class="card-title">Nombre: <?=$paquete['nombre'];?></h2>
              <h2 class="card-title">Hora del evento: <?=$paquete['fecha'];?></h2>
              <h2 class="card-title">Imagen: <img src="http://cdn.local/imagenes/paqueteimagen/<?=$paquete['imagen'];?>" alt="Evento_imagen" height="150" width="150"></h2>

            </div>
          </div>
        </div><hr>

        <div class="col-xl-12 cuadradomodificar">
          <h2>Modificar Imagen:</h2>
          <form class="form" method="POST" action="/paquete/modificarimagen" enctype='multipart/form-data'>
            <input type="hidden" id="idpaquete" name="idpaquete" value="<?=$paquete['idpaquete'];?>">
            <input type="hidden" value="<?=$paquete['idevento'];?>" id="idevento" name="idevento">
            <input type="file" id="imagen" name="imagen" accept=".jpg,.jpeg,.png" size="3MB" required>
            <button type="submit" class="btn btn-primary" id="modificar-button">Modificar</button><br>
          </form>
        </div>
        <br><hr>

        <div class="col-md-12">
          <div class="row">
            <div class="col-lg my-auto">
              <h1>Tipo de tickets</h1>
              <br>
              <p><br>
                &nbsp;<input type="text" class="form-control-sm" name="busquedatipoticket" placeholder="Buscar Tickets" />
                <strong>Cantidad a mostrar: </strong>
                <select name="cantidadtipotickets" id="cantidadtipotickets">
                  <option value="5">5</option>
                  <option value="10">10</option>
                  <option value="15">15</option>
                </select>
                &nbsp;<button class="btn btn-default btn-sm"><big><a href="#" data-toggle="modal" data-target="#agregartipoticket" style="text-decoration: none;color:black;">Agregar Tipo de ticket</a></big></button>
              </p>
              <input type="hidden" value="<?=$paquete['idpaquete'];?>" id="idpaquete" name="idpaquete">
              <div class="table-responsive">
                <table id="tbtipoticket" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">IdtipoTicket</th>
                      <th scope="col">Nombre</th>
                      <th scope="col">Unidades</th>
                      <th scope="col">Precio unitario</th>
                      <th scope="col">Descuento</th>
                      <th scope="col">Cambiar descuento</th>
                      <th scope="col">Cambiar Unidades</th>
                      <th scope="col">Eliminar</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
              <div class="paginaciontipoticket" id="paginacion-estilo">

              </div>
            </div>

          </div>
        </div><hr>
        <div class="col-md-12">
          <div class="row">
            <div class="col-lg my-auto">
              <h1>Comidas</h1>
              <br>
              <p><br>
                <input type="text" class="form-control-sm" name="busquedamenu" placeholder="Buscar comidas" />
                &nbsp;
                <strong>Cantidad a mostrar </strong>
                <select name="cantidadmenus" id="cantidadmenus">
                  <option value="5">5</option>
                  <option value="10">10</option>
                  <option value="15">15</option>
                </select>
                &nbsp;<button class="btn btn-default btn-sm"><big><a href="#" data-toggle="modal" data-target="#agregarcomida" style="text-decoration: none;color:black;">Agregar Comida</a></big></button>
              </p>
              <input type="hidden" value="<?=$paquete['idpaquete'];?>" id="idpaquete" name="idpaquete">
              <div class="table-responsive">
                <table id="tbmenu" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">Idcomida</th>
                      <th scope="col">Nombre</th>
                      <th scope="col">Unidades</th>
                      <th scope="col">Precio unitario</th>
                      <th scope="col">Descuento</th>
                      <th scope="col">Cambiar descuento</th>
                      <th scope="col">Cambiar Unidades</th>
                      <th scope="col">Eliminar</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
              <div class="paginacionmenu" id="paginacion-estilo">

              </div>
              <br>
            </div>
          </div>
        </div><hr>
        <div class="col-md-12">
          <div class="row">
            <div class="col-lg my-auto">
              <h1>Metodos de pago</h1><br>
              
              <p><br>
                <input type="text" class="form-control-sm" name="busquedametodopago" placeholder="Buscar metodos" /> 
                &nbsp;
                <strong>Cantidad a mostrar: </strong>
                <select name="cantidadmetodopago" id="cantidadmetodopago">
                  <option value="5">5</option>
                  <option value="10">10</option>
                  <option value="15">15</option>
                </select>
                &nbsp;<button class="btn btn-default btn-sm"><big><a href="#" data-toggle="modal" data-target="#agregarmetodopago" style="text-decoration: none;color:black;">Agregar Metodo de pago</a></big></button>
              </p>
              <input type="hidden" value="<?=$paquete['idpaquete'];?>" id="idpaquete" name="idpaquete">
              <div class="table-responsive">
                <table id="tbmetodopago" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">Idmetodopago</th>
                      <th scope="col">Nombre</th>
                      <th scope="col">Eliminar</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
              <div class="paginacionmetodopago" id="paginacion-estilo">

              </div>
            </div>  
          </div> <br>
        </div>

      </div>
    </div> <!----------- content -------->

    <div id="agregartipoticket" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h3>Agregar Tipo ticket</h3>
            <button type="button" class="close font-weight-light" data-dismiss="modal" aria-hidden="true">×</button>
          </div>
          <div class="modal-body">
            <form class="form" method="POST" action="/Paquete/agregarTipoTicket">
              <input type="hidden" id="idpaquete" name="idpaquete" value="<?=$paquete['idpaquete'];?>">
              <input type="hidden" value="<?=$paquete['idevento'];?>" id="idevento" name="idevento">
              <br>
              Tipo ticket :
              <select name="tipoticket" required>
                <?php echo "<br>"; 
                foreach ($tipotickets as $tipoticket) {   
                  echo '<option value="';
                  echo $tipoticket['idtipoticket'];
                  echo '">';
                  echo $tipoticket['nombre'];
                  echo '</option>';
                }
                if (count($tipoticket) == 0){
                  echo '<option>';
                  echo 'No hay mas tipo de tickets disponibles';
                  echo '</option>';
                }
                ?>
              </select>
              <br> <br>
              Descuento: 
              <br>
              <input class="form-control" type="text" placeholder="descuento" id="descuento" name="descuento" required>
              <br>
              Cantidad de items: 
              <br>
              <input class="form-control" type="text" placeholder="cantidad" id="cantidad" name="cantidad" required>
              <br> <br>
              <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                <button type="submit" class="btn btn-primary" id="agregar-button">Agregar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>



    <div id="agregarcomida" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h3>Agregar Comida</h3>
            <button type="button" class="close font-weight-light" data-dismiss="modal" aria-hidden="true">×</button>
          </div>
          <div class="modal-body">
            <form class="form" method="POST" action="/paquete/agregarComida">
              <input type="hidden" id="idpaquete" name="idpaquete" value="<?=$paquete['idpaquete'];?>">
              <input type="hidden" id="idevento" name="idevento" value="<?=$paquete['idevento'];?>">
              <br>
              Comida : 
              <select name="idcomida" required> 
                <?php echo "<br>"; 
                foreach ($comidas as $comida) {   
                  echo '<option value="';
                  echo $comida['idcomida'];
                  echo '">';
                  echo $comida['nombre'];
                  echo '</option>';
                }
                if (count($comida) == 0){
                  echo '<option>';
                  echo 'No hay mas comidas disponibles';
                  echo '</option>';
                }
                ?>
              </select>
              <br><br> Descuento: 
              <br>
              <input class="form-control" type="text" placeholder="descuento" id="descuento" name="descuento" required>
              <br>
              Cantidad de items: 
              <br>
              <input class="form-control" type="text" placeholder="cantidad" id="cantidad" name="cantidad" required>
              <br> <br>
              <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                <button type="submit" class="btn btn-primary" id="agregar-button">Agregar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>


    <div id="agregarmetodopago" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h3>Agregar Metodo de pago</h3>
            <button type="button" class="close font-weight-light" data-dismiss="modal" aria-hidden="true">×</button>
          </div>
          <div class="modal-body">
            <form class="form" method="POST" action="/Paquete/agregarMetodoPago">
              Metodos de pago : 
              <select name="metodopago" required>
                <?php echo "<br>"; 
                foreach ($metodospago as $metodopago) {   
                  echo '<option value="';
                  echo $metodopago['idmetodopago'];
                  echo '">';
                  echo $metodopago['nombre'];
                  echo '</option>';
                }
                if (count($metodospago) == 0){
                  echo '<option>';
                  echo 'No hay mas metodos de pago disponibles';
                  echo '</option>';
                }
                ?>
              </select>
              <br><br>
              <input type="hidden" value="<?=$paquete['idpaquete'];?>" id="idpaquete" name="idpaquete">
              <input type="hidden" value="<?=$paquete['idevento'];?>" id="idevento" name="idevento"> <br>
              <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                <button type="submit" class="btn btn-primary" id="agregar-button">Agregar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <script src="<?php echo base_url();?>assets/js/ocultarSidebar.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery-1.12.4.js"></script>
    <script src="<?php echo base_url();?>assets/js/comidaspaquete.js"></script>
    <script src="<?php echo base_url();?>assets/js/metodospagopaquete.js"></script>
    <script src="<?php echo base_url();?>assets/js/tipoticketspaquete.js"></script>  
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
    <script>
      var tipousuario = <?php echo $this->session->userdata('tipo'); ?>;
      var vista = "evento";
    </script>
    <script src="<?php echo base_url();?>assets/js/menuprincipal.js"></script>
  </body>
  </html>
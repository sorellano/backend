<!DOCTYPE html>
<html>
<head>
  <title>Empleados</title>
  <link href="/assets/css/bootstrap.css" rel="stylesheet"/>
  <link rel="icon" href="/assets/img/favicon.ico" type="image/x-icon">
</head>
<body>
  <!------------------------------- wrapper ------------------------------->
  <div class="wrapper">

      <!------------------------------- nav ------------------------------->

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="/">Tickasur</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">Menú
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto menuprincipal">
            </ul>
            <form class="form-inline my-2 my-lg-0">
              <a href="/login/logout" class="btn btn-outline-light my-2 my-sm-0" >Logout</a>
            </form>
          </div>
        </nav>

        <!------------------------------- nav ------------------------------->

        <!------------------------------- content ------------------------------->

    <div class="content">
      <div class="col-md-12">
        <div class="row">
          <div class="col">
              <br>
              <h1>Panel de control de Empleados</h1>
              <br>
            <p>
              <input type="text" class="form-control-md" name="busqueda" placeholder="Buscar Empleados por mail" />&nbsp;
              <strong>Cantidad a mostrar: </strong>
              <select name="cantidad" id="cantidad">
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
              </select>
              &nbsp;
              <strong>Mostrar dados de baja: </strong>
              <select name="baja" id="baja">                
                <option value="0">No</option>
                <option value="1">Si</option>
              </select>
              &nbsp;
              <button class="btn btn-default btn-sm"><big><a href="#" data-toggle="modal" data-target="#altaempleado" style="text-decoration: none;color:black;">Alta empleado</a></big></button>
            </p>
        
            <div class="table-responsive">
            <table id="tbempleados" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th scope="col">Idempleado</th>
                  <th scope="col">Mail</th>
                  <th scope="col">Username</th>
                  <th scope="col">Tipo</th>
                  <th scope="col">Baja</th>
                  <th scope="col">Modificar</th>
                  <th scope="col">Dar de Baja/Alta</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
            </div>
            <div class="paginacion" id="paginacion-estilo"></div>
          </div>
        </div>
      </div>
    </div>
    <!------------------------------- content ------------------------------->
    <!------------------------------------------------------------------ MODALFADE-------------------------------------->
    <div id="altaempleado" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Alta Empleado</h3>
                    <button type="button" class="close font-weight-light" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form class="form" method="POST" action="/empleado/registrar">
                        <input class="form-control" type="text" placeholder="username" id="username" name="username" required>
                        <br>
                        <input class="form-control" type="text" placeholder="mail" id="mail" name="mail" required>
                        <br>
                        <input class="form-control" type="password" placeholder="password" id="password" name="password" required>
                        <br>

                        <div class='radioRightSide'> 
                          <label for="one" class='radioStuff'> 
                            <input type="radio" id="one" name="tipo" value="1" checked class='radioButton'/>1-Marketing
                          </label> 
                          <label for='two' class='radioStuff'><br>
                            <input type='radio' id='two' name='tipo' value='2' class='radioButton'>2-Moderador
                          </label>
                          <label for='three' class='radioStuff'><br>
                            <input type='radio' id='thre' name='tipo' value='3' class='radioButton'>3-Validador
                          </label>
                          <label for='four' class='radioStuff'><br>
                            <input type='radio' id='four' name='tipo' value='4' class='radioButton'>4-Organizador
                          </label>
                        </div>
                        <br>
                        <div class="modal-footer">
                          <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                          <button type="submit" class="btn btn-primary" id="modificarcliente-button">Ingresar</button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
        </div>

        <!------------------------------------------------------------------ MODALFADE-------------------------------------->


    </div>
  </div>

  <!------------------------------- wrapper ------------------------------->

  <script src="<?php echo base_url();?>assets/js/jquery-1.12.4.js"></script>
  <script src="<?php echo base_url();?>assets/js/buscadorempleados.js"></script>
  <!---<script src="<?php echo base_url();?>assets/js/ocultarSidebar.js"></script>--->
  <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>

  <!----------------------------- script menu ------------------------------>

  <script>
      var tipousuario = <?php echo $this->session->userdata('tipo'); ?>;
      var vista = "empleado/empleados";
    </script>
    <script src="<?php echo base_url();?>assets/js/menuprincipal.js"></script>
    <!----------------------------- script menu ------------------------------>
</body>
</html>
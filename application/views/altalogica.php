<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <link href="/assets/css/bootstrap.css" rel="stylesheet"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
</head>
<body>
    <div class="pagina-login2">
        <div class="col-12" id="loginprueba2">
            <div class="tituloLogin2"> <br>
                <h1 class="tituloLogin2">Alta logica detectada</h1>
            </div>
            <div class="tituloLogin2"> <br>
                <h1 class="tituloLogin2"><?=$info?></h1>
            </div>
            <br>
            <form class="form" method="POST" action="<?= $redirect ?>">
                <input type="hidden" name="id" value="<?= $id ?>" />
                <br>
                <button type="submit" class="btn btn-info btn-block" id="buscarempleado-button">Aceptar</button>
            </form>
            <br>
            <div class="tituloLogin2"> <br>
                <h1 class="tituloLogin2"><a href=" <?= $regresar ?> ">Regresar a la página</a> </h1>
            </div><br>
        </div>
    </div>
</body>
</html>

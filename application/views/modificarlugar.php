<!DOCTYPE html>
<html>
<head>
  <title>Modificar lugar</title>
  <link href="/assets/css/bootstrap.css" rel="stylesheet"/>
  <link href="/assets/css/estilo.css" rel="stylesheet"/>
  <link rel="icon" href="/assets/img/favicon.ico" type="image/png">
</head>
<body>
  <div class="wrapper">
    <!------------------------------- nav ------------------------------->

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="/">Tickasur</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">Menú
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto menuprincipal">
        </ul>
        <form class="form-inline my-2 my-lg-0">
          <a href="/login/logout" class="btn btn-outline-light my-2 my-sm-0" >Logout</a>
        </form>
      </div>
    </nav>

    <!------------------------------- nav ------------------------------->
    <div class="content">
      <div class="col-md-12"> <br>
        <h1>Modificar lugar</h1>
        <br>
        <hr>
      </div>
      <div class="col-md-12">
        <div class="row">
          <div class="col-xl-6 cuadradomodificar" >
            <br>
            <h1>Datos del lugar</h1>
            <br>
            <h2 class="card-title">Nombre: <?php echo($lugar['nombre'])?></h2>
            <h2 class="card-title">Plazas: <?php echo($lugar['plazas'])?></h2>
            <h2 class="card-title">Direccion: <?php echo($lugar['direccion'])?></h2>
            <h2 class="card-title">Web: <?php echo($lugar['web'])?></h2>
            <h2 class="card-title">Descripcion: <?php echo($lugar['descripcion'])?></h2>
            <h2 class="card-title">Baja: <?php echo($lugar['baja'])?></h2>
            <h2 class="card-title">Plano: <img src="http://cdn.local/imagenes/planolugar/<?=$lugar['plano'];?>" alt="Lugar sin planos" height="150" width="150"></h2>
          </div>
          <div class="col-xl-3 cuadradomodificar" >
            <h2 class="tituloLogin">Modificar nombre</h2>
            <br>
            <form class="form" method="POST" action="/lugar/modificar">

              <input class="form-control" type="hidden" value="<?php echo($lugar['nombre'])?>" id="nombre" name="nombre" required>
              <br>
              <input class="form-control" type="text" placeholder="nuevo nombre" id="valor" name="valor" required>
              <br>
              <input type="hidden" name="clave" value="nombre" />
              <button type="submit" class="btn btn-primary" id="altalugar-button">Ingresar</button> <br>
            </form>
          </div>
          <div class="col-xl-3 cuadradomodificar" >
            <h2 class="tituloLogin">&zwnj; Modificar plazas &zwnj;</h2>
            <br>
            <form class="form" method="POST" action="/lugar/modificar">
              <input class="form-control" type="hidden" value="<?php echo($lugar['nombre'])?>" id="nombre" name="nombre" required>
              <br>
              <input class="form-control" type="text" placeholder="plazas" id="valor" name="valor" required>
              <br>
              <input type="hidden" name="clave" value="plazas" />
              <button type="submit" class="btn btn-primary" id="altalugar-button">Ingresar</button> <br>
            </form>
          </div>
          <div class="col-xl-3 cuadradomodificar" >
            <h2 class="tituloLogin">&zwnj; Modificar direccion &zwnj;</h2>
            <br>
            <form class="form" method="POST" action="/lugar/modificar">
              <input class="form-control" type="hidden" value="<?php echo($lugar['nombre'])?>" id="nombre" name="nombre" required>
              <br>
              <input class="form-control" type="text" placeholder="direccion" id="valor" name="valor" required>
              <br>
              <input type="hidden" name="clave" value="direccion" />
              <button type="submit" class="btn btn-primary" id="altalugar-button">Ingresar</button> <br>
            </form>
          </div>
          <div class="col-xl-3 cuadradomodificar" >
            <h2 class="tituloLogin">&zwnj; Modificar plano &zwnj;</h2>
            <br>
            <form class="form" method="POST" action="/lugar/modificarplano" enctype='multipart/form-data'>
              <input class="form-control" type="hidden" value="<?php echo($lugar['nombre'])?>" id="nombre" name="nombre" required>
              <br>
              <div class="input-group">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="imagen" name="imagen" accept=".jpg,.jpeg,.png" size="3MB" aria-describedby="inputGroupFileAddon01">
                  <label class="custom-file-label" for="inputGroupFile01">Plano del lugar</label>
                </div>
              </div>
              <br>
              <button type="submit" class="btn btn-primary" id="altalugar-button">Ingresar</button> <br>
            </form>
          </div>
          <div class="col-xl-3 cuadradomodificar" >
            <h2 class="tituloLogin">&zwnj; Modificar web &zwnj;</h2>
            <br>
            <form class="form" method="POST" action="/lugar/modificar">
              <input class="form-control" type="hidden" value="<?php echo($lugar['nombre'])?>" id="nombre" name="nombre" required>
              <br>
              <input class="form-control" type="text" placeholder="web" id="valor" name="valor" required>
              <br>
              <input type="hidden" name="clave" value="web" />
              <button type="submit" class="btn btn-primary" id="altalugar-button">Ingresar</button> <br>
            </form>
          </div>
          <div class="col-xl-3 cuadradomodificar" >
            <h2 class="tituloLogin">Modificar descripcion</h2>
            <br>
            <form class="form" method="POST" action="/lugar/modificar">
              <input class="form-control" type="hidden" value="<?php echo($lugar['nombre'])?>" id="nombre" name="nombre" required>
              <br>
              <input class="form-control" type="text" placeholder="descripcion" id="valor" name="valor" required>
              <br>
              <input type="hidden" name="clave" value="descripcion" />
              <button type="submit" class="btn btn-primary" id="altalugar-button">Ingresar</button> <br>
            </form>
          </div>

        </div>
      </div>
    </div>
  </div>
  <script src="<?php echo base_url();?>assets/js/jquery-1.12.4.js"></script>
  <script src="<?php echo base_url();?>assets/js/buscadoreventos.js"></script>
  <script src="<?php echo base_url();?>assets/js/ocultarSidebar.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
  <script>
    var tipousuario = <?php echo $this->session->userdata('tipo'); ?>;
    var vista = "lugar";
  </script>
  <script src="<?php echo base_url();?>assets/js/menuprincipal.js"></script>
</body>
</html>
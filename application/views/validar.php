<!DOCTYPE html>
<html>
<head>
  <title>Validaciones Evento <?=$idevento?></title>
  <link href="/assets/css/bootstrap.css" rel="stylesheet"/>
  <link href="/assets/css/estilo.css" rel="stylesheet"/>
  <link rel="icon" href="/assets/img/favicon.ico" type="image/png">
</head>
<body>
  <div class="wrapper">
    <!------------------------------- nav ------------------------------->

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="/">Tickasur</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">Menú
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto menuprincipal">
        </ul>
        <form class="form-inline my-2 my-lg-0">
          <a href="/login/logout" class="btn btn-outline-light my-2 my-sm-0" >Logout</a>
        </form>
      </div>
    </nav>

    <!------------------------------- nav ------------------------------->
    <div class="content">
      <div class="col-md-12"> <br>
        <h1>Panel de validaciones del evento <?=$idevento?></h1>
        <br>
        <hr>  
      </div>
      <div class="col-md-12">
        <div class="row">
          <div class="col-sm-1"></div>
          <div class="col-xl-4 cuadradomodificar" >
            <br>
            <h1>Validar Ticket</h1>
            <br>
            <form class="form" method="GET" action="/validacion/verificar">
              <input class="form-control" type="hidden" value="<?=$idevento?>" id="idevento" name="idevento" required>
              <br>
              <input class="form-control-sm" type="text" placeholder="Codigo" id="valor" name="valor" required>
              <br>
              <input type="hidden" name="clave" value="ticket" />
              <br>
              <button type="submit" class="btn btn-primary" id="altalugar-button">Ingresar</button> <br>
            </form>
          </div>
          <div class="col-sm-2"></div>
          <div class="col-xl-4 cuadradomodificar" >
            <br>
            <h1>Validar comida</h1>
            
            <br>
            <form class="form" method="GET" action="/validacion/verificar">
              <input class="form-control" type="hidden" value="<?=$idevento?>" id="idevento" name="idevento" required>
              <br>
              <input class="form-control-sm" type="text" placeholder="Codigo" id="valor" name="valor" required>
              <br>
              <input type="hidden" name="clave" value="comida" />
              <br>
              <button type="submit" class="btn btn-primary" id="altalugar-button">Ingresar</button> <br>
            </form>
          </div>
          <div class="col-sm-1"></div>
        </div>
      </div>

    </div>
    <script src="<?php echo base_url();?>assets/js/jquery-1.12.4.js"></script>
    <script>
      var tipousuario = <?php echo $this->session->userdata('tipo'); ?>;
      var vista = "validacion";
    </script>
    <script src="<?php echo base_url();?>assets/js/menuprincipal.js"></script>
  </body>
  </html>
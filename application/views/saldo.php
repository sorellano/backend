<!DOCTYPE html>
<html>
<head>
  <title>Saldos</title>
  <link href="/assets/css/bootstrap.css" rel="stylesheet"/>
  <link rel="icon" href="/assets/img/favicon.ico" type="image/png">
</head>
<body>
  <div class="wrapper">
        <!------------------------------- nav ------------------------------->

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="/">Tickasur</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">Menú
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto menuprincipal">
            </ul>
            <form class="form-inline my-2 my-lg-0">
              <a href="/login/logout" class="btn btn-outline-light my-2 my-sm-0" >Logout</a>
            </form>
          </div>
        </nav>

        <!------------------------------- nav ------------------------------->
    <div class="content">
      <div class="col-md-12">
        <div class="row">
          <div class="col">
            <br>
            <h1>Recargas de usuario</h1>
            <br>
            <p>
              <input type="text" class="form-control-md" id="busqueda" name="busqueda" placeholder="Buscar por Idusuario"/>
              &nbsp;
              Buscar por día: &nbsp;
              <input type="date" name="fecha" id="fecha" max="<?=date('Y-m-d');?>">
              &nbsp;
              <strong>Cantidad a mostrar: </strong>
              <select name="cantidad" id="cantidad">
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
              </select>
              &nbsp;
              <strong>Mostrar ya procesados: </strong>
              <select name="procesados" id="procesados">                
                <option value="0">No</option>
                <option value="1">Si</option>
              </select>
              &nbsp;
            </p>
            <div class="table-responsive">
              <table id="tbsaldos" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th scope="col">Idrecarga</th>
                    <th scope="col">Saldo</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Idusuario</th>
                    <th scope="col">Fecha</th>
                    <th scope="col">Metodo de pago</th>
                    <th scope="col">Aprobar/Rechazar</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <div class="paginacion" id="paginacion-estilo">
              
            </div>
          </div>
        </div>
      </div> <br>
    </div>
  </div>
  <script src="<?php echo base_url();?>assets/js/jquery-1.12.4.js"></script>
  <script src="<?php echo base_url();?>assets/js/buscadorsaldos.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
  <script>
      var tipousuario = <?php echo $this->session->userdata('tipo'); ?>;
      var vista = "saldo";
    </script>
    <script src="<?php echo base_url();?>assets/js/menuprincipal.js"></script>
</body>
</html>
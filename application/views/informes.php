<!DOCTYPE html>
<html>
<head>
  <title>Informes</title>
  <link href="/assets/css/bootstrap.css" rel="stylesheet"/>
  <link rel="icon" href="/assets/img/favicon.ico" type="image/png">
</head>
<body>

	<div class="wrapper">
        <!------------------------------- nav ------------------------------->

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="/">Tickasur</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">Menú
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto menuprincipal">
            </ul>
            <form class="form-inline my-2 my-lg-0">
              <a href="/login/logout" class="btn btn-outline-light my-2 my-sm-0" >Logout</a>
            </form>
          </div>
        </nav>

        <!------------------------------- nav ------------------------------->
    <div class="content">
      <div class="col-md-12">
        <div class="row">
          <div class="col">
            <br>
            <h1>Informes</h1>
            <br>
            <form class="form" method="get" action="/informes/generarinforme">
            	Fecha de inicio
	            <input type="date" name="fechainicio" max="<?=date('Y-m-d');?>" required>
	            Fecha de fin
	            <input type="date" name="fechafin" max="<?=date('Y-m-d');?>" required>
	            <br>
	            <p> <br>
	              <strong>Top: </strong>
	              <select name="cantidad" id="cantidad">
	                <option value="10">10</option>
	                <option value="15">15</option>
	              </select>
	              <strong> elementos mas y menos utilizados.</strong>
	            </p>
	            <button type="submit" class="btn btn-info">Generar informe</button>
            </form>
          	</div>
          </div>
        </div>
      </div> <br>
    </div>
  </div>
  <script src="<?php echo base_url();?>assets/js/jquery-1.12.4.js"></script>
  <script>
      var tipousuario = <?php echo $this->session->userdata('tipo'); ?>;
      var vista = "informes";
    </script>
    <script src="<?php echo base_url();?>assets/js/menuprincipal.js"></script>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
  <title>Modificar Comida</title>
  <link href="/assets/css/bootstrap.css" rel="stylesheet"/>
  <link href="/assets/css/estilo.css" rel="stylesheet"/>
  <link rel="icon" href="/assets/img/favicon.ico" type="image/png">
</head>
<body>
  <div class="wrapper">
    <!------------------------------- nav ------------------------------->

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="/">Tickasur</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">Menú
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto menuprincipal">
        </ul>
        <form class="form-inline my-2 my-lg-0">
          <a href="/login/logout" class="btn btn-outline-light my-2 my-sm-0" >Logout</a>
        </form>
      </div>
    </nav>

    <!------------------------------- nav ------------------------------->
    <div class="content">
      <div class="col-md-12"> <br>
       <h1>Modificar comida</h1>
       <br>
       <hr>
     </div>
     <div class="col-md-12">
      <div class="row">
        <div class="col-xl-3 cuadradomodificar" >
          <h1>Datos de la comida</h1>
          <br>
          <h2 class="card-title">Nombre: <?php echo($comida['nombre'])?></h2>
          <h2 class="card-title">Descripcion: <?php echo($comida['descripcion'])?></h2>
          <h2 class="card-title">Imagen: <img src="http://cdn.local/imagenes/imagencomida/<?=$comida['imagen'];?>" alt="Comida sin imagen" height="150" width="150"></h2>

        </div>
            <div class="col-xl-3 cuadradomodificar" >
              <h2 class="tituloLogin">Modificar nombre</h2>
              <br>
              <form class="form" method="POST" action="/comida/modificar">
                <input class="form-control" type="hidden" value="<?php echo($comida['nombre'])?>" id="nombre" name="nombre" required>
                <br>
                <input class="form-control-sm" type="text" placeholder="nuevo nombre" id="valor" name="valor" required>
                <br>
                <input type="hidden" name="clave" value="nombre" />
                <br>
                <button type="submit" class="btn btn-primary" id="altalugar-button">Ingresar</button> <br>
              </form>
            </div>
            <div class="col-xl-3 cuadradomodificar" >
              <h2 class="tituloLogin">Modificar descripcion</h2>
              <br>
              <form class="form" method="POST" action="/comida/modificar">
                <input class="form-control" type="hidden" value="<?php echo($comida['nombre'])?>" id="nombre" name="nombre" required>
                <br>
                <input class="form-control-sm" type="text" placeholder="nueva descripcion" id="valor" name="valor" required>
                <br>
                <input type="hidden" name="clave" value="descripcion" />
                <br>
                <button type="submit" class="btn btn-primary" id="altalugar-button">Ingresar</button> <br>
              </form>
            </div>
            <div class="col-xl-3 cuadradomodificar" >
              <h2 class="tituloLogin">Modificar Imagen</h2>
              <form class="form" method="POST" action="/comida/modificarImagen" enctype='multipart/form-data'>
                <br>
                <input class="form-control" type="hidden" value="<?php echo($comida['nombre'])?>" id="nombre" name="nombre" required>
                <br>
                <div class="input-group">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="imagen" name="imagen" accept=".jpg,.jpeg,.png" size="3MB" aria-describedby="inputGroupFileAddon01">
                    <label class="custom-file-label" for="inputGroupFile01">Imagen ilustrativa</label>
                  </div>
                </div>
                <br>
                <input type="hidden" name="clave" value="imagen" />
                <button type="submit" class="btn btn-primary" id="modificarcategoria-button">Ingresar</button> <br>
              </form>
            </div>
          </div>

    </div>
  </div>
  <script src="<?php echo base_url();?>assets/js/jquery-1.12.4.js"></script>
  <script src="<?php echo base_url();?>assets/js/buscadoreventos.js"></script>
  <script src="<?php echo base_url();?>assets/js/ocultarSidebar.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
  <script>
    var tipousuario = <?php echo $this->session->userdata('tipo'); ?>;
    var vista = "comida";
  </script>
  <script src="<?php echo base_url();?>assets/js/menuprincipal.js"></script>
</body>
</html>
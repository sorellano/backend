<!DOCTYPE html>
<html>
<head>
  <title>Modificar Tipo de ticket</title>
  <link href="/assets/css/bootstrap.css" rel="stylesheet"/>
  <link href="/assets/css/estilo.css" rel="stylesheet"/>
  <link rel="icon" href="/assets/img/favicon.ico" type="image/png">
</head>
<body>
  <div class="wrapper">
    <!------------------------------- nav ------------------------------->

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="/">Tickasur</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">Menú
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto menuprincipal">
        </ul>
        <form class="form-inline my-2 my-lg-0">
          <a href="/login/logout" class="btn btn-outline-light my-2 my-sm-0" >Logout</a>
        </form>
      </div>
    </nav>

    <!------------------------------- nav ------------------------------->
    <div class="content">
      <div class="col-md-12"> <br>
            <h1>Modificar el tipo de ticket id: <?=$tipoticket['idtipoticket'];?></h1>
      </div>
      <div class="col-md-12">
        <div class="row">
          <br>
          <div class="col-xl-3 cuadradomodificar">
            <h1>Datos del tipo de ticket</h1>
            <br>
            <h2 class="card-title">Id tipo ticket : <?=$tipoticket['idtipoticket'];?></h2>
            <h2 class="card-title">Nombre : <?=$tipoticket['nombre'];?></h2>
            <h2 class="card-title">Cantidad para la venta: <?=$tipoticket['cantidad'];?></h2>
            <h2 class="card-title">Numerado : <?=$tipoticket['numerado'];?></h2>
            <h2 class="card-title">Precio unitario: <?=$tipoticket['precio'];?></h2>
            <h2 class="card-title">Id del evento: <?=$tipoticket['idevento'];?></h2>
            <h2 class="card-title">Hora del evento: <?=$tipoticket['fecha'];?></h2>
            <h2 class="card-title">Imagen de referencia: <img src="http://cdn.local/imagenes/referenciatipoticket/<?=$tipoticket['referencia'];?>" alt="Referencia_imagen" height="150" width="150"></h2>

          </div>
          <div class="col-sm my-auto">
                <div class="row">
          <div class="col-xl-3 cuadradomodificar" >
            <h2>Modificar nombre del ticket</h2> <br>
            <form class="form" method="POST" action="/evento/modificarTipoTicket">
              <input type="hidden" id="idtipoticket" name="idtipoticket" value="<?=$tipoticket['idtipoticket'];?>">
              <input type="hidden" id="idevento" name="idevento" value="<?=$tipoticket['idevento'];?>">
              <input type="hidden" id="clave" name="clave" value="nombre">
              <input class="form-control-sm" type="text" placeholder="nombre" id="valor" name="valor" required> <br><br>
              <button type="submit" class="btn btn-primary" id="modificar-button">Modificar</button><br>
            </form>
          </div>
          <div class="col-xl-3 cuadradomodificar" >
            <h2>Modificar precio</h2> <br><br>
            <form class="form" method="POST" action="/evento/modificarTipoTicket">
              <input type="hidden" id="idtipoticket" name="idtipoticket" value="<?=$tipoticket['idtipoticket'];?>">
              <input type="hidden" id="idevento" name="idevento" value="<?=$tipoticket['idevento'];?>">
              <input type="hidden" id="clave" name="clave" value="precio">
              <input class="form-control-sm" type="text" placeholder="Nuevo precio" id="valor" name="valor" required><br><br>
              <button type="submit" class="btn btn-primary" id="modificar-button">Modificar</button><br>
            </form>
          </div>
          <div class="col-xl-6 cuadradomodificar" >
            <h2>Modificar Imagen de referencia</h2> <br>
            <form class="form" method="POST" action="/evento/modificarReferencia" enctype='multipart/form-data'>
              <input type="hidden" id="idtipoticket" name="idtipoticket" value="<?=$tipoticket['idtipoticket'];?>">
              <input type="hidden" id="idevento" name="idevento" value="<?=$tipoticket['idevento'];?>">
              <input type="file" id="imagen" name="imagen" accept=".jpg,.jpeg,.png" size="3MB" required><br>
              <br><button type="submit" class="btn btn-primary" id="modificar-button">Modificar</button><br>
            </form>
          </div>
      </div> <br>
      </div>
    </div>
    </div>
  </div>
  </div>
      <script src="<?php echo base_url();?>assets/js/jquery-1.12.4.js"></script>
      <script src="<?php echo base_url();?>assets/js/buscadoreventos.js"></script>
      <script src="<?php echo base_url();?>assets/js/ocultarSidebar.js"></script>
      <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
      <script>
      var tipousuario = <?php echo $this->session->userdata('tipo'); ?>;
      var vista = "evento";
    </script>
    <script src="<?php echo base_url();?>assets/js/menuprincipal.js"></script>
</body>
</html>
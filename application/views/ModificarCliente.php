<!DOCTYPE html>
<html>
<head>
	<title>Modificar cliente</title>
	<link href="/assets/css/bootstrap.css" rel="stylesheet"/>
	<link href="/assets/css/estilo.css" rel="stylesheet"/>
	<link rel="icon" href="/assets/img/favicon.ico" type="image/png">
</head>
<body>
	<div class="wrapper">
		<!------------------------------- nav ------------------------------->

		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="/">Tickasur</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">Menú
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarText">
				<ul class="navbar-nav mr-auto menuprincipal">
				</ul>
				<form class="form-inline my-2 my-lg-0">
					<a href="/login/logout" class="btn btn-outline-light my-2 my-sm-0" >Logout</a>
				</form>
			</div>
		</nav>

		<!------------------------------- nav ------------------------------->
		<div class="content">
			<div class="col-md-12"> <br>
				<h1>Modificar cliente</h1>
				<br>
				<hr>
			</div>
			<div class="col-md-12">
				<div class="row">
					<div class="col-xl-3 cuadradomodificar" >
						<br>
						<h1>Datos del cliente</h1>
						<br>
						<h2 class="card-title">ID : <?=$idusuario;?></h2>
						<h2 class="card-title">Dado de baja : <?=$baja?></h2>
						<h2 class="card-title">Mail :  <?=$mail;?></h2>
						<h2 class="card-title">Nombre :  <?=$nombre;?></h2>
						<h2 class="card-title">Apellido : <?=$apellido;?></h2>
						<h2 class="card-title">Telefono : <?=$telefono;?></h2>
						<h2 class="card-title">Ciudad : <?=$ciudad;?></h2>
						<h2 class="card-title">Calle : <?=$calle;?></h2>
						<h2 class="card-title">Barrio : <?=$barrio;?></h2>
						<h2 class="card-title">C.I. : <?=$ci;?></h2>
						<h2 class="card-title">Saldo : <?=$saldo;?></h2>
						<?php if ($baja=="Si") : ?>
							<br>
							<form class="form" method="POST" action="/Cliente/altalogica">
								<input type="hidden" id="idusuario" name="idusuario" value="<?=$idusuario;?>">
								<button type="submit" class="btn btn-primary" id="modificar-button">Dar de alta al usuario</button> <br>
							</form>
							<?php else: ?>
								<br>
								<form class="form" method="POST" action="/Cliente/baja">
									<input type="hidden" id="idusuario" name="idusuario" value="<?=$idusuario;?>">
									<button type="submit" class="btn btn-primary" id="modificar-button">Dar de baja al usuario</button> <br>
								</form>
							<?php endif; ?>
						</div>
						<div class="col-xl-3 cuadradomodificar" >
							<h2>Modificar Nombre </h2>
							<br>
							<form class="form" method="POST" action="/Cliente/modificarCliente">
								<input type="hidden" id="idusuario" name="idusuario" value="<?=$idusuario;?>">
								<input type="hidden" id="clave" name="clave" value="nombre">
								<input class="form-control-sm" type="text" placeholder="nombre" id="valor" name="valor" required> <br>
								<br><button type="submit" class="btn btn-primary" id="modificar-button">Cambiar Nombre  </button> <br>
							</form>
						</div>
						<div class="col-xl-3 cuadradomodificar" >
							<h2>Modificar Apellido &zwnj;</h2>
							<br>
							<form class="form" method="POST" action="/Cliente/modificarCliente">
								<input type="hidden" id="idusuario" name="idusuario" value="<?=$idusuario;?>">
								<input type="hidden" id="clave" name="clave" value="apellido">
								<input class="form-control-sm" type="text" placeholder="apellido" id="valor" name="valor" required><br>
								<br><button type="submit" class="btn btn-primary" id="modificar-button">Cambiar Apellido</button><br>
							</form>
						</div>
						<div class="col-xl-3 cuadradomodificar" >
							<h2>Modificar Telefono</h2>
							<br>
							<form class="form" method="POST" action="/Cliente/modificarCliente">
								<input type="hidden" id="idusuario" name="idusuario" value="<?=$idusuario;?>">
								<input type="hidden" id="clave" name="clave" value="telefono">
								<input class="form-control-sm" type="text" placeholder="telefono" id="valor" name="valor" required><br>
								<br><button type="submit" class="btn btn-primary" id="modificar-button">Cambiar Telefono</button><br>
							</form>
						</div>
						<div class="col-xl-3 cuadradomodificar" >
							<h2>Modificar Ciudad &zwnj;</h2>
							<br>
							<form class="form" method="POST" action="/Cliente/modificarCliente">
								<input type="hidden" id="idusuario" name="idusuario" value="<?=$idusuario;?>">
								<input type="hidden" id="clave" name="clave" value="ciudad">
								<input class="form-control-sm" type="text" placeholder="ciudad" id="valor" name="valor" required><br>
								<br><button type="submit" class="btn btn-primary" id="modificar-button">Cambiar Ciudad </button><br>
							</form>
						</div>
						<div class="col-xl-3 cuadradomodificar" >
							<h2>&zwnj; &zwnj; Modificar Calle &zwnj; &zwnj; &zwnj;</h2>
							<br>
							<form class="form" method="POST" action="/Cliente/modificarCliente">
								<input type="hidden" id="idusuario" name="idusuario" value="<?=$idusuario;?>">
								<input type="hidden" id="clave" name="clave" value="calle">
								<input class="form-control-sm" type="text" placeholder="calle" id="valor" name="valor" required><br>
								<br><button type="submit" class="btn btn-primary" id="modificar-button">Cambiar Calle </button><br>
							</form>
						</div>
						<div class="col-xl-3 cuadradomodificar" >
							<h2>&zwnj; Modificar Barrio &zwnj; &zwnj;</h2>
							<br>
							<form class="form" method="POST" action="/Cliente/modificarCliente">
								<input type="hidden" id="idusuario" name="idusuario" value="<?=$idusuario;?>">
								<input type="hidden" id="clave" name="clave" value="barrio">
								<input class="form-control-sm" type="text" placeholder="barrio" id="valor" name="valor" required><br>
								<br><button type="submit" class="btn btn-primary" id="modificar-button">Cambiar Barrio</button><br>
							</form>
						</div>
					</div>
				</div>

			</div>
		</div>
		<script src="<?php echo base_url();?>assets/js/jquery-1.12.4.js"></script>
		<script src="<?php echo base_url();?>assets/js/buscadoreventos.js"></script>
		<script src="<?php echo base_url();?>assets/js/ocultarSidebar.js"></script>
		<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
		<script>
			var tipousuario = <?php echo $this->session->userdata('tipo'); ?>;
			var vista = "cliente";
		</script>
		<script src="<?php echo base_url();?>assets/js/menuprincipal.js"></script>
	</body>
	</html>
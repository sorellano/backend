<!DOCTYPE html>
<html>
<head>
  <title>Modificar evento</title>
  <link href="/assets/css/bootstrap.css" rel="stylesheet"/>
  <link href="/assets/css/estilo.css" rel="stylesheet"/>
  <link rel="icon" href="/assets/img/favicon.ico" type="image/png">
</head>
<body>
  <div class="wrapper">
    <!------------------------------- nav ------------------------------->

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="/">Tickasur</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">Menú
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto menuprincipal">
        </ul>
        <form class="form-inline my-2 my-lg-0">
          <a href="/login/logout" class="btn btn-outline-light my-2 my-sm-0" >Logout</a>
        </form>
      </div>
    </nav>

    <!------------------------------- nav ------------------------------->
    <div class="content">
      <div class="col-md-12"> <br>
            <h1>Modificar evento</h1>
              <br>
        </div>
        <!-- DATOS DEL EVENTO -->
        <div class="col-md-12">
          <div class="row">
            <div class="col-xl-3 cuadradomodificar" >
              <h1>Datos del evento</h1>
              <br>
              <h2 class="card-title">Nombre: <?=$evento['nombre'];?></h2>
              <h2 class="card-title">Categoria: <?=$categoria;?></h2>
              <h2 class="card-title">Prioridad: <?=$evento['prioridad'];?></h2>
              <h2 class="card-title">Descripcion: <?=$evento['descripcion'];?></h2>
              <h2 class="card-title">Lugar: <?=$lugar;?></h2>
              <h2 class="card-title">Imagen: <img src="http://cdn.local/imagenes/imagenevento/<?=$evento['imagen'];?>" alt="Evento sin imagen" height="150" width="150"></h2>

            </div>
          <div class="col-sm my-auto">
                <div class="row">
          <div class="col-xl-3 cuadradomodificar" >
            <h2>Modificar Nombre:</h2><br>
            <form class="form" method="POST" action="/evento/modificarevento">
              <input type="hidden" id="idevento" name="idevento" value="<?=$idevento;?>">
              <input type="hidden" id="clave" name="clave" value="nombre">
              <input class="form-control-sm" type="text" placeholder="nombre" id="valor" name="valor" required><br>
              <br><button type="submit" class="btn btn-primary" id="modificar-button">Modificar</button><br>
            </form>
          </div>
          <div class="col-xl-3 cuadradomodificar" >
            <h2>Modificar Descripcion:</h2>
            <form class="form" method="POST" action="/evento/modificarevento">
              <input type="hidden" id="idevento" name="idevento" value="<?=$idevento;?>">
              <input type="hidden" id="clave" name="clave" value="descripcion">

              <textarea id="valor" name="valor" rows="1" cols="15" ></textarea>

              <!--
              <input class="form-control" type="text" placeholder="descripcion" id="valor" name="valor" required>-->

              <br><br><button type="submit" class="btn btn-primary" id="modificar-button">Modificar</button><br>
            </form>
          </div>
          <div class="col-xl-3 cuadradomodificar" >
            <h2>Modificar Prioridad:</h2>
            <form class="form" method="POST" action="/evento/modificarevento">
              <input type="hidden" id="idevento" name="idevento" value="<?=$idevento;?>">
              <input type="hidden" id="clave" name="clave" value="prioridad">
              Prioridad solamente admite valores del 1 al 99:
              <br><input class="form-control-sm" type="text" placeholder="prioridad" id="valor" name="valor" required><br>
              <br><button type="submit" class="btn btn-primary" id="modificar-button">Modificar</button><br>
            </form>
          </div>
          <div class="col-xl-3 cuadradomodificar" >
            <h2>Modificar Categoria:</h2> <br>
            <form class="form" method="POST" action="/evento/modificarevento">
              <input type="hidden" id="clave" name="clave" value="idcategoria">
              <input type="hidden" id="idevento" name="idevento" value="<?=$idevento;?>">
              <select name="valor" required> <!-- Aca lleno la lista de opciones con las categorias disponibles-->
                <?php echo "<br>"; 
                foreach ($categorias as $categoria) {   
                  echo '<option value="';
                  echo $categoria['idcategoria'];
                  echo '">';
                  echo $categoria['nombre'];
                  echo '</option>';
                }
                ?>
              </select>
              <br><br><button type="submit" class="btn btn-primary" id="modificar-button">Modificar</button><br>
            </form>
          </div>
          <div class="col-xl-6 cuadradomodificar" >
            <h2>Modificar Lugar:</h2>
            <form class="form" method="POST" action="/evento/modificarevento">
              <input type="hidden" id="clave" name="clave" value="idlugar">
              <input type="hidden" id="idevento" name="idevento" value="<?=$idevento;?>">
              <select name="valor" required> <!-- Aca lleno la lista de opciones con las categorias disponibles-->
                <?php echo "<br>"; 
                foreach ($lugares as $lugar) {   
                  echo '<option value="';
                  echo $lugar['idlugar'];
                  echo '">';
                  echo $lugar['nombre'];
                  echo '</option>';
                }
                ?>
              </select>
              <br><br><button type="submit" class="btn btn-primary" id="modificar-button">Modificar</button><br>
            </form>
          </div>
          <div class="col-xl-6 cuadradomodificar" >
            <h2>Modificar Imagen:</h2>
            <form class="form" method="POST" action="/evento/modificarimagen" enctype='multipart/form-data'>
              <input type="hidden" id="idevento" name="idevento" value="<?=$idevento;?>">
              <input type="file" id="imagen" name="imagen" accept=".jpg,.jpeg,.png" size="3MB" required>
              <br><br><button type="submit" class="btn btn-primary" id="modificar-button">Modificar</button><br>
            </form>
          </div>
        </div> 
      </div>
    </div>
  </div>
  <hr>


  <!-- FECHAS Y HORARIOS -->
  <div class="col-md-12">
        <div class="row">
          <div class="col-lg my-auto">
            <h2>Buscar fechas</h2>
            <p> <br>
              <strong>Cantidad a mostrar: </strong>
              <select name="cantidadfechas" id="cantidadfechas">
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
              </select>
              &nbsp;<button class="btn btn-default btn-sm"><big><a href="#" data-toggle="modal" data-target="#agregarfecha" style="text-decoration: none;color:black;">Agregar fecha</a></big></button>
            </p>
            <input type="hidden" value="<?=$idevento?>" id="idevento" name="idevento">
            <div class="table-responsive">
            <table id="tbfechas" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th scope="col">Fecha</th>
                  <th scope="col">Eliminar</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
            </div>
            <div class="paginacionfechas" id="paginacion-estilo">

            </div>
          </div>
        </div>
      </div>

      <hr>

      <!-- TICKETS -->
      <div class="col-md-12">
        <div class="row">
            <div class="col-lg my-auto">
              <h2>Tipo de tickets</h2><br>
              
              <p> <br>
                <input type="text" class="form-control-sm" name="busquedatipoticket" placeholder="Buscar Tickets" />
                &nbsp;
                <strong>Cantidad a mostrar: </strong>
                <select name="cantidadtipotickets" id="cantidadtipotickets">
                  <option value="5">5</option>
                  <option value="10">10</option>
                  <option value="15">15</option>
                </select>
                &nbsp; <button class="btn btn-default btn-sm"><big><a href="#" data-toggle="modal" data-target="#agregartipoticket" style="text-decoration: none;color:black;">Agregar tipo de ticket</a></big></button>
              </p>
              <input type="hidden" value="<?=$idevento?>" id="idevento" name="idevento">
              <div class="table-responsive">
              <table id="tbtipoticket" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th scope="col">IdtipoTicket</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Cantidad</th>
                    <th scope="col">Numerado</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Fecha</th>
                    <th scope="col">Modificar</th>
                    <th scope="col">Eliminar</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
              </div>
              <div class="paginaciontipoticket" id="paginacion-estilo">

              </div>
            </div>
          </div>
        </div> <hr>


      <div class="col-md-12">
        <div class="row">
          <!-- MENUS COMIDAS -->
          <div class="col-lg my-auto">
            <h2>Comidas</h2><br>
            
            <p>
              <input type="text" class="form-control-sm" name="busquedamenu" placeholder="Buscar comidas"/>
              &nbsp;
              <strong>Cantidad a mostrar: </strong>
              <select name="cantidadmenus" id="cantidadmenus">
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
              </select>
              &nbsp;
              <button class="btn btn-default btn-sm"><big><a href="#" data-toggle="modal" data-target="#agregarcomida" style="text-decoration: none;color:black;">Agregar comida</a></big></button>
            </p>
            <input type="hidden" value="<?=$idevento?>" id="idevento" name="idevento">
            <div class="table-responsive">
            <table id="tbmenu" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th scope="col">Idcomida</th>
                  <th scope="col">Nombre</th>
                  <th scope="col">Precio</th>
                  <th scope="col">Cambiar Precio</th>
                  <th scope="col">Eliminar</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
            </div>
            <div class="paginacionmenu" id="paginacion-estilo">

            </div>

          </div><br>
        </div>
      </div><hr>

        <!-- PAQUETES (DESCUENTOS)-->
        <div class="col-md-12">
          <div class="row">
            <div class="col-lg my-auto">
              <h2>Agregar paquetes</h2><br>
              <p><br>
                <input type="text" class="form-control-sm" name="busquedapaquete" placeholder="Buscar Paquetes" />
                &nbsp;
                <strong>Cantidad a mostrar: </strong>
                <select name="cantidadpaquetes" id="cantidadpaquetes">
                  <option value="5">5</option>
                  <option value="10">10</option>
                  <option value="15">15</option>
                </select>
                &nbsp; <button class="btn btn-default btn-sm"><big><a href="#" data-toggle="modal" data-target="#agregarpaquete" style="text-decoration: none;color:black;">Agregar paquete</a></big></button>
              </p>
              <input type="hidden" value="<?=$idevento?>" id="idevento" name="idevento">
              <div class="table-responsive">
              <table id="tbpaquetes" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th scope="col">Idpaquete</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Fecha</th>
                    <th scope="col">Modificar</th>
                    <th scope="col">Eliminar</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
              </div>
              <div class="paginacionpaquete" id="paginacion-estilo">
                
              </div> 
            </div>
            </div>
            </div>
          </div> <!------------- content----------->
          </div> <!-------------- wrapper ------->

          <!------------------------------------------------------------------ MODALFADE-------------------------------------->

          <div id="agregarfecha" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h3>Agregar Fecha</h3>
                  <button type="button" class="close font-weight-light" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                  <form class="form" method="POST" action="/evento/agregarfecha">
                     &nbsp; &nbsp;<input type="hidden" id="idevento" name="idevento" value="<?=$idevento;?>">
                    <input type="date" name="date" min="<?=date('Y-m-d');?>" required>, Hora: <input type="time" name="time" value="12:00" required><br>
                    <div class="modal-footer">
                      <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                      <button type="submit" class="btn btn-primary" id="addfecha-button">Agregar fecha</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>


          <div id="agregarcomida" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h3>Agregar Comida</h3>
                  <button type="button" class="close font-weight-light" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                  <form class="form" method="POST" action="/evento/agregarcomida">
                    <input type="hidden" id="idevento" name="idevento" value="<?=$idevento;?>">
                    <br>
                    Comida : 
                    <select name="comida" required> <!-- Aca lleno la lista de opciones con las categorias disponibles-->
                      <?php echo "<br>"; 
                      foreach ($comidas as $comida) {   
                        echo '<option value="';
                        echo $comida['idcomida'];
                        echo '">';
                        echo $comida['nombre'];
                        echo '</option>';
                      }
                      ?>
                    </select>
                    <br><br>
                    Precio: &nbsp;
                    <input class="form-control-sm" type="text" placeholder="precio" id="precio" name="precio" required>
                    <br> <br>
                    <div class="modal-footer">
                      <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                      <button type="submit" class="btn btn-primary" id="agregar-button">Agregar</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <div id="agregartipoticket" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h3>Agregar Tipo ticket</h3>
                  <button type="button" class="close font-weight-light" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                  <form class="form" method="POST" action="/evento/agregarTipoTicket" enctype='multipart/form-data'>
                  <input type="hidden" id="idevento" name="idevento" value="<?=$idevento;?>">
                  Nombre del tipo de ticket:
                  <input class="form-control" type="text" placeholder="Nombre del tipo de ticket" id="nombre" name="nombre" required>
                  <br>
                  Cantidad de tickets:
                  <input class="form-control" type="text" placeholder="Cantidad de tickets" id="cantidad" name="cantidad" required>
                  <br>
                  Precio unitario de cada ticket:
                  <input class="form-control" type="text" placeholder="Precio unitario de cada ticket" id="precio" name="precio" required>
                  <br>
                  <strong>Numerado: </strong>
                  <select name="numerado" id="numerado">
                    <option value="0">No</option>
                    <option value="1">Si</option>
                  </select><br><br>
                  Referencia:<input type="file" id="imagen" name="imagen" accept=".jpg,.jpeg,.png" size="3MB"><br>
                  <br>
                  Fecha :
                  <select name="fecha" required>
                    <?php echo "<br>"; 
                    foreach ($fechas as $fecha) {   
                      echo '<option value="';
                      echo $fecha['fecha'];
                      echo '">';
                      echo $fecha['fecha'];
                      echo '</option>';
                    }
                    ?>
                  </select><br> <br>
                    <div class="modal-footer">
                      Tanto la fecha asociada al ticket, la cantidad disponible o si el mismo es numerado no son atributos modificables.
                      <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                      <button type="submit" class="btn btn-primary" id="agregar-button">Agregar</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <div id="agregarpaquete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h3>Agregar Paquete</h3>
                  <button type="button" class="close font-weight-light" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                  <form class="form" method="POST" action="/evento/agregarPaquete" enctype='multipart/form-data'>
                <input type="hidden" id="idevento" name="idevento" value="<?=$idevento;?>">
                Nombre del paquete:
                <input class="form-control" type="text" placeholder="Nombre del paquete" id="nombre" name="nombre" required>
                <br>
                Fecha :
                <select name="fecha" required>
                  <?php echo "<br>"; 
                  foreach ($fechas as $fecha) {   
                    echo '<option value="';
                    echo $fecha['fecha'];
                    echo '">';
                    echo $fecha['fecha'];
                    echo '</option>';
                  }
                  ?>
                </select>
                <br><br>
                  <div class="input-group">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" id="imagen" name="imagen" accept=".jpg,.jpeg,.png" size="3MB" aria-describedby="inputGroupFileAddon01">
                      <label class="custom-file-label" for="inputGroupFile01">Imagen del paquete</label>
                    </div>
                  </div> 
                <br> <br>
                    <div class="modal-footer">
                      <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                      <button type="submit" class="btn btn-primary" id="agregar-button">Agregar</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

        <!------------------------------------------------------------------ MODALFADE-------------------------------------->

  <script src="<?php echo base_url();?>assets/js/ocultarSidebar.js"></script>
  <script src="<?php echo base_url();?>assets/js/jquery-1.12.4.js"></script>
  <script src="<?php echo base_url();?>assets/js/buscadorfechas.js"></script>
  <script src="<?php echo base_url();?>assets/js/buscadormenus.js"></script>
  <script src="<?php echo base_url();?>assets/js/buscadortipoticket.js"></script>
  <script src="<?php echo base_url();?>assets/js/buscadorpaquete.js"></script>
  <script src="<?php echo base_url();?>assets/js/buscadoreventos.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
  <script>
      var tipousuario = <?php echo $this->session->userdata('tipo'); ?>;
      var vista = "evento";
    </script>
    <script src="<?php echo base_url();?>assets/js/menuprincipal.js"></script>
</body>
</html>

<!DOCTYPE html>
<html>
<head>
  <title>Modificar empleado</title>
  <link href="/assets/css/bootstrap.css" rel="stylesheet"/>
  <link href="/assets/css/estilo.css" rel="stylesheet"/>
  <link rel="icon" href="/assets/img/favicon.ico" type="image/png">
</head>
<body>
  <div class="wrapper">
     <!------------------------------- nav ------------------------------->

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="/">Tickasur</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">Menú
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto menuprincipal">
            </ul>
            <form class="form-inline my-2 my-lg-0">
              <a href="/login/logout" class="btn btn-outline-light my-2 my-sm-0" >Logout</a>
            </form>
          </div>
        </nav>
        <!------------------------------- nav ------------------------------->
        <div class="content">
          <div class="col-md-12"> <br>
          <h1>Modificar empleado</h1>
          <br>
          <hr>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-xl-3 cuadradomodificar" >
                <br>
                <h1>Datos del empleado</h1>
                <br>
                <h2 class="card-title">Id: <?php echo($empleado['idusuario'])?></h2>
                <h2 class="card-title">Username: <?php echo($empleado['username'])?></h2>
                <h2 class="card-title">Mail <br> <?php echo($empleado['mail'])?></h2>
                <h2 class="card-title">Tipo: <?php echo($empleado['tipo'])?></h2>
              </div>
              <div class="col-lg-3 cuadradomodificar">
                <h2>Modificar username</h2>
                <br> <br>
                <form class="form" method="POST" action="/empleado/modificarempleado">

                  <input class="form-control" type="hidden" value="<?php echo($empleado['idusuario'])?>" id="idusuario" name="idusuario" required>
                  <br>
                  <input class="form-control-sm" type="text" placeholder="username" id="valor" name="valor" required>
                  <br><br>
                  <input type="hidden" name="clave" value="username" />
                  <button type="submit" class="btn btn-primary" id="modificarcliente-button">Ingresar</button> <br>

                </form>
              </div>
              <div class="col-lg-3 cuadradomodificar">
                <h2>Modificar password</h2>
                <br> <br>
                <form class="form" method="POST" action="/empleado/modificarempleado">

                  <input class="form-control" type="hidden" value="<?php echo($empleado['idusuario'])?>" id="idusuario" name="idusuario" required>
                  <br>
                  <input class="form-control-sm" type="text" placeholder="password" id="valor" name="valor" required>
                  <br>
                  <br>
                  <input type="hidden" name="clave" value="password" />
                  <button type="submit" class="btn btn-primary" id="modificarcliente-button">Ingresar</button>
                </form>
              </div>
              <div class="col-lg-3 cuadradomodificar">
               <h2> Modificar tipo</h2>
               <br>
               <form class="form" method="POST" action="/empleado/modificarempleado">
                <input class="form-control" type="hidden" value="<?php echo($empleado['idusuario'])?>" id="idusuario" name="idusuario" required>
                <br>
                <input type="radio" name="valor" value="1" checked="checked">1- Marketing <br>
                <input type="radio" name="valor" value="2">2- Moderador <br>
                <input type="radio" name="valor" value="3">3- Validador <br>
                <input type="radio" name="valor" value="4">4- Organizador<br>
                <br>
                <input type="hidden" name="clave" value="tipo" />
                <button type="submit" class="btn btn-primary" id="modificarcliente-button">Ingresar</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    <script src="<?php echo base_url();?>assets/js/jquery-1.12.4.js"></script>
      <script src="<?php echo base_url();?>assets/js/buscadoreventos.js"></script>
      <script src="<?php echo base_url();?>assets/js/ocultarSidebar.js"></script>
      <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
      <script>
      var tipousuario = <?php echo $this->session->userdata('tipo'); ?>;
      var vista = "empleado";
    </script>
    <script src="<?php echo base_url();?>assets/js/menuprincipal.js"></script>
</body>
</html>
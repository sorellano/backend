$(document).on("ready", main);


function main(){
	idevento = $("#idevento").val();
	mostrarMenu("",1,5,idevento);

	
	$("input[name=busquedamenu]").keyup(function(){
		textobuscar = $(this).val();
		valoroption = $("#cantidadmenus").val();
		mostrarMenu(textobuscar,1,valoroption,idevento);
	});

	$("body").on("click",".paginacionmenu li a",function(e){
		e.preventDefault();
		valorhref = $(this).attr("href");
		valorBuscar = $("input[name=busquedamenu]").val();
		valoroption = $("#cantidadmenus").val();
		mostrarMenu(valorBuscar,valorhref,valoroption,idevento);
	});

	$("#cantidadmenus").change(function(){
		valoroption = $(this).val();
		valorBuscar = $("input[name=busquedamenu]").val();
		mostrarMenu(valorBuscar,1,valoroption,idevento);
	});
}
function mostrarMenu(valorBuscar,pagina,cantidad,idevento){
	$.ajax({
		url : "http://backend.local/evento/listarcomidas",
		type: "POST",
		data: {buscar:valorBuscar,nropagina:pagina,cantidad:cantidad,idevento:idevento},
		dataType:"json",
		success:function(response){
			
			filas = "";
			$.each(response.menus,function(key,item){
				filas+="<tr><td>"+item.idcomida+"</td><td>"+item.nombre+"</td><td>"+item.precio+"</td><td><form class='form' method='POST' action='/evento/modificarprecio'><input type='hidden' value='"+item.idevento+"' id='idevento' name='idevento'><input type='hidden' value='"+item.idcomida+"' id='idcomida' name='idcomida'><input type='text' placeholder='nuevo precio' id='precio' name='precio'>&nbsp;<button type='submit' class='btn btn-primary' id='ingresar-button'>Cambiar</button></form></td><td><a href='/evento/borrarcomida/?idevento="+item.idevento+"&idcomida="+item.idcomida+"'>Eliminar</a></td></tr>";
			});

			$("#tbmenu tbody").html(filas);
			linkseleccionado = Number(pagina);
			//total registros
			totalregistros = response.totalregistros;
			//cantidad de registros por pagina
			cantidadregistros = response.cantidad;

			numerolinks = Math.ceil(totalregistros/cantidadregistros);
			paginador = "<ul class='pagination'>";
			if(linkseleccionado>1)
			{
				paginador+="<li><a href='1'>&laquo;</a></li>";
				paginador+="<li><a href='"+(linkseleccionado-1)+"' '>&lsaquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&laquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&lsaquo;</a></li>";
			}
			//muestro de los enlaces 
			//cantidad de link hacia atras y adelante
 			cant = 3;
 			//inicio de donde se va a mostrar los links
			pagInicio = (linkseleccionado > cant) ? (linkseleccionado - cant) : 1;
			//condicion en la cual establecemos el fin de los links
			if (numerolinks > cant)
			{
				//conocer los links que hay entre el seleccionado y el final
				pagRestantes = numerolinks - linkseleccionado;
				//defino el fin de los links
				pagFin = (pagRestantes > cant) ? (linkseleccionado + cant) :numerolinks;
			}
			else 
			{
				pagFin = numerolinks;
			}

			for (var i = pagInicio; i <= pagFin; i++) {
				if (i == linkseleccionado)
					paginador +="<li class='active'><a href='javascript:void(0)'>"+i+"</a></li>";
				else
					paginador +="<li><a href='"+i+"'>"+i+"</a></li>";
			}
			//condicion para mostrar el boton sigueinte y ultimo
			if(linkseleccionado<numerolinks)
			{
				paginador+="<li><a href='"+(linkseleccionado+1)+"' >&rsaquo;</a></li>";
				paginador+="<li><a href='"+numerolinks+"'>&raquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&rsaquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&raquo;</a></li>";
			}
			
			paginador +="</ul>";
			$(".paginacionmenu").html(paginador);

		}
	});
}
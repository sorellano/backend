$(document).on("ready", obtenerSalasAbiertas(1));

window.intervalosSalasAbiertas = [];

$("body").on("click",".paginaciontablasabiertas li a",function(e){
		e.preventDefault();
		paginaelegida = $(this).attr("href");
		obtenerSalasAbiertas(paginaelegida);
});



function obtenerSalasAbiertas(nropagina){ //Agregar la paginacion
	filassalasabiertas = '';
	var cantidad = 10;
	$.ajax({
		url : "http://backend.local/chat/SalasActivas",
		type: "POST",
		data: {nropagina:nropagina,cantidad:cantidad},
		dataType:"json",
		success:function(response){
			//console.log("Salas Activas:");
			//console.log(response);
			salas = response.salas;
			if (salas == 0 ) {
				filassalasabiertas = '';
			}
			$.each(salas,function(index,sala){
				//console.log(sala);
				filassalasabiertas += '<p id=salaActiva'+sala.idsala+'>';
				filassalasabiertas += '<tr>';
				filassalasabiertas += '<td>'+sala.idsala+'</td>';
				filassalasabiertas += '<td>'+sala.nombreusuario+'</td>';
				filassalasabiertas += '<td>'+sala.ultimomensaje+'</td>';
				filassalasabiertas += '<td><a href="/chat/iralchat/?idsala='+sala.idsala+'">Ir al chat</a></td>';
				filassalasabiertas += '</tr>';
				filassalasabiertas += '</p>';
			});
			$("#tbsalasabiertas tbody").html(filassalasabiertas);

			linkseleccionado = Number(nropagina);
			//total registros
			totalregistros = response.totalregistros;
			//cantidad de registros por pagina
			cantidadregistros = cantidad;

			numerolinks = Math.ceil(totalregistros/cantidadregistros);
			paginador = "<ul class='pagination'>";
			if(linkseleccionado>1)
			{
				paginador+="<li><a href='1'>&laquo;</a></li>";
				paginador+="<li><a href='"+(linkseleccionado-1)+"' '>&lsaquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&laquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&lsaquo;</a></li>";
			}
			//muestro de los enlaces 
			//cantidad de link hacia atras y adelante
 			cant = 3;
 			//inicio de donde se va a mostrar los links
			pagInicio = (linkseleccionado > cant) ? (linkseleccionado - cant) : 1;
			//condicion en la cual establecemos el fin de los links
			if (numerolinks > cant)
			{
				//conocer los links que hay entre el seleccionado y el final
				pagRestantes = numerolinks - linkseleccionado;
				//defino el fin de los links
				pagFin = (pagRestantes > cant) ? (linkseleccionado + cant) :numerolinks;
			}
			else 
			{
				pagFin = numerolinks;
			}

			for (var i = pagInicio; i <= pagFin; i++) {
				if (i == linkseleccionado)
					paginador +="<li class='active'><a href='javascript:void(0)'>"+i+"</a></li>";
				else
					paginador +="<li><a href='"+i+"'>"+i+"</a></li>";
			}
			//condicion para mostrar el boton sigueinte y ultimo
			if(linkseleccionado<numerolinks)
			{
				paginador+="<li><a href='"+(linkseleccionado+1)+"' >&rsaquo;</a></li>";
				paginador+="<li><a href='"+numerolinks+"'>&raquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&rsaquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&raquo;</a></li>";
			}
			
			paginador +="</ul>";
			$(".paginaciontablasabiertas").html(paginador);
			limpiarIntervalos(intervalosSalasAbiertas);
			intervalosSalasAbiertas.push(setInterval('obtenerSalasAbiertas('+nropagina+')', 4000));
		},error: function(xhr, status, error) {
			console.log(xhr.responseText);
		}
	});
}
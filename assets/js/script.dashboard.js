$("#cierre").click(function(){
    $.ajax({
      url : "/utu/includes/php/webservices/logoutUsuario",
      dataType : "json",
      data : {},
      success : function (data) {
        if (data.error == 0){
          console.log(data);
          $('.containerCargando').fadeOut(0);
          $('.container').delay( 800 ).fadeOut(500);
          $('.containerCargando').delay( 800 ).fadeIn( "slow" );
          $.when($('.containerCargando').delay( 800 ).fadeOut( "slow" )).done(function(){
            window.location.replace('index');
          })
        }else{
          console.log('Error: ' + data.textoError)
        }
      },
      error : function (data) {
        console.log('Error');
      },
      type : 'POST'
    });
});
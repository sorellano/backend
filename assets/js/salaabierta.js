$(document).on("ready", empezarChat);

window.intervalos = [];

function empezarChat(){
	cargarHTML(idsala);
}

function cargarHTML(idsala){ //Cargar con los datos default y luego se llaman los metodos que realizan los cambios
	
	htmlchatbox = '<br><div class="container">';
	htmlchatbox += '<div id="datossala" class="row justify-content-md-center">';
	htmlchatbox += 'Cargando informacion de la sala...';
	htmlchatbox += '</div><br>';
	htmlchatbox += '</div><hr>';

	htmlchatbox += '<p>';
	htmlchatbox += '<div style="width:450px;height:250px;overflow:hidden;padding:10px;border:1px double #DEBB07;" id="historial" class="historial">';
	htmlchatbox += 'Espere mientras se inicia la carga de mensajes...';
	htmlchatbox += '</div></p><br/>';

	htmlchatbox += '<div class="container">';
	htmlchatbox += '<div id="mensajebox" class="row justify-content-md-center w-100">';
	htmlchatbox += '<input type="text" name="mensaje" id="mensaje" value="Mensaje" />';
	htmlchatbox +=  '&nbsp;<button class="btn btn-primary" onclick="enviarmensaje('+idsala+',mensaje)">Enviar Mensaje</button>';

	htmlchatbox +=  '&nbsp;<button class="btn btn-primary" onclick="cerrarsala('+idsala+')">Cerrar sala de chat</button>';
	htmlchatbox += '</div>';
	//htmlchatbox +=  '<br><br><button onclick="cargarHTML()">Actualizar el Chat</button>';
	$("#chatbox").html(htmlchatbox);

	//Luego de cargado obtengo los mensajes y el estado de la sala
	//console.log(idsala);
	obtenermensajes(idsala);
	cargarDatosSala(idsala);

	//limpiarIntervalos(intervalos); //Agrego los intervalos de refresco para obtener los mensajes y los datos de la sala
	intervalos.push(setInterval('obtenermensajes('+idsala+')', 4000));
	intervalos.push(setInterval('cargarDatosSala('+idsala+')', 4000));
	

}

function cargarDatosSala(idsala){
	htmlsala = "Actualizando informacion de la sala..."
	$.ajax({
		url : "http://backend.local/chat/estadodelaSala",
		type: "POST",
		data: {idsala:idsala},
		dataType:"json",
		success:function(response){
			//console.log(response);
			var estado = response['estado'];
			//console.log(estado[0]);
			htmlsala = '<h2>';
			htmlsala += 'Id sala: '+estado[0].idsala+'&nbsp;';
			htmlsala += 'Expira : '+estado[0].expira+'&nbsp;';
			htmlsala += 'Cerrada : '+estado[0].cerrada+'';
			htmlsala += '</h2>';
			$("#datossala").html(htmlsala);
			if (estado[0].cerrada == 1){

				htmlsala = 'La sala se cerró<br />';
				$("#datossala").html(htmlsala);
				$("#mensajebox").html("");
			}
			if (response.exipiro){

				htmlsala = 'La sala Expiró<br />';
				$("#datossala").html(htmlsala);
				$("#mensajebox").html("");

			}
		},
		error: function(xhr, status, error) {	
	        alert(xhr.responseText);
	        return false;
		}
	});
}


function obtenermensajes(idsala){
	//console.log("Quiero obtener los mensajes de la sala "+idsala);
	//$("#historial").html("Cargando mensajes...");
	htmlmensajes = "";
	$.ajax({
		url : "http://backend.local/chat/obtenerMensajes",
		type: "POST",
		data: {idsala:idsala},
		dataType:"json",
		success:function(response){
			//console.log(response);
			if (response['exito'] == true){
				$.each(response['mensajes'],function(index,mensaje){
					if(mensaje['tipoautor'] == 'CLIENTE'){
						htmlmensajes +=' <font color="blue">';
						htmlmensajes += "<br>CLIENTE: ";
						htmlmensajes +='</font>';
					}else if(mensaje['tipoautor'] == 'EMPLEADO'){
						htmlmensajes +=' <font color="red">';
						htmlmensajes += "<br>Yo: ";
						htmlmensajes +='</font>';
					}else{
						htmlmensajes +=' <font color="green">';
						htmlmensajes += "<br>INFO: ";
						htmlmensajes +='</font>';
					}
					htmlmensajes += "<br>"+mensaje['mensaje']+"<br>"
					//console.log(mensaje['mensaje']);
				});
				$("#historial").html(htmlmensajes);
			}else{
				limpiarIntervalos(intervalos);
				alert(response['errores'].Error);
			}
		},
		error: function(xhr, status, error) {	
	        alert(xhr.responseText);
	        return false;
		}
	});
}

function enviarmensaje(idsala,mensaje){
	//console.log("Quiero enviar un mensaje: "+mensaje.value);
	$.ajax({
		url : "http://backend.local/chat/enviarMensaje",
		type: "POST",
		data: {idsala:idsala,mensaje:mensaje.value},
		dataType:"json",
		success:function(response){
			//console.log(response); //Insertar mensajes en el box y actualizar instantaneamente
			if (response['exito'] == true){
				mensaje.value = "";
				obtenermensajes(idsala);
			}else{
				limpiarIntervalos(intervalos);
				alert(response['errores'].Error);
			}
		},
		error: function(xhr, status, error) {	
	        alert(xhr.responseText);
	        return false;
		}
	});
}

function cerrarsala(idsala){
	//console.log("Quiero cerrar la sala de chat. "+idsala);
	$.ajax({
		url : "http://backend.local/chat/cerrarSala",
		type: "POST",
		data: {idsala:idsala},
		dataType:"json",
		success:function(response){
			//console.log(response);
			if (response['exito'] == true){
				limpiarIntervalos(intervalos);
				alert("Se cerro la sala con exito");
				window.location.href = "/chat";
			}else{
				limpiarIntervalos(intervalos);
				alert(response['errores'].Error);
			}
		},
		error: function(xhr, status, error) {	
	        alert(xhr.responseText);
	        return false;
		}
	});
}

function limpiarIntervalos(intervals){
	intervals.forEach(clearInterval);

}
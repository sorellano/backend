$(document).on("ready", empezarpaquetes);


function empezarpaquetes(){

	idevento = $("#idevento").val();
	mostrarInformacionpaquetes("",1,5,idevento);	

	$("input[name=busquedapaquete]").keyup(function(){
		textobuscar = $(this).val();
		valoroption = $("#cantidadpaquetes").val();
		mostrarInformacionpaquetes(textobuscar,1,valoroption,idevento);
	});

	$("body").on("click",".paginacionpaquete li a",function(e){
		e.preventDefault();
		valorhref = $(this).attr("href");
		valorBuscar = $("input[name=busquedapaquete]").val();
		valoroption = $("#cantidadpaquetes").val();
		mostrarInformacionpaquetes(valorBuscar,valorhref,valoroption,idevento);
	});

	$("#cantidadpaquetes").change(function(){
		valoroption = $(this).val();
		valorBuscar = $("input[name=busquedapaquete]").val();
		mostrarInformacionpaquetes(valorBuscar,1,valoroption,idevento);
	});

}
function mostrarInformacionpaquetes(valorBuscar,pagina,cantidad,idevento){
	
	$.ajax({
		url : "http://backend.local/Evento/listarPaquetes",
		type: "POST",
		data: {buscar:valorBuscar,nropagina:pagina,cantidad:cantidad,idevento:idevento},
		dataType:"json",
		success:function(response){
			filas = "";
			console.log(response.paquetes);
			$.each(response.paquetes,function(key,item){
				filas+="<tr><td>"+item.idpaquete+"</td><td>"+item.nombre+"</td><td>"+item.fecha+"</td><td><a href='/evento/paquete/?idpaquete="+item.idpaquete+"'>Modificar</a></td><td><a href='/evento/borrarpaquete/?idpaquete="+item.idpaquete+"&idevento="+item.idevento+"'>Eliminar</a></td></tr>";
			});

			$("#tbpaquetes tbody").html(filas);
			linkseleccionado = Number(pagina);
			//total registros
			totalregistros = response.totalregistros;
			//cantidad de registros por pagina
			cantidadregistros = response.cantidad;

			numerolinks = Math.ceil(totalregistros/cantidadregistros);
			paginador = "<ul class='pagination'>";
			if(linkseleccionado>1)
			{
				paginador+="<li><a href='1'>&laquo;</a></li>";
				paginador+="<li><a href='"+(linkseleccionado-1)+"' '>&lsaquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&laquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&lsaquo;</a></li>";
			}
			//muestro de los enlaces 
			//cantidad de link hacia atras y adelante
 			cant = 3;
 			//inicio de donde se va a mostrar los links
			pagInicio = (linkseleccionado > cant) ? (linkseleccionado - cant) : 1;
			//condicion en la cual establecemos el fin de los links
			if (numerolinks > cant)
			{
				//conocer los links que hay entre el seleccionado y el final
				pagRestantes = numerolinks - linkseleccionado;
				//defino el fin de los links
				pagFin = (pagRestantes > cant) ? (linkseleccionado + cant) :numerolinks;
			}
			else 
			{
				pagFin = numerolinks;
			}

			for (var i = pagInicio; i <= pagFin; i++) {
				if (i == linkseleccionado)
					paginador +="<li class='active'><a href='javascript:void(0)'>"+i+"</a></li>";
				else
					paginador +="<li><a href='"+i+"'>"+i+"</a></li>";
			}
			//condicion para mostrar el boton sigueinte y ultimo
			if(linkseleccionado<numerolinks)
			{
				paginador+="<li><a href='"+(linkseleccionado+1)+"' >&rsaquo;</a></li>";
				paginador+="<li><a href='"+numerolinks+"'>&raquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&rsaquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&raquo;</a></li>";
			}
			
			paginador +="</ul>";
			$(".paginacionpaquete").html(paginador);

		}
	});
}
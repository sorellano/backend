function remove_accent(a) {
    return a = a.replace(/\~|\!|\#|\$|\%|\^|\&|\*|\(|\)|\_|\{|\}|\"|\?|\>|\<|\:|\-|\=|\[|\]|\'|\,|\.|\;/g, "");
}

function validateForm() {
    if(validate.Mail + validate.Pass + validate.Name + validate.SurN + validate.Date == 5){
      $("#register-button").prop("disabled", false);
    }else{
      $("#register-button").prop("disabled", true);  
    }
}

function doRegister() {
  var a = remove_accent($("#Nombre").val()),
      b = remove_accent($("#Apellido").val()),
      c = $("#Email").val(),
      d = remove_accent($("#Contrasena").val()),
      e = $(".FechaNacimiento").val();
  console.log(a);
  console.log(b);
  console.log(c);
  console.log(d);
  console.log(e);
  
  var url = "/utu/includes/php/webservices/createUsuario";
  var data = {
    Nombre: a,
    Apellido: b,
    Email: c,
    Contrasena: d,
    FechaNacimiento: e
  }
  var dataType = "json"
  
  $.ajax({
    type: "POST",
    url: url,
    data: data,
    success: function(a) {
      if (a.error == 0){
        $("#EmailLogin").removeClass("box-alert");
        $("#ContrasenaLogin").removeClass("box-alert");
        $(".container").css("padding-top", "30vh")
        $(".register").fadeOut(200);
        $(".form").fadeIn(800);
      }
      console.log(a)
    },
    error: function(a) {
      console.log("error")
    },
    dataType: dataType
  });
}

var validate = {
    Mail: 0,
    Pass: 0,
    Name: 0,
    SurN: 0,
    Date: 0
};

$("#register-button").prop("disabled", true);
$("#login-button").click(function() {
    $.ajax({
        url: "/utu/includes/php/webservices/loginUsuario",
        dataType: "json",
        data: {
            Email: $("#EmailLogin").val(),
            Contrasena: $("#ContrasenaLogin").val()
        },
        success: function(a) {
            if (a.error == 0){
              $("#EmailLogin").addClass("box-success");
              $("#ContrasenaLogin").addClass("box-success");
              console.log(a);
//              $(".card-login").fadeOut(500)
              $.when($(".content").delay(800).fadeOut(500)).   done(function() {
                  $(".containerCargando").delay(800).fadeIn("slow");
                }).done(function(){
              $.when($(".containerCargando").delay(800).fadeOut("slow")).   done(function() {
                  window.location.replace("enfermedades");
                })
              })
              
            }else{
              $("#EmailLogin").addClass("box-alert");
              $("#ContrasenaLogin").addClass("box-alert");
            }
        },
        error: function(a) {
            console.log("Error")
        },
        type: "POST"
    })
})

$("#register-button1").click(function(a) {
      a.preventDefault(); 
      $(".card-login").fadeOut(200);
      $(".card-register").fadeIn(800);
})

$("#volver-login").click(function(a) {
    a.preventDefault();
    $(".card-register").fadeOut(200);
    $(".card-login").fadeIn(800);
})

$("#Email").keyup(function() {
    if ($("#Email").val() != $("#ConfirmacionEmail").val()){
      $("#Email").addClass("box-alert");
      $("#ConfirmacionEmail").addClass("box-alert");
      validate.Mail = 0;
    }else{
      $("#Email").removeClass("box-alert");
      $("#ConfirmacionEmail").removeClass("box-alert");
      validate.Mail = 1;
    }
    validateForm();
})

$("#ConfirmacionEmail").keyup(function() {
    if ($("#Email").val() != $("#ConfirmacionEmail").val()){
      $("#Email").addClass("box-alert");
      $("#ConfirmacionEmail").addClass("box-alert");
      validate.Mail = 0;  
    }else{
      $("#Email").removeClass("box-alert");
      $("#ConfirmacionEmail").removeClass("box-alert");
      validate.Mail = 1
    }
    validateForm();
})

$("#Contrasena").keyup(function() {
    if (remove_accent($("#Contrasena").val()) != remove_accent($("#ConfirmacionContrasena").val())){
      $("#Contrasena").addClass("box-alert");
      $("#ConfirmacionContrasena").addClass("box-alert");
      validate.Pass = 0;
    }else{
      $("#Contrasena").removeClass("box-alert");
      $("#ConfirmacionContrasena").removeClass("box-alert");
      validate.Pass = 1;    
    }
    validateForm()
})

$("#ConfirmacionContrasena").keyup(function() {
  if (remove_accent($("#Contrasena").val()) != remove_accent($("#ConfirmacionContrasena").val())){
    $("#Contrasena").addClass("box-alert");
    $("#ConfirmacionContrasena").addClass("box-alert");
    validate.Pass = 0;
  }else{
    $("#Contrasena").removeClass("box-alert");
    $("#ConfirmacionContrasena").removeClass("box-alert");
    validate.Pass = 1;
  } 
  validateForm();
})

$("#Nombre").keyup(function() {
  if (remove_accent($("#Nombre").val()).length < 3){
    $("#Nombre").addClass("box-alert");
    validate.Name = 0
  }else{
    $("#Nombre").removeClass("box-alert");
    validate.Name = 1;
  }
  validateForm();
})

$("#Apellido").keyup(function() {
  if (remove_accent($("#Apellido").val()).length < 3){
    $("#Apellido").addClass("box-alert");
    validate.SurN = 0;
  }else{
    $("#Apellido").removeClass("box-alert");
    validate.SurN = 1;
  }  
  validateForm();
})

$(".FechaNacimiento").change(function() {
  Nacimiento = new Date($(".FechaNacimiento").val());
  var a = new Date,
      b = Math.floor((a - Nacimiento) / 315576e5);
  console.log(" Edad: " + b);
  if (b < 16){
    $(".FechaNacimiento").addClass("box-alert");
    validate.Date = 0;
  }else{
    $(".FechaNacimiento").removeClass("box-alert");
    validate.Date = 1;
  }
  validateForm();
});

$("#register-button").click(function() {
    doRegister();
});
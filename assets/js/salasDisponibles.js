$(document).on("ready", actualizarSalas(1));

window.intervalosSalasDisponibles = [];

$("body").on("click",".paginaciontablasdisponibles li a",function(e){
		e.preventDefault();
		paginaelegida = $(this).attr("href");
		actualizarSalas(paginaelegida);
});



function actualizarSalas(nropagina){
	filas = '';
	var cantidad = 10;
	$.ajax({
		url : "http://backend.local/chat/salasdisponibles",
		type: "POST",
		data: {nropagina:nropagina,cantidad:cantidad},
		dataType:"json",
		success:function(response){
			//console.log("Salas disponibles:");
			//console.log(response);
			salas = response.salas;
			if (salas == 0 ) {
				filas = '';
			}
			$.each(salas,function(index,sala){
				//console.log(sala);
				filas += '<p id=sala'+sala.idsala+'>';
				filas += '<tr>';
				filas += '<td>'+sala.idsala+'</td>';
				filas += '<td>'+sala.idcliente+'</td>';
				filas += '<td><button onclick="elegirSala('+sala.idsala+')">Seleccionar</button></td>';
				filas += '</tr>';
				filas += '</p>';
			});
			$("#tbsalasdisponibles tbody").html(filas);
			linkseleccionado = Number(nropagina);
			//total registros
			totalregistros = response.totalregistros;
			//cantidad de registros por pagina
			cantidadregistros = cantidad;

			numerolinks = Math.ceil(totalregistros/cantidadregistros);
			paginador = "<ul class='pagination'>";
			if(linkseleccionado>1)
			{
				paginador+="<li><a href='1'>&laquo;</a></li>";
				paginador+="<li><a href='"+(linkseleccionado-1)+"' '>&lsaquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&laquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&lsaquo;</a></li>";
			}
			//muestro de los enlaces 
			//cantidad de link hacia atras y adelante
 			cant = 3;
 			//inicio de donde se va a mostrar los links
			pagInicio = (linkseleccionado > cant) ? (linkseleccionado - cant) : 1;
			//condicion en la cual establecemos el fin de los links
			if (numerolinks > cant)
			{
				//conocer los links que hay entre el seleccionado y el final
				pagRestantes = numerolinks - linkseleccionado;
				//defino el fin de los links
				pagFin = (pagRestantes > cant) ? (linkseleccionado + cant) :numerolinks;
			}
			else 
			{
				pagFin = numerolinks;
			}

			for (var i = pagInicio; i <= pagFin; i++) {
				if (i == linkseleccionado)
					paginador +="<li class='active'><a href='javascript:void(0)'>"+i+"</a></li>";
				else
					paginador +="<li><a href='"+i+"'>"+i+"</a></li>";
			}
			//condicion para mostrar el boton sigueinte y ultimo
			if(linkseleccionado<numerolinks)
			{
				paginador+="<li><a href='"+(linkseleccionado+1)+"' >&rsaquo;</a></li>";
				paginador+="<li><a href='"+numerolinks+"'>&raquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&rsaquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&raquo;</a></li>";
			}
			
			paginador +="</ul>";
			$(".paginaciontablasdisponibles").html(paginador);
			limpiarIntervalos(intervalosSalasDisponibles);
			intervalosSalasDisponibles.push(setInterval('actualizarSalas('+nropagina+')', 4000));
		}
	});
}

function elegirSala(idsala){
	//console.log("quiero elegir la sala "+idsala);
	$.ajax({
		url : "http://backend.local/chat/elegirsala",
		type: "POST",
		data: {idsala:idsala},
		dataType:"json",
		success:function(response){
			//console.log(response);
			if (response['exito'] == true){
				//console.log("Sala "+idsala+" elegida con exito");
				actualizarSalas(1);
			}else{
				alert(response['errores'].ERROR);
			}
		}
	});
}
$(document).on("ready", main);


function main(){
	mostrarDatos("",1,5,0,0);
	
	$("input[name=busqueda]").keyup(function(){
		textobuscar = $(this).val();
		valoroption = $("#cantidad").val();
		baja = $("#baja").val();
		vigentes = $("#vigentes").val();
		mostrarDatos(textobuscar,1,valoroption,baja,vigentes);
	});

	$("body").on("click",".paginacion li a",function(e){
		e.preventDefault();
		valorhref = $(this).attr("href");
		valorBuscar = $("input[name=busqueda]").val();
		valoroption = $("#cantidad").val();
		baja = $("#baja").val();
		vigentes = $("#vigentes").val();
		mostrarDatos(valorBuscar,valorhref,valoroption,baja,vigentes);
	});

	$("#cantidad").change(function(){
		valoroption = $(this).val();
		valorBuscar = $("input[name=busqueda]").val();
		baja = $("#baja").val();
		vigentes = $("#vigentes").val();	
		mostrarDatos(valorBuscar,1,valoroption,baja,vigentes);
	});

	$("#baja").change(function(){
		baja = $(this).val();
		valorBuscar = $("input[name=busqueda]").val();
		valoroption = $("#cantidad").val();
		vigentes = $("#vigentes").val();
		mostrarDatos(valorBuscar,1,valoroption,baja,vigentes);
	});

	$("#vigentes").change(function(){
		baja = $("#baja").val();
		valorBuscar = $("input[name=busqueda]").val();
		valoroption = $("#cantidad").val();
		vigentes = $(this).val();
		mostrarDatos(valorBuscar,1,valoroption,baja,vigentes);
	});
}
function mostrarDatos(valorBuscar,pagina,cantidad,baja,vigentes){
	$.ajax({
		url : "http://backend.local/evento/buscar",
		type: "POST",
		data: {buscar:valorBuscar,nropagina:pagina,cantidad:cantidad,baja:baja,vigentes:vigentes},
		dataType:"json",
		success:function(response){
			filas = "";
			$.each(response.eventos,function(key,item){
				filas+="<tr><td>"+item.idevento+"</td><td>"+item.nombre+"</td><td><a href='evento/modificar/?idevento="+item.idevento+"'>Modificar</td>";

				if(item.baja==0){
					filas+="<td><a href='evento/baja/?idevento="+item.idevento+"'>Dar de baja</td>";
				}else{
					filas+="<td><a href='evento/altalogica/?idevento="+item.idevento+"'>Dar de alta logica</td>";
				}
				filas+="</tr>";
			});

			$("#tbeventos tbody").html(filas);
			linkseleccionado = Number(pagina);
			//total registros
			totalregistros = response.totalregistros;
			//cantidad de registros por pagina
			cantidadregistros = response.cantidad;

			numerolinks = Math.ceil(totalregistros/cantidadregistros);
			paginador = "<ul class='pagination'>";
			if(linkseleccionado>1)
			{
				paginador+="<li><a href='1'>&laquo;</a></li>";
				paginador+="<li><a href='"+(linkseleccionado-1)+"' '>&lsaquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&laquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&lsaquo;</a></li>";
			}
			//muestro de los enlaces 
			//cantidad de link hacia atras y adelante
 			cant = 3;
 			//inicio de donde se va a mostrar los links
			pagInicio = (linkseleccionado > cant) ? (linkseleccionado - cant) : 1;
			//condicion en la cual establecemos el fin de los links
			if (numerolinks > cant)
			{
				//conocer los links que hay entre el seleccionado y el final
				pagRestantes = numerolinks - linkseleccionado;
				//defino el fin de los links
				pagFin = (pagRestantes > cant) ? (linkseleccionado + cant) :numerolinks;
			}
			else 
			{
				pagFin = numerolinks;
			}

			for (var i = pagInicio; i <= pagFin; i++) {
				if (i == linkseleccionado)
					paginador +="<li class='active'><a href='javascript:void(0)'>"+i+"</a></li>";
				else
					paginador +="<li><a href='"+i+"'>"+i+"</a></li>";
			}
			//condicion para mostrar el boton sigueinte y ultimo
			if(linkseleccionado<numerolinks)
			{
				paginador+="<li><a href='"+(linkseleccionado+1)+"' >&rsaquo;</a></li>";
				paginador+="<li><a href='"+numerolinks+"'>&raquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&rsaquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&raquo;</a></li>";
			}
			
			paginador +="</ul>";
			$(".paginacion").html(paginador);

		}
	});
}
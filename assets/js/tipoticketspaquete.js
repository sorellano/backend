$(document).on("ready", empezar);


function empezar(){

	idpaquete = $("#idpaquete").val();
	mostrarInformacion("",1,5,idpaquete);	

	$("input[name=busquedatipoticket]").keyup(function(){
		textobuscar = $(this).val();
		valoroption = $("#cantidadtipotickets").val();
		mostrarInformacion(textobuscar,1,valoroption,idpaquete);
	});

	$("body").on("click",".paginaciontipoticket li a",function(e){
		e.preventDefault();
		valorhref = $(this).attr("href");
		valorBuscar = $("input[name=busquedatipoticket]").val();
		valoroption = $("#cantidadtipotickets").val();
		mostrarInformacion(valorBuscar,valorhref,valoroption,idpaquete);
	});

	$("#cantidadtipotickets").change(function(){
		valoroption = $(this).val();
		valorBuscar = $("input[name=busquedatipoticket]").val();
		mostrarInformacion(valorBuscar,1,valoroption,idpaquete);
	});

}
function mostrarInformacion(valorBuscar,pagina,cantidad,idpaquete){
	
	$.ajax({
		url : "http://backend.local/paquete/listarTipoTickets",
		type: "POST",
		data: {buscar:valorBuscar,nropagina:pagina,cantidad:cantidad,idpaquete:idpaquete},
		dataType:"json",
		success:function(response){
			filas = "";
			//console.log(response);
			$.each(response.tipotickets,function(key,item){
				filas+="<tr><td>"+item.idtipoticket+"</td><td>"+item.nombre+"</td><td>"+item.cantidad+"</td><td>"+item.precio+"</td><td>"+item.descuento+"</td><td><form class='form' method='POST' action='/paquete/modificarTipoticket'><input type='hidden' value='"+item.idpaquete+"' id='idpaquete' name='idpaquete'><input type='hidden' value='descuento' id='clave' name='clave'><input type='hidden' value='"+item.idtipoticket+"' id='idtipoticket' name='idtipoticket'><input type='hidden' value='"+item.idevento+"' id='idevento' name='idevento'><input type='text' placeholder='nuevo descuento' id='valor' name='valor'>&nbsp;<button type='submit' class='btn btn-primary' id='ingresar-button'>Cambiar</button></form></td><td><form class='form' method='POST' action='/paquete/modificarTipoticket'><input type='hidden' value='"+item.idpaquete+"' id='idpaquete' name='idpaquete'><input type='hidden' value='cantidad' id='clave' name='clave'><input type='hidden' value='"+item.idtipoticket+"' id='idtipoticket' name='idtipoticket'><input type='hidden' value='"+item.idevento+"' id='idevento' name='idevento'><input type='text' placeholder='nueva cantidad' id='valor' name='valor'>&nbsp;<button type='submit' class='btn btn-primary' id='ingresar-button'>Cambiar</button></form></td><td><a href='/paquete/borrartipoticket/?idtipoticket="+item.idtipoticket+"&idpaquete="+item.idpaquete+"'>Eliminar</a></td></tr>";
			});

			$("#tbtipoticket tbody").html(filas);
			linkseleccionado = Number(pagina);
			//total registros
			totalregistros = response.totalregistros;
			//cantidad de registros por pagina
			cantidadregistros = response.cantidad;

			numerolinks = Math.ceil(totalregistros/cantidadregistros);
			paginador = "<ul class='pagination'>";
			if(linkseleccionado>1)
			{
				paginador+="<li><a href='1'>&laquo;</a></li>";
				paginador+="<li><a href='"+(linkseleccionado-1)+"' '>&lsaquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&laquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&lsaquo;</a></li>";
			}
			//muestro de los enlaces 
			//cantidad de link hacia atras y adelante
 			cant = 3;
 			//inicio de donde se va a mostrar los links
			pagInicio = (linkseleccionado > cant) ? (linkseleccionado - cant) : 1;
			//condicion en la cual establecemos el fin de los links
			if (numerolinks > cant)
			{
				//conocer los links que hay entre el seleccionado y el final
				pagRestantes = numerolinks - linkseleccionado;
				//defino el fin de los links
				pagFin = (pagRestantes > cant) ? (linkseleccionado + cant) :numerolinks;
			}
			else 
			{
				pagFin = numerolinks;
			}

			for (var i = pagInicio; i <= pagFin; i++) {
				if (i == linkseleccionado)
					paginador +="<li class='active'><a href='javascript:void(0)'>"+i+"</a></li>";
				else
					paginador +="<li><a href='"+i+"'>"+i+"</a></li>";
			}
			//condicion para mostrar el boton sigueinte y ultimo
			if(linkseleccionado<numerolinks)
			{
				paginador+="<li><a href='"+(linkseleccionado+1)+"' >&rsaquo;</a></li>";
				paginador+="<li><a href='"+numerolinks+"'>&raquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&rsaquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&raquo;</a></li>";
			}
			
			paginador +="</ul>";
			$(".paginaciontipoticket").html(paginador);

		}
	});
}
$(document).on("ready", cargarmenu);

function cargarmenu(){
 
  //console.log(tipousuario);
  //console.log(vista);

  if (tipousuario == 1){ //Marketing
    var arraylinks = ['informes','categoria','metodopago','comida','lugar','saldo'];
    var nombres = ['Informes','Categoria','Metodopago','Comida','Lugar','Saldo'];
  }else if (tipousuario == 2){ //Moderador
    var arraylinks = ['cliente','chat'];
    var nombres = ['Cliente','Chat'];
  }else if (tipousuario == 3){ //Validador
    var arraylinks = ['validacion'];
    var nombres = ['Validacion'];
  }else if (tipousuario == 4){ //Organizador
    var arraylinks = ['evento','informes'];
    var nombres = ['Evento','Informes'];
  }else if (tipousuario == 5){ //Root
    var arraylinks = ['empleado/empleados','categoria','comida','lugar','metodopago','evento','saldo','cliente','validacion','informes','chat'];
    var nombres = ['Empleados','Categoria','Comida','Lugar','Metodopago','Evento','Saldo','Cliente','Validacion','Informes','Chat'];
  }else{
    alert("No se reconoce el tipo de usuario "+tipousuario);
    var arraylinks = [];
    var nombres = [];
  }

  html="";
  for (var i = arraylinks.length - 1; i >= 0; i--) {
    html+="<li class='nav-item'>";
    
    //console.log(arraylinks[i]);

    var href= arraylinks[i];
    var nombre= nombres[i];

    

    if (arraylinks[i] === vista){
      var a='<a class="nav-link active">'+nombre+'</a>';
    }else{
      var a='<a class="nav-link" href="/'+href+'">'+nombre+'</a>'
    }

    html+=a;
    html+="</li>";
  }

  $(".menuprincipal").html(html);
}
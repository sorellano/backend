$(document).on("ready", inicio);


function inicio(){
	idevento = $("#idevento").val();
	mostrarData(1,5,idevento);	

	$("body").on("click",".paginacionfechas li a",function(e){
		e.preventDefault();
		valorhref = $(this).attr("href");
		valoroption = $("#cantidadfechas").val();
		mostrarData(valorhref,valoroption,idevento);
	});

	$("#cantidadfechas").change(function(){
		valoroption = $(this).val();
		mostrarData(1,valoroption,idevento);
	});

}
function mostrarData(pagina,cantidad,idevento){

	$.ajax({
		url : "http://backend.local/Evento/listarfechas",
		type: "POST",
		data: {nropagina:pagina,cantidad:cantidad,idevento:idevento},
		dataType:"json",
		success:function(response){
			filas = "";
			$.each(response.fechas,function(key,item){
				filas+="<tr><td>"+item.fecha+"</td><td><form class='form' method='POST' action='/evento/borrarfecha'><input type='hidden' id='idevento' name='idevento' value='"+idevento+"'><input type='hidden' id='funcion' name='funcion' value='"+item.fecha+"'> <button type='button' onclick='javascript:confirmar(this.form);' class='btn btn-primary' id='eliminar-button'>Eliminar</button></form></td></tr>";
			});

			$("#tbfechas tbody").html(filas);
			linkseleccionado = Number(pagina);
			//total registros
			totalregistros = response.totalregistros;
			//cantidad de registros por pagina
			cantidadregistros = response.cantidad;

			numerolinks = Math.ceil(totalregistros/cantidadregistros);
			paginador = "<ul class='pagination'>";
			if(linkseleccionado>1)
			{
				paginador+="<li><a href='1'>&laquo;</a></li>";
				paginador+="<li><a href='"+(linkseleccionado-1)+"' '>&lsaquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&laquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&lsaquo;</a></li>";
			}
			//muestro de los enlaces 
			//cantidad de link hacia atras y adelante
 			cant = 3;
 			//inicio de donde se va a mostrar los links
			pagInicio = (linkseleccionado > cant) ? (linkseleccionado - cant) : 1;
			//condicion en la cual establecemos el fin de los links
			if (numerolinks > cant)
			{
				//conocer los links que hay entre el seleccionado y el final
				pagRestantes = numerolinks - linkseleccionado;
				//defino el fin de los links
				pagFin = (pagRestantes > cant) ? (linkseleccionado + cant) :numerolinks;
			}
			else 
			{
				pagFin = numerolinks;
			}

			for (var i = pagInicio; i <= pagFin; i++) {
				if (i == linkseleccionado)
					paginador +="<li class='active'><a href='javascript:void(0)'>"+i+"</a></li>";
				else
					paginador +="<li><a href='"+i+"'>"+i+"</a></li>";
			}
			//condicion para mostrar el boton sigueinte y ultimo
			if(linkseleccionado<numerolinks)
			{
				paginador+="<li><a href='"+(linkseleccionado+1)+"' >&rsaquo;</a></li>";
				paginador+="<li><a href='"+numerolinks+"'>&raquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&rsaquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&raquo;</a></li>";
			}
			
			paginador +="</ul>";
			$(".paginacionfechas").html(paginador);

		}
	});
}

function confirmar(form){
        

	if(confirm("Eliminar una fecha dejara sin efecto todos los tickets vendidos para la misma!"))
    form.submit();
  else
    alert("No se realizaron cambios");
}
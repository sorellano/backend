$(document).on("ready", metodospagopaquete);

function metodospagopaquete(){

	idpaquete = $("#idpaquete").val();
	metodosdepago("",1,5,idpaquete);	

	$("input[name=busquedametodopago]").keyup(function(){
		textobuscar = $(this).val();
		valoroption = $("#cantidadmetodopago").val();
		metodosdepago(textobuscar,1,valoroption,idpaquete);
	});

	$("body").on("click",".paginacionmetodopago li a",function(e){
		e.preventDefault();
		valorhref = $(this).attr("href");
		valorBuscar = $("input[name=busquedametodopago]").val();
		valoroption = $("#cantidadmetodopago").val();
		metodosdepago(valorBuscar,valorhref,valoroption,idpaquete);
	});

	$("#cantidadmetodopago").change(function(){
		valoroption = $(this).val();
		valorBuscar = $("input[name=busquedametodopago]").val();
		metodosdepago(valorBuscar,1,valoroption,idpaquete);
	});

}


function metodosdepago(valorBuscar,pagina,cantidad,idpaquete){
	$.ajax({
		url : "http://backend.local/Paquete/listarmetodosdepago",
		type: "POST",
		data: {buscar:valorBuscar,nropagina:pagina,cantidad:cantidad,idpaquete:idpaquete},
		dataType:"json",
		success:function(response){
			filas = "";
			$.each(response.metodospago,function(key,item){

				filas+="<tr><td>"+item.idmetodopago+"</td><td>"+item.nombre+"</td><td><a href='/paquete/borrarmetodopago/?idpaquete="+item.idpaquete+"&idmetodopago="+item.idmetodopago+"'>Eliminar</a></td></tr>";
			});

			$("#tbmetodopago tbody").html(filas);
			linkseleccionado = Number(pagina);
			//total registros
			totalregistros = response.totalregistros;
			//cantidad de registros por pagina
			cantidadregistros = response.cantidad;

			numerolinks = Math.ceil(totalregistros/cantidadregistros);
			paginador = "<ul class='pagination'>";
			if(linkseleccionado>1)
			{
				paginador+="<li><a href='1'>&laquo;</a></li>";
				paginador+="<li><a href='"+(linkseleccionado-1)+"' '>&lsaquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&laquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&lsaquo;</a></li>";
			}
			//muestro de los enlaces 
			//cantidad de link hacia atras y adelante
 			cant = 3;
 			//inicio de donde se va a mostrar los links
			pagInicio = (linkseleccionado > cant) ? (linkseleccionado - cant) : 1;
			//condicion en la cual establecemos el fin de los links
			if (numerolinks > cant)
			{
				//conocer los links que hay entre el seleccionado y el final
				pagRestantes = numerolinks - linkseleccionado;
				//defino el fin de los links
				pagFin = (pagRestantes > cant) ? (linkseleccionado + cant) :numerolinks;
			}
			else 
			{
				pagFin = numerolinks;
			}

			for (var i = pagInicio; i <= pagFin; i++) {
				if (i == linkseleccionado)
					paginador +="<li class='active'><a href='javascript:void(0)'>"+i+"</a></li>";
				else
					paginador +="<li><a href='"+i+"'>"+i+"</a></li>";
			}
			//condicion para mostrar el boton sigueinte y ultimo
			if(linkseleccionado<numerolinks)
			{
				paginador+="<li><a href='"+(linkseleccionado+1)+"' >&rsaquo;</a></li>";
				paginador+="<li><a href='"+numerolinks+"'>&raquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&rsaquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&raquo;</a></li>";
			}
			
			paginador +="</ul>";
			$(".paginacionmetodopago").html(paginador);

		}
	});
}
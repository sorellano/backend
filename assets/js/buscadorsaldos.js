$(document).on("ready", main);


function main(){

	mostrarDatos("","",1,5,0);
	
	$("input[name=busqueda]").keyup(function(){
		valorBuscar = $(this).val();
		fecha = $("input[name=fecha]").val();
		valoroption = $("#cantidad").val();
		procesados = $("#procesados").val();
		mostrarDatos(valorBuscar,fecha,1,valoroption,procesados);
	});

	$("#fecha").change(function(){
		fecha = $(this).val();
		console.log(fecha);
		valorBuscar = $("input[name=busqueda]").val();
		valoroption = $("#cantidad").val();
		procesados = $("#procesados").val();
		mostrarDatos(valorBuscar,fecha,1,valoroption,procesados);
	});

	$("body").on("click",".paginacion li a",function(e){
		e.preventDefault();
		valorhref = $(this).attr("href");
    	fecha = $("input[name=fecha]").val();
    	valorBuscar = $("input[name=busqueda]").val();
		valoroption = $("#cantidad").val();
		procesados = $("#procesados").val();
		mostrarDatos(valorBuscar,fecha,valorhref,valoroption,procesados);
	});

	$("#cantidad").change(function(){
		valoroption = $(this).val();
    	fecha = $("input[name=fecha]").val();
    	valorBuscar = $("input[name=busqueda]").val();
		procesados = $("#procesados").val();
		mostrarDatos(valorBuscar,fecha,1,valoroption,procesados);
	});

	$("#procesados").change(function(){
		procesados = $("#procesados").val();
    	fecha = $("input[name=fecha]").val();
    	valorBuscar = $("input[name=busqueda]").val();
		valoroption = $("#cantidad").val();
		mostrarDatos(valorBuscar,fecha,1,valoroption,procesados);
	});
}
function mostrarDatos(busqueda,fecha,pagina,cantidad,procesados){
	$.ajax({
		url : "http://backend.local/saldo/buscar",
		type: "POST",
		data: {buscar:busqueda,fecha:fecha,nropagina:pagina,cantidad:cantidad,procesados:procesados},
		dataType:"json",
		success:function(response){
			console.log(response);
			filas = "";
			$.each(response.saldos,function(key,item){
				filas+="<tr><td>"+item.idrecarga+"</td><td>"+item.saldo+"</td><td>"+item.aprobado+"</td><td>"+item.idusuario+"</td><td>"+item.fecha+"</td><td>"+item.idmetodopago+" : "+item.nombre+"</td>";
				
				if (item.aprobado == 1) {
					filas+= "<td><a href='saldo/aprobar/?idrecarga="+item.idrecarga+"'>Aprobar<br><a href='saldo/rechazar/?idrecarga="+item.idrecarga+"'>Rechazar</td></tr>";
				}else{
					filas+= "<td>La recarga ya está procesada</td></tr>";
				}

				

			});

			$("#tbsaldos tbody").html(filas);
			linkseleccionado = Number(pagina);
			//total registros
			totalregistros = response.totalregistros;
			//cantidad de registros por pagina
			cantidadregistros = response.cantidad;

			numerolinks = Math.ceil(totalregistros/cantidadregistros);
			paginador = "<ul class='pagination'>";
			if(linkseleccionado>1)
			{
				paginador+="<li><a href='1'>&laquo;</a></li>";
				paginador+="<li><a href='"+(linkseleccionado-1)+"' '>&lsaquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&laquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&lsaquo;</a></li>";
			}
			//muestro de los enlaces 
			//cantidad de link hacia atras y adelante
 			cant = 3;
 			//inicio de donde se va a mostrar los links
			pagInicio = (linkseleccionado > cant) ? (linkseleccionado - cant) : 1;
			//condicion en la cual establecemos el fin de los links
			if (numerolinks > cant)
			{
				//conocer los links que hay entre el seleccionado y el final
				pagRestantes = numerolinks - linkseleccionado;
				//defino el fin de los links
				pagFin = (pagRestantes > cant) ? (linkseleccionado + cant) :numerolinks;
			}
			else 
			{
				pagFin = numerolinks;
			}

			for (var i = pagInicio; i <= pagFin; i++) {
				if (i == linkseleccionado)
					paginador +="<li class='active'><a href='javascript:void(0)'>"+i+"</a></li>";
				else
					paginador +="<li><a href='"+i+"'>"+i+"</a></li>";
			}
			//condicion para mostrar el boton sigueinte y ultimo
			if(linkseleccionado<numerolinks)
			{
				paginador+="<li><a href='"+(linkseleccionado+1)+"' >&rsaquo;</a></li>";
				paginador+="<li><a href='"+numerolinks+"'>&raquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&rsaquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&raquo;</a></li>";
			}
			
			paginador +="</ul>";
			$(".paginacion").html(paginador);

		}
	});
}
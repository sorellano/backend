$(document).on("ready", obtenerSalasCerradas(1));

window.intervalosSalasCerradas = [];

$("body").on("click",".paginaciontablascerradas li a",function(e){
		e.preventDefault();
		paginaelegida = $(this).attr("href");
		obtenerSalasCerradas(paginaelegida);
});



function obtenerSalasCerradas(nropagina){ //Agregar la paginacion
	filassalascerradas = '';
	var cantidad = 10;
	$.ajax({
		url : "http://backend.local/chat/SalasCerradas",
		type: "POST",
		data: {nropagina:nropagina,cantidad:cantidad},
		dataType:"json",
		success:function(response){
			//console.log("Salas cerradas:");
			//console.log(response);
			salas = response.salas;
			if (salas == 0 ) {
				filassalascerradas = '';
			}
			$.each(salas,function(index,sala){
				//console.log(sala);
				filassalascerradas += '<p id=salacerrada'+sala.idsala+'>';
				filassalascerradas += '<tr>';
				filassalascerradas += '<td>'+sala.idsala+'</td>';
				filassalascerradas += '<td>'+sala.idcliente+'</td>';
				filassalascerradas += '<td><a href="/chat/ObtenerMensajesSalaCerrada/?idsala='+sala.idsala+'">Ver mensajes</a></td>';
				filassalascerradas += '</tr>';
				filassalascerradas += '</p>';
			});
			$("#tbsalascerradas tbody").html(filassalascerradas);

			linkseleccionado = Number(nropagina);
			//total registros
			totalregistros = response.totalregistros;
			//cantidad de registros por pagina
			cantidadregistros = cantidad;

			numerolinks = Math.ceil(totalregistros/cantidadregistros);
			paginador = "<ul class='pagination'>";
			if(linkseleccionado>1)
			{
				paginador+="<li><a href='1'>&laquo;</a></li>";
				paginador+="<li><a href='"+(linkseleccionado-1)+"' '>&lsaquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&laquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&lsaquo;</a></li>";
			}
			//muestro de los enlaces 
			//cantidad de link hacia atras y adelante
 			cant = 3;
 			//inicio de donde se va a mostrar los links
			pagInicio = (linkseleccionado > cant) ? (linkseleccionado - cant) : 1;
			//condicion en la cual establecemos el fin de los links
			if (numerolinks > cant)
			{
				//conocer los links que hay entre el seleccionado y el final
				pagRestantes = numerolinks - linkseleccionado;
				//defino el fin de los links
				pagFin = (pagRestantes > cant) ? (linkseleccionado + cant) :numerolinks;
			}
			else 
			{
				pagFin = numerolinks;
			}

			for (var i = pagInicio; i <= pagFin; i++) {
				if (i == linkseleccionado)
					paginador +="<li class='active'><a href='javascript:void(0)'>"+i+"</a></li>";
				else
					paginador +="<li><a href='"+i+"'>"+i+"</a></li>";
			}
			//condicion para mostrar el boton sigueinte y ultimo
			if(linkseleccionado<numerolinks)
			{
				paginador+="<li><a href='"+(linkseleccionado+1)+"' >&rsaquo;</a></li>";
				paginador+="<li><a href='"+numerolinks+"'>&raquo;</a></li>";

			}
			else
			{
				paginador+="<li class='disabled'><a href='#'>&rsaquo;</a></li>";
				paginador+="<li class='disabled'><a href='#'>&raquo;</a></li>";
			}
			
			paginador +="</ul>";
			$(".paginaciontablascerradas").html(paginador);

			limpiarIntervalos(intervalosSalasCerradas);
			intervalosSalasCerradas.push(setInterval('obtenerSalasCerradas('+nropagina+')', 4000));
		}
	});
}